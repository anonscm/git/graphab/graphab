/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.math.MathException;
import org.geotools.feature.SchemaException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.AbstractGraph.Type;
import org.thema.graphab.graph.DefaultGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.links.CostLinkset;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Linkset.Distance;
import org.thema.graphab.links.Linkset.Topology;

/**
 *
 * @author gvuidel
 */
public class CLIToolsTest {
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    private File prjDir = new File("target/test-classes/org/thema/graphab/cross_project");
    private String prjFile = "target/test-classes/org/thema/graphab/cross_project/cross.xml";
    private String landFile = "target/test-classes/org/thema/graphab/cross_project/land.tif";

    /**
     * Test of execute method, of class CLITools.
     */
    @Test
    public void testExecute1() throws Exception {
        System.out.println("--help,--metrics");

        new CLITools().execute(new String[]{"--help"});
        new CLITools().execute(new String[]{"--advanced"});
        new CLITools().execute(new String[]{"--metrics"});
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"-nooption"});
    }
    
    
    /**
     * Test of execute method, of class CLITools.
     */
    @Test
    public void testExecuteProject() throws Exception {
        System.out.println("--create,--project");

        new CLITools().execute(new String[]{"--create", "test", landFile, "dir=target/test-classes/org/thema/graphab"});
        
        new CLITools().execute(new String[]{"--project", prjFile});
        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--show"});
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--nocmd"});

    }
    
    @Test
    public void testExecuteHabitat() throws Exception {
        System.out.println("--habitat");

        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--habitat", "name=testhab", "codes=0,3", "con8", "novoronoi"});
        checkAndRemoveHabitat("testhab", Arrays.asList(0,3), true, 0, 0, false, 3);
        
        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--habitat", "name=testhab", "codes=0,3", "minarea=0.0004", "novoronoi"});
        checkAndRemoveHabitat("testhab", Arrays.asList(0,3), false, 4, 0, false, 1);
        
        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--habitat", "name=testhab", "codes=0,3", "maxsize=1"});
        checkAndRemoveHabitat("testhab", Arrays.asList(0,3), false, 0, 1, true, 9);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--habitat"});
    }
    
    @Test
    public void testExecuteVHabitat() throws Exception {
        System.out.println("--vhabitat");

        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--vhabitat", "name=testvhab", "from=h034", "mincapa=2", "novoronoi"});
        checkAndRemoveHabitat("testvhab", null, false, 2, 0, false, 1);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--vhabitat"});
    }
    
    @Test
    public void testExecuteMetaPatch() throws Exception {
        System.out.println("--metapatch");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "cout1_2_3", "--metapatch"});
        checkAndRemoveHabitat("h0-meta-cout1_2_3", null, false, 0, 0, true, 2);
        
        Project project = new CLITools().execute(new String[]{"-nosave", "--project", prjFile, "--usegraph", "cout1_2_3", "--metapatch", "novoronoi"});
        checkHabitat(project, "h0-meta-cout1_2_3", null, false, 0, 0, false, 2);
        
        project = new CLITools().execute(new String[]{"-nosave", "--project", prjFile, "--usegraph", "cout1_2_3", "--metapatch", "mincapa=2"});
        checkHabitat(project, "h0-meta-cout1_2_3", null, false, 2, 0, false, 1);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--metapatch", "unknown"});
    }
    
    @Test
    public void testExecuteMergeHabitat() throws Exception {
        System.out.println("--mergehabitat");

        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--mergehabitat", "novoronoi"});
        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--mergehabitat", "ALL", "novoronoi"});
        new CLITools().execute(new String[]{"-proc", "2", "--project", prjFile, "--mergehabitat", "h4,h0"});
        assertTrue(new File(prjDir, "h0-h4").exists());
        assertTrue(new File(new File(prjDir, "h0-h4"), Habitat.VORONOI_SHAPE).exists());
        assertTrue(new File(new File(prjDir, "h0-h4"), Habitat.LINKS_SHAPE).exists());
    }
    
    private void checkAndRemoveHabitat(String name, List<Integer> codes, boolean con8, double minArea, double maxSize, boolean voronoi, int nbPatches) throws IOException, SchemaException, MathException {
        assertTrue(new File(new File(prjFile).getParent(), name).exists());

        checkHabitat(Project.loadProject(new File(prjFile), false), name, codes, con8, minArea, maxSize, voronoi, nbPatches);
        
        new CLITools().execute(new String[]{"--project", prjFile, "--removehabitat", name});
        assertFalse(new File(prjDir, name).exists());
    }
    
    private void checkHabitat(Project project, String name, List<Integer> codes, boolean con8, double minArea, double maxSize, boolean voronoi, int nbPatches) {
        AbstractMonoHabitat hab = (AbstractMonoHabitat) project.getHabitat(name);
        
        if(hab instanceof MonoHabitat) {
            assertEquals(new HashSet<>(codes), ((MonoHabitat)hab).getPatchCodes());
            assertEquals(con8, ((MonoHabitat)hab).isCon8());
            assertEquals(minArea, ((MonoHabitat)hab).getMinArea(), 1e-10);
            assertEquals(maxSize, ((MonoHabitat)hab).getMaxSize(), 1e-10);
        }
        assertEquals(voronoi, hab.hasVoronoi());
        assertEquals(nbPatches, hab.getPatches().size());
        for(DefaultFeature patch : hab.getPatches()) {
            assertTrue(Habitat.getPatchArea(patch) >= minArea);
        }
    }
    
    /**
     * Test of execute method, of class CLITools.
     */
    @Test
    public void testExecuteLinkset() throws Exception {
        System.out.println("--linkset");

        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=euclid"});
        checkAndRemoveLinkset("h0_euclid_plan0.0", Topology.PLANAR, Distance.EUCLID, false, 0.0, false, false, 8);
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=euclid", "maxcost=2"});
        checkAndRemoveLinkset("h0_euclid_plan2.0", Topology.PLANAR, Distance.EUCLID, false, 2.0, false, false, 4);
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=euclid", "topo=complete", "nopathsaved"});
        checkAndRemoveLinkset("h0_euclid_comp0.0", Topology.COMPLETE, Distance.EUCLID, false, 0.0, false, true, 10);
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=euclid", "name=comp2", "topo=complete", "maxcost=2"}); 
        checkAndRemoveLinkset("comp2", Topology.COMPLETE, Distance.EUCLID, false, 2.0, false, false, 4);
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=cost", "topo=planarcost", "name=plan", "0,1,2,3,4=1", "5=10"});
        checkAndRemoveLinkset("plan", Topology.PLANAR_COST, Distance.COST, false, 0.0, false, false, 8);
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=cost", "topo=planar", "name=plan4", "maxcost=4", "0,2,3,4,5=1", "1=2"});
        checkAndRemoveLinkset("plan4", Topology.PLANAR, Distance.COST, false, 4.0, false, false, 3);
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=cost", "topo=complete", "remcrosspath", "0,1=1", "2,3,4,5=1"});
        checkAndRemoveLinkset("h0_cost_0_1", Topology.COMPLETE, Distance.COST, false, 0.0, true, false, 8);
        Project project = new CLITools().execute(new String[]{"-nosave", "--project", prjFile, "--usehabitat", "h0", "--linkset", "distance=cost", "0,1=1:1:2", "2,3,4,5=1"});
        checkLinkset(project, "h0_cost_0_1-1.0", Topology.PLANAR, Distance.COST, false, 0.0, false, false, 8);
        checkLinkset(project, "h0_cost_0_1-2.0", Topology.PLANAR, Distance.COST, false, 0.0, false, false, 8);
        
        new CLITools().execute(new String[]{"--project", prjFile, "--mergehabitat", "h0,h4", "--linkset", "distance=cost", "name=inter", "inter", "0,1,2,3,4,5=1"});
        checkAndRemoveLinkset("inter", Topology.PLANAR, Distance.COST, true, 0.0, false, false, 8);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--linkset"});

    }
    
    private void checkAndRemoveLinkset(String name, Topology topo, Distance distance, boolean inter, double maxcost, boolean rem, boolean nopathsaved, int nbLinks) throws IOException, SchemaException, MathException {
        Project prj = Project.loadProject(new File(prjFile), false);
        checkLinkset(prj, name, topo, distance, inter, maxcost, rem, nopathsaved, nbLinks);
        new CLITools().execute(new String[]{"--project", prjFile, "--removelinkset", name});
    }
    
    private void checkLinkset(Project prj, String name, Topology topo, Distance distance, boolean inter, double maxcost, boolean rem, boolean nopathsaved, int nbLinks) throws IOException, SchemaException, MathException {
        assertTrue("Linkset exists", prj.isLinksetExists(name));
        
        Linkset linkset = prj.getLinkset(name);
        assertEquals(topo, linkset.getTopology());
        assertEquals(distance, linkset.getTypeDist());
        assertEquals(inter, linkset.isInter());
        assertEquals(maxcost, linkset.getDistMax(), 1e-10);
        assertEquals(!nopathsaved, linkset.isRealPaths());
        if(distance == Distance.COST) {
            assertEquals(rem, ((CostLinkset)linkset).isRemoveCrossPatch());
        }
        assertEquals(nbLinks, linkset.getPaths().size());
    }
    
    @Test
    public void testExecuteCorridor() throws Exception {
        System.out.println("--corridor");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--corridor", "maxcost=3"});
        File f = new File(prjDir, "all-corridor-3.0.gpkg");
        assertTrue(f.exists());
        assertEquals(4, IOFeature.loadFeatures(f).size());
        assertTrue(f.delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--corridor", "maxcost=3", "format=raster"});
        assertTrue(new File(prjDir, "all-corridor-3.0.tif").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--corridor", "maxcost=3", "format=raster", "beta=1", "d=3", "p=0.5"});
        assertTrue(new File(prjDir, "all-corridor-3.0-beta1.0-d3.0-p0.5.tif").delete());
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--corridor"});
    }
    
    /**
     * Test of execute method, of class CLITools.
     */
    @Test
    public void testExecuteGraph() throws Exception {
        System.out.println("--graph");

        new CLITools().execute(new String[]{"--project", prjFile, "--uselinkset", "jeulien1", "--graph"});
        checkAndRemoveGraph("comp_jeulien1", Type.COMPLETE, 0, 8, true);
        new CLITools().execute(new String[]{"--project", prjFile, "--uselinkset", "jeulien1", "--graph", "nointra", "name=comptest"});
        checkAndRemoveGraph("comptest", Type.COMPLETE, 0, 8, false);
        new CLITools().execute(new String[]{"--project", prjFile, "--uselinkset", "jeulien1", "--graph", "threshold=3:2:5"});
        checkAndRemoveGraph("thresh_3.0_jeulien1", Type.PRUNED, 3, 4, true);
        checkAndRemoveGraph("thresh_5.0_jeulien1", Type.PRUNED, 5, 8, true);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--graph", "name=test", "threshold=1:1:5"});
    }
    
    @Test
    public void testExecuteMergeGraph() throws Exception {
        System.out.println("--mergegraph");

        new CLITools().execute(new String[]{"--project", prjFile, "--mergegraph", "name=multi", "all,comp_cout1_h0_h4"});
        checkAndRemoveGraph("multi", Type.MULTI, 0, 8+20, true);
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all,comp_cout1_h0_h4", "--mergegraph"});
        checkAndRemoveGraph("all-comp_cout1_h0_h4", Type.MULTI, 0, 8+20, true);
        
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--mergegraph"});
    }
    
    @Test
    public void testExecuteCluster() throws Exception {
        System.out.println("--cluster");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--cluster", "d=1", "p=1"});
        checkAndRemoveGraph("mod1_all", Type.COMPLETE, 0.0, 8, true);
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--cluster", "d=1", "p=1", "beta=1", "nb=2"});
        checkAndRemoveGraph("mod2_all", Type.COMPLETE, 0.0, 4, true);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--cluster"});
    }
    
    private void checkAndRemoveGraph(String name, Type type, double threshold, int nbLinks, boolean intra) throws IOException, SchemaException, MathException {
        Project prj = Project.loadProject(new File(prjFile), false);
        assertTrue("Graph exists", prj.isGraphExists(name));
        
        AbstractGraph graph = prj.getGraph(name);
        assertTrue(graph.getGraphFile().exists());
        assertEquals(type, graph.getType());
        if(threshold > 0) {
            assertEquals(threshold, ((DefaultGraph)graph).getThreshold(), 0.0);
        }
        assertEquals(nbLinks, graph.getEdges().size());
        assertEquals(intra, graph.isIntraPatchDist());
        new CLITools().execute(new String[]{"--project", prjFile, "--removegraph", name});
        assertFalse(graph.getGraphFile().exists());
    }
    
    @Test
    public void testExecuteDataset() throws Exception {
        System.out.println("--dataset");
        new CLITools().execute(new String[]{"--project", prjFile, "--dataset", "name=testpoint", "file="+new File(prjDir.getParent(),"points.gpkg")});
        assertTrue(new File(prjDir, "testpoint").exists());
        new CLITools().execute(new String[]{"--project", prjFile, "--removedataset", "testpoint"});
        assertFalse(new File(prjDir, "testpoint").exists());
    }
    
    @Test
    public void testExecuteDistance() throws Exception {
        System.out.println("--distance_matrix");
        new CLITools().execute(new String[]{"--project", prjFile, "--dataset", "name=testpoint", "file="+new File(prjDir.getParent(),"points.gpkg"), 
            "--uselinkset", "jeulien1", "--distance_matrix", "type=space", "distance=leastcost"});
        assertTrue(new File(prjDir, "distance_testpoint_space_jeulien1-leastcost.txt").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usedataset", "testpoint",
            "--uselinkset", "jeulien1", "--distance_matrix", "type=space", "distance=circuit"});
        assertTrue(new File(prjDir, "distance_testpoint_space_jeulien1-circuit.txt").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usedataset", "testpoint",
            "--distance_matrix", "type=graph", "distance=leastcost"});
        assertTrue(true); // nothing done
        
        new CLITools().execute(new String[]{"--project", prjFile, 
            "--usegraph", "all", "--distance_matrix", "type=graph", "distance=flow", "d=3", "p=0.5"});
        assertTrue(new File(prjDir, "distance_h0_graph_all-flow-d3-p0.5.txt").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--removedataset", "testpoint"});
    }
    
    @Test
    public void testExecuteGmetric() throws Exception {
        System.out.println("--gmetric");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--gmetric", "IIC"});
        assertTrue(new File(prjDir, "IIC.txt").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--gmetric", "IIC", "resfile=gmetric.txt"});
        assertTrue(new File(prjDir, "gmetric.txt").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--gmetric", "EC", "d=10", "p=1"});
        File f = new File(prjDir, "EC.txt");
        assertTrue(f.exists());
        try(BufferedReader r = new BufferedReader(new FileReader(f))) {
            assertArrayEquals(new String[]{"Graph", "d", "p", "EC"}, r.readLine().split("\\s+"));
            assertArrayEquals(new String[]{"all", "10.0", "1.0", "5.0"}, r.readLine().split("\\s+"));
        }
        assertTrue(f.delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--gmetric", "EC", "d=1:1:2", "p=1"});
        f = new File(prjDir, "EC.txt");
        assertTrue(f.exists());
        try(BufferedReader r = new BufferedReader(new FileReader(f))) {
            assertArrayEquals(new String[]{"Graph", "d", "p", "EC"}, r.readLine().split("\\s+"));
            assertArrayEquals(new String[]{"all", "1.0", "1.0", "5.0"}, r.readLine().split("\\s+"));
            assertArrayEquals(new String[]{"all", "2.0", "1.0", "5.0"}, r.readLine().split("\\s+"));
        }
        assertTrue(f.delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "cross", "--gmetric", "EC", "minp=0.5", "d=3,6", "p=0.5"});
        f = new File(prjDir, "EC.txt");
        assertTrue(f.exists());
        try(BufferedReader r = new BufferedReader(new FileReader(f))) {
            assertArrayEquals(new String[]{"Graph", "minp", "d", "p", "EC"}, r.readLine().split("\\s+"));
            assertArrayEquals(new String[]{"cross", "0.5", "3.0", "0.5", ""+Math.sqrt(5+4*0.5*2)}, r.readLine().split("\\s+"));
            assertArrayEquals(new String[]{"cross", "0.5", "6.0", "0.5", ""+Math.sqrt(1+4*Math.sqrt(2)/2 + 4*(1+Math.sqrt(2)/2+3*0.5))}, r.readLine().split("\\s+"));
        }
        assertTrue(f.delete());
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--gmetric", "PC"});
    }
    
    @Test
    public void testExecuteCmetric() throws Exception {
        System.out.println("--cmetric");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--cmetric", "H"});
        List<DefaultFeature> comps = IOFeature.loadFeatures(new File(new File(prjDir, "h0"), "all-voronoi.gpkg"), "Id");
        assertEquals(1, comps.size());
        assertEquals(9.0, ((Number)comps.get(0).getAttribute("h_all")).doubleValue(), 0);
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "cross", "--cmetric", "EC", "d=1:1:2", "p=1"});
        comps = IOFeature.loadFeatures(new File(new File(prjDir, "h0"), "cross-voronoi.gpkg"), "Id");
        assertEquals(1, comps.size());
        assertEquals(5.0, ((Number)comps.get(0).getAttribute("ec_d1_p1_cross")).doubleValue(), 0);
        assertEquals(5.0, ((Number)comps.get(0).getAttribute("ec_d2_p1_cross")).doubleValue(), 0);
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--cmetric", "NO"});
    }
    
    @Test
    public void testExecuteLmetric() throws Exception {
        System.out.println("--lmetric");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--lmetric", "Dg"});
        List<DefaultFeature> patches = IOFeature.loadFeatures(new File(new File(prjDir, "h0"), AbstractMonoHabitat.PATCH_SHAPE), "Id");
        for(Feature patch : patches) {
            assertEquals(((Integer)patch.getId()) == 3 ? 4.0 : 3.0, ((Number)patch.getAttribute("dg_all")).doubleValue(), 0);
        }
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--removemetric", "Dg_all", "--lmetric", "Dg", "filter=Id = 3"});
        patches = IOFeature.loadFeatures(new File(new File(prjDir, "h0"), AbstractMonoHabitat.PATCH_SHAPE), "Id");
        for(Feature patch : patches) {
            assertEquals(((Integer)patch.getId()) == 3 ? 4.0 : null, patch.getAttribute("dg_all"));
        }
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--lmetric", "F", "d=1:1:2", "p=1", "beta=1:1:2"});
        patches = IOFeature.loadFeatures(new File(new File(prjDir, "h0"), AbstractMonoHabitat.PATCH_SHAPE), "Id");
        List<String> attributeNames = patches.get(0).getAttributeNames();
        assertTrue(attributeNames.containsAll(Arrays.asList("f_d1_p1_beta1_all", "f_d2_p1_beta1_all", 
                "f_d1_p1_beta2_all", "f_d2_p1_beta2_all")));
        for(Feature patch : patches) {
            assertEquals(4.0, ((Number)patch.getAttribute("f_d1_p1_beta1_all")).doubleValue(), 0);
        }
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "cross", "--lmetric", "F", "minp=0.5", "d=3,6", "p=0.5", "beta=1"});
        patches = IOFeature.loadFeatures(new File(new File(prjDir, "h0"), AbstractMonoHabitat.PATCH_SHAPE), "Id");
        attributeNames = patches.get(0).getAttributeNames();
        assertTrue(attributeNames.containsAll(Arrays.asList("f_d3_p0.5_beta1_minp0.5_cross", "f_d6_p0.5_beta1_minp0.5_cross")));
        for(Feature patch : patches) {
            if((Integer)patch.getId() == 3) {
                assertEquals(4*0.5, ((Number)patch.getAttribute("f_d3_p0.5_beta1_minp0.5_cross")).doubleValue(), 1e-10);
                assertEquals(4*Math.sqrt(2)/2, ((Number)patch.getAttribute("f_d6_p0.5_beta1_minp0.5_cross")).doubleValue(), 1e-10);
            } else {
                assertEquals(1*0.5, ((Number)patch.getAttribute("f_d3_p0.5_beta1_minp0.5_cross")).doubleValue(), 1e-10);
                assertEquals(3*0.5+1*Math.sqrt(2)/2, ((Number)patch.getAttribute("f_d6_p0.5_beta1_minp0.5_cross")).doubleValue(), 1e-10);
            }
        }
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--lmetric"});
    }
    
    @Test
    public void testExecuteInterp() throws Exception {
        System.out.println("--interp");
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--lmetric", "Dg"});
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--interp", "var=dg", "d=3", "p=0.5"});
        assertTrue(new File(prjDir, "dg_all.tif").delete());
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--interp", "var=dg", "d=3", "p=0.5", 
            "name=interp"});
        assertTrue(new File(prjDir, "interp-dg_all.tif").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--interp", "var=dg", "d=3", "p=0.5", 
            "resol=0.5", "multi=3", "ag=sum"});
        assertTrue(new File(prjDir, "dg_all.tif").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--lmetric", "Dg", "--interp", "d=3", "p=0.5"});
        assertTrue(new File(prjDir, "dg_all.tif").delete());
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--lmetric", "IF", "d=3", "p=0.05", "beta=1", "--interp", "multi"});
        File fInterp = new File(prjDir, "if_d3_p0.05_beta1_all.tif");
        File fInterp2 = new File(prjDir, "if_d3_p0.05_beta1_all_2.tif");
        assertTrue(fInterp.exists());
        fInterp.renameTo(fInterp2);
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--lmetric", "IF", "d=3", "p=0.05", "beta=1", "--interp", "multi=3"});
        assertTrue(fInterp.exists());
        
        assertArrayEquals("Interpolation differs", Files.readAllBytes(Paths.get(fInterp.getAbsolutePath())), 
            Files.readAllBytes(Paths.get(fInterp2.getAbsolutePath())));
        fInterp.delete();
        fInterp2.delete();
    }
    
    
    @Test
    public void testExecuteCapa() throws Exception {
        System.out.println("--capa");

        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--capa", "area", "0=2.0"});
        Project prj = Project.loadProject(new File(prjFile), false);
        for(DefaultFeature patch : prj.getHabitats().iterator().next().getPatches()) {
            assertEquals(Habitat.getPatchArea(patch)*2, Habitat.getPatchCapacity(patch), 1e-10);
        }
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usehabitat", "h0", "--capa", "area", "0=1.0"});
        prj = Project.loadProject(new File(prjFile), false);
        for(DefaultFeature patch : prj.getHabitats().iterator().next().getPatches()) {
            assertEquals(Habitat.getPatchArea(patch), Habitat.getPatchCapacity(patch), 1e-10);
        }
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--capa", "vds"});
    }
    
    @Test
    public void testExecuteLandmod() throws Exception {
        System.out.println("--landmod");

        new CLITools().execute(new String[]{"--project", prjFile, "--landmod", "zone=target/test-classes/org/thema/graphab/points.gpkg", "id=id", "code=id", 
                "--habitat", "name=testhab", "codes=0", "novoronoi"});
        for(int i = 1; i <= 4; i++) {
            Project prj = Project.loadProject(new File(prjDir, i+"/cross-"+i+".xml"), false);
            assertEquals(5, prj.getHabitat("testhab").getPatches().size());
        }
        
        thrown.expect(IllegalArgumentException.class);
        new CLITools().execute(new String[]{"--project", prjFile, "--landmod", "vds"});
    }
    
    @Test
    public void testExecuteLandmodgm() throws Exception {
        System.out.println("--landmodgm");

        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "all", "--gmetric", "EC", "d=6", "p=0.5", 
            "--landmodgm", "zone=target/test-classes/org/thema/graphab/landmod.gpkg", "id=Id", "code=code"});
        for(int i = 1; i <= 4; i++) {
            Project prj = Project.loadProject(new File(prjDir, i+"/cross-"+i+".xml"), false);
            assertEquals(6, prj.getHabitat("h0").getPatches().size());
            assertNotNull(prj.getLinkset("jeulien1"));
            assertNotNull(prj.getGraph("all"));
            assertNotNull(prj.getMetric("ec_d6_p0.5_all"));
        }
        
        new CLITools().execute(new String[]{"--project", prjFile, "--usegraph", "comp_cout1_h0_h4", "--gmetric", "EC", "d=6", "p=0.5", 
            "--landmodgm", "zone=target/test-classes/org/thema/graphab/landmod.gpkg", "id=Id", "code=code"});
        for(int i = 1; i <= 4; i++) {
            Project prj = Project.loadProject(new File(prjDir, i+"/cross-"+i+".xml"), false);
            assertEquals(6, prj.getHabitat("h0").getPatches().size());
            assertEquals(3, prj.getHabitat("h4").getPatches().size());
            assertNotNull(prj.getLinkset("comp_cout1_h0_h4"));
            assertNotNull(prj.getGraph("comp_cout1_h0_h4"));
            assertNotNull(prj.getMetric("ec_d6_p0.5_comp_cout1_h0_h4"));
        }
    }
}
