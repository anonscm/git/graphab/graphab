/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import java.io.IOException;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.locationtech.jts.geom.Geometry;
import org.thema.common.Config;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.habitat.MultiHabitat;
import static org.thema.graphab.links.LinksetTest.checkLinkset;

/**
 *
 * @author gvuidel
 */
public class EuclideLinksetTest {
    
    private static EuclideLinkset linkset;
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        project = ProjectTest.createCrossProjectH0H4();
        linkset = new EuclideLinkset(project.getHabitat("h0"), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        project.addLinkset(linkset, false);
    }


    /**
     * Test of calcLinkset method, of class EuclideLinkset.
     */
    @Test
    public void testCalcLinkset() throws Exception {
        System.out.println("calcLinkset");
        EuclideLinkset link = new EuclideLinkset(project.getHabitats().iterator().next(), "euclide", Linkset.Topology.PLANAR, false, null, false, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 8, 2*4+Math.sqrt(8)*4, 2*4+Math.sqrt(8)*4);
        
        link = new EuclideLinkset(project.getHabitats().iterator().next(), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 8, 2*4+Math.sqrt(8)*4, 2*4+Math.sqrt(8)*4);
        
        link = new EuclideLinkset(project.getHabitats().iterator().next(), "euclide", Linkset.Topology.PLANAR, false, null, true, 2);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 4, 2*4, 2*4);
        
        link = new EuclideLinkset(project.getHabitats().iterator().next(), "euclide", Linkset.Topology.COMPLETE, false, null, true, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 10, 2*4 + Math.sqrt(8)*4 + 5*2, 2*4 + Math.sqrt(8)*4 + 5*2);
        
        link = new EuclideLinkset(project.getHabitats().iterator().next(), "euclide", Linkset.Topology.COMPLETE, false, null, true, 2);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 4, 2*4, 2*4);
        
        MultiHabitat mh = new MultiHabitat(project.getHabitats(), true);
        link = new EuclideLinkset(mh, "mh", Linkset.Topology.PLANAR, false, null, true, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 12, 2*12, 2*12);
        
        link = new EuclideLinkset(mh, "mh", Linkset.Topology.COMPLETE, false, null, true, 5);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 26, 2*12 + Math.sqrt(8)*8 + 5*6, 2*12 + Math.sqrt(8)*8 + 5*6);
        
        link = new EuclideLinkset(mh, "inter", Linkset.Topology.PLANAR, true, null, true, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 8, 2*8, 2*8);
        
        link = new EuclideLinkset(mh, "inter", Linkset.Topology.COMPLETE, true, null, true, 3);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 12, 2*8 + Math.sqrt(8)*4, 2*8 + Math.sqrt(8)*4);
    }
    
    /**
     * Test of getInfo method, of class EuclideLinkset.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        assertNotNull(linkset.getInfo());
    }

    /**
     * Test of calcCorridor method, of class EuclideLinkset.
     */
    @Test
    public void testCalcCorridor() throws IOException {
        System.out.println("calcCorridor");
        for(Path path : linkset.getPaths()) {
            Geometry corridor = linkset.calcCorridor(path, 3);
            if(path.getCost() > 2) {
                assertEquals(0, corridor.getArea(), 1e-10);
            } else {
                assertEquals(2, corridor.getArea(), 1e-10);
            }
        }
        
        for(Path path : linkset.getPaths()) {
            Geometry corridor = linkset.calcCorridor(path, 4);
            if(path.getCost() > 2) {
                assertEquals(0, corridor.getArea(), 1e-10);
            } else {
                assertEquals(6, corridor.getArea(), 1e-10);
            }
        }
        
        for(Path path : linkset.getPaths()) {
            Geometry corridor = linkset.calcCorridor(path, 5);
            if(path.getCost() > 2) {
                assertEquals(8, corridor.getArea(), 1e-10);
            } else {
                assertEquals(12, corridor.getArea(), 1e-10);
            }
        }
    }

    /**
     * Test of calcRasterCorridor method, of class EuclideLinkset.
     */
    @Test
    public void testCalcRasterCorridor() throws Exception {
        System.out.println("calcRasterCorridor");
        // TODO
    }

    /**
     * Test of getCostVersion method, of class EuclideLinkset.
     */
    @Test
    public void testGetCostVersion() {
        System.out.println("getCostVersion");
        CostLinkset costLinkset = linkset.getCostVersion(linkset.getHabitat());
        assertNotNull(costLinkset);
        assertEquals(Linkset.Distance.COST, costLinkset.getTypeDist());
        assertEquals(linkset.getProject().getResolution(), costLinkset.getCosts()[0], 1e-10);
        assertEquals(linkset.getTopology(), costLinkset.getTopology());
        assertEquals(linkset.isInter(), costLinkset.isInter());
    }

    /**
     * Test of getCircuitVersion method, of class EuclideLinkset.
     */
    @Test
    public void testGetCircuitVersion() {
        System.out.println("getCircuitVersion");
        CircuitLinkset circuitLinkset = linkset.getCircuitVersion(linkset.getHabitat());
        assertNotNull(circuitLinkset);
        assertEquals(Linkset.Distance.CIRCUIT, circuitLinkset.getTypeDist());
        assertEquals(linkset.getProject().getResolution(), circuitLinkset.getCosts()[0], 1e-10);
        assertEquals(linkset.getTopology(), circuitLinkset.getTopology());
        assertEquals(linkset.isInter(), circuitLinkset.isInter());
    }

    /**
     * Test of getPathFinder method, of class EuclideLinkset.
     */
    @Test
    public void testGetPathFinder() throws Exception {
        System.out.println("getPathFinder");
        assertNotNull(linkset.getPathFinder());
    }
    
}
