/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import java.io.IOException;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.common.Config;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;

/**
 *
 * @author gvuidel
 */
public class CircuitLinksetTest {
    
    private static CircuitLinkset linkset;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        Project project = ProjectTest.createCrossProjectH0();
        linkset = new CircuitLinkset(project.getHabitat("h0"), "circ", Linkset.Topology.COMPLETE, false, null, new double[]{1.0,1,1,1,1,1}, null, false, 0);
        project.addLinkset(linkset, false);
        
    }
    

    /**
     * Test of calcLinkset method, of class CircuitLinkset.
     */
    @Test
    public void testCalcLinkset() throws Exception {
        System.out.println("calcLinkset");
        
        LinksetTest.checkLinkset(linkset, 10, 5.230468337451936, Double.NaN);
        
        CircuitLinkset link = new CircuitLinkset(linkset.getHabitat(), "circ", Linkset.Topology.PLANAR, false, null, linkset.getCosts(), null, true, 0);
        link.calcLinkset(Config.getProgressBar());
        LinksetTest.checkLinkset(link, 8, 3.9791734225414235, Double.NaN);

    }

    /**
     * Test of getCostVersion method.
     */
    @Test
    public void testGetCostVersion() {
        System.out.println("getCostVersion");
        CostLinkset costLinkset = linkset.getCostVersion(linkset.getHabitat());
        assertNotNull(costLinkset);
        assertEquals(Linkset.Distance.COST, costLinkset.getTypeDist());
        assertEquals(linkset.getCosts()[0], costLinkset.getCosts()[0], 1e-10);
        assertEquals(linkset.getTopology(), costLinkset.getTopology());
        assertEquals(linkset.isInter(), costLinkset.isInter());
    }

    /**
     * Test of getCircuitVersion method.
     */
    @Test
    public void testGetCircuitVersion() {
        System.out.println("getCircuitVersion");
        CircuitLinkset circuitLinkset = linkset.getCircuitVersion(linkset.getHabitat());
        assertNotNull(circuitLinkset);
        assertEquals(Linkset.Distance.CIRCUIT, circuitLinkset.getTypeDist());
        assertEquals(linkset.getCosts()[0], circuitLinkset.getCosts()[0], 1e-10);
        assertEquals(linkset.getTopology(), circuitLinkset.getTopology());
        assertEquals(linkset.isInter(), circuitLinkset.isInter());
    }

    /**
     * Test of getCircuit method, of class CircuitLinkset.
     */
    @Test
    public void testGetCircuit() throws Exception {
        System.out.println("getCircuit");
        assertNotNull(linkset.getCircuit());
    }
    
}
