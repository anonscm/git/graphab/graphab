/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import org.locationtech.jts.geom.Geometry;
import org.thema.common.Config;
import org.thema.common.collection.HashMap2D;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MultiHabitat;
import static org.thema.graphab.links.LinksetTest.checkLinkset;
/**
 *
 * @author gvuidel
 */
public class CostLinksetTest {
    
    private static CostLinkset linkset, linkset1_2, linksetMulti, linksetMultiCost, linksetPlanarCost, linksetExt;
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        project = ProjectTest.createCrossProjectH0H4();
        double [] costs = new double[project.getCodes().last()+1];
        Arrays.fill(costs, 1.0);
        linkset = new CostLinkset(project.getHabitat("h0"), "cost", Linkset.Topology.PLANAR, false, null, costs, true, false, 0, 0);
        project.addLinkset(linkset, false);
        linkset1_2 = new CostLinkset(project.getHabitat("h0"), "cost1-2", Linkset.Topology.PLANAR, false, null, new double[]{1.0, 2, 1, 1, 1, 1}, true, false, 0, 0);
        project.addLinkset(linkset1_2, false);
        linksetPlanarCost = new CostLinkset(project.getHabitat("h0"), "planarcost", Linkset.Topology.PLANAR_COST, false, null, costs, true, false, 0, 0);
        project.addLinkset(linksetPlanarCost, false);
        linksetMulti = new CostLinkset(new MultiHabitat(project.getHabitats(), true), "cost_multi", Linkset.Topology.PLANAR, false, null, costs, true, false, 0, 0);
        project.addLinkset(linksetMulti, false);
        linksetMultiCost = new CostLinkset(new MultiHabitat(project.getHabitats(), true), "cost_multi_cost", Linkset.Topology.PLANAR_COST, false, null, costs, true, false, 0, 0);
        project.addLinkset(linksetMultiCost, false);
        linksetExt = new CostLinkset(project.getHabitat("h0"), "costext", Linkset.Topology.PLANAR, false, null, true, false, 0, new File("target/test-classes/org/thema/graphab/land_cross.tif"), 0);
        project.addLinkset(linksetExt, false);
    }


    /**
     * Test of calcLinkset method, of class EuclideLinkset.
     */
    @Test
    public void testCalcLinkset() throws Exception {
        System.out.println("calcLinkset");
        
        checkLinkset(linkset, 8, 3*4+3*Math.sqrt(2)*4, 3*4+3*Math.sqrt(2)*4);
        checkLinkset(linkset1_2, 8, 3*3 + 3*Math.sqrt(2)*2 + 2*2+1 + 2*(2*Math.sqrt(2)+4), 3*4+3*Math.sqrt(2)*2 + 2*(2*Math.sqrt(2)+4));
        checkLinkset(linksetMulti, 12, 3*12, 3*12);
        checkLinkset(linksetMultiCost, 12, 3*12, 3*12);
        checkLinkset(linksetPlanarCost, 8, 3*4+3*Math.sqrt(2)*4, 3*4+3*Math.sqrt(2)*4);
       
        CostLinkset link = new CostLinkset(project.getHabitat("h0"), "test", Linkset.Topology.COMPLETE, false, null, linkset.getCosts(), true, false, 0, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 10, 3*4 + 3*Math.sqrt(2)*4 + 6*2, 3*4 + 3*Math.sqrt(2)*4 + 6*2);
        
        link = new CostLinkset(project.getHabitat("h0"), "test", Linkset.Topology.COMPLETE, false, null, linkset.getCosts(), true, false, 3, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 4, 3*4, 3*4);
        
        link = new CostLinkset(project.getHabitat("h0"), "test", Linkset.Topology.COMPLETE, false, null, linkset.getCosts(), true, true, 0, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 8, 3*4 + 3*Math.sqrt(2)*4, 3*4 + 3*Math.sqrt(2)*4);
        
        MultiHabitat mh = new MultiHabitat(project.getHabitats(), true);
        link = new CostLinkset(mh, "mh", Linkset.Topology.PLANAR, false, null, linkset.getCosts(), true, true, 0, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 12, 3*12, 3*12);
        
        link = new CostLinkset(mh, "mh", Linkset.Topology.COMPLETE, false, null, linkset.getCosts(), true, false, 6, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 26, 3*12 + 3*Math.sqrt(2)*8 + 6*6, 3*12 + 3*Math.sqrt(2)*8 + 6*6);
        
        link = new CostLinkset(mh, "inter", Linkset.Topology.PLANAR, true, null, linkset.getCosts(), true, false, 0, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 8, 3*8, 3*8);
        
        link = new CostLinkset(mh, "inter", Linkset.Topology.COMPLETE, true, null, linkset.getCosts(), true, false, 5, 0);
        link.calcLinkset(Config.getProgressBar());
        checkLinkset(link, 12, 3*8 + 3*Math.sqrt(2)*4, 3*8 + 3*Math.sqrt(2)*4);
    }
    
    /**
     * Test of getInfo method, of class EuclideLinkset.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        assertNotNull(linkset.getInfo());
        assertNotNull(linksetMulti.getInfo());
        assertNotNull(linksetPlanarCost.getInfo());
        assertNotNull(linksetExt.getInfo());
    }

    /**
     * Test of calcCorridor method, of class EuclideLinkset.
     */
    @Test
    public void testCalcCorridor() throws IOException {
        System.out.println("calcCorridor");
        for(Path path : linkset.getPaths()) {
            Geometry corridor = linkset.calcCorridor(path, 3);
            if(path.getCost() > 3) {
                assertEquals(0, corridor.getArea(), 1e-10);
            } else {
                assertEquals(2, corridor.getArea(), 1e-10);
            }
        }
        
        for(Path path : linkset.getPaths()) {
            Geometry corridor = linkset.calcCorridor(path, 4);
            if(path.getCost() > 4) {
                assertEquals(0, corridor.getArea(), 1e-10);
            } else {
                assertEquals(6, corridor.getArea(), 1e-10);
            }
        }
        
        for(Path path : linkset.getPaths()) {
            Geometry corridor = linkset.calcCorridor(path, 5);
            if(path.getCost() > 4) {
                assertEquals(8, corridor.getArea(), 1e-10);
            } else {
                assertEquals(12, corridor.getArea(), 1e-10);
            }
        }
    }

    /**
     * Test of calcRasterCorridor method, of class EuclideLinkset.
     */
    @Test
    public void testCalcRasterCorridor() throws Exception {
        System.out.println("calcRasterCorridor");
        // TODO
    }

    /**
     * Test of getCostVersion method.
     */
    @Test
    public void testGetCostVersion() {
        System.out.println("getCostVersion");
        CostLinkset costLinkset = linkset.getCostVersion(linkset.getHabitat());
        assertNotNull(costLinkset);
        assertEquals(Linkset.Distance.COST, costLinkset.getTypeDist());
        assertEquals(linkset.getCosts()[0], costLinkset.getCosts()[0], 1e-10);
        assertEquals(linkset.getTopology(), costLinkset.getTopology());
        assertEquals(linkset.isInter(), costLinkset.isInter());
    }

    /**
     * Test of getCircuitVersion method.
     */
    @Test
    public void testGetCircuitVersion() {
        System.out.println("getCircuitVersion");
        CircuitLinkset circuitLinkset = linkset.getCircuitVersion(linkset.getHabitat());
        assertNotNull(circuitLinkset);
        assertEquals(Linkset.Distance.CIRCUIT, circuitLinkset.getTypeDist());
        assertEquals(linkset.getCosts()[0], circuitLinkset.getCosts()[0], 1e-10);
        assertEquals(linkset.getTopology(), circuitLinkset.getTopology());
        assertEquals(linkset.isInter(), circuitLinkset.isInter());
    }

    /**
     * Test of getPathFinder method.
     */
    @Test
    public void testGetPathFinder() throws Exception {
        System.out.println("getPathFinder");
        assertNotNull(linkset.getPathFinder());
        assertNotNull(linksetMulti.getPathFinder());
        assertNotNull(linksetPlanarCost.getPathFinder());
        assertNotNull(linksetExt.getPathFinder());
    }

    /**
     * Test of estimCost method, of class CostLinkset.
     */
    @Test
    public void testEstimCost() {
        System.out.println("estimCost");

        assertEquals(4, linkset.estimCost(4, 0, 0), 1e-7);
        assertEquals(4, linksetMulti.estimCost(4, 0, 0), 1e-7);
        assertEquals(4, linksetPlanarCost.estimCost(4, 0, 0), 1e-7);
        
        assertEquals(4.194, linkset1_2.estimCost(4, 0, 0), 1e-3);
    }

    /**
     * Test of hasVoronoi method, of class CostLinkset.
     */
    @Test
    public void testHasVoronoi() {
        System.out.println("hasVoronoi");
        assertEquals(true, linkset.hasVoronoi());
        assertEquals(true, linksetMulti.hasVoronoi());
        assertEquals(true, linksetPlanarCost.hasVoronoi());
    }

    /**
     * Test of getVoronoi method, of class CostLinkset.
     */
    @Test
    public void testGetVoronoi() throws IOException {
        System.out.println("getVoronoi");
        // save and loads linksetMultiCost for testing costvoronoi loading
        linksetMultiCost.saveLinks(true);
        CostLinkset loadLinkset = new CostLinkset(linksetMultiCost.getHabitat(), linksetMultiCost.getName(), linksetMultiCost.getTopology(), 
                linksetMultiCost.isInter(), linksetMultiCost.getNodeFilter(), linksetMultiCost.getCosts(), linksetMultiCost.isRealPaths(), 
                linksetMultiCost.isRemoveCrossPatch(), linksetMultiCost.getDistMax(), linksetMultiCost.getCoefSlope());
        
        for(int id = 1; id <= 5; id++) {
            Feature voronoi = linksetMulti.getVoronoi(id);
            assertNotNull(voronoi);
            assertEquals(9, voronoi.getGeometry().getArea(), 1e-10);
            voronoi = loadLinkset.getVoronoi(id);
            assertNotNull(voronoi);
            assertEquals(9, voronoi.getGeometry().getArea(), 1e-10);
        }
        for(int id = 1+Habitat.ID_RANGE; id <= 4+Habitat.ID_RANGE; id++) {
            Feature voronoi = linksetMulti.getVoronoi(id);
            assertNotNull(voronoi);
            assertEquals(9, voronoi.getGeometry().getArea(), 1e-10);
            voronoi = loadLinkset.getVoronoi(id);
            assertNotNull(voronoi);
            assertEquals(9, voronoi.getGeometry().getArea(), 1e-10);
        }
    }

    /**
     * Test of getCostVoronoiFile method, of class CostLinkset.
     */
    @Test
    public void testGetCostVoronoiFile() {
        System.out.println("getCostVoronoiFile");

        File file = linkset.getCostVoronoiFile();
        assertNotNull(file);
        assertFalse(file.exists());
    }

    /**
     * Test of getPathsWithoutCost method, of class CostLinkset.
     */
    @Test
    public void testGetPathsWithoutCost() throws Exception {
        System.out.println("getPathsWithoutCost");

        assertEquals(8, linkset.getPathsWithoutCost(2).size());
        assertEquals(7, linkset1_2.getPathsWithoutCost(2).size());
       
    }

    /**
     * Test of extractCostFromPath method, of class CostLinkset.
     */
    @Test
    public void testExtractCostFromPath() throws Exception {
        System.out.println("extractCostFromPath");

        HashMap2D<Path, Double, Integer> result = linkset.extractCostFromPath();
        for(Path p : linkset.getPaths()) {
            assertEquals(1, result.getLine(p).size());
            if(p.getDist() == 3.0) {
                assertEquals(p.getCost()+1, result.getValue(p, 1.0), 1e-10);
            } else { // diagonale
                assertEquals(4, (int)result.getValue(p, 1.0));
            }
        }
        
        result = linkset1_2.extractCostFromPath();
        for(Path p : linkset1_2.getPaths()) {
            if(Math.abs(p.getDist()-p.getCost()) > 1e-3) {
                assertEquals(2, result.getLine(p).size());
                assertEquals(2, (int)result.getValue(p, 1.0));
                assertEquals(2, (int)result.getValue(p, 2.0));
            } else {
                assertEquals(0, (int)result.getValue(p, 2.0));
                if(p.getDist()== 3.0) {
                    assertEquals(p.getCost()+1, result.getValue(p, 1.0), 1e-10);
                } else { // diagonale
                    if(p.getDist() > 5) {
                        assertEquals(7, (int)result.getValue(p, 1.0));
                    } else {
                        assertEquals(4, (int)result.getValue(p, 1.0));
                    }
                }
            }
        }
        
    }


    /**
     * Test of saveLinks method, of class CostLinkset.
     */
    @Test
    public void testSaveLinks() throws Exception {
        System.out.println("saveLinks");
        File linkFile = linksetPlanarCost.getLinkFile();
        File intraLinkFile = linksetPlanarCost.getIntraLinks().getIntraLinkFile();
        File voronoiFile = linksetPlanarCost.getCostVoronoiFile();
        assertFalse(linkFile.exists());
        assertFalse(intraLinkFile.exists());
        assertFalse(voronoiFile.exists());
        linksetPlanarCost.saveLinks(false);
        assertTrue(linkFile.exists());
        assertFalse(intraLinkFile.exists());
        assertFalse(voronoiFile.exists());
        linksetPlanarCost.saveLinks(true);
        assertTrue(linkFile.exists());
        assertTrue(intraLinkFile.exists());
        assertTrue(voronoiFile.exists());
    }
    
}
