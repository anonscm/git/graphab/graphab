/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.common.Config;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;

/**
 *
 * @author gvuidel
 */
public class LinksetTest {
    
    private static Project project;
    private static Linkset linkset;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        project = ProjectTest.createCrossProjectH0();
        linkset = new EuclideLinkset(project.getHabitats().iterator().next(), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        project.addLinkset(linkset, false);
    }

    /**
     * Test of getProject method, of class Linkset.
     */
    @Test
    public void testGetProject() {
        System.out.println("getProject");
        assertEquals(project, linkset.getProject());
    }


    /**
     * Test of computeCorridor method, of class Linkset.
     */
    @Test
    public void testComputeCorridor() {
        System.out.println("computeCorridor");

        List<Feature> corridors = linkset.computeCorridor(Config.getProgressBar(), null, 3);
        assertEquals(4, corridors.size());
        for(Feature f : corridors) {
            assertEquals(2, f.getGeometry().getArea(), 1e-15);
        }

        corridors = linkset.computeCorridor(Config.getProgressBar(), null, 4);
        assertEquals(4, corridors.size());
        for(Feature f : corridors) {
            assertEquals(6, f.getGeometry().getArea(), 1e-15);
        }
        
        corridors = linkset.computeCorridor(Config.getProgressBar(), null, 5);
        assertEquals(8, corridors.size());
    }
    
    /**
     * Test of computeCorridor method, of class Linkset.
     */
    @Test
    public void testComputeRasterCorridor() {
        System.out.println("computeRasterCorridor");

        Raster corridors = linkset.computeRasterCorridor(Config.getProgressBar(), null, 3, 0, 0, null);
        assertEquals(4, corridors.getSample(5, 5, 0));
        assertEquals(1, corridors.getSample(3, 5, 0));
        assertEquals(1, corridors.getSample(5, 3, 0));
        assertEquals(1, corridors.getSample(4, 5, 0));
        assertEquals(1, corridors.getSample(5, 4, 0));
        assertEquals(1, corridors.getSample(5, 6, 0));
        assertEquals(1, corridors.getSample(6, 5, 0));
        assertEquals(1, corridors.getSample(7, 5, 0));
        assertEquals(1, corridors.getSample(5, 7, 0));

        corridors = linkset.computeRasterCorridor(Config.getProgressBar(), null, 4, 0, 0, null);
        assertEquals(4, corridors.getSample(5, 5, 0));
        assertEquals(2, corridors.getSample(4, 4, 0));
        assertEquals(2, corridors.getSample(4, 6, 0));
        assertEquals(2, corridors.getSample(6, 4, 0));
        assertEquals(2, corridors.getSample(6, 6, 0));
        assertEquals(1, corridors.getSample(5, 6, 0));
        assertEquals(1, corridors.getSample(6, 5, 0));
        assertEquals(1, corridors.getSample(7, 5, 0));
        assertEquals(1, corridors.getSample(5, 7, 0));
        
        corridors = linkset.computeRasterCorridor(Config.getProgressBar(), null, 5, 0, 0, null);
        assertEquals(4, corridors.getSample(5, 5, 0));
        assertEquals(3, corridors.getSample(2, 5, 0));
        assertEquals(3, corridors.getSample(5, 2, 0));
    }
    
    /**
     * Test of saveLinks method, of class Linkset.
     */
    @Test
    public void testSaveLinks() throws Exception {
        System.out.println("saveLinks");
        File linkFile = linkset.getLinkFile();
        File intraLinkFile = linkset.getIntraLinks().getIntraLinkFile();
        assertFalse(linkFile.exists());
        assertFalse(intraLinkFile.exists());
        linkset.saveLinks(false);
        assertTrue(linkFile.exists());
        assertFalse(intraLinkFile.exists());
        linkset.saveLinks(true);
        assertTrue(linkFile.exists());
        assertTrue(intraLinkFile.exists());
    }
    
    public static void checkLinkset(Linkset link, int nbLinks, double sumCost, double sumDist) {
        assertEquals("Nb links " + link.getName(), nbLinks, link.getPaths().size());
        double sumCosts = 0, sumDists = 0;
        for(Path p : link.getPaths()) {
            sumCosts += p.getCost();
            sumDists += p.getDist();
        }
        assertEquals("Sum of cost " + link.getName(), sumCost, sumCosts, 1e-5);
        if(!Double.isNaN(sumDist)) {
            assertEquals("Sum of length " + link.getName(), sumDist, sumDists, 1e-10);
        }
    }
}
