/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.addpatch;

import java.io.IOException;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.graph.DefaultGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author gvuidel
 */
public class AddPatchGraphGeneratorTest {
    
    private static Project prj;
    private static DefaultGraph graph;
    private static AbstractMonoHabitat h0;
    
    
    @BeforeClass
    public static void setUp() throws IOException, SchemaException {
        prj = ProjectTest.createCrossProjectH0();
        h0 = prj.getHabitat("h0");
        prj.addLinkset(new EuclideLinkset(h0, "comp", Linkset.Topology.COMPLETE, false, null, false, 2.5), false);
        graph = new DefaultGraph("test", prj.getLinkset("comp"), false);
        prj.addGraph(graph, false);
    }

    /**
     * Test of addPatch method, of class AddPatchGraphGenerator.
     */
    @Test
    public void testAddPatchAndReset() throws IOException {
        System.out.println("addPatch and reset");
        AddPatchGraphGenerator addPatchGraph = new AddPatchGraphGenerator(graph);
        addPatchGraph.addPatch(new GeometryFactory().createPoint(new Coordinate(2.5, -2.5)), 1);
        assertEquals(graph.getNodes().size()+1, addPatchGraph.getNodes().size());
        assertEquals(graph.getEdges().size()+3, addPatchGraph.getEdges().size());
        GraphPathFinder pathFinder = addPatchGraph.getPathFinder(addPatchGraph.getNode(h0.getPatch(1)));
        assertEquals(2*Math.sqrt(2), pathFinder.getCost(addPatchGraph.getNode(h0.getPatch(2))), 1e-10);
        
        addPatchGraph.reset();
        assertEquals(graph.getNodes().size(), addPatchGraph.getNodes().size());
        assertEquals(graph.getEdges().size(), addPatchGraph.getEdges().size());
        pathFinder = addPatchGraph.getPathFinder(addPatchGraph.getNode(h0.getPatch(1)));
        assertEquals(4.0, pathFinder.getCost(addPatchGraph.getNode(h0.getPatch(2))), 1e-10);
    }

    
    
}
