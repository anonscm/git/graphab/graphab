/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import org.geotools.feature.SchemaException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;

/**
 *
 * @author gvuidel
 */
public class MultiHabitatTest {
    
    private static Project prj;
    private static MultiHabitat multiHab;
    private static MonoHabitat h0, h4;
    
    /**
     * Initialize test
     */
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        prj = ProjectTest.createCrossProject();
        h0 = new MonoHabitat("h0", prj, Collections.singleton(0), false, 0, 0, false);
        prj.addHabitat(h0, false);
        h4 = new MonoHabitat("h4", prj, Collections.singleton(4), false, 0, 0, false);
        prj.addHabitat(h4, false);
        multiHab = new MultiHabitat(Arrays.asList(h0, h4), true);
    }
    

    @Test
    public void testConstructor() throws Exception {
        System.out.println("constructor");
        MultiHabitat h = new MultiHabitat(Arrays.asList(h0, h4), false);
        assertEquals(true, h.hasVoronoi()); // true because multihabitat with voronoi already created in setupClass
        h = new MultiHabitat(Arrays.asList(h0, new MonoHabitat("h1", prj, Collections.singleton(1), false, 0, 0, false)), false);
        assertEquals(false, h.hasVoronoi());
    }
    
    /**
     * Test of getPatches method, of class MultiHabitat.
     */
    @Test
    public void testGetPatches() {
        System.out.println("getPatches");
        assertEquals(h0.getPatches().size()+h4.getPatches().size(), multiHab.getPatches().size());
        assertTrue(multiHab.getPatches().containsAll(h0.getPatches()));
        assertTrue(multiHab.getPatches().containsAll(h4.getPatches()));

    }

    /**
     * Test of getPatch method, of class MultiHabitat.
     */
    @Test
    public void testGetPatch() {
        System.out.println("getPatch");
        for(DefaultFeature patch : h0.getPatches()) {
            assertEquals(patch, multiHab.getPatch((int)patch.getId()));
        }
        for(DefaultFeature patch : h4.getPatches()) {
            assertEquals(patch, multiHab.getPatch((int)patch.getId()));
        }
    }

    /**
     * Test of getPatchId method, of class MultiHabitat.
     */
    @Test
    public void testGetPatchId() {
        System.out.println("getPatchId");
        assertEquals(-1, multiHab.getPatchId(0, 0));
        assertEquals(0, multiHab.getPatchId(1, 1));
        assertEquals(Habitat.ID_RANGE+1, multiHab.getPatchId(2, 2));
        assertEquals(1, multiHab.getPatchId(5, 2));
        assertEquals(3, multiHab.getPatchId(5, 5));
        assertEquals(Habitat.ID_RANGE+4, multiHab.getPatchId(8, 8));
        
    }

    /**
     * Test of getPatchId method, of class MultiHabitat.
     */
    @Test
    public void testGetHabitat() {
        System.out.println("getHabitat");
        assertEquals(h0, multiHab.getHabitat(1));
        assertEquals(h0, multiHab.getHabitat(5));
        assertEquals(h4, multiHab.getHabitat(Habitat.ID_RANGE+1));
        assertEquals(h4, multiHab.getHabitat(Habitat.ID_RANGE+15));
    }

    /**
     * Test of getRasterPatchBounds method, of class MultiHabitat.
     */
    @Test
    public void testGetRasterPatchBounds() {
        System.out.println("getRasterPatchBounds");
        assertEquals(h0.getRasterPatchBounds(), multiHab.getRasterPatchBounds());
    }

    /**
     * Test of getTotalPatchCapacity method, of class MultiHabitat.
     */
    @Test
    public void testGetTotalPatchCapacity() {
        System.out.println("getTotalPatchCapacity");
        assertEquals(h0.getTotalPatchCapacity()+h4.getTotalPatchCapacity(), multiHab.getTotalPatchCapacity(), 1e-10);
    }

    /**
     * Test of getHabitats method, of class MultiHabitat.
     */
    @Test
    public void testGetHabitats() {
        System.out.println("getHabitats");
        assertTrue(multiHab.getHabitats().size() == 2);
        assertTrue(multiHab.getHabitats().contains(h0));
        assertTrue(multiHab.getHabitats().contains(h4));
    }

    /**
     * Test of getIdHabitats method, of class MultiHabitat.
     */
    @Test
    public void testGetIdHabitats() {
        System.out.println("getIdHabitats");
        assertTrue(multiHab.getIdHabitats().size() == 2);
        assertTrue(multiHab.getIdHabitats().contains(0));
        assertTrue(multiHab.getIdHabitats().contains(1));
    }
    
}
