/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import org.geotools.feature.SchemaException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;

/**
 *
 * @author gvuidel
 */
public class AbstractMonoHabitatTest {
    
    private Project prj;
    
    
    @Before
    public void setUp() throws IOException, SchemaException {
        prj = ProjectTest.createCrossProject();
    }

    /**
     * Test of load method, of class AbstractMonoHabitat.
     */
    @Test
    public void testLoad() throws Exception {
        System.out.println("load");
        new MonoHabitat("test", prj, new HashSet<>(Arrays.asList(0)), false, 0, 0, false);
        AbstractMonoHabitat hab = new AbstractMonoHabitat(0, "test", prj) { };
        hab.load(prj);
        assertEquals(5, hab.getPatches().size());
        for(int i = 1; i <= 5; i++) {
            assertEquals(i, hab.getPatch(i).getId());
        }
    }

    /**
     * Test of createHabitat method, of class AbstractMonoHabitat.
     */
    @Test
    public void testCreateHabitat() throws IOException, SchemaException {
        System.out.println("createHabitat");
        AbstractMonoHabitat hab = new MonoHabitat("test", prj, new HashSet<>(Arrays.asList(0,3)), true, 0, 0, false).createHabitat("testCreateHabitat", 2);

        assertEquals(1, hab.getPatches().size());
        for(Feature patch : hab.getPatches()) {
            assertTrue(Habitat.getPatchArea(patch) >= 2);
            assertTrue(Habitat.getPatchCapacity(patch) >= 2);
        }
    }
    
}
