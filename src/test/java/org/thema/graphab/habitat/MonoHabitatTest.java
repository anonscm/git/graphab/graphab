/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.SchemaException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;

/**
 *
 * @author gvuidel
 */
public class MonoHabitatTest {
    
    private Project prj;
    
    @Before
    public void setUp() throws IOException, SchemaException {
        prj = ProjectTest.createCrossProject();
    }
    
    @After
    public void tearDown() {
        // TODO delete recursively
        prj.getDirectory().delete();
        prj = null;
    }

    /**
     * Check patch number, sizes and planar links 
     * @throws Exception 
     */
    @Test
    public void testConstructor() throws Exception {

        MonoHabitat hab = new MonoHabitat("test1", prj, Collections.singleton(0), false, 0, 0, true);
        checkHabitat(hab, 5, 5, true, 8);
        hab = new MonoHabitat("test2", prj, Collections.singleton(0), false, 0, 0, false);
        checkHabitat(hab, 5, 5, false, -1);
        hab = new MonoHabitat("test3", prj, new HashSet<>(Arrays.asList(0,3)), false, 0, 0, true);
        checkHabitat(hab, 6, 9, true, 10); // ou 9 links
        hab = new MonoHabitat("test4", prj, new HashSet<>(Arrays.asList(0,3)), true, 0, 0, true);
        checkHabitat(hab, 3, 9, true, 3);
        hab = new MonoHabitat("test5", prj, new HashSet<>(Arrays.asList(0,3)), true, 2, 0, true);
        checkHabitat(hab, 1, 7, false, -1);
        hab = new MonoHabitat("test6", prj, new HashSet<>(Arrays.asList(0,3)), true, 0, 2, true);
        checkHabitat(hab, 6, 9, true, 9);
    }
    
    private void checkHabitat(MonoHabitat hab, int nbPatch, double area, boolean voronoi, int nbLinks) {
        assertEquals("Number of patches", nbPatch, hab.getPatches().size());
        double a = 0;
        for(Feature patch : hab.getPatches()) {
            a += Habitat.getPatchArea(patch);
        }
        assertEquals("Patches area", area, a, 1e-13);
        a = 0;
        for(Feature patch : hab.getPatches()) {
            a += patch.getGeometry().getArea();
        }
        assertEquals("Patches area", area, a, 1e-13);
        
        assertEquals("Has Voronoi", voronoi, hab.hasVoronoi());
        
        if(voronoi) {
            assertEquals("Nb planar links", nbLinks, hab.getPlanarLinks().getFeatures().size());
        }
    }
    
    @Test
    public void testSetCapacity() throws IOException, SchemaException {
        MonoHabitat hab = new MonoHabitat("testcapa1", prj, Collections.singleton(0), false, 0, 0, false);
        CapaPatchDialog.CapaPatchParam param = new CapaPatchDialog.CapaPatchParam();
        param.calcArea = true;
        param.codeWeight = new HashMap<Integer, Double>() {{ put(0, 1.0);}};
        Assert.assertTrue(param.isArea());
        hab.setCapacities(param, false);
        for(DefaultFeature patch : hab.getPatches()) {
            Assert.assertEquals(Habitat.getPatchArea(patch), Habitat.getPatchCapacity(patch), 1e-10);
        }
        param = new CapaPatchDialog.CapaPatchParam();
        param.calcArea = true;
        param.codeWeight = new HashMap<Integer, Double>() {{ put(0, 2.0);}};
        Assert.assertFalse(param.isArea());
        hab.setCapacities(param, false);
        for(DefaultFeature patch : hab.getPatches()) {
            Assert.assertEquals(Habitat.getPatchArea(patch)*2, Habitat.getPatchCapacity(patch), 1e-10);
        }
        param = new CapaPatchDialog.CapaPatchParam();
        param.importFile = new File("target/test-classes/org/thema/graphab/capa_cross.csv");
        param.idField="id";
        param.capaField="capa";
        hab.setCapacities(param, false);
        for(DefaultFeature patch : hab.getPatches()) {
            Assert.assertEquals((Integer)patch.getId(), Habitat.getPatchCapacity(patch), 1e-10);
        }
        
        hab = new MonoHabitat("testcapa2", prj, new TreeSet(Arrays.asList(0,3)), true, 0, 0, false);
        param = new CapaPatchDialog.CapaPatchParam();
        param.calcArea = true;
        param.codeWeight = new HashMap<Integer, Double>() {{ put(0, 2.0); put(3, 2.0);}};
        Assert.assertFalse(param.isArea());
        hab.setCapacities(param, false);
        for(DefaultFeature patch : hab.getPatches()) {
            Assert.assertEquals(Habitat.getPatchArea(patch)*2, Habitat.getPatchCapacity(patch), 1e-10);
        }
        param = new CapaPatchDialog.CapaPatchParam();
        param.calcArea = true;
        param.codeWeight = new HashMap<Integer, Double>() {{ put(0, 0.5); put(3, 2.0);}};
        Assert.assertFalse(param.isArea());
        hab.setCapacities(param, false);
        for(DefaultFeature patch : hab.getPatches()) {
            if(Habitat.getPatchArea(patch) == 1.0) {
                Assert.assertEquals(Habitat.getPatchArea(patch), 2*Habitat.getPatchCapacity(patch), 1e-10);
            } else {
                Assert.assertEquals(3*0.5+4*2, Habitat.getPatchCapacity(patch), 1e-10);
            }
        }
        
    }
}
