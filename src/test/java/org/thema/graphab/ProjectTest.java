/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeSet;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.SchemaException;
import org.geotools.graph.structure.Node;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import org.thema.common.Config;
import org.thema.data.IOImage;
import org.thema.data.feature.Feature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Path;

/**
 * Test Project class
 * @author Gilles Vuidel
 */
public class ProjectTest {
    
    private static Project project;
    
    /**
     * Initialize test
     * @throws Exception 
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
        project = loadCrossProject();
    }

    /**
     * Check loadProject
     */
    @Test
    public void testLoadProject() throws IOException {
        System.out.println("Load project");
        Assert.assertNotNull(project);
    }
    
    /**
     * Check intra patch distances
     */
    @Test
    public void testIntraPatchDist() {
        // TODO add test with larger patches
        System.out.println("Test intra patch distance");
        AbstractGraph graph = project.getGraph("all");
        Linkset linkset = project.getLinkset("jeulien1");
        for(Node node : graph.getNodes()) {
            GraphPathFinder pathFinder = graph.getPathFinder(node);
            for(Path p : linkset.getPaths()) {
                if(p.getPatch1().equals(node.getObject())) {
                    assertTrue("Compare direct link with path with intrapatch between " + node.getObject() + " and " + p.getPatch2() + " lien : " + p.getCost() +" - chemin : " + pathFinder.getCost(graph.getNode(p.getPatch2())), 
                            p.getCost() <= pathFinder.getCost(graph.getNode(p.getPatch2()))*(1+1e-7));
                } else if(p.getPatch2().equals(node.getObject())) {
                    assertTrue("Compare direct link with path with intrapatch between " + node.getObject() + " and " + p.getPatch2() + " lien : " + p.getCost() +" - chemin : " + pathFinder.getCost(graph.getNode(p.getPatch1())), 
                            p.getCost() <= pathFinder.getCost(graph.getNode(p.getPatch1()))*(1+1e-7));
                }
            }
        }
    }
    
    @Test
    public void testCreateMetaPatch() throws IOException, SchemaException {
        System.out.println("Test createMetaPatch");
        AbstractGraph graph = project.getGraph("cout1_2_3");
        AbstractMonoHabitat habitat = project.createMetaPatchHabitat("testMetaPatch", graph, 0, 0, false);
        assertEquals(graph.getComponents().size(), habitat.getPatches().size());
        
        habitat = project.createMetaPatchHabitat("testMetaPatch", graph, 0, 4, false);
        assertEquals(1, habitat.getPatches().size());
        for(Feature patch : habitat.getPatches()) {
            assertTrue(Habitat.getPatchArea(patch) >= 4);
            assertTrue(Habitat.getPatchCapacity(patch) >= 4);
        }
    }
    
    public static Project loadCrossProject() throws IOException {
        // init 2 threads
        Config.setNodeClass(ProjectTest.class);
        Config.setParallelProc(2);
        return Project.loadProject(new File("target/test-classes/org/thema/graphab/cross_project/cross.xml"), true);
    }
    
    public static Project createCrossProject() throws IOException, SchemaException {
        // init 2 threads
        Config.setNodeClass(ProjectTest.class);
        Config.setParallelProc(2);
        
        GridCoverage2D cov = IOImage.loadTiff(new File("target/test-classes/org/thema/graphab/land_cross.tif"));
        return new Project("cross_test", Files.createTempDirectory("graphab_test").toFile(), cov, 
                new TreeSet(Arrays.asList(0, 1, 2, 3, 4, 5)), Double.NaN);
    }
    
    public static Project createCrossProjectH0() throws IOException, SchemaException {
        Project prj = createCrossProject();
        prj.addHabitat(new MonoHabitat("h0", prj, Collections.singleton(0), false, 0, 0, true), true);
        return prj;
    }
    
    public static Project createCrossProjectH0H4() throws IOException, SchemaException {
        Project prj = createCrossProjectH0();
        prj.addHabitat(new MonoHabitat("h4", prj, Collections.singleton(4), false, 0, 0, true), true);
        return prj;
    }
}
