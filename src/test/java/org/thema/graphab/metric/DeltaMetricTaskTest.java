/*
 * Copyright (C) 2015 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric;

import java.util.Collections;
import java.util.Map;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.thema.common.swing.TaskMonitor;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.CapaPatchDialog;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.metric.global.DeltaPCMetric;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.graphab.metric.global.NCMetric;
import org.thema.graphab.metric.global.PCMetric;
import org.thema.parallel.ExecutorService;

/**
 *
 * @author Gilles Vuidel
 */
public class DeltaMetricTaskTest {
    
    private Project project;
    private AbstractGraph cross, all;
    private Habitat hab;
    
    @Before
    public void beforeTest() throws Exception {
        project = ProjectTest.loadCrossProject();
        // set capacities to 2
        CapaPatchDialog.CapaPatchParam params = new CapaPatchDialog.CapaPatchParam();
        params.codeWeight = Collections.singletonMap(0, 2.0);
        ((MonoHabitat)project.getHabitat("h0")).setCapacities(params, false);
        cross = project.getGraph("cross");
        all = project.getGraph("all");
        hab = all.getHabitat();
        
    }
    
    /**
     * Test delta metric 
     */
    @Test
    public void testDeltaMetric()  {
        System.out.println("Delta metric NC");
        
        NCMetric nc = new NCMetric();
        GlobalMetricResult init = new GlobalMetricResult(nc, cross).calculate(true, null);
        DeltaMetricTask task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init);
        ExecutorService.execute(task);
        Map<Object, Double[]> result = task.getResult();
        assertEquals(cross.getNodes().size()+cross.getEdges().size(), result.size());
        assertEquals((1-4)/1, result.get(3)[0], 1e-10);
        assertEquals(0, result.get(1)[0], 1e-10);
        assertEquals((1-2)/1, result.get("3-1")[0], 1e-10);
        
        init = new GlobalMetricResult(nc, all).calculate(true, null);
        task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init, all.getHabitat().getPatches(), Collections.emptyList());
        ExecutorService.execute(task);
        result = task.getResult();
        assertEquals(all.getNodes().size(), result.size());
        for(Object id : result.keySet()) {
            assertEquals(0, result.get(3)[0], 1e-10);
        }
    }

    /**
     * Test delta PC metrics
     */
    @Test
    public void testDeltaPCMetric()  {
        System.out.println("Delta PC metric");
        
        PCMetric pc = new PCMetric();
        pc.setParamFromDetailName("_d3_p0.5");
        GlobalMetricResult init = new GlobalMetricResult(pc, cross).calculate(true, null);
        DeltaMetricTask task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init);
        ExecutorService.execute(task);
        Map<Object, Double[]> result = task.getResult();
        double initPC = init.getResult()[0];
        double a2 = Math.pow(project.getArea(), 2);
        assertEquals((initPC-(2*2*4/a2))/initPC, result.get(3)[0], 1e-10);
        assertEquals((initPC-(2*2*(1+0.5*3 + 3*(1+0.5+2*0.25))/a2))/initPC, result.get(1)[0], 1e-10);
        assertEquals((initPC-(2*2*(1+1+0.5*3 + 3*(1+0.5+2*0.25))/a2))/initPC, result.get("3-1")[0], 1e-10);
        
        pc.setParamFromDetailName("_d3_p1");
        init = new GlobalMetricResult(pc, all).calculate(true, null);
        task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init);
        ExecutorService.execute(task);
        result = task.getResult();
        initPC = init.getResult()[0];
        for(Object id : result.keySet()) {
            // if it's a node
            if(id instanceof Integer) {
                assertEquals((initPC-(2*4*2*4/a2))/initPC, result.get(id)[0], 1e-10);
            } else { // an edge
                assertEquals(0.0, result.get(id)[0], 1e-10);
            }
        }
        
        pc.setParamFromDetailName("_d3_p0.5");
        init = new GlobalMetricResult(pc, project.getGraph("plan_cout1_h0_h4")).calculate(true, null);
        task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init);
        ExecutorService.execute(task);
        result = task.getResult();
        initPC = init.getResult()[0];
        for(Object id : result.keySet()) {
            double d;
            // if it's an edge
            if(id instanceof String) {
                // if edge between patches of h0
                if(id.toString().length() == 3) {
                    d = 2*(2*2*(0.5-0.125+0.25-0.0625));
                } else { // edge between h0 and h4
                    d = 2*(1*2*(0.5-0.125)+1*1*(0.25-0.0625));
                }
            } else { // it is a node
                if((int)id == 3) {
                    d = 2*2 + 2*(2*2*4*0.5+1*2*4*0.25) + 4*(2*2*(0.25-0.0625));
                } else if((int)id < Habitat.ID_RANGE) {
                    d = 2*2 + 2*(2*2*(0.5+3*0.25)+1*2*(2*0.5+2*0.125)) + 2*(1*1*(0.25-0.0625));
                } else {
                    d = 1*1 + 2*(1*2*(2*0.5+0.25+2*0.125)+1*1*(2*0.25+1*0.0625)) + 0;
                }
            }
            assertEquals("Delta on " + id, d/a2/initPC, result.get(id)[0], 1e-10);
        }
        
        
        DeltaPCMetric deltaPC = new DeltaPCMetric();
        deltaPC.setParamFromDetailName("_d3_p1");
        init = new GlobalMetricResult(deltaPC, cross).calculate(true, null);
           
        task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init);
        ExecutorService.execute(task);
        result = task.getResult();
        initPC = init.getResult()[2];
        assertArrayEquals(new Double[]{2*2/initPC, 2*2*2*4/initPC, 2*2*3*4/initPC}, result.get(3));
        assertArrayEquals(new Double[]{2*2/initPC, 2*2*2*4/initPC, 0.0}, result.get(1));
        assertArrayEquals(new Double[]{0.0, 0.0, 2*2*2*4/initPC}, result.get("3-1"));
        
        deltaPC.setParamFromDetailName("_d3_p0.5");
        init = new GlobalMetricResult(deltaPC, project.getGraph("plan_cout1_h0_h4")).calculate(true, null);
        task = new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), init);
        ExecutorService.execute(task);
        result = task.getResult();
        initPC = init.getResult()[2];
        for(Object id : result.keySet()) {
            // if it's an edge
            if(id instanceof String) {
                // if edge between patches of h0
                if(id.toString().length() == 3) {
                    assertArrayEquals("Delta on " + id, new Double[]{0.0, 0.0, 2*(2*2*(0.5-0.125+0.25-0.0625))/initPC}, result.get(id));
                } else { // edge between h0 and h4
                    assertArrayEquals("Delta on " + id, new Double[]{0.0, 0.0, 2*(1*2*(0.5-0.125)+1*1*(0.25-0.0625))/initPC}, result.get(id));
                }
            } else { // it is a node
                if((int)id == 3) {
                    assertArrayEquals("Delta on " + id, new Double[]{2*2/initPC, 2*(2*2*4*0.5+1*2*4*0.25)/initPC, 4*(2*2*(0.25-0.0625))/initPC}, result.get(id));
                } else if((int)id < Habitat.ID_RANGE) {
                    assertArrayEquals("Delta on " + id, new Double[]{2*2/initPC, 2*(2*2*(0.5+3*0.25)+1*2*(2*0.5+2*0.125))/initPC, 2*(1*1*(0.25-0.0625))/initPC}, result.get(id));
                } else {
                    assertArrayEquals("Delta on " + id, new Double[]{1*1/initPC, 2*(1*2*(2*0.5+0.25+2*0.125)+1*1*(2*0.25+1*0.0625))/initPC, 0/initPC}, result.get(id));
                }
            }
        }
    }
    
}
