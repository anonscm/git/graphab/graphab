/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.local;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Node;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.CapaPatchDialog;
import org.thema.graphab.habitat.Habitat;
import static org.thema.graphab.habitat.Habitat.ID_RANGE;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.links.Path;
import org.thema.graphab.metric.MHMetric;

/**
 *
 * @author gvuidel
 */
public class LocalMetricTest {
    
    private Project project;
    private AbstractGraph cross, all;
    private Habitat hab;
    
    @Before
    public void beforeTest() throws Exception {
        project = ProjectTest.loadCrossProject();
        // set capacities to 2
        CapaPatchDialog.CapaPatchParam params = new CapaPatchDialog.CapaPatchParam();
        params.codeWeight = Collections.singletonMap(0, 2.0);
        ((MonoHabitat)project.getHabitat("h0")).setCapacities(params, false);
        
        cross = project.getGraph("cross");
        all = project.getGraph("all");
        hab = all.getHabitat();
        
    }
    
    @Test
    public void testDrMetric() {
        DrLocalMetric metric = new DrLocalMetric();
        
        metric.setParamFromDetailName("_capa2");
        checkAllNodes(metric, cross, 0);
        checkAllNodes(metric, all, 0);
        
        metric.setParamFromDetailName("_capa4");
        checkAllNodes(metric, cross, 3);
        checkAllNodes(metric, all, 3);
        
        metric.setParamFromDetailName("_capa10");
        assertEquals(3, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(3, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        assertEquals(6, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(6, metric.calcSingleMetric(cross.getNode(hab.getPatch(2)), cross), 1e-15);
        assertEquals(6, metric.calcSingleMetric(cross.getNode(hab.getPatch(4)), cross), 1e-15);
        assertEquals(6, metric.calcSingleMetric(cross.getNode(hab.getPatch(5)), cross), 1e-15);
        
        metric.setParamFromDetailName("_capa100");
        checkAllNodes(metric, cross, Double.POSITIVE_INFINITY);
        checkAllNodes(metric, all, Double.POSITIVE_INFINITY);
    }
    
    @Test
    public void testRrMetric() {
        RrLocalMetric metric = new RrLocalMetric();
        
        metric.setParamFromDetailName("_dist1_capa2");
        checkAllNodes(metric, cross, 1);
        checkAllNodes(metric, all, 1);
        
        metric.setParamFromDetailName("_dist1_capa4");
        checkAllNodes(metric, cross, 0.5);
        checkAllNodes(metric, all, 0.5);
        
        metric.setParamFromDetailName("_dist3_capa10");
        checkAllNodes(metric, cross, 0.2);
        checkAllNodes(metric, all, 0.2);
        
        metric.setParamFromDetailName("_dist6_capa10");
        assertEquals(0.2+0.8/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(0.2+0.8/2, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        assertEquals(0.2+0.2/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(0.2+0.2/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(2)), cross), 1e-15);
        assertEquals(0.2+0.2/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(4)), cross), 1e-15);
        assertEquals(0.2+0.2/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(5)), cross), 1e-15);
        
        metric.setParamFromDetailName("_dist6_capa4");
        checkAllNodes(metric, cross, 0.5+0.5/2);
        checkAllNodes(metric, all, 0.5+0.5/2);
        
        metric.setParamFromDetailName("_dist12_capa10");
        assertEquals(0.2+0.8*3/4, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(0.2+0.8*3/4, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        assertEquals(0.2+0.2*3/4+0.6/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(0.2+0.2*3/4+0.6/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(2)), cross), 1e-15);
        assertEquals(0.2+0.2*3/4+0.6/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(4)), cross), 1e-15);
        assertEquals(0.2+0.2*3/4+0.6/2, metric.calcSingleMetric(cross.getNode(hab.getPatch(5)), cross), 1e-15);
    }
    
    @Test
    public void testIFhMetric() {
        IFhLocalMetric metric = new IFhLocalMetric();
        // test with monohabitat
        testIFMetric(metric);
        metric.setSelection(MHMetric.ALL);
        AbstractGraph multiHab = project.getGraph("plan_cout1_h0_h4");
        Habitat mh = multiHab.getHabitat();
        metric.setParamFromDetailName("_d3_p0.5_beta1");
        assertArrayEquals(new Double[]{2*2*(0.5*4+1), 1*2*(0.25*4)}, metric.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        assertArrayEquals(new Double[]{2*2*(1+0.5+3*0.25), 1*2*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(1)), multiHab));
        assertArrayEquals(new Double[]{2*2*(1+0.5+3*0.25), 1*2*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(2)), multiHab));
        assertArrayEquals(new Double[]{2*2*(1+0.5+3*0.25), 1*2*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(4)), multiHab));
        assertArrayEquals(new Double[]{2*2*(1+0.5+3*0.25), 1*2*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(5)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125), 1*1*(1+0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+1)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125), 1*1*(1+0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+2)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125), 1*1*(1+0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+3)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125), 1*1*(1+0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+4)), multiHab));
        
        metric.setParamFromDetailName("_d3_p0.5_beta0.5");
        assertArrayEquals(new Double[]{2*(0.5*4+1), Math.sqrt(1*2)*(0.25*4)}, metric.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        
        assertEquals(2, metric.getResultNames(multiHab).length);
        
        IFhLocalMetric metricInter = new IFhLocalMetric();
        metricInter.setParamFromDetailName("_d3_p0.5_beta1");
        metricInter.setSelection(MHMetric.INTER);
        assertArrayEquals(new String[]{MHMetric.INTER}, metricInter.getResultNames(multiHab));
        
        assertArrayEquals(new Double[]{1*2*(0.25*4)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(1)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(2)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(4)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(5)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+1)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+2)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+3)), multiHab));
        assertArrayEquals(new Double[]{1*2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+4)), multiHab));
        
        IFhLocalMetric metric1 = new IFhLocalMetric();
        metric1.setParamFromDetailName("_d3_p0.5_beta1");
        metric1.setSelection("2");
        assertArrayEquals(new String[]{"2"}, metric1.getResultNames(multiHab));
        
        assertArrayEquals(new Double[]{1*2*(0.25*4)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(1)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(2)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(4)), multiHab));
        assertArrayEquals(new Double[]{1*2*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(5)), multiHab));
        assertArrayEquals(new Double[]{1*1*(1+0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+1)), multiHab));
        assertArrayEquals(new Double[]{1*1*(1+0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+2)), multiHab));
        assertArrayEquals(new Double[]{1*1*(1+0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+3)), multiHab));
        assertArrayEquals(new Double[]{1*1*(1+0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+4)), multiHab));
    }
    
    @Test
    public void testIFMetric() {
        testIFMetric(new IFLocalMetric());
    }
    
    private void testIFMetric(LocalMetric metric) {
        System.out.println(metric.getShortName() + " metric");

        metric.setParamFromDetailName("_d3_p0.5_beta1");
        assertEquals(2*2*(0.5*4+1), metric.calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-15);
        assertEquals(2*2*(0.5*4+1), metric.calcMetric(all.getNode(hab.getPatch(3)), all)[0], 1e-15);
        assertEquals(2*2*(1+0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(1)), cross)[0], 1e-15);
        assertEquals(2*2*(1+0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(2)), cross)[0], 1e-15);
        assertEquals(2*2*(1+0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(4)), cross)[0], 1e-15);
        assertEquals(2*2*(1+0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(5)), cross)[0], 1e-15);
        
        metric.setParamFromDetailName("_d3_p0.5_beta0.5");
        assertEquals(2*(0.5*4+1), metric.calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-15);
        assertEquals(2*(0.5*4+1), metric.calcMetric(all.getNode(hab.getPatch(3)), all)[0], 1e-15);
        
        metric.setParamFromDetailName("_d1_p1_beta1");
        checkAllNodes(metric, all, 2*2*5);
        checkAllNodes(metric, cross, 2*2*5);
        
        assertEquals(1, metric.getResultNames(cross).length);
    }
    
    @Test
    public void testFhMetric() {
        FhLocalMetric metric = new FhLocalMetric();
        // test with monohabitat
        testFMetric(metric);
        
        metric.setSelection(MHMetric.ALL);
        AbstractGraph multiHab = project.getGraph("plan_cout1_h0_h4");
        Habitat mh = multiHab.getHabitat();
        metric.setParamFromDetailName("_d3_p0.5_beta1");
        assertArrayEquals(new Double[]{2*(0.5*4), 1*(0.25*4)}, metric.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        assertArrayEquals(new Double[]{2*(0.5+3*0.25), 1*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(1)), multiHab));
        assertArrayEquals(new Double[]{2*(0.5+3*0.25), 1*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(2)), multiHab));
        assertArrayEquals(new Double[]{2*(0.5+3*0.25), 1*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(4)), multiHab));
        assertArrayEquals(new Double[]{2*(0.5+3*0.25), 1*(0.5*2+0.125*2)}, metric.calcMetric(multiHab.getNode(mh.getPatch(5)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125), 1*(0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+1)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125), 1*(0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+2)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125), 1*(0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+3)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125), 1*(0.25*2+0.0625)}, metric.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+4)), multiHab));
        
        metric.setParamFromDetailName("_d3_p0.5_beta0.5");
        assertArrayEquals(new Double[]{Math.sqrt(2)*(0.5*4), 1*(0.25*4)}, metric.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        
        assertEquals(2, metric.getResultNames(multiHab).length);
        
        FhLocalMetric metricInter = new FhLocalMetric();
        metricInter.setParamFromDetailName("_d3_p0.5_beta1");
        metricInter.setSelection(MHMetric.INTER);
        assertArrayEquals(new String[]{MHMetric.INTER}, metricInter.getResultNames(multiHab));
        
        assertArrayEquals(new Double[]{1*(0.25*4)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(1)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(2)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(4)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(5)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+1)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+2)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+3)), multiHab));
        assertArrayEquals(new Double[]{2*(2*0.5+0.25+2*0.125)}, metricInter.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+4)), multiHab));
        
        FhLocalMetric metric1 = new FhLocalMetric();
        metric1.setParamFromDetailName("_d3_p0.5_beta1");
        metric1.setSelection("2");
        assertArrayEquals(new String[]{"2"}, metric1.getResultNames(multiHab));
        
        assertArrayEquals(new Double[]{1*(0.25*4)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(3)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(1)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(2)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(4)), multiHab));
        assertArrayEquals(new Double[]{1*(0.5*2+0.125*2)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(5)), multiHab));
        assertArrayEquals(new Double[]{1*(0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+1)), multiHab));
        assertArrayEquals(new Double[]{1*(0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+2)), multiHab));
        assertArrayEquals(new Double[]{1*(0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+3)), multiHab));
        assertArrayEquals(new Double[]{1*(0.25*2+0.0625)}, metric1.calcMetric(multiHab.getNode(mh.getPatch(ID_RANGE*2+4)), multiHab));
        
    }
    
    @Test
    public void testFMetric() {
        testFMetric(new FLocalMetric());
    }
    
    private void testFMetric(LocalMetric metric) {
        System.out.println(metric.getShortName() + " metric");

        metric.setParamFromDetailName("_d3_p0.5_beta1");
        assertEquals(2*(0.5*4), metric.calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-15);
        assertEquals(2*(0.5*4), metric.calcMetric(all.getNode(hab.getPatch(3)), all)[0], 1e-15);
        assertEquals(2*(0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(1)), cross)[0], 1e-15);
        assertEquals(2*(0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(2)), cross)[0], 1e-15);
        assertEquals(2*(0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(4)), cross)[0], 1e-15);
        assertEquals(2*(0.5+3*0.25), metric.calcMetric(cross.getNode(hab.getPatch(5)), cross)[0], 1e-15);
        
        metric.setParamFromDetailName("_d3_p0.5_beta0.0");
        assertEquals(1*(0.5*4), metric.calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-15);
        assertEquals(1*(0.5*4), metric.calcMetric(all.getNode(hab.getPatch(3)), all)[0], 1e-15);
        
        metric.setParamFromDetailName("_d1_p1_beta1");
        checkAllNodes(metric, all, 2*4);
        checkAllNodes(metric, cross, 2*4);
        
        assertEquals(1, metric.getResultNames(cross).length);
    }
    
    @Test
    public void testBCMetric() {
        testBCMetric(new BCLocalMetric(), false);
    }
    
    @Test
    public void testBCCircMetric() {
        BCCircuitLocalMetric metric = new BCCircuitLocalMetric();
        testBCMetric(metric, true);
        
        AbstractGraph graph = project.getGraph("plan_cout1_h0_h4");
        metric.setParamFromDetailName("_d1_p1_beta0");
        LocalMetricResult resMetric = new LocalMetricResult(metric, graph).calculate(true, null);
        String attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + graph.getName(), attrName);
        for(DefaultFeature patch : hab.getPatches()) {
            if((int)patch.getId() == 3) {
                assertEquals(11, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
            } else {
                assertEquals(8+1.0/6, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
            }
        }
        for(DefaultFeature patch : project.getHabitat("h4").getPatches()) {
            assertEquals(4.4+1.0/60, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
        }
        
        for(Edge e : graph.getEdges()) {
            // if edge on the cross connecting hab0 patches
            if((int)Habitat.getPatch(e.getNodeA()).getId() < ID_RANGE && (int)Habitat.getPatch(e.getNodeB()).getId() < ID_RANGE) {
                assertEquals(7.5, resMetric.getMetric().calcMetric(e, graph)[0], 1e-10);
            } else {
                assertEquals(8.4+1.0/60, resMetric.getMetric().calcMetric(e, graph)[0], 1e-10);
            }
        }

    }
    
    @Test
    public void testBChMetric() {
        BChLocalMetric metric = new BChLocalMetric();
        // test with monohabitat
        testBCMetric(metric, false);
        
        metric.setSelection(MHMetric.ALL);
        AbstractGraph graph = project.getGraph("comp_cout1_h0_h4");
        Habitat mh = graph.getHabitat();
        metric.setParamFromDetailName("_d1_p1_beta1");
        LocalMetricResult resMetric = new LocalMetricResult(metric, graph).calculate(true, null);
        List<String> attrNames = resMetric.getAttrNames();
        assertEquals(Arrays.asList(metric.getDetailName() + "|" + metric.getResultNames(graph)[0] + "_" + graph.getName(),
                    metric.getDetailName() + "|" + metric.getResultNames(graph)[1] + "_" + graph.getName(),
                    metric.getDetailName() + "|" + metric.getResultNames(graph)[2] + "_" + graph.getName()), attrNames);

        assertArrayEquals(new Double[]{2*2*2.0, 2*1*8.0, 1*1*2.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(3)), graph));
        assertEquals(2*2*2.0, (Double)mh.getPatch(3).getAttribute(attrNames.get(0)), 1e-15);
        assertEquals(2*1*8.0, (Double)mh.getPatch(3).getAttribute(attrNames.get(1)), 1e-15);
        assertEquals(1*1*2.0, (Double)mh.getPatch(3).getAttribute(attrNames.get(2)), 1e-15);
        assertArrayEquals(new Double[]{0.0, 0.0, 1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(1)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(2)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(4)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(5)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+1)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+2)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+3)), graph));
        assertArrayEquals(new Double[]{0.0, 0.0, 0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+4)), graph));
        
        Edge e = graph.getNode(mh.getPatch(3)).getEdge(graph.getNode(mh.getPatch(1)));
        assertArrayEquals(new Double[]{2*2*2.0, 2*1*2.0, 0.0}, resMetric.getMetric().calcMetric(e, graph));
        assertEquals(2*2*2.0, (Double)((Path)e.getObject()).getAttribute(attrNames.get(0)), 1e-15);
        assertEquals(2*1*2.0, (Double)((Path)e.getObject()).getAttribute(attrNames.get(1)), 1e-15);
        assertEquals(0.0, (Double)((Path)e.getObject()).getAttribute(attrNames.get(2)), 1e-15);
        e = graph.getNode(mh.getPatch(1)).getEdge(graph.getNode(mh.getPatch(2)));
        assertArrayEquals(new Double[]{2*2*1.0, 0.0, 0.0}, resMetric.getMetric().calcMetric(e, graph));       
        e = graph.getNode(mh.getPatch(1)).getEdge(graph.getNode(mh.getPatch(ID_RANGE*2+1)));
        assertArrayEquals(new Double[]{0.0, 2*1*1.0, 1*1*1.0}, resMetric.getMetric().calcMetric(e, graph));
        e = graph.getNode(mh.getPatch(3)).getEdge(graph.getNode(mh.getPatch(ID_RANGE*2+1)));
        assertArrayEquals(new Double[]{0.0, 2*1*3.0, 1*1*1.0}, resMetric.getMetric().calcMetric(e, graph));
        
        BChLocalMetric metricInter = new BChLocalMetric();
        metricInter.setParamFromDetailName("_d1_p1_beta1");
        metricInter.setSelection(MHMetric.INTER);
        assertArrayEquals(new String[]{MHMetric.INTER}, metricInter.getResultNames(graph));
        resMetric = new LocalMetricResult(metricInter, graph).calculate(true, null);
        assertArrayEquals(new Double[]{2*1*8.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(3)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(1)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(2)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(4)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(5)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+1)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+2)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+3)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+4)), graph));
        
        e = graph.getNode(mh.getPatch(3)).getEdge(graph.getNode(mh.getPatch(1)));
        assertArrayEquals(new Double[]{2*1*2.0}, resMetric.getMetric().calcMetric(e, graph));
        e = graph.getNode(mh.getPatch(1)).getEdge(graph.getNode(mh.getPatch(2)));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(e, graph));
        e = graph.getNode(mh.getPatch(1)).getEdge(graph.getNode(mh.getPatch(ID_RANGE*2+1)));
        assertArrayEquals(new Double[]{2*1*1.0}, resMetric.getMetric().calcMetric(e, graph));
        e = graph.getNode(mh.getPatch(3)).getEdge(graph.getNode(mh.getPatch(ID_RANGE*2+1)));
        assertArrayEquals(new Double[]{2*1*3.0}, resMetric.getMetric().calcMetric(e, graph));
        
        
        BChLocalMetric metric1 = new BChLocalMetric();
        metric1.setParamFromDetailName("_d1_p1_beta1");
        metric1.setSelection("2/2");
        assertArrayEquals(new String[]{"2/2"}, metric1.getResultNames(graph));
        resMetric = new LocalMetricResult(metric1, graph).calculate(true, null);
        assertArrayEquals(new Double[]{1*1*2.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(3)), graph));
        assertArrayEquals(new Double[]{1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(1)), graph));
        assertArrayEquals(new Double[]{1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(2)), graph));
        assertArrayEquals(new Double[]{1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(4)), graph));
        assertArrayEquals(new Double[]{1*1*1.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(5)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+1)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+2)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+3)), graph));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(graph.getNode(mh.getPatch(ID_RANGE*2+4)), graph));
        
        e = graph.getNode(mh.getPatch(3)).getEdge(graph.getNode(mh.getPatch(1)));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(e, graph));        
        e = graph.getNode(mh.getPatch(1)).getEdge(graph.getNode(mh.getPatch(2)));
        assertArrayEquals(new Double[]{0.0}, resMetric.getMetric().calcMetric(e, graph));       
        e = graph.getNode(mh.getPatch(1)).getEdge(graph.getNode(mh.getPatch(ID_RANGE*2+1)));
        assertArrayEquals(new Double[]{1*1*1.0}, resMetric.getMetric().calcMetric(e, graph));        
        e = graph.getNode(mh.getPatch(3)).getEdge(graph.getNode(mh.getPatch(ID_RANGE*2+1)));
        assertArrayEquals(new Double[]{1*1*1.0}, resMetric.getMetric().calcMetric(e, graph));
    }
    
    private void testBCMetric(LocalMetric metric, boolean circuit) {
        System.out.println(metric.getShortName() + " metric");

        metric.setParamFromDetailName("_d1_p1_beta1");
        LocalMetricResult resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        String attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(2*2*6.0, resMetric.getMetric().calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-10);
        assertEquals(2*2*6.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(1).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(2).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(4).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(5).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(2*2*4.0, (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(2*2*4.0, resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        if(!circuit) {
            resMetric = new LocalMetricResult(metric, all).calculate(true, null);
            attrName = resMetric.getAttrNames().get(0);
            assertEquals(metric.getDetailName() + "_" + all.getName(), attrName);
            assertEquals(2*2*2.0, resMetric.getMetric().calcMetric(all.getNode(hab.getPatch(3)), all)[0], 1e-15);
            assertEquals(2*2*2.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-15);
        }
        
        metric.setParamFromDetailName("_d1_p1_beta0.5");
        resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(2*6.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(2*4.0, (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(2*4.0, resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        metric.setParamFromDetailName("_d3_p0.5_beta1");
        resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(2*2*6.0*0.25, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(1).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(2).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(4).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(5).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(2*2*(0.5+3*0.25), (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(2*2*(0.5+3*0.25), resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        if(!circuit) {
            resMetric = new LocalMetricResult(metric, all).calculate(true, null);
            attrName = resMetric.getAttrNames().get(0);
            assertEquals(metric.getDetailName() + "_" + all.getName(), attrName);
            assertEquals(2*2*2.0*0.25, (Double)hab.getPatch(3).getAttribute(attrName), 1e-15);
            assertEquals(2*2*2.0*0.25, resMetric.getMetric().calcMetric(all.getNode(hab.getPatch(3)), all)[0], 1e-15);
        }
    }
     
    @Test
    public void testCFMetric() {
        System.out.println("CF metric");
        CFLocalMetric metric = new CFLocalMetric(); 
        
        metric.setParamFromDetailName("_beta1");
        LocalMetricResult resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        String attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(2*2*6.0, resMetric.getMetric().calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-10);
        assertEquals(2*2*6.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(1).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(2).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(4).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(5).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(2*2*4.0, (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(2*2*4.0, resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        metric.setParamFromDetailName("_beta0.0");
        resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(2*6.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(2*4.0, (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(2*4.0, resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        AbstractGraph graph = project.getGraph("plan_cout1_h0_h4");
        metric.setParamFromDetailName("_beta0.0");
        resMetric = new LocalMetricResult(metric, graph).calculate(true, null);
        attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + graph.getName(), attrName);
        for(DefaultFeature patch : hab.getPatches()) {
            if((int)patch.getId() == 3) {
                assertEquals(16, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
            } else {
                assertEquals(10, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
            }
        }
        for(DefaultFeature patch : project.getHabitat("h4").getPatches()) {
            assertEquals(4, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
        }
        for(Edge e : graph.getEdges()) {
            assertEquals(12, resMetric.getMetric().calcMetric(e, graph)[0], 1e-10);
        }
        
    }
    
    @Test
    public void testPCFMetric() {
        System.out.println("PCF metric");
        PCFLocalMetric metric = new PCFLocalMetric(); 
        
        metric.setParamFromDetailName("_beta1");
        LocalMetricResult resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        String attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(4.0, resMetric.getMetric().calcMetric(cross.getNode(hab.getPatch(3)), cross)[0], 1e-10);
        assertEquals(4.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(1).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(2).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(4).getAttribute(attrName), 1e-10);
        assertEquals(0.0, (Double)hab.getPatch(5).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(10/3.0, (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(10/3.0, resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        metric.setParamFromDetailName("_beta0.0");
        resMetric = new LocalMetricResult(metric, cross).calculate(true, null);
        attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + cross.getName(), attrName);
        assertEquals(2.0, (Double)hab.getPatch(3).getAttribute(attrName), 1e-10);
        for(Edge e : cross.getEdges()) {
            assertEquals(5/3.0, (Double)((Path)e.getObject()).getAttribute(attrName), 1e-10);
            assertEquals(5/3.0, resMetric.getMetric().calcMetric(e, cross)[0], 1e-10);
        }
        
        AbstractGraph graph = project.getGraph("plan_cout1_h0_h4");
        metric.setParamFromDetailName("_beta0.0");
        resMetric = new LocalMetricResult(metric, graph).calculate(true, null);
        attrName = resMetric.getAttrNames().get(0);
        assertEquals(metric.getDetailName() + "_" + graph.getName(), attrName);
        for(DefaultFeature patch : hab.getPatches()) {
            if((int)patch.getId() == 3) {
                assertEquals(7.333693937345052, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
            } else {
                assertEquals(5.39766573295985, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
            }
        }
        for(DefaultFeature patch : project.getHabitat("h4").getPatches()) {
            assertEquals(3.1801152644966026, resMetric.getMetric().calcMetric(graph.getNode(patch), graph)[0], 1e-10);
        }
        
        for(Edge e : graph.getEdges()) {
            // if edge on the cross connecting hab0 patches
            if((int)Habitat.getPatch(e.getNodeA()).getId() < ID_RANGE && (int)Habitat.getPatch(e.getNodeB()).getId() < ID_RANGE) {
                assertEquals(5.571608873434433, resMetric.getMetric().calcMetric(e, graph)[0], 1e-10);
            } else {
                assertEquals(5.809523809523809, resMetric.getMetric().calcMetric(e, graph)[0], 1e-10);
            }
        }
        
    }
    
    
    @Test
    public void testCCMetric() {
        System.out.println("CC metric");
        CCLocalMetric metric = new CCLocalMetric(); 
        
        assertEquals(0.0, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(2.0/3, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        
        assertEquals(0.0, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(2.0/3, metric.calcSingleMetric(all.getNode(hab.getPatch(1)), all), 1e-15);
    }
    
    @Test
    public void testCCeMetric() {
        System.out.println("CCe metric");
        ClosenessLocalMetric metric = new ClosenessLocalMetric(); 
        
        assertEquals(3, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(3, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        
        assertEquals((3+3*6)/4.0, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals((3+6+2*3*Math.sqrt(2))/4.0, metric.calcSingleMetric(all.getNode(hab.getPatch(1)), all), 1e-6);
    }
    
    @Test
    public void testEcMetric() {
        System.out.println("Ec metric");
        EccentricityLocalMetric metric = new EccentricityLocalMetric(); 
        
        assertEquals(3, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(3, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        
        assertEquals(6, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(6, metric.calcSingleMetric(all.getNode(hab.getPatch(1)), all), 1e-15);
    }
    
    @Test
    public void testCCorMetric() {
        System.out.println("CCor metric");
        ConCorrLocalMetric metric = new ConCorrLocalMetric(); 
        
        assertEquals(4, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(4*4/12.0, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        
        assertEquals(1/4.0, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(3.0*3/(4+3+3), metric.calcSingleMetric(all.getNode(hab.getPatch(1)), all), 1e-15);
    }
    
    @Test
    public void testDgMetric() {
        System.out.println("Dg metric");
        DgLocalMetric metric = new DgLocalMetric(); 
        
        assertEquals(4, metric.calcSingleMetric(cross.getNode(hab.getPatch(3)), cross), 1e-15);
        assertEquals(4, metric.calcSingleMetric(all.getNode(hab.getPatch(3)), all), 1e-15);
        
        assertEquals(1, metric.calcSingleMetric(cross.getNode(hab.getPatch(1)), cross), 1e-15);
        assertEquals(3, metric.calcSingleMetric(all.getNode(hab.getPatch(1)), all), 1e-15);
    }
    
    private void checkAllNodes(LocalMetric metric, AbstractGraph graph, double expected) {
        for(Node n : graph.getNodes()) {
            assertEquals(expected, metric.calcMetric(n, graph)[0], 1e-15);
        }
    }

}
