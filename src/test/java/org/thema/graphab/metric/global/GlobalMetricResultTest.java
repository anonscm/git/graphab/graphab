/*
 * Copyright (C) 2015 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.global;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.CapaPatchDialog;
import org.thema.graphab.habitat.MonoHabitat;

/**
 *
 * @author Gilles Vuidel
 */
public class GlobalMetricResultTest {
    
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        // load all metrics
        Project.loadPluginMetric(GlobalMetricResultTest.class.getClassLoader());
        project = ProjectTest.loadCrossProject();
        // set capacities to 2
        CapaPatchDialog.CapaPatchParam params = new CapaPatchDialog.CapaPatchParam();
        params.codeWeight = Collections.singletonMap(0, 2.0);
        ((MonoHabitat)project.getHabitat("h0")).setCapacities(params, false);
    }
    
    /**
     * Test of calcMetric method, of class GlobalMetricLauncher.
     * Test all global metrics
     */
    @Test
    public void testCalcMetric() {
        final double alpha = Math.log(0.5)/3;
        
        HashMap<String, Double> resIndices = new HashMap<String, Double>() {{
            put("EC_d1000_p0.05-one", project.getHabitat("h045").getTotalPatchCapacity());
            put("EC_d100_p1-one", project.getHabitat("h045").getTotalPatchCapacity());
            put("EC_d3_p0.5-cross", 2*Math.sqrt(1+0.5*4 + 4*(1+0.5+3*0.25)));
            put("EC_d3_p0.5-all", 2*Math.sqrt(1+0.5*4 + 4*(1+0.5+0.25+2*Math.exp(alpha*3*Math.sqrt(2)))));
              
//            put("ECh_d1000_p0.05-one", project.getHabitat("h045").getTotalPatchCapacity());
//            put("ECh_d100_p1-one", project.getHabitat("h045").getTotalPatchCapacity());
//            put("ECh_d3_p0.5-cross", 2*Math.sqrt(1+0.5*4 + 4*(1+0.5+3*0.25)));
//            put("ECh_d3_p0.5-all", 2*Math.sqrt(1+0.5*4 + 4*(1+0.5+0.25+2*Math.exp(alpha*3*Math.sqrt(2)))));
              
            put("ECCirc_d1000_p0.05-one", project.getHabitat("h045").getTotalPatchCapacity());
            put("ECCirc_d100_p1-one", project.getHabitat("h045").getTotalPatchCapacity());
            put("ECCirc_d3_p0.5-cross", 2*Math.sqrt(1+0.5*4 + 4*(1+0.5+3*0.25)));
            put("ECCirc_d3_p0.5-all", 2*4.222730272893764);
            
            put("S#IF_d1000_p0.05_beta1-one", Math.pow(project.getHabitat("h045").getTotalPatchCapacity(), 2));
            put("S#IF_d100_p1_beta1-one", Math.pow(project.getHabitat("h045").getTotalPatchCapacity(), 2));
            put("S#IF_d3_p0.5_beta1-cross", 2*2*(1+0.5*4 + 4*(1+0.5+3*0.25)));
            put("S#IF_d3_p0.5_beta1-all", 2*2*(1+0.5*4 + 4*(1+0.5+0.25+2*Math.exp(alpha*3*Math.sqrt(2)))));
                
            put("PC_d1000_p0.05-one", Math.pow(project.getHabitat("h045").getTotalPatchCapacity() / project.getArea(), 2));
            put("PC_d100_p1-one", Math.pow(project.getHabitat("h045").getTotalPatchCapacity() / project.getArea(), 2));
            put("PC_d3_p0.5-cross", 2*2*(1+0.5*4 + 4*(1+0.5+3*0.25)) / Math.pow(project.getArea(), 2));
            put("PC_d3_p0.5-all", 2*2*(1+0.5*4 + 4*(1+0.5+0.25+2*Math.exp(alpha*3*Math.sqrt(2)))) / Math.pow(project.getArea(), 2));
            
            put("S#F_d1000_p0.05_beta1-one", 0.0);
            put("S#F_d100_p1_beta1-one", 0.0);
            put("S#F_d3_p0.5_beta1-cross", 2*(0.5*4 + 4*(0.5+3*0.25)));
            put("S#F_d3_p0.5_beta1-all", 2*(0.5*4 + 4*(0.5+0.25+2*Math.exp(alpha*3*Math.sqrt(2)))));
            
            put("IIC-one", Math.pow(project.getHabitat("h045").getTotalPatchCapacity() / project.getArea(), 2));
            put("IIC-cross", 2*2*(1+0.5*4 + 4*(1+0.5+3*1/3.0)) / Math.pow(project.getArea(), 2));
            
            put("GD-one", 0.0);
            put("GD-cross", 6.0);
            put("GD-all", 6.0);
            
            put("H-one", 0.0);
            put("H-cross", 4*1 + 6*0.5);
            put("H-all", 8*1 + 2*0.5);
            
            put("NC-all", 1.0);
            put("NC-cross", 1.0);
            put("NC-cout1_2_3", 2.0);
            put("MSC-all", 2*5.0);
            put("MSC-cross", 2*5.0);
            put("MSC-cout1_2_3", 2*2.5);
            put("SLC-all", 2*5.0);
            put("SLC-cross", 2*5.0);
            put("SLC-cout1_2_3", 2*4.0);
            put("CCP-all", 1.0);
            put("CCP-cross", 1.0);
            put("CCP-cout1_2_3", 1.0/5*1.0/5 + 4.0/5*4.0/5);
            put("ECS-all", project.getHabitat("h0").getTotalPatchCapacity());
            put("ECS-cross", project.getHabitat("h0").getTotalPatchCapacity());
            put("ECS-cout1_2_3", 2*(1 + 4*4) / 5.0);
            
        }};
        
        HashSet<String> testIndices = new HashSet<>();
        for(String varName : resIndices.keySet()) {
            System.out.println("Test global metric : " + varName);
            String indName = varName.substring(0, varName.indexOf("-"));
            if(indName.contains("_")) {
                indName = indName.substring(0, indName.indexOf("_"));
            }
            GlobalMetric metric = Project.getGlobalMetric(indName);
            metric.setParamFromDetailName(varName.substring(0, varName.indexOf("-")));
            AbstractGraph gen = project.getGraph(varName.substring(varName.indexOf("-")+1));
            Double[] res = new GlobalMetricResult(metric, gen).calculate(true, null).getResult();
            double err = 1e-7;
            assertEquals("Metric " + metric.getDetailName() + "-" + gen.getName(), resIndices.get(varName), 
                    res[0], res[0]*err);
            testIndices.add(indName);
        }
        
        // The Wilks metric and S#IFI are tested outside
        assertEquals("Check all global metrics", project.getGlobalMetricsFor(Project.Method.GLOBAL).size()-2, testIndices.size());
        
    }
    
}
