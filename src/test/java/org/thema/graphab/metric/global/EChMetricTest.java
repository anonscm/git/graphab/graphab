/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.global;

import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graph.Modularity;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.ModularityGraph;
import org.thema.graphab.metric.MHMetric;
import org.thema.graphab.metric.global.SumLocal2GlobalMetric.SumIFInterMetric;
import org.thema.graphab.metric.global.SumLocal2GlobalMetric.SumIFMetric;

/**
 *
 * @author gvuidel
 */
public class EChMetricTest {
    
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        project = ProjectTest.loadCrossProject();
    }

    /**
     * Test of calcMetric method, of class WilksMetric.
     */
    @Test
    public void testCalcMetric() {
        System.out.println("calcMetric ECh");
        AbstractGraph graph = project.getGraph("comp_cout1_h0_h4");
        
        EChMetric ech = new EChMetric();
        ech.setParamFromDetailName("_d100_p1");
        Double[] result = new GlobalMetricResult(ech, graph).calculate(true, null).getResult();
        assertArrayEquals(new Double[]{9.0}, result);
        
        ech.setSelection(MHMetric.ALL);
        result = new GlobalMetricResult(ech, graph).calculate(true, null).getResult();
        assertArrayEquals(new Double[]{5.0, Math.sqrt(40), 4.0}, result);
        
        ech.setSelection(MHMetric.INTER);
        result = new GlobalMetricResult(ech, graph).calculate(true, null).getResult();
        assertArrayEquals(new Double[]{Math.sqrt(40)}, result);
        
        ech.setSelection("0/0");
        result = new GlobalMetricResult(ech, graph).calculate(true, null).getResult();
        assertArrayEquals(new Double[]{5.0}, result);
       
    }
}
