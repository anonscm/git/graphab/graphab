/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.global;

import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graph.Modularity;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.ModularityGraph;
import org.thema.graphab.metric.global.SumLocal2GlobalMetric.SumIFInterMetric;
import org.thema.graphab.metric.global.SumLocal2GlobalMetric.SumIFMetric;

/**
 *
 * @author gvuidel
 */
public class IFIMetricTest {
    
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        project = ProjectTest.loadCrossProject();
    }

    /**
     * Test of calcMetric method, of class WilksMetric.
     */
    @Test
    public void testCalcMetric() {
        System.out.println("calcMetric S#IFI");
        AbstractGraph graph = project.getGraph("comp_cout1_h0_h4");
        Modularity mod = new Modularity(graph.getGraph(), new Modularity.OneWeighter());
        mod.partitions();
        ModularityGraph graphMod = new ModularityGraph(null, graph, mod.getBestPartition());
        
//        IFInterLocalMetric metric = new IFInterLocalMetric();
//        metric.setParamFromDetailName("_d3_p0.5_beta1");
//        assertEquals(2*0.5+3*Math.exp(Math.log(0.5)*Math.sqrt(2)), metric.calcSingleMetric(graphMod.getNode(graph.getHabitat().getPatch(3)), graphMod), 1e-7);
        
        SumIFInterMetric IFI = new SumLocal2GlobalMetric.SumIFInterMetric();
        IFI.setParamFromDetailName("_d3_p0.5_beta1");
        Double[] resultIfi = new GlobalMetricResult(IFI, graphMod).calculate(true, null).getResult();
        
        SumIFMetric IF = new SumLocal2GlobalMetric.SumIFMetric();
        IF.setParamFromDetailName("_d3_p0.5_beta1");
        Double[] resultIf = new GlobalMetricResult(IF,graphMod).calculate(true, null).getResult();
        
        Double[] result = new GlobalMetricResult(IF,graph).calculate(true, null).getResult();
        
        assertEquals(result[0], resultIfi[0]+resultIf[0], 1e-10);
       
    }
}
