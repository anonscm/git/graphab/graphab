/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import org.geotools.feature.SchemaException;
import org.geotools.graph.structure.Node;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graph.pathfinder.Path;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.links.CostLinkset;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author gvuidel
 */
public class GraphPathFinderTest {
    
    private static Project prj, prj034;
    private static AbstractMonoHabitat h0, h4, h034;
    private static DefaultGraph g034;
    
    @BeforeClass
    public static void setUp() throws IOException, SchemaException {
        prj = ProjectTest.loadCrossProject();
        h0 = prj.getHabitat("h0");
        h4 = prj.getHabitat("h4");
        
        prj034 = ProjectTest.createCrossProject(); 
        h034 = new MonoHabitat("h034", prj034, new HashSet<>(Arrays.asList(0,3,4)), false, 0, 0, true);
        prj034.addHabitat(h034, false);
        prj034.addLinkset(new CostLinkset(h034, "linkset", Linkset.Topology.PLANAR, false, null, 
                new double[]{1.0, 2.0, 2.0, 1.0, 1.0, 2.0}, true, false, 0, 0), false);
        g034 = new DefaultGraph("graph", prj034.getLinksets().iterator().next(), true);
        prj034.addGraph(g034, false);
    }

    /**
     * Test of getCost method, of class GraphPathFinder.
     */
    @Test
    public void testGetCost() throws IOException, SchemaException {
        System.out.println("getCost");
        
        AbstractGraph graph = prj.getGraph("all");
        GraphPathFinder pathfinder = new GraphPathFinder(graph.getNode(h0.getPatch(3)), graph);
        assertEquals(5, pathfinder.getComputedNodes().size());
        for(Node n : pathfinder.getComputedNodes()) {
            if(pathfinder.getNodeOrigin() == n) {
                assertEquals(0.0, pathfinder.getCost(n), 1e-10);
            } else {
                assertEquals(3.0, pathfinder.getCost(n), 1e-10);
            }
        }
        
        pathfinder = new GraphPathFinder(graph.getNode(h0.getPatch(3)), Double.NaN, -Math.log(0.5)/3, graph);
        assertEquals(5, pathfinder.getComputedNodes().size());
        for(Node n : pathfinder.getComputedNodes()) {
            if(pathfinder.getNodeOrigin() == n) {
                assertEquals(0.0, pathfinder.getCost(n), 1e-10);
            } else {
                assertEquals(-Math.log(1*1*0.5/(5*5)), pathfinder.getCost(n), 1e-10);
            }
        }
        
        pathfinder = new GraphPathFinder(g034.getNode(h034.getPatch(10)), g034);
        assertEquals(Math.sqrt(2)*3, pathfinder.getCost(g034.getNode(h034.getPatch(5))), 1e-6);
        assertEquals(Math.sqrt(2)*3 +5*2, pathfinder.getCost(g034.getNode(h034.getPatch(1))), 1e-6);
        
        pathfinder = new GraphPathFinder(g034.getNode(h034.getPatch(5)), 5, g034);
        assertNull(pathfinder.getCost(g034.getNode(h0.getPatch(1))));
        assertEquals(7, pathfinder.getComputedNodes().size());
        for(Node n : pathfinder.getComputedNodes()) {
            assertTrue(pathfinder.getCost(n) <= 5);
        }
    }

    /**
     * Test of getPath method, of class GraphPathFinder.
     */
    @Test
    public void testGetPath() {
        System.out.println("getPath");
        
        GraphPathFinder pathfinder = new GraphPathFinder(g034.getNode(h034.getPatch(10)), g034);
        Path path = pathfinder.getPath(g034.getNode(h034.getPatch(1)));
        assertEquals(4, path.getEdges().size());
        assertEquals(5, path.getNodes().size());
        path = pathfinder.getPath(g034.getNode(h034.getPatch(5)));
        assertEquals(Arrays.asList(g034.getEdge("10-7"), g034.getEdge("7-5")), path.getEdges());
        assertEquals(Arrays.asList(g034.getNode(h034.getPatch(10)), g034.getNode(h034.getPatch(7)), g034.getNode(h034.getPatch(5))), path.getNodes());
        
        pathfinder = new GraphPathFinder(g034.getNode(h034.getPatch(5)), 5, g034);
        assertNull(pathfinder.getPath(g034.getNode(h0.getPatch(1))));
        
    }

    /**
     * Test of getComputedNodes method, of class GraphPathFinder.
     */
    @Test
    public void testGetComputedNodes() {
        System.out.println("getComputedNodes");
        AbstractGraph graph = prj.getGraph("cout1_2_3");
        GraphPathFinder pathfinder = new GraphPathFinder(graph.getNode(h0.getPatch(1)), graph);
        assertEquals(1, pathfinder.getComputedNodes().size());
        assertEquals(pathfinder.getNodeOrigin(), pathfinder.getComputedNodes().iterator().next());
        
        pathfinder = new GraphPathFinder(graph.getNode(h0.getPatch(3)), graph);
        assertEquals(4, pathfinder.getComputedNodes().size());
        
    }
    
}
