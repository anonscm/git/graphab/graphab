/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.io.IOException;
import org.geotools.feature.SchemaException;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.Habitat;

/**
 *
 * @author gvuidel
 */
public class DeltaGraphTest {
    
    private static Project prj;
    private static AbstractMonoHabitat h0, h4;
    
    @BeforeClass
    public static void setUp() throws IOException, SchemaException {
        prj = ProjectTest.loadCrossProject();
        h0 = prj.getHabitat("h0");
        h4 = prj.getHabitat("h4");
    }

    /**
     * Test of removeElem method, of class DeltaGraph.
     */
    @Test
    public void testRemoveElem() {
        System.out.println("removeElem");
        
        DeltaGraph g = new DeltaGraph(prj.getGraph("cross"));
        checkRemove(g, g.getNode(h0.getPatch(3)), 4, 0, 4);
        checkRemove(g, g.getEdge("3-1"), 5, 3, 2);
        
        g = new DeltaGraph(prj.getGraph("plan_cout1_h0_h4"));
        checkRemove(g, g.getNode(h0.getPatch(3)), 8, 8, 1);
        checkRemove(g, g.getNode(h4.getPatch(Habitat.ID_RANGE+1)), 8, 10, 1);
        checkRemove(g, g.getEdge("3-1"), 9, 11, 1);
        
    }

    private void checkRemove(DeltaGraph g, Graphable remElem, int nbNode, int nbEdge, int nbComp) {
        assertNull(g.getRemovedElem());
        assertEquals(g.getParentGraph().getNodes().size(), g.getNodes().size());
        assertEquals(g.getParentGraph().getEdges().size(), g.getEdges().size());
        assertEquals(g.getParentGraph().getComponents().size(), g.getComponents().size());
        g.removeElem(remElem);
        assertEquals(nbNode, g.getNodes().size());
        assertEquals(nbEdge, g.getEdges().size());
        assertEquals(nbComp, g.getComponents().size());
        assertEquals(remElem, g.getRemovedElem());
        assertNull(remElem instanceof  Node ? g.getNode(Habitat.getPatch((Node)remElem)) : g.getEdge(((Feature)remElem.getObject()).getId().toString()));
        g.reset();
        assertNull(g.getRemovedElem());
        assertEquals(g.getParentGraph().getNodes().size(), g.getNodes().size());
        assertEquals(g.getParentGraph().getEdges().size(), g.getEdges().size());
        assertEquals(g.getParentGraph().getComponents().size(), g.getComponents().size());
    }
    
}
