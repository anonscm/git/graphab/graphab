/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import org.geotools.feature.SchemaException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author gvuidel
 */
public class DeltaAddGraphTest {
    
    private static Project prj;
    
    @BeforeClass
    public static void setUp() throws IOException, SchemaException {
        prj = ProjectTest.loadCrossProject();
    }

    /**
     * Test of addElem method, of class DeltaAddGraph.
     */
    @Test
    public void testAddElem() {
        System.out.println("addElem");
        
        DeltaAddGraph g = new DeltaAddGraph(prj.getGraph("cross"), Arrays.asList(3), Collections.emptyList());
        assertEquals(4, g.getNodes().size());
        assertEquals(0, g.getEdges().size());
        assertEquals(4, g.getComponents().size());
        g.addElem(3);
        assertEquals(5, g.getNodes().size());
        assertEquals(4, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.reset();
        assertEquals(4, g.getNodes().size());
        assertEquals(0, g.getEdges().size());
        assertEquals(4, g.getComponents().size());
        
        g = new DeltaAddGraph(prj.getGraph("all"), Collections.emptyList(), Arrays.asList("2-1"));
        assertEquals(5, g.getNodes().size());
        assertEquals(7, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.addElem("2-1");
        assertEquals(5, g.getNodes().size());
        assertEquals(8, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.reset();
        assertEquals(5, g.getNodes().size());
        assertEquals(7, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        
        g = new DeltaAddGraph(prj.getGraph("all"), Arrays.asList(1, 2), Collections.emptyList());
        assertEquals(3, g.getNodes().size());
        assertEquals(3, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.addElem(1);
        assertEquals(4, g.getNodes().size());
        assertEquals(5, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.reset();
        assertEquals(3, g.getNodes().size());
        assertEquals(3, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.addElem(2);
        assertEquals(4, g.getNodes().size());
        assertEquals(5, g.getEdges().size());
        assertEquals(1, g.getComponents().size());
        g.reset();
        assertEquals(3, g.getNodes().size());
        assertEquals(3, g.getEdges().size());
        assertEquals(1, g.getComponents().size());

    }

}
