/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.io.IOException;
import java.util.Arrays;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author gvuidel
 */
public class MultiGraphTest {
    
    private static MultiGraph graph;
    private static DefaultGraph graphH0, graphH4, graphMultiHab;
    private static EuclideLinkset linksetH0, linksetH4, linksetMulti;
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        project = ProjectTest.createCrossProjectH0H4();
        linksetH0 = new EuclideLinkset(project.getHabitat("h0"), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        project.addLinkset(linksetH0, false);
        graphH0 = new DefaultGraph("comp_intra_h0", linksetH0, true);
        project.addGraph(graphH0, false);
        linksetH4 = new EuclideLinkset(project.getHabitat("h4"), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        project.addLinkset(linksetH4, false);
        graphH4 = new DefaultGraph("comp_intra_h4", linksetH4, true);
        project.addGraph(graphH4, false);
        
        graph = new MultiGraph("multigraph", Arrays.asList(graphH0, graphH4));
        project.addGraph(graph, false);
    }
    
    /**
     * Test of newInstance method, of class MultiGraph.
     */
    @Test
    public void testNewInstance() {
        System.out.println("newInstance");
        MultiGraph g2 = graph.newInstance("test");
        assertTrue(graph.getType() == g2.getType() && graph.isIntraPatchDist() == g2.isIntraPatchDist());
    }

    /**
     * Test of createGraph method, of class MultiGraph.
     */
    @Test
    public void testCreateGraph() {
        System.out.println("createGraph");
        assertEquals(graph.getHabitat().getPatches().size(), graph.getGraph().getNodes().size());
        assertEquals(graphH0.getNodes().size()+graphH4.getNodes().size(), graph.getGraph().getNodes().size());
        assertEquals(graphH0.getEdges().size()+graphH4.getEdges().size(), graph.getGraph().getEdges().size());
    }

    /**
     * Test of getIntraLinks method, of class MultiGraph.
     */
    @Test
    public void testGetIntraLinks() {
        System.out.println("getIntraLinks");
        assertNotNull(graph.getIntraLinks());
    }

    /**
     * Test of getLinksets method, of class MultiGraph.
     */
    @Test
    public void testGetLinksets() {
        System.out.println("getLinksets");
        assertEquals(2, graph.getLinksets().size());
        assertTrue(graph.getLinksets().contains(linksetH0));
        assertTrue(graph.getLinksets().contains(linksetH4));
    }

    /**
     * Test of getHabitat method, of class MultiGraph.
     */
    @Test
    public void testGetHabitat() {
        System.out.println("getHabitat");
        assertEquals(2, graph.getHabitat().getIdHabitats().size());
        assertTrue(graph.getHabitat().getIdHabitats().contains(project.getHabitat("h0").getIdHab()));
        assertTrue(graph.getHabitat().getIdHabitats().contains(project.getHabitat("h4").getIdHab()));
    }
    
}
