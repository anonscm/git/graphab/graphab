/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.io.IOException;
import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.thema.graphab.Project;
import org.thema.graphab.ProjectTest;
import org.thema.graphab.habitat.MultiHabitat;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author gvuidel
 */
public class DefaultGraphTest {
    
    private static DefaultGraph graph, graphNoIntra, graphPruned2, graphPruned2NoIntra, graphMultiHab;
    private static EuclideLinkset linkset, linksetMulti;
    private static Project project;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SchemaException {
        project = ProjectTest.createCrossProjectH0H4();
        linkset = new EuclideLinkset(project.getHabitat("h0"), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        project.addLinkset(linkset, false);
        graph = new DefaultGraph("comp_intra", linkset, true);
        project.addGraph(graph, false);
        graphNoIntra = new DefaultGraph("comp_nointra", linkset, false);
        project.addGraph(graphNoIntra, false);
        graphPruned2 = new DefaultGraph("pruned_intra", linkset, 2, true);
        project.addGraph(graphPruned2, false);
        graphPruned2NoIntra = new DefaultGraph("pruned_nointra", linkset, 2, false);
        project.addGraph(graphPruned2NoIntra, false);
        
        linksetMulti = new EuclideLinkset(new MultiHabitat(project.getHabitats(), true), "euclide", Linkset.Topology.PLANAR, false, null, true, 0);
        project.addLinkset(linksetMulti, false);
        graphMultiHab = new DefaultGraph("multi_comp_intra", linksetMulti, true);
        project.addGraph(graphMultiHab, false);
    }
    
    /**
     * Test of newInstance method, of class DefaultGraph.
     */
    @Test
    public void testNewInstance() {
        System.out.println("newInstance");
        
        assertTrue(compPramGraph(graph, graph.newInstance("test")));
        assertTrue(compPramGraph(graphNoIntra, graphNoIntra.newInstance("test")));
        assertTrue(compPramGraph(graphPruned2, graphPruned2.newInstance("test")));
        assertTrue(compPramGraph(graphPruned2NoIntra, graphPruned2NoIntra.newInstance("test")));
    }
    
    private boolean compPramGraph(DefaultGraph g1, DefaultGraph g2) {
        return g1.getType() == g2.getType() && g1.isIntraPatchDist() == g2.isIntraPatchDist() &&
                    g1.getLinkset() == g2.getLinkset() && g1.getThreshold() == g2.getThreshold();
    }

    /**
     * Test of createGraph method, of class DefaultGraph.
     */
    @Test
    public void testCreateGraph() {
        System.out.println("createGraph");
        assertEquals(graph.getHabitat().getPatches().size(), graph.getGraph().getNodes().size());
        assertEquals(linkset.getPaths().size(), graph.getGraph().getEdges().size());
        assertEquals(linkset.getPaths().size(), graphNoIntra.getGraph().getEdges().size());
        assertEquals(4, graphPruned2.getGraph().getEdges().size());
        assertEquals(4, graphPruned2NoIntra.getGraph().getEdges().size());
        
        assertEquals(graphMultiHab.getHabitat().getPatches().size(), graphMultiHab.getGraph().getNodes().size());
        assertEquals(12, graphMultiHab.getGraph().getEdges().size());
    }

    /**
     * Test of getIntraLinks method, of class DefaultGraph.
     */
    @Test
    public void testGetIntraLinks() {
        System.out.println("getIntraLinks");
        assertNotNull(graph.getIntraLinks());
        assertNotNull(graphPruned2.getIntraLinks());
        assertNotNull(graphNoIntra.getIntraLinks());
        assertNotNull(graphPruned2NoIntra.getIntraLinks());
    }

    /**
     * Test of getLinkset method, of class DefaultGraph.
     */
    @Test
    public void testGetLinkset() {
        System.out.println("getLinkset");
        assertEquals(linkset, graph.getLinkset());
    }

    /**
     * Test of getLinksets method, of class DefaultGraph.
     */
    @Test
    public void testGetLinksets() {
        System.out.println("getLinksets");
        assertEquals(1, graph.getLinksets().size());
        assertEquals(linkset, graph.getLinksets().iterator().next());
    }

    /**
     * Test of getThreshold method, of class DefaultGraph.
     */
    @Test
    public void testGetThreshold() {
        System.out.println("getThreshold");
        assertEquals(0, graph.getThreshold(), 0);
        assertEquals(2, graphPruned2.getThreshold(), 0);
        assertEquals(0, graphNoIntra.getThreshold(), 0);
        assertEquals(2, graphPruned2NoIntra.getThreshold(), 0);
    }

    /**
     * Test of getInfo method, of class DefaultGraph.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        assertNotNull(graph.getInfo());
        assertNotNull(graphPruned2.getInfo());
        assertNotNull(graphNoIntra.getInfo());
        assertNotNull(graphPruned2NoIntra.getInfo());
    }

    /**
     * Test of getHabitat method, of class DefaultGraph.
     */
    @Test
    public void testGetHabitat() {
        System.out.println("getHabitat");
        assertEquals(linkset.getHabitat(), graph.getHabitat());
    }
    
}
