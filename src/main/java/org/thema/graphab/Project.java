/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab;

import org.thema.graphab.habitat.HabitatLayer;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.links.LinkLayer;
import com.thoughtworks.xstream.XStream;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.geom.util.NoninvertibleTransformationException;
import java.awt.geom.Rectangle2D;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridEnvelope2D;
import org.geotools.feature.SchemaException;
import org.geotools.geometry.Envelope2D;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Node;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultEngineeringCRS;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.thema.common.Config;
import org.thema.common.JTS;
import org.thema.common.JavaLoader;
import org.thema.common.ProgressBar;
import org.thema.common.Util;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.drawshape.image.CoverageShape;
import org.thema.drawshape.image.RasterShape;
import org.thema.drawshape.layer.DefaultGroupLayer;
import org.thema.drawshape.layer.Layer;
import org.thema.drawshape.layer.RasterLayer;
import org.thema.drawshape.style.RasterStyle;
import org.thema.graph.shape.GraphGroupLayer;
import org.thema.graphab.dataset.DatasetLayer;
import org.thema.graphab.dataset.VectorDataset;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.DefaultGraph;
import org.thema.graphab.graph.ModifiedGraph;
import org.thema.graphab.graph.MultiGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.Habitat;
import static org.thema.graphab.habitat.Habitat.getPatchCapacity;
import org.thema.graphab.habitat.MonoVectorHabitat;
import org.thema.graphab.links.CircuitLinkset;
import org.thema.graphab.links.CostLinkset;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Linkset.Topology;
import org.thema.graphab.links.RasterLinkset;
import org.thema.graphab.metric.AbstractLocalMetricResult;
import org.thema.graphab.metric.Metric;
import org.thema.graphab.metric.MetricResult;
import org.thema.graphab.metric.global.AbstractLocal2GlobalMetric.TypeElem;
import org.thema.graphab.metric.global.CCPMetric;
import org.thema.graphab.metric.global.CompMetricResult;
import org.thema.graphab.metric.global.DeltaPCMetric;
import org.thema.graphab.metric.global.ECSMetric;
import org.thema.graphab.metric.global.EChMetric;
import org.thema.graphab.metric.global.GDMetric;
import org.thema.graphab.metric.global.GlobalMetric;
import org.thema.graphab.metric.global.HMetric;
import org.thema.graphab.metric.global.IICMetric;
import org.thema.graphab.metric.global.MSCMetric;
import org.thema.graphab.metric.global.NCMetric;
import org.thema.graphab.metric.global.PCMetric;
import org.thema.graphab.metric.global.SLCMetric;
import org.thema.graphab.metric.global.SumLocal2GlobalMetric;
import org.thema.graphab.metric.global.WilksMetric;
import org.thema.graphab.metric.local.BChLocalMetric;
import org.thema.graphab.metric.local.CCLocalMetric;
import org.thema.graphab.metric.local.CFLocalMetric;
import org.thema.graphab.metric.local.ClosenessLocalMetric;
import org.thema.graphab.metric.local.ConCorrLocalMetric;
import org.thema.graphab.metric.local.DgLocalMetric;
import org.thema.graphab.metric.local.EccentricityLocalMetric;
import org.thema.graphab.metric.local.FLocalMetric;
import org.thema.graphab.metric.local.FhLocalMetric;
import org.thema.graphab.metric.local.IFhLocalMetric;
import org.thema.graphab.metric.local.LocalMetric;
import org.thema.graphab.util.RSTGridReader;

/**
 * Contains all the data of a project.
 * - the landscape raster
 * - the patch raster
 * - the feature patches
 * - the linksets
 * - the graphs
 * - the pointsets
 * - the DEM raster if any
 * - and all project parameters
 * The class is serialized into the xml project file.
 * This class creates new project, loads and saves whole project.
 * 
 * @author Gilles Vuidel
 */
public final class Project {

    public enum Method {
        GLOBAL, COMP, LOCAL, DELTA
    }
    
    // project files
    public static final String LAND_RASTER = "land.tif"; //NOI18N
    
    private transient File dir;

    private String name;
    
    private String version = "3.0"; //NOI18N
    
    private TreeSet<Integer> codes;
    private double noData;

    private double resolution;
    private AffineTransformation grid2space, space2grid;

    private TreeMap<String, AbstractMonoHabitat> habitats;
    
    private TreeMap<String, Linkset> linksets;

    private TreeMap<String, AbstractGraph> graphs;

    private Rectangle2D zone;
    
    private String wktCRS;
    
    private File demFile;

    private transient DefaultGroupLayer rootLayer, habitatLayers, linkLayers, graphLayers, dataLayers, analysisLayers;
    
    private transient Ref<WritableRaster> srcRaster;
    
    private transient HashMap<File, SoftRef<Raster>> extRasters;

    /**
     * Generates a new project and saves it.
     * No linkset is created.
     * @param name the name of the project
     * @param prjPath the directory of the project
     * @param land the landscape map
     * @param codes the value set of the landscape map
     * @param noData the nodata value or NaN
     * @throws IOException
     * @throws SchemaException 
     */
    public Project(String name, File prjPath, GridCoverage2D land, TreeSet<Integer> codes, double noData) throws IOException, SchemaException {

        String ver = MainFrame.class.getPackage().getImplementationVersion();
        if(ver != null) {
            this.version = ver;
        }
        this.name = name;
        this.dir = prjPath;
        this.codes = codes;
        this.noData = noData;

        if(!Double.isNaN(noData)) {
            codes.remove((int)noData);
        }
        
        Envelope2D gZone = land.getEnvelope2D();
        zone = gZone.getBounds2D();
        CoordinateReferenceSystem crs = land.getCoordinateReferenceSystem2D();
        if(crs instanceof DefaultEngineeringCRS) {
            crs = null;
        }
        if(crs != null)   {  
            wktCRS = crs.toWKT();
        }

        dir.mkdirs();
        
        IOImage.saveTiffCoverage(new File(dir, LAND_RASTER), land, "LZW"); //NOI18N
        
        
        GridEnvelope2D range = land.getGridGeometry().getGridRange2D();
        grid2space = new AffineTransformation(zone.getWidth() / range.getWidth(), 0,
                    zone.getMinX() - zone.getWidth() / range.getWidth(),
                0, -zone.getHeight() / range.getHeight(),
                    zone.getMaxY() + zone.getHeight() / range.getHeight());
        try {
            space2grid = grid2space.getInverse();
        } catch (NoninvertibleTransformationException ex) {
            Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
        }
        resolution = grid2space.getMatrixEntries()[0];

        habitats = new TreeMap<>();

        graphs = new TreeMap<>();

        linksets = new TreeMap<>();
        
        save();
    }
    
    
    public void addHabitat(AbstractMonoHabitat habitat, boolean save) throws IOException {
        habitats.put(habitat.getName().toLowerCase(), habitat);

        if(save) {
            save();
        }
        
        if(habitatLayers != null) {
            Layer l = new HabitatLayer(habitat);
            habitatLayers.addLayerFirst(l);
        }
    }
    /**
     * @param habName a habitat name
     * @return the habitat named habName or null if it does not exist
     */
    public AbstractMonoHabitat getHabitat(String habName) {
        if(habName == null) {
            return null;
        } else {
            return habitats.get(habName.toLowerCase());
        }
    }

    /**
     * @return all habitat names
     */
    public Set<String> getHabitatNames() {
        return habitats.keySet();
    }
    
    /**
     * @param habName a habitat name
     * @return true if the habitat name already exists (case insensitive)
     */
    public boolean isHabitatExists(String habName) {
        return habitats.containsKey(habName.toLowerCase());
    }
    
    /**
     * @return all habitats
     */
    public Collection<AbstractMonoHabitat> getHabitats() {
        return habitats.values();
    }
    
    /**
     * Removes the habitat from the project and saves the project.
     * @param habitatName the habitat name to remove
     * @param force remove linksets and graphs using this habitat ?
     * @throws IOException 
     * @throws IllegalStateException if force == false and the habitat is used by linksets
     */
    public void removeHabitat(String habitatName, boolean force) throws IOException {
        Habitat habitat = habitats.remove(habitatName.toLowerCase());
        
        if(habitat == null) {
            return;
        }
        List<String> linkNames = new ArrayList<>();
        for (Linkset l : getLinksets()) {
            if (l.getHabitat().getName().equalsIgnoreCase(habitatName)) {
                linkNames.add(l.getName());
            }
            // linkset based on multihabitat
            if(l.getHabitat().getName().contains("-") && Arrays.asList(l.getHabitat().getName().toLowerCase().split("-")).contains(habitatName.toLowerCase())) { //NOI18N
                linkNames.add(l.getName());
            }
        }
        if(!linkNames.isEmpty() && !force) {
            throw new IllegalStateException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("THE HABITAT IS USED BY THE LINKSETS : {0}"), new Object[] {Arrays.deepToString(linkNames.toArray())}));
        }
        for(String lName : linkNames) {
            removeLinkset(lName, force);
        }
 
        habitat.removeFiles();
        save();
        habitat.getDir().delete();
    }
    
    public void addDataset(VectorDataset dataset, boolean save) throws IOException {
        habitats.put(dataset.getName().toLowerCase(), dataset);

        if(save) {
            save();
        }
        
        if(dataLayers != null) {
            Layer l = new DatasetLayer(dataset);
            dataLayers.addLayerFirst(l);
        }
    }
    /**
     * @return the costs associated with a existing Linkset if any, null otherwise
     */
    public double[] getLastCosts() {
        for(Linkset cost : linksets.values()) {
            if(cost instanceof RasterLinkset) {
                return ((RasterLinkset)cost).getCosts();
            }
        }
        return null;
    }

    /**
     * @param linkName a linkset name
     * @return the linkset named linkName or null if it does not exist
     */
    public Linkset getLinkset(String linkName) {
        if(linkName == null) {
            return null;
        } else {
            return linksets.get(linkName.toLowerCase());
        }
    }

    /**
     * @return all linkset names
     */
    public Set<String> getLinksetNames() {
        return linksets.keySet();
    }
    
    /**
     * @param linkName a linkset name
     * @return true if the linkset name already exists (case insensitive)
     */
    public boolean isLinksetExists(String linkName) {
        return linksets.containsKey(linkName.toLowerCase());
    }
    
    /**
     * @return all linksets
     */
    public Collection<Linkset> getLinksets() {
        return linksets.values();
    }

    /**
     * @return the project's name
     */
    public String getName() {
        return name;
    }

    /**
     * Calculates the new linkset and add it to the project
     * @param linkset the linkset to create
     * @param save save the linkset and the project ?
     * @throws IOException
     * @throws SchemaException 
     */
    public void addLinkset(Linkset linkset, boolean save) throws IOException, SchemaException {
        ProgressBar progressBar = Config.getProgressBar();
        
        linkset.compute(progressBar);
        linksets.put(linkset.getName().toLowerCase(), linkset);
        
        if(save) {
            linkset.saveLinks(true);
            save();
        }
        
        if(linkLayers != null) {
            Layer l = new LinkLayer(linkset);
            l.setVisible(linkset.getTopology() != Topology.COMPLETE || linkset.getDistMax() > 0);
            linkLayers.addLayerFirst(l);
        }
        
        progressBar.close();
    }
    
    
    /**
     * Removes the linkset from the project and saves the project.
     * @param linksetName the linkset name to remove
     * @param force remove graphs using this linkset ?
     * @throws IOException 
     * @throws IllegalStateException if force == false and the linkset is used by graphs
     */
    public void removeLinkset(String linksetName, boolean force) throws IOException {
        
        Linkset linkset = getLinkset(linksetName);
        if(linkset == null) {
            return;
        }
        
        List<String> graphNames = new ArrayList<>();
        for (AbstractGraph g : getGraphs()) {
            for(Linkset l : g.getLinksets()) {
                if(l.getName().equalsIgnoreCase(linksetName)) {
                    graphNames.add(g.getName());
                }
            }
        }
        if(!graphNames.isEmpty() && !force) {
            throw new IllegalStateException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("THE LINKSET IS USED BY THE GRAPHS : {0}"), new Object[] {Arrays.deepToString(graphNames.toArray())}));
        }
        for(String gName : graphNames) {
            removeGraph(gName, force);
        }
        
        linkset.removeFiles();
        linksets.remove(linkset.getName().toLowerCase());
        save();
        
        if(linkLayers != null) {
            for(Layer l : new ArrayList<>(linkLayers.getLayers())) {
                if(l.getName().equals(linkset.getName())) {
                    linkLayers.removeLayer(l);
                }
            }
        }
    }
    

    /**
     * Creates the layers of the project if not already created.
     * @return the root group layer of the project
     */
    public synchronized DefaultGroupLayer getRootLayer() {
        if(rootLayer == null) {
            createLayers();
        }
        return rootLayer;
    }
    
    /**
     * Creates the layers of the project if not already created.
     * @return the group layer of the habitats
     */
    public synchronized DefaultGroupLayer getHabitatLayers() {
        if(habitatLayers == null) {
            createLayers();
        }
        return habitatLayers;
    }
    
    /**
     * Creates the layers of the project if not already created.
     * @return the group layer of the linksets
     */
    public synchronized DefaultGroupLayer getLinksetLayers() {
        if(linkLayers == null) {
            createLayers();
        }
        return linkLayers;
    }
    
    /**
     * Creates the layers of the project if not already created.
     * @return the group layer of the graphs
     */
    public synchronized DefaultGroupLayer getGraphLayers() {
        if(graphLayers == null) {
            createLayers();
        }
        return graphLayers;
    }
    
    /**
     * Creates the layers of the project if not already created.
     * @return the group layer of the datasets
     */
    public synchronized DefaultGroupLayer getDatasetLayers() {
        if(dataLayers == null) {
            createLayers();
        }
        return dataLayers;
    }
    
    /**
     * Creates the layers of the project if not already created.
     * @return the group layer for other analysis
     */
    public synchronized DefaultGroupLayer getAnalysisLayer() {
        if(analysisLayers == null) {
            analysisLayers = new DefaultGroupLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle")
                                                        .getString("MainFrame.analysisMenu.text"), true);
            getRootLayer().addLayerLast(analysisLayers);
        }
        return analysisLayers;
    }

    /**
     * Add a layer to the root project layer
     * @param l the layer to add
     */
    public void addLayer(Layer l) {
        getRootLayer().addLayerLast(l);
    }

    /**
     * Remove a layer from the root project layer
     * @param l the layer to remove
     */
    public void removeLayer(Layer l) {
        getRootLayer().removeLayer(l);
    }

    /**
     * @return the boundary of the map
     */
    public Rectangle2D getZone() {
        return zone;
    }

    public Envelope2D getRasterPatchEnv() {
        return new Envelope2D(getCRS(),
                zone.getX()-resolution, zone.getY()-resolution, zone.getWidth()+2*resolution, zone.getHeight()+2*resolution);
    }
    /**
     * @return the landscape map codes
     */
    public TreeSet<Integer> getCodes() {
        return codes;
    }

    /**
     * Creates and add the graph to the project
     * @param graph the graph definition
     * @param save save the graph and the project ?
     * @throws IOException
     * @throws SchemaException 
     */
    public void addGraph(AbstractGraph graph, boolean save) throws IOException, SchemaException {
        ProgressBar progressBar = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Create_graph..."));
        progressBar.setIndeterminate(true);
        
        graphs.put(graph.getName().toLowerCase(), graph);
        if(save) {
            graph.save();
            save();
        }

        if(rootLayer != null) {
            rootLayer.setLayersVisible(false);
            graphLayers.setExpanded(true);
            GraphGroupLayer gl = graph.getLayers();
            gl.setExpanded(true);
            graphLayers.addLayerFirst(gl);
        }
        
        progressBar.close();
    }

    /**
     * Removes a graph from the project and saves the project.
     * Removes patch and path attributes associated with this graph.
     * Deletes the voronoi shape file of the graph and removes the graph layer.
     * @param name a name of a graph
     */
    public void removeGraph(String name, boolean force) {
        AbstractGraph g = graphs.remove(name.toLowerCase());
        
        if(g == null) {
            return;
        }
        
        if(!force && !g.getMetrics().isEmpty()) {
            throw new IllegalStateException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("THE GRAPH CONTAINS METRICS : {0}"), Arrays.deepToString(g.getMetricsName().toArray())));
        }
        
        List<String> patchAttrs = g.getPatchAttr();
        for(String attr : patchAttrs) {
            DefaultFeature.removeAttribute(attr, g.getHabitat().getPatches());
        }

        List<String> linkAttrs = g.getLinkAttr();
        for(String attr : linkAttrs) {
            DefaultFeature.removeAttribute(attr, g.getLinkset().getPaths());
        }

        try {
            if(!linkAttrs.isEmpty()) {
                g.getLinkset().saveLinks(false);
            }
            if(!patchAttrs.isEmpty()) {
                g.getHabitat().savePatch();
            }
            save();
        } catch (IOException | SchemaException ex) {
            Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
        }

        g.removeFiles();

        if(graphLayers != null) {
            for(Layer l : new ArrayList<>(graphLayers.getLayers())) {
                if(l.getName().equals(name)) {
                    graphLayers.removeLayer(l);
                }
            }
        }
    }

    /**
     * @return all graph in the project
     */
    public Collection<AbstractGraph> getGraphs() {
        return graphs.values();
    }
    
    /**
     * Return the corresponding graph.
     * If the graph name contains "!", creates a new graph removing patches and paths following "!"
     * @param name the name of a graph
     * @return the graph 
     * @throws IllegalArgumentException if the graph does not exist
     */
    public AbstractGraph getGraph(String name) {
        if(graphs.containsKey(name.toLowerCase())) {
            return graphs.get(name.toLowerCase());
        }
        if(name.contains("!")) { //NOI18N
            String[] split = name.split("!"); //NOI18N
            return new ModifiedGraph(graphs.get(split[0].toLowerCase()), split[1], split[2]);
        }
        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN GRAPH {0}"), new Object[] {name}));
    }
    
    /**
     * @param graphName a graph name
     * @return true if the graph name already exists (case insensitive)
     */
    public boolean isGraphExists(String graphName) {
        return graphs.containsKey(graphName.toLowerCase());
    }
    
    /**
     * @return all graph names
     */
    public Set<String> getGraphNames() {
        return graphs.keySet();
    }

    public void addMetric(MetricResult metric, boolean save) throws IOException, SchemaException {
        ProgressBar mon = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Calc_local_metrics...") + metric.getFullName());
        metric.calculate(true, mon);
        metric.getGraph().addMetric(metric);
        
        if(save) {
            mon.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("SAVING..."));
            if(metric instanceof CompMetricResult) {
                metric.getGraph().save();
            } else if(metric instanceof AbstractLocalMetricResult) {
                if(((AbstractLocalMetricResult) metric).calcEdges()) {
                    metric.getGraph().getLinkset().saveLinks(false);
                }
                if(((AbstractLocalMetricResult) metric).calcNodes()) {
                    metric.getGraph().getHabitat().savePatch();
                }
            } 
            save();
        }
        mon.close();
    }
    
    /**
     * @return all metric fullnames
     */
    public Set<String> getMetricNames() {
        return getMetrics().keySet();
    }
    
    /**
     * @return all metrics
     */
    public Map<String, MetricResult> getMetrics() {
        Map<String, MetricResult> metrics = new HashMap<>();
        for(AbstractGraph graph : getGraphs()) {
            for(MetricResult metric : graph.getMetrics()) {
                metrics.put(metric.getFullName().toLowerCase(), metric);
            }
        }
        return metrics;
    }
    
    public MetricResult getMetric(String fullName) {
        return getMetrics().get(fullName.toLowerCase());
    }
    
    public void removeMetric(String fullName) throws IOException, SchemaException {
        MetricResult metric = getMetric(fullName.toLowerCase());
        metric.getGraph().removeMetric(metric);
        if(metric instanceof AbstractLocalMetricResult) {
            AbstractLocalMetricResult lmetric = (AbstractLocalMetricResult) metric;
            for(String attr : (List<String>)lmetric.getAttrNames()) {
                if(lmetric.calcNodes()) {
                    DefaultFeature.removeAttribute((String)attr, metric.getGraph().getHabitat().getPatches());
                }
                if(lmetric.calcEdges()) {
                    DefaultFeature.removeAttribute((String)attr, metric.getGraph().getLinkset().getPaths());
                }
            }
            if(lmetric.calcNodes()) {
                metric.getGraph().getHabitat().savePatch();
            }
            if(lmetric.calcEdges()) {
                metric.getGraph().getLinkset().saveLinks(false);
            }
        }
        
        save();
    }
    
    public GridCoverage2D getLandCover() {
        try {
            return IOImage.loadTiffWithoutCRS(new File(dir, LAND_RASTER));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * If it is not already loaded, load the landscape map.
     * @return the landscape raster map
     */
    public synchronized WritableRaster getRasterLand() {
        WritableRaster raster = srcRaster != null ? srcRaster.get() : null;
        if(raster == null) {
            WritableRaster r = getLandCover().getRenderedImage().copyData(null);
            raster = r.createWritableTranslatedChild(1, 1);
            srcRaster = new SoftRef<>(raster);
        }
        return raster;
    }

    /**
     * Saves the xml project file.
     * @throws IOException 
     */
    public void save() throws IOException {
        version = JavaLoader.getVersion(MainFrame.class);
        try (FileWriter fw = new FileWriter(getProjectFile())) {
            getXStream().toXML(this, fw);
        }
    }
        
    /**
     * @return the xml project file
     */
    public File getProjectFile() {
        return new File(dir, name + ".xml"); //NOI18N
    }

    /**
     * @return the directory of the project
     */
    public File getDirectory() {
        return dir;
    }

    /**
     * @return no data value of landscape map
     */
    public double getNoData() {
        return noData;
    }
    
    public int getNextIdHab() {
        int nextId = 0;
        boolean exist = true;
        while(exist) {
            exist = false;
            for(AbstractMonoHabitat hab : habitats.values()) {
                if(hab.getIdHab() == nextId) {
                    nextId++;
                    exist = true;
                    break;
                }
            }
        }
        
        return nextId;
    }

    /**
     * Creates a new habitat based on the graph by aggregating patches in meta patch.
     * 
     * @param habName the new habitat name
     * @param graph the graph for aggregating patches
     * @param alpha the exponential coefficient for distance decreasing or zero
     * @param minCapa the minimum capacity for metapatch
     * @param voronoi calculate planar voronoi ?
     * @return the new habitat 
     * @throws IOException
     * @throws SchemaException 
     */
    public AbstractMonoHabitat createMetaPatchHabitat(String habName, AbstractGraph graph, double alpha, 
            double minCapa, boolean voronoi) throws IOException, SchemaException {
        ProgressBar monitor = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("META PATCH..."));
        monitor.setIndeterminate(true);

        if(alpha != 0) {
            throw new UnsupportedOperationException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("ALPHA PARAMETER IS NOT SUPPORTED YET"));
        }
        GeometryFactory factory = new GeometryFactory();
        int ind = 1;
        List<Feature> newPatches = new ArrayList<>();
        List<String> attrNames = Collections.singletonList("capa"); //NOI18N
        for(Graph comp : graph.getComponents().values()) {
            double capa = 0;
            for(Node n : (Collection<Node>)comp.getNodes()) {
                capa += getPatchCapacity(n);
            }
            if(capa >= minCapa) {
                List<Geometry> geoms = new ArrayList<>();
                for(Node n : (Collection<Node>)comp.getNodes()) {
                    geoms.add(((Feature)n.getObject()).getGeometry());
                }
                if(geoms.isEmpty()) {
                    geoms = null;
                }
                newPatches.add(new DefaultFeature(ind++, JTS.flattenGeometryCollection(factory.buildGeometry(geoms)), attrNames, Arrays.asList(capa)));
            } 
        }
        return new MonoVectorHabitat(habName, this, newPatches, "capa", voronoi); //NOI18N
    }


    /**
     * Create project layers
     */
    private void createLayers() {
        CoordinateReferenceSystem crs = getCRS();
        rootLayer = new DefaultGroupLayer(name, true);
        
        habitatLayers = new DefaultGroupLayer("Habitats"); //NOI18N
        rootLayer.addLayerFirst(habitatLayers);
        
        for(AbstractMonoHabitat habitat : habitats.values()) {
            if(!(habitat instanceof VectorDataset)) {
                habitatLayers.addLayerFirst(new HabitatLayer(habitat));
            }
        }

        linkLayers = new DefaultGroupLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Link_sets"));
        rootLayer.addLayerFirst(linkLayers);
       
        RasterLayer lr = new RasterLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Landscape_map"), new RasterShape(
            getRasterLand().getParent(), zone, new RasterStyle(), true), crs);
        lr.getStyle().setNoDataValue(noData);
        lr.setVisible(false);
        lr.setDrawLegend(false);
        rootLayer.addLayerLast(lr);
        
        for(Linkset linkset : linksets.values()) {
            Layer l = new LinkLayer(linkset);
            l.setVisible(false);
            linkLayers.addLayerLast(l);
        }

//        if(planarLinks != null) {
//            Layer l = new FeatureLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Voronoi_links"), 
//                    planarLinks.getFeatures(), new LineStyle(new Color(0xbcc3ac)), crs);
//            l.setVisible(false);
//            linkLayers.addLayerLast(l);
//        }
        
        graphLayers = new DefaultGroupLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Graphs"));
        rootLayer.addLayerFirst(graphLayers);      
        for(AbstractGraph g : graphs.values()) {
            try {
                GraphGroupLayer l = g.getLayers();
                l.setLayersVisible(false);
                graphLayers.addLayerLast(l);
            } catch(Exception e) {
                Logger.getLogger(Project.class.getName()).log(Level.WARNING, null, e);
            }
        }
        
        dataLayers = new DefaultGroupLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DATASETS"));
        rootLayer.addLayerFirst(dataLayers);
        
        for(AbstractMonoHabitat data : habitats.values()) {
            if(data instanceof VectorDataset) {
                dataLayers.addLayerFirst(new DatasetLayer((VectorDataset) data));
            }
        }
        
        if(isDemExist()) {
            if(getAbsoluteFile(demFile).exists()) {
                try {
                    GridCoverage2D g = IOImage.loadCoverage(getAbsoluteFile(demFile));
                    RasterLayer l = new RasterLayer(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DEM"), 
                            new CoverageShape(g, new RasterStyle()));
                    l.setVisible(false);
                    l.setDrawLegend(false);
                    rootLayer.addLayerLast(l);
                } catch (IOException ex) {
                    Logger.getLogger(Project.class.getName()).log(Level.WARNING, null, ex);
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DEM FILE CANNOT BE LOADED."), java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DEM"), JOptionPane.WARNING_MESSAGE);
                }
            } else {
                demFile = null;
            }
        }
        
    }
    
    public static XStream getXStream() {
        XStream xstream = Util.getXStream();
        xstream.alias("Project", Project.class); //NOI18N
        xstream.alias("Habitat", MonoHabitat.class); //NOI18N
        xstream.alias("CostLinkset", CostLinkset.class); //NOI18N
        xstream.alias("CircuitLinkset", CircuitLinkset.class); //NOI18N
        xstream.alias("EuclideLinkset", EuclideLinkset.class); //NOI18N
        xstream.alias("Graph", DefaultGraph.class); //NOI18N
        xstream.alias("MultiGraph", MultiGraph.class); //NOI18N
        
        return xstream;
    }
    
    /**
     * Loads a project.
     * @param file the xml project file
     * @param all preloads all linksets ?
     * @return the loaded project 
     * @throws IOException 
     */
    public static synchronized Project loadProject(File file, boolean all) throws IOException {
        
        XStream xstream = getXStream();
        
        Project prj;
        try (FileReader fr = new FileReader(file)) {
            prj = (Project) xstream.fromXML(fr);
        }

        prj.dir = file.getAbsoluteFile().getParentFile();
        
        ProgressBar monitor = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Loading_project..."), 
                100*(2+prj.linksets.size()));
        
        for(AbstractMonoHabitat habitat : prj.habitats.values()) {
            habitat.load(prj);
        }
        
        if(all) {
            for(Linkset linkset : prj.linksets.values()) {
                linkset.loadPaths(monitor.getSubProgress(100));
            }
        }

        monitor.close();
        
        return prj;
    }

    /**
     * Loads a coverage.
     * Supported formats are : tif, asc, and rst.
     * Checks if the grid geometry matches the landscape map.
     * @param file the file coverage
     * @return the grid coverage
     * @throws IOException 
     * @throws IllegalArgumentException if the grid geometry does not match the landscape map
     */
    private GridCoverage2D loadCoverage(File file) throws IOException {
        GridCoverage2D cov;
        if(file.getName().toLowerCase().endsWith(".tif")) { //NOI18N
            cov = IOImage.loadTiffWithoutCRS(file);
        } else if(file.getName().toLowerCase().endsWith(".asc")) { //NOI18N
            cov = IOImage.loadArcGrid(file);
        } else {
            cov = new RSTGridReader(file).read(null);
        }

        GridEnvelope2D grid = cov.getGridGeometry().getGridRange2D();
        Envelope2D env = cov.getEnvelope2D();
        double res = env.getWidth() / grid.getWidth();
        if(res != resolution) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Resolution_does_not_match."));
        }
        if(env.getWidth() != zone.getWidth() || env.getHeight() != zone.getHeight()) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Raster_extent_does_not_match."));
        }
        if(Math.abs(env.getX() - zone.getX()) > res || Math.abs(env.getY() - zone.getY()) > res) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Raster_position_does_not_match."));
        }

        return cov;
    }
    
    /**
     * Loads an external raster.
     * The raster is cached in soft reference to avoid multiple loading.
     * Supported formats are : tif, asc, and rst.
     * Checks if the grid geometry matches the landscape map.
     * @param file the image file
     * @return the raster
     */
    public synchronized Raster getExtRaster(File file) {
        Raster raster = null;
        
        if(extRasters == null) {
            extRasters = new HashMap<>();
        }
        if(extRasters.containsKey(file)) {
            raster = extRasters.get(file).get();
        }
        
        if(raster == null) {
            try {
                raster = loadCoverage(file).getRenderedImage().getData();
                raster = raster.createTranslatedChild(1, 1);
                extRasters.put(file, new SoftRef<>(raster));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        
        return raster;
    }

    /**
     * @return the area of the landscape map
     */
    public double getArea() {
        return zone.getWidth()*zone.getHeight();
    }


    /**
     * @return the DEM raster
     * @throws IOException 
     * @throws IllegalStateException if the project does not contain a DEM file
     */
    public Raster getDemRaster() throws IOException {
        if(!isDemExist()) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO DEM FILE"));
        }
        
        return getExtRaster(getAbsoluteFile(demFile));
    }
    
    /**
     * Sets the DEM file for this project, loads it and save the project.
     * The file format can be tif, asc or rst.
     * @param demFile the DEM file
     * @throws IOException 
     */
    public void setDemFile(File demFile, boolean save) throws IOException {
        String prjPath = getDirectory().getAbsolutePath();
        if(demFile.getAbsolutePath().startsWith(prjPath)) { 
            this.demFile = new File(demFile.getAbsolutePath().substring(prjPath.length()+1));
        } else {
            this.demFile = demFile.getAbsoluteFile();
        }

        if(rootLayer != null) {
            // if the DEM layer already exists, remove it
            String layerName = java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DEM");
            for(Layer l : new ArrayList<>(rootLayer.getLayers())) {
                if(l.getName().equals(layerName)) {
                    rootLayer.removeLayer(l);
                }
            }

            // Add new layer
            try {
                GridCoverage2D g = IOImage.loadCoverage(demFile);
                RasterLayer l = new RasterLayer(layerName, 
                        new CoverageShape(g, new RasterStyle()));
                l.setVisible(false);
                rootLayer.addLayerLast(l);
            } catch (IOException ex) {
                Logger.getLogger(Project.class.getName()).log(Level.WARNING, null, ex);
                JOptionPane.showMessageDialog(null, 
                        java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DEM FILE CANNOT BE LOADED."), 
                        java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DEM"), JOptionPane.WARNING_MESSAGE);
            }
        }
        if(save) {
            save();
        }
    }
    
    /**
     * @return is the project contain a DEM
     */
    public boolean isDemExist() {
        return demFile != null;
    }
    
    /**
     * If the path of the f is relative, sets the project directory as parent directory and return a new absolute file
     * @param f 
     * @return the absolute path for a file
     */
    public File getAbsoluteFile(File f) {
        if(f.isAbsolute()) {
            return f;
        } else {
            return new File(getDirectory(), f.getPath());
        }
    }
    
    /**
     * @return the resolution of the landscape map
     */
    public double getResolution() {
        return resolution;
    }

    /**
     * @return the transformation between the grid coordinate and the world coordinate
     */
    public AffineTransformation getGrid2space() {
        return grid2space;
    }

    /**
     * @return the transformation between the world coordinate and the grid coordinate
     */
    public AffineTransformation getSpace2grid() {
        return space2grid;
    }

    /**
     * @return the projection of the landscape if exists, null otherwise.
     */
    public CoordinateReferenceSystem getCRS() {
        if(wktCRS != null && !wktCRS.isEmpty()) {
            try {
                return CRS.parseWKT(wktCRS);
            } catch (FactoryException ex) {
                Logger.getLogger(Project.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        return null;
    }
    
    
    /**
     * Check if point (x,y) in world coordinate lies in the landscape map, and does not fall in nodata pixel.
     * @param x in world coordinate
     * @param y in world coordinate
     * @return true if this coordinate is in the study area
     * @throws IOException if the land use cannot be loaded
     */
    public boolean isInZone(double x, double y) throws IOException {
        if(!zone.contains(x, y)) {
            return false;
        }
        Coordinate cg = space2grid.transform(new Coordinate(x, y), new Coordinate());
        return getRasterLand().getSample((int)cg.x, (int)cg.y, 0) != noData;
    }
    
    /**
     * @param method
     * @return the global metrics which can be used for a given method
     */
    public static List<GlobalMetric> getGlobalMetricsFor(Method method) {
        List<GlobalMetric> indices = new ArrayList<>();
        for(GlobalMetric ind : GLOBAL_METRICS) {
            if(ind.isAcceptMethod(method)) {
                indices.add(ind);
            }
        }
        return indices;
    }
    
    /**
     * @return the local metrics
     */
    public static List<LocalMetric> getLocalMetrics() {
        List<LocalMetric> indices = new ArrayList<>();
        for(LocalMetric ind : LOCAL_METRICS) {
            indices.add(ind);
        }
        return indices;
    }
    
    /**
     * @param method
     * @return the metric usable for a given method
     */
    public static List<? extends Metric> getMetricsFor(Method method) {
        if(method == Method.LOCAL) {
            return getLocalMetrics();
        } else {
            return getGlobalMetricsFor(method);
        }
    }
    /**
     * 
     * @param metrics a list of metrics
     * @param shortName the shortname metric to search
     * @return true if the list contains a metric with this short name
     */
    private static boolean containsMetric(List<? extends Metric> metrics, String shortName) {
        for(Metric ind : metrics) {
            if(ind.getShortName().equals(shortName)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param shortName the short name of the global metric
     * @return the global metric given its short name
     * @throws IllegalArgumentException if the metric is not found
     */
    public static GlobalMetric getGlobalMetric(String shortName) {
        for(GlobalMetric ind : GLOBAL_METRICS) {
            if(ind.getShortName().equals(shortName)) {
                return ind;
            }
        }
        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN METRIC {0}"), shortName));
    }
    
    /**
     * @param shortName the short name of the local metric
     * @return the local metric given its short name
     * @throws IllegalArgumentException if the metric is not found
     */
    public static LocalMetric getLocalMetric(String shortName) {
        for(LocalMetric ind : LOCAL_METRICS) {
            if(ind.getShortName().equals(shortName)) {
                return ind;
            }
        }
        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN METRIC {0}"), shortName));
    }
    
    /** Global metrics available */
    public static List<GlobalMetric> GLOBAL_METRICS;
    /** Local metrics available */
    public static List<LocalMetric> LOCAL_METRICS;
    
    static {
        GLOBAL_METRICS = new ArrayList(Arrays.asList( 
                //new ECMetric(), 
                new EChMetric(), new PCMetric(), new IICMetric(), 
                new SumLocal2GlobalMetric(new FLocalMetric(), TypeElem.NODE), new CCPMetric(),
                new MSCMetric(), new SLCMetric(), new ECSMetric(), new GDMetric(), new HMetric(), new NCMetric(),
                new DeltaPCMetric(), new WilksMetric()));
        LOCAL_METRICS = new ArrayList(Arrays.asList((LocalMetric)/*new FLocalMetric(), 
                new BCLocalMetric(), new IFLocalMetric(),*/ new IFhLocalMetric(), new FhLocalMetric(), 
                new BChLocalMetric(), //new DrLocalMetric(), new RrLocalMetric(),
                new CFLocalMetric(), new DgLocalMetric(), new CCLocalMetric(), new ClosenessLocalMetric(), 
                new ConCorrLocalMetric(), new EccentricityLocalMetric()));
    }
    
    /**
     * Loads additional metrics available in the subdirectory named plugins of the directory where is the jar file.
     * 
     * @throws Exception 
     */
    public static void loadPluginMetric() throws Exception {
        URL url = Project.class.getProtectionDomain().getCodeSource().getLocation();
        File dir = new File(url.toURI()).getParentFile();
        File loc = new File(dir, "plugins"); //NOI18N

        if(!loc.exists()) {
            return;
        }
        
        File[] flist = loc.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {return file.getPath().toLowerCase().endsWith(".jar");} //NOI18N
        });
        if(flist == null || flist.length == 0) {
            return;
        }
        URL[] urls = new URL[flist.length];
        for (int i = 0; i < flist.length; i++) {
            urls[i] = flist[i].toURI().toURL();
        }
        URLClassLoader ucl = new URLClassLoader(urls);

        loadPluginMetric(ucl);
    }
    
    /**
     * Loads other metrics given a specific class loader
     * @param loader
     */
    public static void loadPluginMetric(ClassLoader loader)  {
        ServiceLoader<Metric> sl = ServiceLoader.load(Metric.class, loader);
        Iterator<Metric> it = sl.iterator();
        while (it.hasNext()) {
            Metric metric = it.next();
            if(metric instanceof GlobalMetric) {
                if(!containsMetric(GLOBAL_METRICS, metric.getShortName())) {
                    GLOBAL_METRICS.add((GlobalMetric)metric);
                }
            } else if(metric instanceof LocalMetric) {
                if(!containsMetric(LOCAL_METRICS, metric.getShortName())) {
                    LOCAL_METRICS.add((LocalMetric)metric);
                }
            } else {
                throw new RuntimeException("Class " +metric.getClass().getCanonicalName() + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString(" DOES NOT INHERIT FROM GLOBALMETRIC OR LOCALMETRIC"));
            }
        }
    }

    /**
     * A reference to an object of type T
     * @param <T> 
     */
    public interface Ref<T> {
        /**
         * @return the object or null if the object has been cleared
         */
        public T get();
    }
    
    /**
     * Soft reference to an object of type T.
     * Encapsulates the java class {@link SoftReference} to implement interface {@link Ref}
     * @param <T> 
     */
    public static class SoftRef<T> implements Ref<T>{

        private final SoftReference<T> ref;

        /**
         * Creates a SoftReference to the object val
         * @param val an object
         */
        public SoftRef(T val) {
            ref = new SoftReference<>(val);
        }
        
        @Override
        public T get() {
            return ref.get();   
        }
    }
    
    /**
     * Strong reference to an object of type T.
     * Keeps a reference to an object to ensure that it will not claimed by the garbage collector
     * @param <T> 
     */
    public static class StrongRef<T> implements Ref<T>{

        private final T ref;

        /**
         * Keeps the object val
         * @param val 
         */
        public StrongRef(T val) {
            ref = val;
        }
        
        @Override
        public T get() {
            return ref;   
        }
    }
}
