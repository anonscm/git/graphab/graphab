/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.links;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import java.awt.Point;
import java.awt.image.BandedSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.ParallelFTask;
import org.thema.common.parallel.SimpleParallelTask;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MultiHabitat;
import org.thema.parallel.AbstractParallelTask;
import org.thema.parallel.ExecutorService;

/**
 * Represents a set of links (ie. paths) between the patches of the project.
 * The topology can be COMPLETE or PLANAR.
 * The distance can be EUCLID, COST or CIRCUIT
 * 
 * @author Gilles Vuidel
 */
public abstract class Linkset implements Comparable<Linkset>{

    /** Linkset type (ie. topology) complete, planar (euclidean), planar (cost), multi */
    public enum Topology { COMPLETE, PLANAR, PLANAR_COST, MULTI }

    /** Linkset distance type */
    public enum Distance { EUCLID, COST, CIRCUIT }

    /** Linkset name */
    private final String name;
    /** Habitat for the linkset may be MultiHabitat */
    private final Habitat habitat;
    /** Linkset topology */
    private final Topology topology;
    /** Distance type Euclidean, Cost or Circuit */
    private final Distance typeDist;
    /** Is interhabitat linkset ? */
    private final boolean inter;
    /** Node filtering or null */
    protected final String nodeFilter;
    /** Max distance of a link, for speeding up calculation */
    private final double distMax;
    /** Is real path (geometry) are saved ? */
    private final boolean realPaths;
    
    protected transient List<Path> paths;
    private transient IntraLinks intraLinks;
      
    
    /**
     * Creates a linkset
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset topology COMPLETE or PLANAR
     * @param dist linkset distance type 
     * @param inter is interhabitat linkset
     * @param nodeFilter node filtering or null
     * @param realPaths are real paths stored
     * @param distMax max cost distance or 0 for no max
     */
    public Linkset(Habitat habitat, String name, Topology topo, Distance dist, boolean inter, String nodeFilter, boolean realPaths, double distMax) {
        if(topo == Topology.PLANAR_COST && dist == Distance.EUCLID) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("PLANAR COST TOPOLOGY CANNOT BE USED WITH EUCLIDEAN DISTANCE"));
        }
        this.habitat = habitat;
        this.name = name;
        this.topology = topo;
        this.typeDist = dist;
        this.distMax = distMax;
        this.realPaths = realPaths;
        this.inter = inter;
        this.nodeFilter = nodeFilter;
    }


    public Habitat getHabitat() {
        return habitat;
    }

    /**
     * @return the project attached to this linkset
     */
    public Project getProject() {
        return habitat.getProject();
    }

    /**
     * @return the max distance for links or zero
     */
    public double getDistMax() {
        return distMax;
    }

    /**
     * @return the name of the linkset
     */
    public String getName() {
        return name;
    }

    /**
     * @return real paths are stored ?
     */
    public boolean isRealPaths() {
        return realPaths;
    }

    /**
     * @return the topology : COMPLETE or PLANAR
     */
    public Topology getTopology() {
        return topology;
    }

    /**
     * @return the distance type : EUCLID, COST or CIRCUIT
     */
    public Distance getTypeDist() {
        return typeDist;
    }

    /**
     * @return true if linkset is interhabitat
     */
    public boolean isInter() {
        return inter;
    }

    public String getNodeFilter() {
        return nodeFilter;
    }

    /**
     * @return true if isCostLength() and is not euclidean distance
     */
    public boolean isCostUnit() {
        return typeDist != Distance.EUCLID;
    }
    

    /**
     * load if it's not already the case and return all paths
     * @return the paths of the linkset
     */
    public synchronized List<Path> getPaths() {
        if(paths == null)  {
            try {
                loadPaths(new TaskMonitor.EmptyMonitor());
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return paths;
    }
    
    public IntraLinks getIntraLinks() {
        if(!isRealPaths()) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("INTRA LINKS NEEDS REAL PATHS"));
        }
        if(intraLinks == null) {
            intraLinks = new IntraLinks(this);
        }
        return intraLinks;
    }
    
    /**
     * Estimates the cost from a distance.Do a linear regression in double log  between cost and length of all links.
     * @param distance metric distance
     * @param minCost minimal path cost for including in the regression
     * @param costMax paths containing costs >= costMax are excluded from the regression if costMax > 0
     * @return the cost corresponding to the distance
     */
    public double estimCost(double distance, double minCost, double costMax) {
        return distance;
    }

    /**
     * Returns detailed informations of the linkset.
     * 
     * The language is local dependent
     * @return 
     */
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle");
        
        String info = bundle.getString("LinksetPanel.nameLabel.text") + " : " + name + "\n";
        info += "Habitat : " + (getHabitat().getIdHabitats().size() == 1 ? getHabitat().getName() 
                : ((MultiHabitat)getHabitat()).getHabitats().stream().map(Habitat::getName).collect(Collectors.joining(", "))) + "\n";
        info += bundle.getString("LinksetPanel.topoPanel.border.title") + " : ";
        if(topology == Topology.COMPLETE) {
            info += bundle.getString("LinksetPanel.completeRadioButton.text");
            if(distMax > 0) {
                info += " " + bundle.getString("LinksetPanel.distMaxLabel.text") + " " + distMax;
            }
        }
        else {
            info += bundle.getString("LinksetPanel.planarRadioButton.text");
        }
        info += "\n" + bundle.getString("LinksetPanel.distPanel.border.title") + " : ";
        
        info += "?DISTANCE?";
        
        if(realPaths) {
            info += "\n" + bundle.getString("LinksetPanel.realPathCheckBox.text");
        }

        info += "\n\n" + java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("# LINKS : {0}"), 
                getPaths().size());

        return info;
    }
    
    /**
     * @return the name of the linkset
     */
    @Override
    public String toString() {
        return name;
    }
    
    /**
     * Predicate used at linkset calculation to define if a path between two patches (origId, destId) 
     * must be created
     * @param filterPatchIds set of id patch or null if no patch filtering
     * @param origId must be in filterPatchIds (if not null)
     * @param destId any patch id
     * @return true if the path between patch oridId and destId must be created
     */
    protected boolean filter(Set<Integer> filterPatchIds, int origId, int destId) {
        return ((filterPatchIds != null && !filterPatchIds.contains(destId)) || origId < destId) 
                && (!isInter() || Habitat.getPatchIdHab(origId) != Habitat.getPatchIdHab(destId));
    }
    
    /**
     * Creates a pathfinder for the given linkset
     * @return a new pathfinder
     * @throws IllegalArgumentException if the linkset is a circuit linkset
     */
    public abstract SpacePathFinder getPathFinder();
    
    /**
     * Compute all links defined in this linkset.
     * 
     * This method is called only once by the project
     * @param progressBar the progression bar, cannot be null
     * @throws IOException 
     */
    public void compute(ProgressBar progressBar) throws IOException {
        progressBar.setNote(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("CREATE LINKSET {0}"), getName()));
       
        calcLinkset(progressBar);
        
        progressBar.reset();
        
        if(isRealPaths()) {
            progressBar.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("CREATE INTRA LINKS..."));
            intraLinks = new IntraLinks(this);
            intraLinks.calcIntraLinks(progressBar);
        }
    }
    
    protected abstract void calcLinkset(ProgressBar progressBar) throws IOException;
    
    protected PlanarLinks getPlanarLinks() {
        if(getTopology() == Topology.PLANAR) {
            return getHabitat().getPlanarLinks();
        } else {
            return null;
        }
    }
    
    /**
     * Compute and return vector corridors of all paths existing in this linkset
     * @param progressBar
     * @param graph
     * @param maxCost maximal cost distance 
     * @return list of features where id is equal to id path and geometry is a polygon or multipolygon
     */
    public List<Feature> computeCorridor(ProgressBar progressBar, final AbstractGraph graph, final double maxCost) {
        
        final List<Feature> corridors = Collections.synchronizedList(new ArrayList<>(getPaths().size()));
        
        ParallelFTask task = new SimpleParallelTask<Path>(graph != null ? graph.getLinks() : getPaths(), progressBar) {
            @Override
            protected void executeOne(Path path) {
                try {
                    Geometry corridor = calcCorridor(path, maxCost);
                   
                    if(!corridor.isEmpty()) {
                        corridors.add(new DefaultFeature(path.getId(), corridor));
                    }
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
        
        new ParallelFExecutor(task).executeAndWait();
        
        return corridors;
    }
    
    
    protected abstract Geometry calcCorridor(Path path, double maxCost) throws IOException;
    
    
    /**
     * Compute and return corridors of all paths existing in this linkset summarize in a Raster.If alpha == 0, number of corridors
        If alpha >= 0 and metric == null, maximum probability gradient (a1*a2)^beta * e^-alpha*cost
        If alpha >= 0 and metric != null, maximum probability gradient metric * e^-alpha*(cost-pathcost)
     * @param progressBar
     * @param graph
     * @param maxCost maximal cost distance 
     * @param alpha cost exponent, if 0 resut is just the number of corridors
     * @param beta capacity exponent, alpha must be different of 0
     * @param metric metric's name, alpha must be different of 0
     * @return raster of number of corridors (if alpha = 0) or inverse cost gradient with alpha greater than 0 multiply by capacities (beta) or a metric or one if beta=0
     */
    public Raster computeRasterCorridor(ProgressBar progressBar, final AbstractGraph graph, final double maxCost, final double alpha, final double beta, final String metric) {
        
        final WritableRaster corridors = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_DOUBLE, 
                    getProject().getRasterLand().getWidth(), getProject().getRasterLand().getHeight(), 1), new Point(1, 1));

        final List<Path> paths = graph != null ? graph.getLinks() : getPaths();
        
        AbstractParallelTask<Raster, Raster> task = 
                new AbstractParallelTask<Raster, Raster>(progressBar) {
            @Override
            public Raster execute(int start, int end) {
                WritableRaster raster = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_FLOAT, 
                    getProject().getRasterLand().getWidth(), getProject().getRasterLand().getHeight(), 1), new Point(1, 1));
                for(Path path : paths.subList(start, end)) {
                    try {
                        Raster corridor = calcRasterCorridor(path, maxCost, true, alpha == 0 ? 1.0 : null);
                        double val = metric != null ? ((Number)path.getAttribute(metric)).doubleValue() :
                                    (Math.pow(Habitat.getPatchCapacity(path.getPatch1()) * Habitat.getPatchCapacity(path.getPatch2()), beta)
                                        * Math.exp(-alpha*path.getCost()));
                        if(corridor != null && !Double.isNaN(val)) {
                            for(int y = corridor.getMinY(); y < corridor.getBounds().getMaxY(); y++) {
                                for (int x = corridor.getMinX(); x < corridor.getBounds().getMaxX(); x++) {
                                    double cost = corridor.getSampleDouble(x, y, 0);
                                    if(cost > 0) {
                                        double v0 = raster.getSampleDouble(x, y, 0);
                                        raster.setSample(x, y, 0, alpha == 0 ? v0+cost :
                                                Math.max(v0, val * Math.exp(-alpha*(cost-path.getCost()))));
                                    }
                                }
                            }
                        }

                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                    incProgress(1);
                }
                return raster;
            }

            @Override
            public void gather(Raster raster) {
                double[] result = ((DataBufferDouble)corridors.getDataBuffer()).getData();
                float[] buf = ((DataBufferFloat)raster.getDataBuffer()).getData();
                if(alpha == 0) {
                    for(int i = 0; i < buf.length; i++) {
                        result[i] += buf[i];
                    }
                } else {
                    for(int i = 0; i < buf.length; i++) {
                        result[i] = Math.max(result[i], buf[i]);
                    }
                }
            }

            @Override
            public int getSplitRange() {
                return paths.size();
            }

            @Override
            public Raster getResult() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates. //NOI18N
            }
        };
        
        ExecutorService.execute(task);
        return corridors;
    }
    
   
    protected abstract Raster calcRasterCorridor(Path path, double maxCost, final boolean inPatch, final Double val) throws IOException;

     /**
     * Return a new virtual linkset for cost calculation for the given habitat.
     * @param habitat
     * @return a new linkset or this
     * @throws IllegalArgumentException if the linkset is euclidean
     */
    public abstract CostLinkset getCostVersion(Habitat habitat);
    
     /**
     * Return a new virtual linkset for circuit calculation for the given habitat.
     * @param habitat
     * @return a new linkset or this
     * @throws IllegalArgumentException if the linkset is euclidean
     */
    public abstract CircuitLinkset getCircuitVersion(Habitat habitat);

    public boolean hasVoronoi() {
        return getHabitat().hasVoronoi();
    }
    
    /**
     * The voronoi polygon associated with the patch id
     * @param id the patch identifier
     * @return a polygon feature representing the voronoi polygon of the patch id
     */
    public Feature getVoronoi(int id) {
        return getHabitat().getVoronoi(id);
    }
    
    /**
     * Loads the links from the geopackage
     * @param mon
     * @throws IOException 
     */
    public void loadPaths(ProgressBar mon) throws IOException {

        List<DefaultFeature> features = getHabitat().getPatches().size() > 1 ? IOFeature.loadFeatures(getLinkFile(), "Id") : Collections.EMPTY_LIST; //NOI18N

        paths = new ArrayList<>();
        for(Feature f : features) {
            paths.add(Path.loadPath(f, habitat));
        }

    }
    
    /**
     * Saves the links into a gpkg file in the project directory
     * @param saveAll save also intraLinks ?
     * @throws IOException
     */
    public void saveLinks(boolean saveAll) throws IOException {
        if(paths != null && !paths.isEmpty()) {  // if data already loaded
            IOFeature.saveFeatures(paths, getLinkFile(), getProject().getCRS());
            if(saveAll && intraLinks != null) {
                intraLinks.saveIntraLinks();
            }
        }
    }
    
    protected File getLinkFile() {
        File dir = getHabitat().getDir();
        dir.mkdir();
        return new File(dir, name + "-links.gpkg"); //NOI18N
    }
    
    public void removeFiles() {
        getLinkFile().delete();
        if(isRealPaths()) {
            getIntraLinks().getIntraLinkFile().delete();
        }
    }
    
    /**
     * Calculates and add links for the new patch
     * The patch have to be added in the project before (Project.addPatch)
     * @param patch must be a point geometry
     */
    public void addLinks(DefaultFeature patch) {
        Map<DefaultFeature, Path> links = calcNewLinks(patch);
        for(DefaultFeature d : links.keySet()) {
            if(isRealPaths()) {
                paths.add(new Path(patch, d, links.get(d).getCost(), (LineString)links.get(d).getGeometry(), paths.get(0).getAttributeNames()));      
            } else {
                paths.add(new Path(patch, d, links.get(d).getCost(), links.get(d).getDist(), paths.get(0).getAttributeNames()));      
            }
        }
    }
    
    /**
     * Calculates links for the new patch.
     * @param patch patch created by Project.createPatch()
     * @return the new links
     */
    public Map<DefaultFeature, Path> calcNewLinks(DefaultFeature patch) {
        SpacePathFinder pathfinder = getPathFinder();
        HashMap<DefaultFeature, Path> newPaths = pathfinder.calcPaths(patch.getGeometry(), distMax, realPaths);
        newPaths.remove(patch); 
        if(isInter()) {
            for(Iterator<DefaultFeature> it = newPaths.keySet().iterator(); it.hasNext(); ) {
                if(Habitat.getPatchIdHab(patch) == Habitat.getPatchIdHab(it.next())) {
                    it.remove();
                }
            }
        }
        return newPaths;
    }
    
    /**
     * Remove last links created for the new patch.
     * The links must have been created by addLinks
     * @param patch 
     */
    public void removeLinks(DefaultFeature patch) {
        while(paths.get(paths.size()-1).getPatch1().equals(patch)) {
            paths.remove(paths.size()-1);
        }
    }

    @Override
    public int compareTo(Linkset l) {
        return getName().compareTo(l.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linkset other = (Linkset) obj;
        return Objects.equals(this.name, other.name);
    }
    
}
