/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.links;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.index.strtree.STRtree;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.thema.common.ProgressBar;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.habitat.Habitat;
import org.thema.parallel.AbstractParallelTask;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.ParallelTask;

/**
 * Represents a set of links (ie. paths) between the patches of the project with euclidean links
 * The topology can be COMPLETE or PLANAR.
 * 
 * @author Gilles Vuidel
 */
public class EuclideLinkset extends Linkset {

    private File heightRasterFile;
    
    /**
     * Creates a linkset with euclidean distance.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param inter is interhabitat ?
     * @param nodeFilter 
     * @param realPaths are real paths stored
     * @param distMax max cost distance or 0 for no max
     */
    public EuclideLinkset(Habitat habitat, String name, Topology topo, boolean inter, String nodeFilter, boolean realPaths, double distMax) {
        this(habitat, name, topo, inter, nodeFilter, realPaths, distMax, null);
    }
    
    /**
     * Creates a linkset with 3D euclidean distance.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param inter is interhabitat
     * @param realPaths are real paths stored
     * @param distMax max cost distance or 0 for no max
     * @param heightRasterFile raster with object height
     */
    public EuclideLinkset(Habitat habitat, String name, Topology topo, boolean inter, String nodeFilter, boolean realPaths, double distMax, File heightRasterFile) {
        super(habitat, name, topo, Distance.EUCLID, inter, nodeFilter, realPaths, distMax);
        this.heightRasterFile = heightRasterFile;
    }

    @Override
    protected void calcLinkset(ProgressBar progressBar) throws IOException {
        final boolean planar = getTopology() == Topology.PLANAR;
        final EuclidePathFinder pathFinder = getPathFinder();
        final List<DefaultFeature> filterPatches = getHabitat().getPatches(nodeFilter);
        final Set<Integer> filterPatchIds = filterPatches.size() < getHabitat().getPatches().size() ? 
                filterPatches.stream().map(f -> (Integer)f.getId()).collect(Collectors.toSet()) : null;
        Path.newSetOfPaths();
        
        final STRtree index = getHabitat().getPatchIndex();
        
        long start = System.currentTimeMillis();
        ParallelTask<List<Path>, List<Path>> task = new AbstractParallelTask<List<Path>, List<Path>>(progressBar) {
            private List<Path> result = new ArrayList<>();
            @Override
            public List<Path> execute(int start, int end) {  
                List<Path> links = new ArrayList<>();
                for(DefaultFeature orig : filterPatches.subList(start, end)) {
                    if(isCanceled()) {
                        throw new CancellationException();
                    }
                    if(!planar) {
                        List<DefaultFeature> nearPatches = getHabitat().getPatches();
                        if(getDistMax() > 0) {
                            Envelope env = orig.getGeometry().getEnvelopeInternal();
                            env.expandBy(getDistMax());
                            nearPatches = (List<DefaultFeature>)index.query(env);
                        }
                        
                        for(DefaultFeature dest : nearPatches) {
                            if (filter(filterPatchIds, (int)orig.getId(), (int)dest.getId())) {
                                Path p = pathFinder.createPath(orig, dest);
                                if (getDistMax() <= 0 || p.getDist() <= getDistMax()) {
                                    links.add(p);
                                }
                            }
                        }
                    } else {
                        for (Integer dId : getPlanarLinks().getNeighbors(orig)) {
                            if (filter(filterPatchIds, (int)orig.getId(), dId)) {
                                DefaultFeature dest = getHabitat().getPatch(dId);
                                Path p = pathFinder.createPath(orig, dest);
                                if (getDistMax() <= 0 || p.getDist() <= getDistMax()) {                                   
                                    links.add(p);
                                }
                            }
                        }
                    }

                    incProgress(1);
                }
                return links;
            }

            @Override
            public int getSplitRange() {
                return filterPatches.size();
            }

            @Override
            public List<Path> getResult() {
                return result;
            }

            @Override
            public void gather(List<Path> results) {
                result.addAll(results);
            }
        };

        ExecutorService.execute(task);

        if(task.isCanceled()) {
            throw new CancellationException();
        }
        Logger.getLogger(EuclideLinkset.class.getName()).info("Elapsed time : " + (System.currentTimeMillis()-start));
        paths = task.getResult();
    }

    @Override
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle");
        
        String info = super.getInfo();
       
        return info.replace("?DISTANCE?", bundle.getString("LinksetPanel.euclidRadioButton.text"));
    }
    
    @Override
    protected Geometry calcCorridor(Path path, double maxCost) throws IOException {
        return getCostVersion(getHabitat()).calcCorridor(path, maxCost);
    }

    @Override
    protected Raster calcRasterCorridor(Path path, double maxCost, boolean inPatch, Double val) throws IOException {
        return getCostVersion(getHabitat()).calcRasterCorridor(path, maxCost, inPatch, val);
    }

    @Override
    public CostLinkset getCostVersion(Habitat habitat) {
        double [] costs = new double[getProject().getCodes().last()+1];
        Arrays.fill(costs, getProject().getResolution());
        return new CostLinkset(habitat, getName(), getTopology(), isInter(), nodeFilter, costs, isRealPaths(), false, getDistMax(), 0);
    }

    @Override
    public CircuitLinkset getCircuitVersion(Habitat habitat) {
        return getCostVersion(habitat).getCircuitVersion(habitat);
    }

    @Override
    public EuclidePathFinder getPathFinder() {
        return heightRasterFile != null ? new Euclide3DPathFinder(getHabitat(), getProject().getExtRaster(heightRasterFile)) : 
                new EuclidePathFinder(getHabitat());
    }
       
  
}
