/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.locationtech.jts.geom.Geometry;
import org.thema.common.ProgressBar;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MultiHabitat;

/**
 *
 * @author gvuidel
 */
public class MultiLinkset extends Linkset {
    private List<Linkset> linksets;

    public MultiLinkset(Collection<Linkset> linksets) {
        super(new MultiHabitat(linksets.stream().flatMap(l->l.getHabitat().getIdHabitats().stream()).collect(Collectors.toSet()), 
                    linksets.iterator().next().getProject(), false), 
                linksets.stream().map(l->l.getName()).collect(Collectors.joining("_")), Topology.MULTI, linksets.iterator().next().getTypeDist(), false, null,  //NOI18N
                linksets.stream().anyMatch(l -> l.isRealPaths()), 0.0);
        if(linksets.stream().map(l->l.getTypeDist()).collect(Collectors.toSet()).size() > 1) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("ALL LINKSETS MUST HAVE THE SAME DISTANCE TYPE"));
        }
        
        this.linksets = new ArrayList<>(linksets);
        Collections.sort(this.linksets);
        
        this.paths = linksets.stream().flatMap(l->l.getPaths().stream()).collect(Collectors.toList());
    }

    @Override
    public double estimCost(double distance, double minCost, double costMax) {
        if(getTypeDist() == Distance.EUCLID) {
            return distance;
        }
        List<Path> lstPaths = costMax > 0 ?
            linksets.stream().flatMap(l->((RasterLinkset)l).getPathsWithoutCost(costMax).stream()).collect(Collectors.toList()) :
            paths;
        return RasterLinkset.estimCost(lstPaths, minCost, distance);
    }

    
    public List<Linkset> getLinksets() {
        return linksets;
    }

    @Override
    public SpacePathFinder getPathFinder() {
        return linksets.iterator().next().getPathFinder();
    }

    @Override
    public void saveLinks(boolean saveAll) throws IOException {
        for(Linkset linkset : linksets) {
            linkset.saveLinks(saveAll);
        }
    }

    @Override
    public void removeLinks(DefaultFeature patch) {
        linksets.stream().forEach(l->l.removeLinks(patch));
        paths = linksets.stream().flatMap(l->l.getPaths().stream()).collect(Collectors.toList());
    }

    @Override
    public Map<DefaultFeature, Path> calcNewLinks(DefaultFeature patch) {
        Map<DefaultFeature, Path> newLinks = new HashMap<>();
        for(Linkset linkset : linksets) {
            if(linkset.getHabitat().getIdHabitats().contains(Habitat.getPatchIdHab(patch))) {
                newLinks.putAll(linkset.calcNewLinks(patch));
            }
        }
        return newLinks;
    }

    @Override
    public void addLinks(DefaultFeature patch) {
        for(Linkset linkset : linksets) {
            if(linkset.getHabitat().getIdHabitats().contains(Habitat.getPatchIdHab(patch))) {
                linkset.addLinks(patch);
            }
        }
        paths = linksets.stream().flatMap(l->l.getPaths().stream()).collect(Collectors.toList());
    }

    
    @Override
    public Raster computeRasterCorridor(ProgressBar progressBar, AbstractGraph graph, double maxCost, double alpha, double beta, String metric) {
        WritableRaster raster = null;
        for(Linkset linkset : linksets) {
            Raster r = linkset.computeRasterCorridor(progressBar, graph, maxCost, alpha, beta, metric);
            if(raster == null) {
                raster = (WritableRaster) r;
            } else {
                for(int y = r.getMinY(); y < r.getBounds().getMaxY(); y++) {
                    for(int x = r.getMinX(); x < r.getBounds().getMaxX(); x++) {
                        double v = r.getSampleDouble(x, y, 0);
                        if(v > 0) {
                            double v0 = raster.getSampleDouble(x, y, 0);
                            raster.setSample(x, y, 0, alpha == 0 ? v0+v :
                                    Math.max(v0, v));
                        }
                    }
                }
            }
        }
        return raster;
    }

    @Override
    public List<Feature> computeCorridor(ProgressBar progressBar, AbstractGraph graph, double maxCost) {
        List<Feature> corridors = new ArrayList<>();
        for(Linkset linkset : linksets) {
            corridors.addAll(linkset.computeCorridor(progressBar, graph, maxCost));
        }
        return corridors;
    }

    
    @Override
    protected void calcLinkset(ProgressBar progressBar) throws IOException {
        throw new UnsupportedOperationException();
    }
    @Override
    protected Geometry calcCorridor(Path path, double maxCost) throws IOException {
        throw new UnsupportedOperationException();
    }
    @Override
    protected Raster calcRasterCorridor(Path path, double maxCost, boolean inPatch, Double val) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public CostLinkset getCostVersion(Habitat habitat) {
        return linksets.iterator().next().getCostVersion(habitat);
    }

    @Override
    public CircuitLinkset getCircuitVersion(Habitat habitat) {
        return linksets.iterator().next().getCircuitVersion(habitat);
    }
    
    
}
