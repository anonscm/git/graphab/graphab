/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.thema.common.Config;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.util.SpatialOp;

/**
 *
 * @author gvuidel
 */
public abstract class RasterLinkset extends Linkset {
    
    private double [] costs;
    private double coefSlope;
    private File extCostFile;
    
    private transient Map<Integer, Feature> costVoronoi;
    
    /**
     * Creates a linkset from external map or from landscape map codes.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param dist distance type
     * @param inter is interhabitat ?
     * @param nodeFilter
     * @param costs cost for each landscape code or null
     * @param realPaths are real paths stored
     * @param distMax max cost distance or 0 for no max
     * @param extCostFile raster file containing costs or null
     * @param coefSlope coefficient for slope or 0 to avoid slope calculation
     */
    public RasterLinkset(Habitat habitat, String name, Topology topo, Distance dist, boolean inter, String nodeFilter, double[] costs, File extCostFile, boolean realPaths, 
            double distMax, double coefSlope) {
        super(habitat, name, topo, dist, inter, nodeFilter, realPaths, distMax);
        
        if(costs != null && extCostFile != null || costs == null && extCostFile == null) {
            throw new IllegalArgumentException();
        }
       
        if(costs != null) {
            this.costs = Arrays.copyOf(costs, costs.length);
        }

        this.coefSlope = coefSlope;
        if(extCostFile != null) {
            String prjPath = habitat.getProject().getDirectory().getAbsolutePath();
            if(extCostFile.getAbsolutePath().startsWith(prjPath)) {
                this.extCostFile = new File(extCostFile.getAbsolutePath().substring(prjPath.length()+1));
            } else {
                this.extCostFile = extCostFile.getAbsoluteFile();
            }
        }
    }
    
    @Override
    public boolean hasVoronoi() {
        return getTopology() == Topology.PLANAR_COST || super.hasVoronoi(); 
    }
    
    /**
     * The voronoi polygon associated with the patch id
     * @param id the patch identifier
     * @return a polygon feature representing the voronoi polygon of the patch id
     */
    public Feature getVoronoi(int id) {
        if(getTopology() == Topology.PLANAR_COST) {
            return getCostVoronoi().get(id);
        } else {
            return super.getVoronoi(id);
        }
    }
    
    protected PlanarLinks getPlanarLinks() {
        if(getTopology() == Topology.PLANAR) {
            return getHabitat().getPlanarLinks();
        } else if(getTopology() == Topology.PLANAR_COST) {
            Raster voronoiRaster = getPathFinder().calcVoronoi();
            PlanarLinks planar = getHabitat().createLinks(voronoiRaster, Config.getProgressBar());
            List<? extends Feature> voronois = SpatialOp.vectorizeVoronoi(voronoiRaster, getProject().getGrid2space());
            costVoronoi = voronois.stream().collect(Collectors.toMap((Feature f) -> (Integer)f.getId(), (f) -> f));
            return planar;
        } else {
            return null;
        }
    }
    /**
     * @return the voronoi features 
     * @throws IllegalStateException if the habitat has no voronoi
     */
    protected synchronized Map<Integer, Feature> getCostVoronoi() {
        if(costVoronoi == null) {
            File file = getCostVoronoiFile();
            if(!file.exists()) {
                throw new IllegalStateException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("THE LINKSET {0} DOES NOT CONTAIN VORONOI."), getName()));
            }
            try {
                List<DefaultFeature> features = IOFeature.loadFeatures(file, "Id"); //NOI18N
                costVoronoi = features.stream().collect(Collectors.toMap((Feature f) -> (Integer)f.getId(), (f) -> f));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        return costVoronoi;
    }
   
    protected File getCostVoronoiFile() {
        return new File(getLinkFile().getParentFile(), getName() + "-links-voronoi.gpkg"); //NOI18N
    }
    
    /**
     * Estimates the cost from a distance. 
     * Do a linear regression in double log  between cost and length of all links.
     * @param distance metric distance
     * @param costMax paths containing costs >= costMax are excluded from the regression if costMax > 0
     * @return the cost corresponding to the distance
     */
    @Override
    public double estimCost(double distance, double minCost, double costMax) {
        return estimCost(getPathsWithoutCost(costMax), minCost, distance);
    }
    
    /**
     * Estimates the cost from a distance.Do a linear regression in double log  between cost and length of all links.
     * @param paths list of links
     * @param minCost
     * @param distance metric distance
     * @return the cost corresponding to the distance
     */
    public static double estimCost(List<Path> paths, double minCost, double distance) {
        SimpleRegression reg = new SimpleRegression(false);
        for(Feature f : paths) {
            double dist = ((Number)f.getAttribute(Path.DIST_ATTR)).doubleValue();
            double cost = ((Number)f.getAttribute(Path.COST_ATTR)).doubleValue();
            
            if(dist > 0 && cost > 0 && cost >= minCost) {
                reg.addData(Math.log(dist), Math.log(cost));       
            }
        }
        return Math.exp(Math.log(distance) * reg.getSlope());
    }
    
    /**
     * Returns paths which do not contains costs >= costMax.
     * @param costMax costMax paths containing costs >= costMax are excluded if costMax > 0
     * @return list of paths with some excluded
     */
    public List<Path> getPathsWithoutCost(double costMax) {
        return getPaths();
    }
    
    @Override
    public RasterPathFinder getPathFinder()  {
        try {
            if(isExtCost()) {
                if(getExtCostFile().exists()) {
                    Raster extRaster = getProject().getExtRaster(getExtCostFile());
                    return new RasterPathFinder(getHabitat(), extRaster, getCoefSlope());
                } else {
                    throw new RuntimeException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("COST RASTER FILE {0} NOT FOUND"), new Object[] {getExtCostFile()}));
                }
            } else {
                return new RasterPathFinder(getHabitat(), getProject().getRasterLand(), getCosts(), getCoefSlope());
            }
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void saveLinks(boolean saveAll) throws IOException {
        super.saveLinks(saveAll); 
        if(getTopology() == Topology.PLANAR_COST && saveAll) {
            IOFeature.saveFeatures(costVoronoi.values(), getCostVoronoiFile(), getProject().getCRS());
        }
    }
    
    public void removeFiles() {
        super.removeFiles();
        if(getTopology() == Topology.PLANAR_COST) {
            getCostVoronoiFile().delete();
        }
    }
    
    /**
     * @return slope is used for cost calculation ?
     */
    public boolean isUseSlope() {
        return coefSlope != 0;
    }

    /**
     * @return the slope coefficient
     */
    public double getCoefSlope() {
        return coefSlope;
    }

    /**
     * @return is external cost map ?
     */
    public boolean isExtCost() {
        return extCostFile != null;
    }

    /**
     * Used only for COST distance without external cost map
     * @return the cost for each code of the landscape map or null
     */
    public double[] getCosts() {
        return costs;
    }

    /**
     * Used only for COST distance with external cost map
     * @return the file containing the costs or null
     */
    public File getExtCostFile() {
        if(extCostFile == null) {
            return null;
        } else {
            return getProject().getAbsoluteFile(extCostFile);
        }
    }
    
    @Override
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle");
        
        String info = (getTypeDist() == Distance.COST ? java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("COST") :
                java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("CIRCUIT")) + "\n";
        
        if(isExtCost()) {
            info += bundle.getString("LinksetPanel.rasterRadioButton.text") + "\nFile : " + extCostFile.getAbsolutePath();
        } else {
            info += bundle.getString("LinksetPanel.costRadioButton.text") + "\n";
            for(Integer code : getProject().getCodes()) {
                info += code + " : " + costs[code] + "\n";
            }
        }       
        if(isUseSlope()) {
            info += java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("USE SLOPE : {0}"), new Object[] {coefSlope}) + "\n";
        }

        return super.getInfo().replace("?DISTANCE?", info); //NOI18N
    }
}
