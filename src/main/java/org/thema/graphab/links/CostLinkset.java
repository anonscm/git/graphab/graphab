/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.links;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.linearref.LengthIndexedLine;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BandedSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CancellationException;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.thema.common.JTS;
import org.thema.common.ProgressBar;
import org.thema.common.collection.HashMap2D;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.util.SpatialOp;
import org.thema.parallel.AbstractParallelTask;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.ParallelTask;

/**
 * Represents a set of links (ie. paths) between the patches of the project.
 * The topology can be COMPLETE or PLANAR.
 * The distance can be EUCLID, COST or CIRCUIT
 * 
 * @author Gilles Vuidel
 */
public class CostLinkset extends RasterLinkset {
    
    private boolean removeCrossPatch;
    
    /**
     * Creates a linkset with cost distance from landscape map codes.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param inter
     * @param nodeFilter
     * @param costs cost for each landscape code
     * @param realPaths are real paths stored
     * @param removeCrossPatch remove links crossing patch ?
     * @param distMax max cost distance for complete topology only or 0 for no max
     * @param coefSlope coefficient for slope or 0 to avoid slope calculation
     */
    public CostLinkset(Habitat habitat, String name, Topology topo, boolean inter, String nodeFilter, double[] costs, boolean realPaths, 
            boolean removeCrossPatch, double distMax, double coefSlope) {
        this(habitat, name, topo, inter, nodeFilter, costs, null, realPaths, removeCrossPatch, distMax, coefSlope);
    }
    

    /**
     * Creates a linkset with cost distance from external map.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param inter
     * @param nodeFilter
     * @param realPaths are real paths stored
     * @param removeCrossPatch remove links crossing patch ?
     * @param distMax max cost distance or 0 for no max
     * @param extCostFile raster file containing costs
     * @param coefSlope coefficient for slope or 0 to avoid slope calculation
     */
    public CostLinkset(Habitat habitat, String name, Topology topo, boolean inter, String nodeFilter, boolean realPaths, 
            boolean removeCrossPatch, double distMax, File extCostFile, double coefSlope) {
        this(habitat, name, topo, inter, nodeFilter, null, extCostFile, realPaths, removeCrossPatch, distMax, coefSlope);
    }
    
    /**
     * Creates a linkset with cost distance from external map or from landscape map codes.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param inter is interhabitat ?
     * @param nodeFilter
     * @param costs cost for each landscape code or null
     * @param realPaths are real paths stored
     * @param removeCrossPatch remove links crossing patch ?
     * @param distMax max cost distance or 0 for no max
     * @param extCostFile raster file containing costs or null
     * @param coefSlope coefficient for slope or 0 to avoid slope calculation
     */
    protected CostLinkset(Habitat habitat, String name, Topology topo, boolean inter, String nodeFilter, double[] costs, File extCostFile, boolean realPaths, 
            boolean removeCrossPatch, double distMax, double coefSlope) {
        super(habitat, name, topo, Distance.COST, inter, nodeFilter, costs, extCostFile, realPaths, distMax, coefSlope);

        this.removeCrossPatch = removeCrossPatch;        
    }
    
    /**
     * @return links crossing patches are removed ?
     */
    public boolean isRemoveCrossPatch() {
        return removeCrossPatch;
    }

    @Override
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle");
        
        String info = super.getInfo();
        
        if(removeCrossPatch) {
            info += "\n" + bundle.getString("LinksetPanel.removeCrossPatchCheckBox.text");
        }

        return info;
    }
    
    /**
     * Return a new virtual linkset for circuit calculation based on cost linkset.
     * @return a new linkset 
     */
    @Override
    public CircuitLinkset getCircuitVersion(Habitat habitat) {
        return new CircuitLinkset(habitat, getName(), getTopology(), isInter(), nodeFilter, getCosts(), getExtCostFile(), true, getCoefSlope());
    }
    
    /**
     * @return new cost linkset
     */
    @Override
    public CostLinkset getCostVersion(Habitat habitat) {
        return new CostLinkset(habitat, getName(), getTopology(), isInter(), nodeFilter, getCosts(), getExtCostFile(), isRealPaths(), isRemoveCrossPatch(), getDistMax(), getCoefSlope());
    }    

    @Override
    protected Geometry calcCorridor(Path path, double maxCost) throws IOException {
        Raster corridor = calcRasterCorridor(path, maxCost, false, 1.0);
        
        if(corridor == null) {
            return new GeometryFactory().buildGeometry(Collections.EMPTY_LIST);
        }
        Envelope env = JTS.rectToEnv(corridor.getBounds());
        env.expandBy(-1);
        Geometry geom =  SpatialOp.vectorize(corridor, env, 1);
        return getProject().getGrid2space().transform(geom);
    }
    
    @Override
    protected Raster calcRasterCorridor(Path path, double maxCost, final boolean inPatch, final Double val) throws IOException {
        if(path.getCost() > maxCost) {
            return null;
        }
        RasterPathFinder pathfinder = getPathFinder();
        Raster r1 = pathfinder.getDistRaster(path.getPatch1(), maxCost);
        Raster r2 = pathfinder.getDistRaster(path.getPatch2(), maxCost);
        final Rectangle rect = r1.getBounds().intersection(r2.getBounds());
        final int id1 = (Integer)path.getPatch1().getId();
        final int id2 = (Integer)path.getPatch2().getId();
        WritableRaster corridor = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_DOUBLE, rect.width+2, rect.height+2, 1), new Point(rect.x-1, rect.y-1));
        for(int y = rect.y; y < rect.getMaxY(); y++) {
            for(int x = rect.x; x < rect.getMaxX(); x++) {
                if(!inPatch) {
                    int id = getHabitat().getPatchId(x, y);
                    if(id == id1 || id == id2) {
                        continue;
                    }
                }
                double cost = r1.getSampleDouble(x, y, 0)+r2.getSampleDouble(x, y, 0);
                if(cost <= maxCost) {
                    corridor.setSample(x, y, 0, val == null ? cost : val);
                }
            }
        }
        return corridor;
    }
    
    @Override
    protected void calcLinkset(ProgressBar progressBar) {        
        Path.newSetOfPaths();
        long start = System.currentTimeMillis();
        final PlanarLinks planar = getPlanarLinks();
        final List<DefaultFeature> filterPatches = getHabitat().getPatches(nodeFilter);
        final Set<Integer> filterPatchIds = filterPatches.size() < getHabitat().getPatches().size() ? 
                filterPatches.stream().map(f -> (Integer)f.getId()).collect(Collectors.toSet()) : null;
        
        ParallelTask<List<Path>, List<Path>> task = new AbstractParallelTask<List<Path>, List<Path>>(progressBar) {
            private List<Path> result = new ArrayList<>();
            @Override
            public List<Path> execute(int start, int end) {
                List<Path> links = new ArrayList<>();
                RasterPathFinder pathfinder = getPathFinder();
                for(DefaultFeature orig : filterPatches.subList(start, end)) {
                    if(isCanceled()) {
                        throw new CancellationException();
                    }
                    HashMap<Feature, Path> paths;
                    if(getTopology() == Topology.COMPLETE) {
                        paths = pathfinder.calcPaths(orig, getDistMax(), isRealPaths(), dId -> filter(filterPatchIds, (int) orig.getId(), dId));
                    } else {
                        List<DefaultFeature> dests = new ArrayList<>();
                        for(Integer dId : planar.getNeighbors(orig)) {
                            if(filter(filterPatchIds, (int) orig.getId(), dId)) {
                                dests.add(getHabitat().getPatch(dId));
                            }
                        }
                        if(dests.isEmpty()) {
                            continue;
                        }
                        paths = pathfinder.calcPaths(orig, dests, getDistMax());
                    }

                    for(Feature d : paths.keySet()) {
                        Path p = paths.get(d);
                        boolean add = true;
                        if(isRemoveCrossPatch() && isRealPaths()) {
                            List lst = getHabitat().getPatchIndex().query(p.getGeometry().getEnvelopeInternal());
                            for(Object o : lst) {
                                Feature f = (Feature) o;
                                if(f != orig && f != d && f.getGeometry().intersects(p.getGeometry())) {
                                    add = false;
                                    break;
                                }
                            }
                        }
                        if(add) {
                            links.add(p);
                        }
                    }  
                    incProgress(1);
                }
            
                return links;
            }
            @Override
            public int getSplitRange() {
                return filterPatches.size();
            }

            @Override
            public List<Path> getResult() {
                return result;
            }
            @Override
            public void gather(List<Path> results) {
                result.addAll(results);
            }
        };

        ExecutorService.execute(task);

        if(task.isCanceled()) {
            throw new CancellationException();
        }
        
        Logger.getLogger(CostLinkset.class.getName()).info("Elapsed time : " + (System.currentTimeMillis()-start));
        
        paths = task.getResult();
    }

    
    
    /**
     * Returns paths which do not contains costs >= costMax.
     * @param costMax costMax paths containing costs >= costMax are excluded if costMax > 0
     * @return list of paths with some excluded
     */
    public List<Path> getPathsWithoutCost(double costMax) {
        if(costMax <= 0) {
            return getPaths();
        }
        HashMap2D<Path, Double, Integer> costPaths = extractCostFromPath();
        ArrayList<Path> inPaths = new ArrayList<>();
        Set<Double> exCosts = new TreeSet<>(costPaths.getKeys2()).tailSet(costMax);
        for(Path p : costPaths.getKeys1()) {
            boolean include = true;
            for(Double cost : exCosts) {
                Integer nb = costPaths.getValue(p, cost);
                if(nb != null && nb > 0) {
                    include = false;
                    break;
                }
            }
            if(include) {
                inPaths.add(p);
            }
        }
        
        return inPaths;
    }
    
    /**
     * Extract for each path the number of pixels for each cost
     * @return a 2D mapping (Path, cost) : #pixels
     */
    public HashMap2D<Path, Double, Integer> extractCostFromPath() {
        if(!isRealPaths()) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("LINKSET MUST HAVE REAL PATH."));
        }
        if(isExtCost()) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("LINKSET MUST HAVE COST FROM LANDSCAPE MAP."));
        }
        AffineTransformation trans = getProject().getSpace2grid();
        WritableRaster land = getProject().getRasterLand();
        
        Set<Double> costSet = new TreeSet<>();
        for(double c : getCosts()) {
            costSet.add(c);
        }
        HashMap2D<Path, Double, Integer> map = new HashMap2D<>(getPaths(), costSet, 0);
        
        for(Path p : getPaths()) {                
            Set<Coordinate> pixels = new HashSet<>();
            Geometry g = trans.transform(p.getGeometry());
            LengthIndexedLine index = new LengthIndexedLine(g);
            for(int l = 0; l <= g.getLength(); l++) {
                Coordinate c = index.extractPoint(l);
                pixels.add(new Coordinate((int)c.x, (int)c.y));
            }
            // end point
            Coordinate c = index.extractPoint(index.getEndIndex());
            pixels.add(new Coordinate((int)c.x, (int)c.y));
            
            for(Coordinate pixel : pixels) {
                double cost = getCosts()[land.getSample((int)pixel.x, (int)pixel.y, 0)];
                map.setValue(p, cost, map.getValue(p, cost) + 1);
            }
        }
        
        return map;
    }

}
