/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.links;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.locationtech.jts.geom.Coordinate;
import org.thema.common.Config;
import org.thema.common.ProgressBar;
import org.thema.common.collection.HashMapList;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.SimpleParallelTask;
import org.thema.data.feature.Feature;

/**
 *
 * @author gvuidel
 */
public class IntraLinks {
    
    private Linkset linkset;
    private HashMap<MultiKey, double[]> intraLinks;

    public IntraLinks(Linkset linkset) {
        this.linkset = linkset;
    }
    
    /**
     * Returns the cost and the length between 2 coordinates of the border of a patch.
     * The two coordinates must correspond to the endpoint of two paths connected to the same patch.
     * @param c1 the first coordinate
     * @param c2 the second coordinate
     * @return the cost and the length (may be the same for euclidean distance)
     */
    public double[] getIntraLinkCost(Coordinate c1, Coordinate c2) {
        if(c1.compareTo(c2) < 0) {
            return getIntraLinks().get(new MultiKey(c1, c2));
        } else {
            return getIntraLinks().get(new MultiKey(c2, c1));
        }
    }
    
    /**
     * Returns the cost and the length between 2 paths connected to the same patch.
     * @param p1 the first path
     * @param p2 the second path
     * @return the cost and the length (may be the same for euclidean distance)
     */
    public double[] getIntraLinkCost(Path p1, Path p2) {
        Feature patch = Path.getCommonPatch(p1, p2);
        Coordinate c1 = p1.getCoordinate(patch);
        Coordinate c2 = p2.getCoordinate(patch);
        return getIntraLinkCost(c1, c2);
    }
    
    private HashMap<MultiKey, double[]> getIntraLinks() {
        if(intraLinks == null) {
            try {
                loadIntraLinks();
            } catch (IOException ex) {
                Logger.getLogger(Linkset.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return intraLinks;
    }
    
    
    public void calcIntraLinks(ProgressBar progressBar) {
        final HashMapList<Feature, Path> mapLinks = new HashMapList<>();
        for(Path p : linkset.getPaths()) {
            mapLinks.putValue(p.getPatch1(), p);
            mapLinks.putValue(p.getPatch2(), p);
        }
        
        final HashMap<MultiKey, double[]> mapIntraLinks = new HashMap<>();
        SimpleParallelTask<Feature> task = new SimpleParallelTask<Feature>(new ArrayList<>(mapLinks.keySet()), progressBar) {
            @Override
            protected void executeOne(Feature patch) {
                SpacePathFinder pathFinder = linkset.getPathFinder();

                List<Path> links = mapLinks.get(patch);
                HashSet<Coordinate> pointSet = new HashSet<>();
                for(Path link : links) {
                    pointSet.add(link.getCoordinate(patch));
                }

                List<Coordinate> pointList = new ArrayList<>(pointSet);
                for(int i = 0; i < pointList.size()-1; i++) { 
                    Coordinate c1 = pointList.get(i);

                    List<Coordinate> dests = pointList.subList(i+1, pointList.size());
                    List<double[]> values = pathFinder.calcPaths(c1, dests);
                    for(int k = 0; k < values.size(); k++) {
                        synchronized(mapIntraLinks) {
                            if(c1.compareTo(dests.get(k)) < 0) {
                                mapIntraLinks.put(new MultiKey(c1, dests.get(k)), values.get(k));
                            } else {
                                mapIntraLinks.put(new MultiKey(dests.get(k), c1), values.get(k));
                            }
                        }
                    }
                }
                
            }
        };
        
        new ParallelFExecutor(task).executeAndWait();
        if(task.isCanceled())  {
            throw new CancellationException();
        }
        intraLinks = mapIntraLinks;
    }
    
    public File getIntraLinkFile() {
        return new File(linkset.getLinkFile().getParentFile(), linkset.getName() + "-links-intra.csv"); //NOI18N
    }
    
    /** 
     * Load intralinks if not already loaded.
     * If the CSv file does not exists, calculate intralinks and save it
     * @throws IOException 
     */
    public synchronized void loadIntraLinks() throws IOException {
        
        if(intraLinks != null) {
            return;
        }
        
        File fCSV = getIntraLinkFile();

        if(!fCSV.exists()) {
            calcIntraLinks(Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/links/Bundle").getString("INTRA-PATCH LINKS...")));
            saveIntraLinks();
            return;
        }
        
        try (CSVReader r = new CSVReader(new FileReader(fCSV))) {
            r.readNext();
            intraLinks = new HashMap<>();
            String [] tab;
            String splitChar = null;
            while((tab = r.readNext()) != null) {
                if(splitChar == null) { // for compatibility < 2.9.8 TODO remove for final 3.0
                    splitChar = tab[0].contains("_") ? "_" : "-";
                }
                String[] ordinates = tab[0].split(splitChar);
                Coordinate c0 = new Coordinate(Double.parseDouble(ordinates[0]), Double.parseDouble(ordinates[1]));
                ordinates = tab[1].split(splitChar);
                Coordinate c1 = new Coordinate(Double.parseDouble(ordinates[0]), Double.parseDouble(ordinates[1]));
                intraLinks.put(new MultiKey(c0, c1), new double[]{Double.parseDouble(tab[2]), Double.parseDouble(tab[3])});
            }
        }
    }

    /**
     * Saves intra links. Called only ones at linkset computation
     * @throws IOException 
     */
    public void saveIntraLinks() throws IOException {
        File fCSV = getIntraLinkFile();
        try (CSVWriter w = new CSVWriter(new FileWriter(fCSV))) {
            w.writeNext(new String[]{"Coord1", "Coord2", "Cost", "Length"});
            
            for(MultiKey key : intraLinks.keySet()) {
                double [] val = intraLinks.get(key);
                Coordinate c0 = (Coordinate) key.getKey(0);
                Coordinate c1 = (Coordinate) key.getKey(1);
                w.writeNext(new String[]{c0.x + "_" + c0.y, c1.x + "_" + c1.y, ""+val[0], ""+val[1]});
            }
        }
    }
}
