/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.links;

import org.locationtech.jts.geom.Geometry;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.AbstractParallelFTask;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.ParallelFTask;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.habitat.Habitat;

/**
 * Represents a set of links (ie. paths) between the patches of the project with circuit distance.
 * The topology can be COMPLETE or PLANAR.
 * 
 * @author Gilles Vuidel
 */
public class CircuitLinkset extends RasterLinkset {

    private boolean optimCirc;
    

    /**
     * Creates a linkset with circuit distance from external map or from landscape map codes.
     * @param habitat the habitat where adding this linkset
     * @param name linkset name
     * @param topo linkset type (ie. topology) COMPLETE or PLANAR
     * @param inter is interhabitat ?
     * @param nodeFilter
     * @param costs cost for each landscape code or null
     * @param extCostFile raster file containing costs or null
     * @param coefSlope coefficient for slope or 0 to avoid slope calculation
     * @param optimCirc optimize raster size for circuit calculation ?
     */
    public CircuitLinkset(Habitat habitat, String name, Topology topo, boolean inter, String nodeFilter, double[] costs, File extCostFile, boolean optimCirc, double coefSlope) {
        super(habitat, name, topo, Distance.CIRCUIT, inter, nodeFilter, costs, extCostFile, false, Double.NaN, coefSlope);

        this.optimCirc = optimCirc;
        
    }

    /**
     * Circuit specific option
     * @return true if raster size is optimized for circuit
     */
    public boolean isOptimCirc() {
        return optimCirc;
    }

    /**
     * @return this
     */
    @Override
    public CircuitLinkset getCircuitVersion(Habitat habitat) {
        return new CircuitLinkset(habitat, getName(), getTopology(), isInter(), nodeFilter, getCosts(), getExtCostFile(), isOptimCirc(), getCoefSlope());
    }
    
    /**
     * Return a new virtual linkset for cost calculation based on circuit linkset.
     * Return this if this linkset is already a cost linkset
     * @return a new linkset or this
     */
    @Override
    public CostLinkset getCostVersion(Habitat habitat) {
        return new CostLinkset(habitat, getName(), getTopology(), isInter(), nodeFilter, getCosts(), getExtCostFile(), true, false, Double.NaN, getCoefSlope());
    }
       
    @Override
    protected Geometry calcCorridor(Path path, double maxCost) throws IOException {
        CircuitRaster circuit = getCircuit();
        CircuitRaster.PatchODCircuit odCircuit = circuit.getODCircuit(path.getPatch1(), path.getPatch2());
        return odCircuit.getCorridor(maxCost);
    }
    
    @Override
    protected Raster calcRasterCorridor(Path path, double maxCost, final boolean inPatch, final Double val) throws IOException {
        CircuitRaster circuit = getCircuit();
        CircuitRaster.PatchODCircuit odCircuit = circuit.getODCircuit(path.getPatch1(), path.getPatch2());
        return odCircuit.getCurrentMap();
    }
       
    @Override
    protected void calcLinkset(ProgressBar progressBar) throws IOException {
        final List<Path> links = Collections.synchronizedList(new ArrayList<>(getHabitat().getPatches().size() * 4));
        Path.newSetOfPaths();
        final List<DefaultFeature> filterPatches = getHabitat().getPatches(nodeFilter);
        final Set<Integer> filterPatchIds = filterPatches.size() < getHabitat().getPatches().size() ? 
                filterPatches.stream().map(f -> (Integer)f.getId()).collect(Collectors.toSet()) : null;
        PlanarLinks planar = getPlanarLinks();
        long start = System.currentTimeMillis();
        final CircuitRaster circuit = getCircuit();
        ParallelFTask task;
        try (FileWriter w = new FileWriter(new File(getProject().getDirectory(), getName() + "-stats.csv"))) {
            w.write("Id1,Id2,Area1,Area2,W,H,T,Iter,InitSErr,R,MErr,M2Err,SErr\n");
            task = new AbstractParallelFTask(progressBar) {
                @Override
                protected Object execute(int start, int end) {
                    for(DefaultFeature orig : getHabitat().getPatches().subList(start, end)) {
                        if(isCanceled()) {
                            throw new CancellationException();
                        }
                        if(getTopology() == Topology.COMPLETE) {
                            for(DefaultFeature dest : getHabitat().getPatches()) {
                                if(filter(filterPatchIds, (int) orig.getId(), (int) dest.getId())) {
                                    double r = circuit.getODCircuit(orig, dest).getR();
                                    links.add(new Path(orig, dest, r, Double.NaN));
                                }
                            }
                        } else {
                            for(Integer dId : planar.getNeighbors(orig)) {
                                if(filter(filterPatchIds, (int) orig.getId(), dId)) {
                                    DefaultFeature dest = getHabitat().getPatch(dId);
                                    long t1 = System.currentTimeMillis();
                                    CircuitRaster.PatchODCircuit odCircuit = circuit.getODCircuit(orig, dest);
                                    odCircuit.solve();
                                    long t2 = System.currentTimeMillis();
                                    double r = odCircuit.getR();
                                    synchronized (CircuitLinkset.this) {
                                        try {
                                            w.write(orig.getId() + "," + dest.getId() + "," + Habitat.getPatchArea(orig) + "," + Habitat.getPatchArea(dest) + "," +
                                                    odCircuit.getZone().getWidth() + "," + odCircuit.getZone().getHeight() + "," + (t2 - t1) / 1000.0 + "," + odCircuit.getNbIter() + "," +
                                                    odCircuit.getInitErrSum() + "," + r + "," + odCircuit.getErrMax() + "," + odCircuit.getErrMaxWithoutFirst() + "," + odCircuit.getErrSum() + "\n");
                                            w.flush();
                                        } catch (IOException ex) {
                                            Logger.getLogger(CircuitLinkset.class.getName()).log(Level.WARNING, null, ex);
                                        }
                                    }
                                    links.add(new Path(orig, dest, r, Double.NaN));
                                }
                            }
                        }
                        
                        incProgress(1);
                    }
                    
                    return null;
                }
                @Override
                public int getSplitRange() {
                    return filterPatches.size();
                }
                @Override
                public void finish(Collection results) {
                }
                @Override
                public Object getResult() {
                    throw new UnsupportedOperationException("Not supported.");
                }
            };  
            new ParallelFExecutor(task).executeAndWait();
        }
        
        if(task.isCanceled()) {
            throw new CancellationException();
        }
        
        Logger.getLogger(CircuitLinkset.class.getName()).info("Elapsed time : " + (System.currentTimeMillis()-start));
        
        paths = links;
    }
    
    /**
     * Creates a raster circuit for the given linkset
     * @return a new raster circuit
     * @throws IOException 
     */
    public CircuitRaster getCircuit() throws IOException {

        if(isExtCost()) {
            if(getExtCostFile().exists()) {
                Raster extRaster = getProject().getExtRaster(getExtCostFile());
                return new CircuitRaster(getHabitat(), extRaster, true, isOptimCirc(), getCoefSlope());
            } else {
                throw new RuntimeException("Cost raster file " + getExtCostFile() + " not found");
            }
        } else {
            return new CircuitRaster(getHabitat(), getProject().getRasterLand(), getCosts(), true, isOptimCirc(), getCoefSlope());
        }
    }
}
