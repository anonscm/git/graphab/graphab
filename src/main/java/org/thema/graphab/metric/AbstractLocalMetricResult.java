/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric;

import com.ezylang.evalex.EvaluationException;
import com.ezylang.evalex.Expression;
import com.ezylang.evalex.parser.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.links.Path;

/**
 * Class for calculating a local metric on a graph.
 * 
 * @author Gilles Vuidel
 * @param <T>
 */
public abstract class AbstractLocalMetricResult<T extends Metric> extends AbstractAttributeMetricResult<T> {

    protected String filter;
    private transient Expression filterExpr;
    private transient List<DefaultFeature> nodes;
    private transient List<Path> edges;
    
    public AbstractLocalMetricResult(T metric, AbstractGraph graph, Method method) {
        this(metric.getDetailName(), metric, graph, method);
    }
    
    public AbstractLocalMetricResult(String name, T metric, AbstractGraph graph, Method method) {
        super(name, metric, graph, method);
        filter = null;
    }
    
    public boolean calcNodes() {
        return !getNodeFeatures().isEmpty();
    }
    
    public boolean calcEdges() {
        return !getEdgeFeatures().isEmpty();
    }
    
    public List<DefaultFeature> getNodeFeatures() {
        if(nodes == null) {
            List<DefaultFeature> features = getGraph().getHabitat().getPatches();
            if(filter != null) {
                nodes = features.stream().filter(f -> accept(f)).collect(Collectors.toList());
            } else {
                nodes = features;
            }
        }
        return nodes;
    }
    
    public List<Path> getEdgeFeatures() {
        if(edges == null) {
            List<Path> features = getGraph().getLinkset().getPaths();
            if(filter != null) {
                edges = features.stream().filter(f -> accept(f)).collect(Collectors.toList());
            } else {
                edges = features;
            }
        }
        return edges;
    }

    public void setFilter(String expr) {
        this.filter = expr;
    }

    public String getFilter() {
        return filter;
    }
    
    public synchronized boolean accept(DefaultFeature feature) {
        if(filter == null) {
            return true;
        }

        if(filterExpr == null) {
            filterExpr = new Expression(filter);
        }

        try {
            Set<String> vars = filterExpr.getUsedVariables();
            Map<String, Object> values = new HashMap<>();
            for(String var : vars) {
                if(var.equalsIgnoreCase("node")) {
                    values.put(var, !(feature instanceof Path));
                } else if(var.equalsIgnoreCase("edge")) {
                    values.put(var, feature instanceof Path);
                } else if(var.equalsIgnoreCase("id")) {
                    values.put(var, feature.getId());
                } else {
                    Object val = feature.getAttribute(var);
                    if(val == null) {
                        values.put(var, "");
                    } else {
                        values.put(var, val);
                    }
                }
            }
            return filterExpr.withValues(values).evaluate().getBooleanValue();
        } catch(EvaluationException | ParseException ex) {
            throw new RuntimeException(ex);
        }
    }
}
