/*
 * Copyright (C) 2023 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric;

import java.util.ArrayList;
import java.util.List;
import org.thema.graphab.Project;
import org.thema.graphab.graph.AbstractGraph;

/**
 *
 * @author Gilles Vuidel
 * @param <T>
 */
public abstract class AbstractAttributeMetricResult<T extends Metric> extends MetricResult<T> {
    
    protected List<String> attrNames;

    public AbstractAttributeMetricResult(String name, T metric, AbstractGraph graph, Project.Method method) {
        super(name, metric, graph, method);
        this.attrNames = new ArrayList<>();
    }
    
    public List<String> getAttrNames() {
        return attrNames;
    }
}
