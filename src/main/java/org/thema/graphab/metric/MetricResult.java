/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric;

import java.util.ResourceBundle;
import org.thema.common.ProgressBar;
import org.thema.graphab.Project;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.metric.global.GlobalMetricResult;

/**
 *
 * @author Gilles Vuidel
 * @param <T>
 */
public abstract class MetricResult<T extends Metric> implements Comparable<MetricResult>{
    private final String name;
    private final T metric;
    private final AbstractGraph graph;
    private final Method method;
    
    protected MetricResult(String name, T metric, AbstractGraph graph, Method method) {
        this.name = name;
        this.metric = (T)metric.dupplicate();
        this.graph = graph;
        this.method = method;
    }
    
    public T getMetric() {
        return metric;
    }

    public String getName() {
        return name.toLowerCase();
    }
    
    public String getFullName() {
        return (name + "_" + graph.getName()).toLowerCase(); //NOI18N
    }

    public AbstractGraph getGraph() {
        return graph;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return getFullName();
    }    

    @Override
    public int compareTo(MetricResult t) {
        return this.getFullName().compareTo(t.getFullName());
    }
    
    public abstract MetricResult<T> calculate(boolean threaded, ProgressBar monitor);
    
    public static String getInfo(MetricResult m) {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/metric/Bundle");
        
        String info = m.getMetric().getName() + " - " + m.getMethod() + "\n" + bundle.getString("GRAPH") + " : " + m.getGraph().getName() + "\n" +
                    (m.getMetric().hasParams() ? java.text.MessageFormat.format(bundle.getString("PARAMETERS : {0}"), m.getMetric().getParams().toString()) : "") + "\n"; //NOI18N
        if(m.getMetric() instanceof MHMetric) {
            MHMetric mh = (MHMetric)m.getMetric();
            if(mh.isMH()) {
                info += java.text.MessageFormat.format(bundle.getString("MULTI HABITAT : {0}"), mh.getSelection()) + "\n";
            }
        }
        if(m.getMethod() == Project.Method.GLOBAL) {
            info += bundle.getString("RESULTS") + "\n";
            GlobalMetricResult gm = (GlobalMetricResult)m;
            String[] resultNames = gm.getMetric().getResultNames(gm.getGraph());
            for(int i = 0; i < resultNames.length; i++) {
                info += resultNames[i] + " : " + gm.getResult()[i] + "\n"; //NOI18N
            }
        } else {
            info += java.text.MessageFormat.format(bundle.getString("ATTRIBUTES : {0}"), ((AbstractAttributeMetricResult)m).getAttrNames()) + "\n";
            if(m instanceof AbstractLocalMetricResult) {
                String filter = ((AbstractLocalMetricResult)m).getFilter();
                if(filter != null) {
                    info += java.text.MessageFormat.format(bundle.getString("FILTER : {0}"), filter) + "\n";
                }
            }
        }
        
        return info;
    }
}
