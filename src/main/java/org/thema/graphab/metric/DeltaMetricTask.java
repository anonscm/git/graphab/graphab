/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab.metric;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Graphable;
import org.thema.common.ProgressBar;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.DeltaGraph;
import org.thema.graphab.links.Path;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.parallel.AbstractParallelTask;

/**
 * Task for calculating global metric in delta mode on nodes and/or edges.
 * 
 * Works on threaded environment only.
 * @author Gilles
 */
public class DeltaMetricTask extends AbstractParallelTask<Map<Object, Double[]>, Map<Object, Double[]>>
        implements Serializable {

    private GlobalMetricResult metric;

    private List ids;

    private AbstractGraph graph;
    private Map<Object, Double[]> result;
    
    /**
     * Creates a new DeltaMetricTask
     * @param monitor progress monitor
     * @param metric the global metric
     * @param ids list of patch and/or link ids
     */
    public DeltaMetricTask(ProgressBar monitor, GlobalMetricResult metric, List ids) {
        super(monitor);
        this.graph = metric.getGraph();
        this.metric = metric;
        this.ids = ids;
    }
    
    /**
     * Creates a new DeltaMetricTask
     * @param monitor progress monitor
     * @param metric the global metric
     * @param nodes
     * @param edges
     */
    public DeltaMetricTask(ProgressBar monitor, GlobalMetricResult metric, Collection<DefaultFeature> nodes, Collection<Path> edges) {
        super(monitor);
        this.graph = metric.getGraph();
        this.metric = metric;
        this.ids = new ArrayList();
        for(DefaultFeature node : nodes) {
            ids.add(node.getId());
        }
        for(Path edge : edges) {
            ids.add(edge.getId());
        }
    }
    
    /**
     * Creates a new DeltaMetricTask
     * @param monitor progress monitor
     * @param metric the global metric
     */
    public DeltaMetricTask(ProgressBar monitor, GlobalMetricResult metric) {
        this(monitor, metric, metric.getGraph().getHabitat().getPatches(), metric.getGraph().getLinkset().getPaths());
    }

    @Override
    public Map<Object, Double[]> execute(int start, int end) {
        HashSet felems = new HashSet(ids.subList(start, end));
        monitor.setNote("Delta...");
        DeltaGraph deltaGen = new DeltaGraph(graph);
        Graph graph = deltaGen.getGraph();
        List<Graphable> elems = new ArrayList<>(felems.size());
        for(Object n : graph.getNodes()) {
            if(felems.contains(((Feature)((Graphable)n).getObject()).getId())) {
                elems.add((Graphable)n);
            }
        }
        for(Object n : graph.getEdges()) {
            if(felems.contains(((Feature)((Graphable)n).getObject()).getId())) {
                elems.add((Graphable)n);
            }
        }

        Double[] init = metric.getResult();
        Map<Object, Double[]> results = new HashMap<>();

        for(Graphable elem : elems) {
            if(isCanceled()) {
                throw new CancellationException();
            }
            deltaGen.removeElem(elem);
            GlobalMetricResult gmetric = new GlobalMetricResult("delta", metric.getMetric(), deltaGen);
            gmetric.calculate(false, null);
            Double[] res = gmetric.getResult();
            DefaultFeature f = (DefaultFeature)elem.getObject();
            Double [] delta = new Double[init.length];
            for(int i = 0; i < init.length; i++) {
                double ind;
                if(init[i] == null) {
                    if(init[init.length-1] != null) {
                        ind = res[i] / init[init.length-1];
                    } else {
                        ind = res[i];
                    }
                } else {
                    ind = (init[i] - res[i]) / init[i];
                }
                if(Math.abs(ind) < 1e-14) {
                    ind = 0;     
                }
                delta[i] = ind;
            }

            results.put(f.getId(), delta);
            deltaGen.reset();
            incProgress(1);
        }

        return results;
    }

    @Override
    public int getSplitRange() {
        return ids.size();
    }

    @Override
    public void gather(Map<Object, Double[]> partRes) {
        if(result == null) {
            result = new HashMap<>();
        }
        result.putAll(partRes);
    }

    @Override
    public Map<Object, Double[]> getResult() {
        return result;
    }

}
