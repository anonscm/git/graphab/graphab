/*
 * Copyright (C) 2023 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric;

/**
 * Multi Habitat metric
 * @author Gilles Vuidel
 */
public interface MHMetric {
    public static final String INTER = "inter";
    public static final String ALL = "all";
    
    public void setSelection(String selection);
    public String getSelection();
    public boolean isMH();
    public boolean isSelectAll();
    public boolean isSelectInter();
    public boolean isSelectOne();
}
