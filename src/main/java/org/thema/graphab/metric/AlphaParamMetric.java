/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thema.graphab.links.Linkset;

/**
 * Class for delegating parameter management for metrics having parameters :
 * d, p, beta 
 * and alpha = -Math.log(p) / d;
 * 
 * @author Gilles Vuidel
 */
public final class AlphaParamMetric implements Serializable {
    
    public static final String DIST = "d"; //NOI18N
    public static final String PROBA = "p"; //NOI18N
    public static final String BETA = "beta"; //NOI18N
    public static final String MAX_DIST = "maxd"; //NOI18N
    public static final String MIN_PROBA = "minp"; //NOI18N
    
    private boolean hasBeta;
    private double alpha = 6.931471805599453E-4;
    private double d = 1000;
    private double p = 0.5;
    private double beta = 1.0;
    
    private double maxD = Double.POSITIVE_INFINITY;
    private double minP = 0;

    public AlphaParamMetric() {
        this(true);
    }
    
    public AlphaParamMetric(boolean hasBeta) {
        this.hasBeta = hasBeta;
    }
    
    /**
     * @return alpha = -Math.log(p) / d;
     */
    public double getAlpha() {
        return alpha;
    }

    /**
     * @return beta the exponent of the capacity
     */
    public double getBeta() {
        if(!hasBeta) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/metric/Bundle").getString("THIS METRIC HAS NOT BETA PARAMETER."));
        }
        return beta;
    }
    
    public double getMaxCost() {
        if(minP > 0) {
            return -Math.log(minP) / alpha;
        } else {
            return maxD;
        }
    }
    
    /**
     * Set the parameters and calculate alpha
     * @param params map containing the 3 parameters
     */
    public void setParams(Map<String, Object> params) {
        if(params.containsKey(DIST)) {
            d = ((Number)params.get(DIST)).doubleValue();
        } else {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/metric/Bundle").getString("PARAMETER {0} NOT FOUND"), DIST));
        }
        if(params.containsKey(PROBA)) {
            p = ((Number)params.get(PROBA)).doubleValue();
        } else {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/metric/Bundle").getString("PARAMETER {0} NOT FOUND"), PROBA));
        }
        if(hasBeta) {
            if(params.containsKey(BETA)) {
                beta = ((Number)params.get(BETA)).doubleValue();
            } else {
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/metric/Bundle").getString("PARAMETER {0} NOT FOUND"), BETA));
            }
        }
        if(params.containsKey(MAX_DIST)) {
            maxD = ((Number)params.get(MAX_DIST)).doubleValue();
        } else {
            maxD = Double.POSITIVE_INFINITY;
        }
        if(params.containsKey(MIN_PROBA)) {
            minP = ((Number)params.get(MIN_PROBA)).doubleValue();
        } else {
            minP = 0;
        }
        alpha = getAlpha(d, p);
    }

    /**
     * @return the parameters name and value
     */
    public LinkedHashMap<String, Object> getParams() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put(DIST, d);
        params.put(PROBA, p);
        if(hasBeta) {
            params.put(BETA, beta);
        }
        if(minP > 0) {
            params.put(MIN_PROBA, minP);
        } else if(maxD < Double.POSITIVE_INFINITY) {
            params.put(MAX_DIST, maxD);
        } 
        return params;
    }
    
    /**
     * @param linkset the selected linkset
     * @return DistProbaPanel for editing parameters
     */
    public ParamPanel getParamPanel(Linkset linkset) {
        if(hasBeta) {
            return new DistProbaPanel(linkset, d, p, beta);
        } else {
            return new DistProbaPanel(linkset, d, p);
        }
    }
    
    /**
     * Calculates alpha exponent given distance and probability
     * @param dist distance
     * @param proba probability
     * @return alpha exponent
     */
    public static double getAlpha(double dist, double proba) {
        return -Math.log(proba) / dist;
    }
}
