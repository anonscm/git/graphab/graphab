/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.global;

import java.util.LinkedHashMap;
import java.util.Map;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.Feature;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.DeltaGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.ParamPanel;

/**
 * PC decomposition for delta method.
 * This metric works only in delta mode, for calculating the decomposition of the PC metric:
 * dPCIntra, dPCFlux, dPCConnector
 * @author Gilles Vuidel
 */
public class DeltaPCMetric extends AbstractPathMetric {

    private AlphaParamMetric alphaParam = new AlphaParamMetric(false);
    
    @Override
    public boolean isAcceptMethod(Method method) {
        return method == Method.DELTA;
    }

    @Override
    public Double calcPartMetric(Node start, AbstractGraph g) {
        GraphPathFinder finder = g.getPathFinder(start, alphaParam.getMaxCost());
        double sum = 0;
        double srcCapa = Habitat.getPatchCapacity(start);
        for(Node node : finder.getComputedNodes()) { 
            sum += srcCapa * Habitat.getPatchCapacity(node) * Math.exp(-alphaParam.getAlpha()*finder.getCost(node));
        }
        
        return sum;
    }

    @Override
    public void mergePart(Object part) {
        metric += (Double)part;
    }

    @Override
    public Double[] calcMetric(AbstractGraph g) {
        // if an element of the graph has been removed 
        // calculates Intra and Flux for the node
        if(g instanceof DeltaGraph && ((DeltaGraph)g).getRemovedElem() != null) {
            Graphable remElem = ((DeltaGraph)g).getRemovedElem();
            double intra = 0, flux = 0;
            if(remElem instanceof Node) {
                Node remNode = (Node) remElem;
                intra = Math.pow(Habitat.getPatchCapacity(remNode), 2);
                AbstractGraph parentGraph = ((DeltaGraph)g).getParentGraph();
                GraphPathFinder pathFinder = parentGraph.getPathFinder(parentGraph.getNode((Feature) remNode.getObject()), alphaParam.getMaxCost());
                double srcCapa = Habitat.getPatchCapacity(remNode);
                for(Node node : pathFinder.getComputedNodes()) {
                    if (node.getObject() != remNode.getObject()) {
                        flux += srcCapa * Habitat.getPatchCapacity(node) * Math.exp(-alphaParam.getAlpha()*pathFinder.getCost(node));
                    }
                }
                
                flux = 2 * flux;
            }
            return new Double[] {intra, flux, metric+intra+flux};
        } else {
            // no element has been removed from the graph
            // return only the PC value
            return new Double[] {null, null, metric};
        }
    }

    @Override
    public String[] getResultNames(AbstractGraph graph) {
        return new String[] {"PCIntra", "PCFlux", "PCCon"};
    }

    @Override
    public String getShortName() {
        return "dPC";
    }
    
    
    @Override
    public void setParams(Map<String, Object> params) {
        alphaParam.setParams(params);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        return alphaParam.getParams();
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return alphaParam.getParamPanel(linkset);
    }
    
    @Override
    public Type getType() {
        return Type.WEIGHT;
    }
}
