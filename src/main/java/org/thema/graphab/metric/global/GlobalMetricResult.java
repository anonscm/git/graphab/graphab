/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.global;

import java.util.concurrent.CancellationException;
import org.thema.common.ProgressBar;
import org.thema.common.swing.TaskMonitor;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.metric.MetricResult;
import org.thema.graphab.metric.PreCalcMetric;
import org.thema.graphab.metric.PreCalcMetricTask;
import org.thema.parallel.ExecutorService;

/**
 * Class for calculating a global metric on a graph.
 * 
 * @author Gilles Vuidel
 */
public class GlobalMetricResult extends MetricResult<GlobalMetric> {
    private Double[] result;

    public GlobalMetricResult(GlobalMetric metric, AbstractGraph graph) {
        this(metric.getDetailName(), metric, graph);
    }
    
    public GlobalMetricResult(String name, GlobalMetric metric, AbstractGraph graph) {
        super(name, metric, graph, Method.GLOBAL);
        this.result = null;
    }

    public Double[] getResult() {
        return result;
    }

    @Override
    public GlobalMetricResult calculate(boolean threaded, ProgressBar monitor) {
        if(monitor == null) {
            monitor = new TaskMonitor.EmptyMonitor();
        }
        
        monitor.setMaximum(100);
        
        if(getMetric() instanceof PreCalcMetric) {
            PreCalcMetricTask pathTask = new PreCalcMetricTask(getGraph(), (PreCalcMetric)getMetric(), monitor);
            if(threaded) {
                ExecutorService.execute(pathTask);
            } else {
                ExecutorService.executeSequential(pathTask);
            } 
            if(pathTask.isCanceled()) {
                throw new CancellationException();
            }
        }

        result = getMetric().calcMetric(getGraph());
        monitor.setProgress(100);
        return this;
    }

}
