/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.global;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import org.thema.common.ProgressBar;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.links.Path;
import org.thema.graphab.metric.AbstractLocalMetricResult;
import org.thema.graphab.metric.DeltaMetricTask;
import org.thema.parallel.ExecutorService;

/**
 * Class for calculating a local metric on a graph.
 * 
 * @author Gilles Vuidel
 */
public class DeltaMetricResult extends AbstractLocalMetricResult<GlobalMetric> {
    
    private GlobalMetricResult initMetric = null;
    private List ids = null;
    
    public DeltaMetricResult(GlobalMetricResult metric, List ids) {
        this("d_" + metric.getMetric().getDetailName(), metric.getMetric(), metric.getGraph());
        this.initMetric = metric;
        if(ids != null && !ids.isEmpty()) {
            this.ids = ids;
        }
    }
    
    public DeltaMetricResult(GlobalMetric metric, AbstractGraph graph) {
        this("d_" + metric.getDetailName(), metric, graph);
    }
    
    public DeltaMetricResult(String name, GlobalMetric metric, AbstractGraph graph) {
        super(name, metric, graph, Method.DELTA);
    }

    /**
     * Calculates a local metric on a graph patches and links.
     * @param threaded
     * @param monitor the progress monitor
     * @return 
     */
    @Override
    public DeltaMetricResult calculate(boolean threaded, ProgressBar monitor) {
        if(monitor == null) {
            monitor = new TaskMonitor.EmptyMonitor();
        }
        if(initMetric == null) {
            initMetric = new GlobalMetricResult(getMetric(), getGraph()).calculate(threaded, null);
        }

        DeltaMetricTask task = ids != null ? new DeltaMetricTask(monitor, initMetric, ids)
                : new DeltaMetricTask(monitor, initMetric, getNodeFeatures(), getEdgeFeatures());
        if(threaded) {
            ExecutorService.execute(task);
        } else {
            ExecutorService.executeSequential(task);
        }
        if(task.isCanceled()) {
            throw new CancellationException();
        }
        String [] resNames = getMetric().getResultNames(getGraph());
        attrNames = new ArrayList<>();
        for(String name : resNames) {
            attrNames.add(getName() + (resNames.length > 1 ? "|" + name : "") + "_" + getGraph().getName().toLowerCase());
        }
        for(String attrName : attrNames) {
            if(calcEdges()) {
                DefaultFeature.addAttribute(attrName, getGraph().getLinkset().getPaths(), Double.NaN);
            }
            if(calcNodes()) {
                DefaultFeature.addAttribute(attrName, getGraph().getHabitat().getPatches(), Double.NaN);
            }
        }
        Map<Object, Double[]> result = task.getResult();
        if (calcNodes()) {
            for (DefaultFeature f : getNodeFeatures()) {
                if (result.keySet().contains(f.getId())) {
                    Double[] res1 = result.get(f.getId());
                    for (int j = 0; j < attrNames.size(); j++) {
                        f.setAttribute(attrNames.get(j), res1[j]);
                    }
                }
            }
        }
        if (calcEdges()) {
            for (DefaultFeature f : getEdgeFeatures()) {
                if (result.keySet().contains(f.getId())) {
                    Double[] res2 = result.get(f.getId());
                    for (int j = 0; j < attrNames.size(); j++) {
                        f.setAttribute(attrNames.get(j), res2[j]);
                    }
                }
            }                  
        }
 
        monitor.setProgress(100);
        return this;
    }
    
    public List<DefaultFeature> getNodeFeatures() {
        if(ids != null && !(ids.iterator().next() instanceof Integer)) {
            return Collections.emptyList();
        } else {
            return super.getNodeFeatures();
        }
    }
    
    public List<Path> getEdgeFeatures() {
        if(ids != null && !(ids.iterator().next() instanceof String)) {
            return Collections.emptyList();
        } else {
            return super.getEdgeFeatures();
        }
    }
}
