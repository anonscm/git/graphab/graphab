/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.global;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import org.geotools.graph.structure.Node;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.MHMetric;
import static org.thema.graphab.metric.MHMetric.ALL;
import static org.thema.graphab.metric.MHMetric.INTER;
import org.thema.graphab.metric.ParamPanel;

/**
 * ECh global metric
 * 
 * @author Gilles Vuidel
 */
public class EChMetric extends AbstractPathMetric implements MHMetric {

    private AlphaParamMetric alphaParam = new AlphaParamMetric(false);
    
    private String selection;
    
    private Double[] result;
    
    private double[] cum;
    
    private int[][] index;
    
    @Override
    public void startCalc(AbstractGraph g) {
        List<String> names = Arrays.asList(getAllResultNames(g));
        cum = new double[names.size()];
        List<Integer> codes = new ArrayList<>(g.getHabitat().getIdHabitats());
        index = new int[Collections.max(codes)+1][Collections.max(codes)+1];
        for(int c1 : codes) {
            for(int c2 : codes) {
                index[c1][c2] = names.indexOf(getName(c1, c2));
                index[c2][c1] = index[c1][c2];
            }
        }
    }

    @Override
    public double[] calcPartMetric(Node start, AbstractGraph g) {
        double [] sum = new double[cum.length];
        GraphPathFinder finder = g.getPathFinder(start, alphaParam.getMaxCost());
        double srcCapa = MonoHabitat.getPatchCapacity(start);
        int srcCode = MonoHabitat.getPatchIdHab(start);
        for(Node node : finder.getComputedNodes()) { 
            int destCode = MonoHabitat.getPatchIdHab(node);
            sum[index[srcCode][destCode]] += srcCapa * MonoHabitat.getPatchCapacity(node) * Math.exp(-alphaParam.getAlpha()*finder.getCost(node));
        }
        
        return sum;
    }
    
    @Override
    public void mergePart(Object part) {
        double [] sum = (double[]) part;
        for(int i = 0; i < cum.length; i++) {
            cum[i] += sum[i];
        }
    }

    @Override
    public void endCalc(AbstractGraph g) {
        String[] names = getResultNames(g);
        result = new Double[names.length];
        
        if(!isMH()) {
            double sum = 0.0;
            for(double val : cum) {
                sum += val;
            }
            result[0] = Math.sqrt(sum);
        } else {
            if(isSelectAll()) {
                for(int i = 0; i < result.length; i++) {
                    result[i] = Math.sqrt(cum[i]);
                }
            } else if(isSelectInter()) {
                TreeSet<Integer> codes = new TreeSet<>(g.getHabitat().getIdHabitats());
                double sum = 0.0;
                for(int c1 : codes) {
                    for(int c2 : codes.tailSet(c1, false)) {
                        sum += cum[index[c1][c2]];
                    }
                }
                result[0] = Math.sqrt(sum);
            } else { // only one
                String[] codes = names[0].split("/");
                result[0] = Math.sqrt(cum[index[Integer.parseInt(codes[0])][Integer.parseInt(codes[1])]]);
            }
        }
    }
   

    @Override
    public Double[] calcMetric(AbstractGraph g) {
        return result;
    }

    @Override
    public String[] getResultNames(AbstractGraph graph) {
        if(!isMH()) {
            return super.getResultNames(graph);
        }
        if(isSelectAll()) {
            return getAllResultNames(graph);
        } else {
            return new String[] {getSelection()};
        }
    }
    
    private String[] getAllResultNames(AbstractGraph graph) {
         List<Integer> codes = new ArrayList<>(graph.getHabitat().getIdHabitats());
        List<String> names = new ArrayList<>();
        for(int i = 0; i < codes.size(); i++) {
            for(int j = i; j < codes.size(); j++) {
                names.add(getName(codes.get(i), codes.get(j)));
            }
        }
        return names.toArray(new String[names.size()]);
    }

    private String getName(int code1, int code2) {
        return Math.min(code1, code2) + "/" + Math.max(code1, code2);
    }
    
    @Override
    public String getShortName() {
        return "EC";
    }
    
    
    @Override
    public void setParams(Map<String, Object> params) {
        alphaParam.setParams(params);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        return alphaParam.getParams();
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return alphaParam.getParamPanel(linkset);
    }
    
    @Override
    public Type getType() {
        return Type.WEIGHT;
    }
    
    @Override
    public void setSelection(String selection) {
        this.selection = selection;
    }

    @Override
    public String getSelection() {
        return selection;
    }
    
    public boolean isMH() {
        return selection != null;
    }
    
    public boolean isSelectAll() {
        return ALL.equals(selection);
    }
    
    public boolean isSelectInter() {
        return INTER.equals(selection);
    }
    
    public boolean isSelectOne() {
        return isMH() && !isSelectAll() && !isSelectInter();
    }
}
