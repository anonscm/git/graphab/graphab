/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.global;

import java.util.ArrayList;
import org.thema.common.ProgressBar;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.metric.AbstractAttributeMetricResult;

/**
 * Class for calculating a global metric on each component of a graph.
 * 
 * @author Gilles Vuidel
 */
public class CompMetricResult extends AbstractAttributeMetricResult<GlobalMetric> {

    public CompMetricResult(String name, GlobalMetric metric, AbstractGraph graph) {
        super(name, metric, graph, Method.COMP);
    }

    @Override
    public CompMetricResult calculate(boolean threaded, ProgressBar monitor) {
        if(monitor == null) {
            monitor = new TaskMonitor.EmptyMonitor();
        }
        
        String [] resNames = getMetric().getResultNames(getGraph());
        attrNames = new ArrayList<>();
        for(String name : resNames) {
            String attr = getName() + (resNames.length > 1 ? "|" + name : "") + "_" + getGraph().getName().toLowerCase();
            DefaultFeature.addAttribute(attr, getGraph().getComponents().keySet(), Double.NaN);
            attrNames.add(attr);
        }
        monitor.setNote(getFullName());
        monitor.setMaximum(getGraph().getComponents().size());

        for(DefaultFeature comp : getGraph().getComponents().keySet()) {
            GlobalMetricResult gmetric = new GlobalMetricResult("comp", getMetric(), getGraph().getComponentGraphGen(comp));
            gmetric.calculate(threaded, monitor.getSubProgress(1));
            for(int j = 0; j < resNames.length; j++) {
                comp.setAttribute(attrNames.get(j), gmetric.getResult()[j]);
            }
        }
        monitor.setProgress(100);
        return this;
    }

}
