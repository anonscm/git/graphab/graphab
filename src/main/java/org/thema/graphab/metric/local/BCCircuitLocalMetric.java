/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.local;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.geotools.graph.structure.Node;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.Circuit;
import org.thema.graphab.metric.ParamPanel;

/**
 * Betweeness Centrality Circuit version.
 * The shortest path is replaced by the current of the circuit.
 * 
 * @author Gilles Vuidel
 */
public final class BCCircuitLocalMetric extends AbstractBCLocalMetric {
      
    private AlphaParamMetric alphaParam = new AlphaParamMetric();
    
    private transient Circuit circuit;

    @Override
    public Map<Object, Double> calcPartMetric(Node start, AbstractGraph g) {
        GraphPathFinder finder = g.getPathFinder(start, alphaParam.getMaxCost());
        HashMap<Object, Double> result = new HashMap<>();
        double srcCapa = MonoHabitat.getPatchCapacity(start);
        for(Node n2 : finder.getComputedNodes()) {
            if (((Integer)MonoHabitat.getPatch(start).getId()) < (Integer)MonoHabitat.getPatch(n2).getId()) {
                double flow = Math.pow(MonoHabitat.getPatchCapacity(n2) * srcCapa, alphaParam.getBeta()) * Math.exp(-alphaParam.getAlpha() * finder.getCost(n2));
                Map<Object, Double> courant = circuit.computeCourant(start, n2, 1);
                for (Object id : courant.keySet()) {
                    if (result.containsKey(id)) {
                        result.put(id, courant.get(id)*flow + result.get(id));
                    } else {
                        result.put(id, courant.get(id)*flow);
                    }
                }
            }
        }
        return result;
    }
    
    @Override
    public void startCalc(AbstractGraph gen) {
        super.startCalc(gen);
        circuit = new Circuit(gen);
    }

    @Override
    public void endCalc(AbstractGraph g) {
        super.endCalc(g); 
        circuit = null;
    }
    
    @Override
    public String getShortName() {
        return "BCCirc";
    }

    @Override
    public void setParams(Map<String, Object> params) {
        alphaParam.setParams(params);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        return alphaParam.getParams();
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return alphaParam.getParamPanel(linkset);
    }

}
