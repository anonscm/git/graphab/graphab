/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.local;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.Feature;
import org.thema.graph.pathfinder.Path;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.ParamPanel;

/**
 * Betweeness Centrality metric.
 * 
 * @author Gilles Vuidel
 */
public final class BCLocalMetric extends AbstractBCOptimLocalMetric<Node> {

    private AlphaParamMetric alphaParam = new AlphaParamMetric();

    @Override
    public Void calcPartMetric(Node start, AbstractGraph g) {
        GraphPathFinder finder = g.getPathFinder(start, alphaParam.getMaxCost());
        double srcCapa = Habitat.getPatchCapacity(start);
        int startId = (Integer)Habitat.getPatch(start).getId();
        for(Node node : finder.getComputedNodes()) {
            if (startId < (Integer)Habitat.getPatch(node).getId()) {
                Path path = finder.getPath(node);
                if (path == null) {
                    continue;
                }
                double v = Math.pow(Habitat.getPatchCapacity(node) * srcCapa, alphaParam.getBeta())
                        * Math.exp(-alphaParam.getAlpha() * finder.getCost(node));
                
                List<Node> nodes = path.getNodes();
                for (int i = 1; i < nodes.size()-1; i++) {
                    Feature f = (Feature)nodes.get(i).getObject();
                    mapVal.get(f.getId()).add(v);
                }
                for (Edge e : path.getEdges()) {
                    Feature f = (Feature)e.getObject();
                    mapVal.get(f.getId()).add(v);
                }
            }
        }
        return null;
    }

    @Override
    public String getShortName() {
        return "BC";
    }
    
    @Override
    public void setParams(Map<String, Object> params) {
        alphaParam.setParams(params);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        return alphaParam.getParams();
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return alphaParam.getParamPanel(linkset);
    }
    
}
