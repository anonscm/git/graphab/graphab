/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.local;

import java.util.ArrayList;
import java.util.List;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Node;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.SimpleParallelTask;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project.Method;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.metric.AbstractLocalMetricResult;
import org.thema.graphab.metric.PreCalcMetric;
import org.thema.graphab.metric.PreCalcMetricTask;
import org.thema.parallel.ExecutorService;

/**
 * Class for calculating a local metric on a graph.
 * 
 * @author Gilles Vuidel
 */
public class LocalMetricResult extends AbstractLocalMetricResult<LocalMetric> {

    public LocalMetricResult(LocalMetric metric, AbstractGraph graph) {
        this(metric.getDetailName(), metric, graph);
    }
    
    public LocalMetricResult(String name, LocalMetric metric, AbstractGraph graph) {
        super(name, metric, graph, Method.LOCAL);
    }

    /**
     * Calculates a local metric on a graph patches and links.
     * @param threaded
     * @param monitor the progress monitor
     * @return 
     */
    @Override
    public LocalMetricResult calculate(boolean threaded, ProgressBar monitor) {
        if(monitor == null) {
            monitor = new TaskMonitor.EmptyMonitor();
        }
        
        monitor.setNote(getFullName());
        monitor.setMaximum(100);
        if(getMetric() instanceof PreCalcMetric) {
            PreCalcMetricTask pathTask = new PreCalcMetricTask(getGraph(), (PreCalcMetric)getMetric(), monitor.getSubProgress(99));
            if(threaded) {
                ExecutorService.execute(pathTask);
            } else {
                ExecutorService.executeSequential(pathTask);
            }
            monitor = monitor.getSubProgress(1);
        }
        
        String [] resNames = getMetric().getResultNames(getGraph());
        attrNames = new ArrayList<>();
        for(String name : resNames) {
            attrNames.add(getName() + (resNames.length > 1 || !name.equals(getMetric().getShortName()) ? "|" + name : "") + "_" + getGraph().getName().toLowerCase());
        }
        
        monitor.setMaximum((getMetric().calcNodes() ? getGraph().getGraph().getNodes().size() : 0) +
                (getMetric().calcEdges() ? getGraph().getGraph().getEdges().size() : 0));

        if(calcNodes()) {
            for(String attrName : attrNames) {
                DefaultFeature.addAttribute(attrName, getGraph().getHabitat().getPatches(), Double.NaN);
            }
            List<Node> nodes = new ArrayList<>(getGraph().getGraph().getNodes());
            SimpleParallelTask<Node> task = new SimpleParallelTask<Node>(nodes, monitor.getSubProgress(nodes.size())) {
                @Override
                protected void executeOne(Node node) {
                    DefaultFeature f = (DefaultFeature)node.getObject();
                    if(filter == null || accept(f)) {
                        Double[] val = getMetric().calcMetric(node, getGraph());
                        for(int i = 0; i < attrNames.size(); i++) {
                            f.setAttribute(attrNames.get(i), val[i]);                              
                        }
                    }
                }
            };

            try {
                new ParallelFExecutor(task).executeAndWait();
            } catch(Exception ex) {
                for(String attrName : attrNames) {
                    DefaultFeature.removeAttribute(attrName, getGraph().getHabitat().getPatches());          
                }
                throw new RuntimeException(ex);
            }
        }

        if(calcEdges()) {
            for(String attrName : attrNames) {
                DefaultFeature.addAttribute(attrName, getGraph().getLinkset().getPaths(), Double.NaN);
            }
            
            List<Edge> edges = new ArrayList<>(getGraph().getGraph().getEdges());
            SimpleParallelTask<Edge> task = new SimpleParallelTask<Edge>(edges, monitor.getSubProgress(edges.size())) {
                @Override
                protected void executeOne(Edge edge) {
                    DefaultFeature f = (DefaultFeature)edge.getObject();
                    if(filter == null || accept(f)) {
                        Double[] val = getMetric().calcMetric(edge, getGraph());
                        for(int i = 0; i < attrNames.size(); i++) {
                            f.setAttribute(attrNames.get(i), val[i]);                              
                        }
                    }
                }   
            };

            try {
                new ParallelFExecutor(task).executeAndWait();
            } catch(Exception ex) {
                for(String attrName : attrNames) {
                    DefaultFeature.removeAttribute(attrName, getGraph().getLinkset().getPaths());          
                }
                throw new RuntimeException(ex);
            }
        }
        monitor.setProgress(100);
        return this;
    }

    @Override
    public boolean calcNodes() {
        return getMetric().calcNodes();
    }

    @Override
    public boolean calcEdges() {
        return getMetric().calcEdges();
    }
    
}
