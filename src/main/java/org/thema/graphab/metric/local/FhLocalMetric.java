/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.local;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.ParamPanel;

/**
 * Flux metric.
 * 
 * @author Gilles Vuidel
 */
public final class FhLocalMetric extends AbstractMHLocalMetric {

    private AlphaParamMetric alphaParam = new AlphaParamMetric();

    @Override
    public Double[] calcMetric(Graphable g, AbstractGraph gen) {
        final int startCode = Habitat.getPatchIdHab((Node)g);
        final int filterCode = isSelectOne() ? Integer.parseInt(getSelection()) : -1;
        final List<Integer> codes = new ArrayList<>(gen.getHabitat().getIdHabitats());
        Collections.sort(codes);
        final Double[] sum = isSelectAll() ? new Double[codes.size()] : new Double[1];
        Arrays.fill(sum, 0.0);
        GraphPathFinder pathFinder = gen.getPathFinder((Node)g, alphaParam.getMaxCost());
        for(Node node : pathFinder.getComputedNodes()) {
            int code = Habitat.getPatchIdHab(node);
            if (node != g && (!isMH() || isSelectAll() || (isSelectInter() && startCode != code) || (isSelectOne() && code == filterCode))) {
                sum[sum.length == 1 ? 0 : codes.indexOf(code)] += Math.exp(-alphaParam.getAlpha() * pathFinder.getCost(node)) * Math.pow(MonoHabitat.getPatchCapacity(node), alphaParam.getBeta());            
            }
        }            
        
        return sum;
    }

    @Override
    public String[] getResultNames(AbstractGraph graph) {
        if(!isMH()) {
            return super.getResultNames(graph);
        }
        if(isSelectAll()) {
            List<Integer> codes = new ArrayList<>(graph.getHabitat().getIdHabitats());
            Collections.sort(codes);
            String[] names = new String[codes.size()];
            for(int i = 0; i < codes.size(); i++) {
                names[i] = codes.get(i)+"";
            }
            return names;
        } else {
            return new String[] {getSelection()};
        }
    }

    @Override
    public String getShortName() {
        return "F";
    }

    @Override
    public boolean calcNodes() {
        return true;
    }
    
    @Override
    public void setParams(Map<String, Object> params) {
        alphaParam.setParams(params);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        return alphaParam.getParams();
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return alphaParam.getParamPanel(linkset);
    }
    
    @Override
    public Type getType() {
        return Type.WEIGHT;
    }

}
