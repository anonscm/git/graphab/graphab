/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.local;

import java.util.HashMap;
import java.util.concurrent.atomic.DoubleAdder;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.Feature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.metric.PreCalcMetric;

/**
 * Base class for all Betweeness Centrality local metric.Optimized version with atomic adder
 * 
 * @author Gilles Vuidel
 * @param <T>
 */
public abstract class AbstractBCOptimLocalMetric<T> extends LocalSingleMetric implements PreCalcMetric<T> {
  
    protected transient HashMap<Object, DoubleAdder> mapVal;

    @Override
    public void endCalc(AbstractGraph g) {
    }

    @Override
    public void startCalc(AbstractGraph g) {
        mapVal = new HashMap<>();
        for(Node node : g.getNodes()) {
            mapVal.put(((Feature)node.getObject()).getId(), new DoubleAdder());
        }
        for(Edge edge : g.getEdges()) {
            mapVal.put(((Feature)edge.getObject()).getId(), new DoubleAdder());
        }
    }

    @Override
    public void mergePart(Object part) {  
    }
    
    @Override
    public double calcSingleMetric(Graphable g, AbstractGraph gen) {
        return mapVal.get(((Feature)g.getObject()).getId()).doubleValue();
    }

    @Override
    public boolean calcNodes() {
        return true;
    }

    @Override
    public boolean calcEdges() {
        return true;
    }
    
    @Override
    public PreCalcMetric.TypeParam getTypeParam() {
        return PreCalcMetric.TypeParam.NODE;
    } 
    
    @Override
    public Type getType() {
        return Type.WEIGHT;
    }
}
