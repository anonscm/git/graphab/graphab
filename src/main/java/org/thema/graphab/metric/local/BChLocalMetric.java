/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.local;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.DoubleAdder;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.Feature;
import org.thema.graph.pathfinder.Path;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.ParamPanel;
import org.thema.graphab.metric.PreCalcMetric;

/**
 * Betweeness Centrality metric.
 * 
 * @author Gilles Vuidel
 */
public final class BChLocalMetric extends AbstractMHLocalMetric implements PreCalcMetric<Node> {

    private AlphaParamMetric alphaParam = new AlphaParamMetric();
    
    private transient Map<Object, Map<String, DoubleAdder>> mapVal;

    @Override
    public void endCalc(AbstractGraph g) {
    }

    @Override
    public void startCalc(AbstractGraph g) {
        String[] names = getResultNames(g);
        mapVal = new HashMap<>();
        for(Node node : g.getNodes()) {
            HashMap<String, DoubleAdder> map = new HashMap<>();
            for(String name : names) {
                map.put(name, new DoubleAdder());
            }
            mapVal.put(((Feature)node.getObject()).getId(), map);
        }
        for(Edge edge : g.getEdges()) {
            HashMap<String, DoubleAdder> map = new HashMap<>();
            for(String name : names) {
                map.put(name, new DoubleAdder());
            }
            mapVal.put(((Feature)edge.getObject()).getId(), map);
        }
    }

    @Override
    public void mergePart(Object part) {  
    }
    
    @Override
    public Double[] calcMetric(Graphable g, AbstractGraph gen) {
        Map<String, DoubleAdder> mapRes = mapVal.get(((Feature)g.getObject()).getId());
        String[] names = getResultNames(gen);
        Double[] res = new Double[names.length];
        int i = 0;
        for(String name : names) {
            res[i++] = mapRes.get(name).doubleValue();
        }
        return res;
    }

    @Override
    public boolean calcNodes() {
        return true;
    }

    @Override
    public boolean calcEdges() {
        return true;
    }
    
    @Override
    public Void calcPartMetric(Node start, AbstractGraph g) {
        List<String> names = Arrays.asList(getResultNames(g));
        GraphPathFinder finder = g.getPathFinder(start, alphaParam.getMaxCost());
        double srcCapa = MonoHabitat.getPatchCapacity(start);
        int srcCode = MonoHabitat.getPatchIdHab(start);
        int startId = (Integer)Habitat.getPatch(start).getId();
        for(Node node : finder.getComputedNodes()) {
            if (startId < (Integer)Habitat.getPatch(node).getId()) {
                int destCode = Habitat.getPatchIdHab(node);
                String code = getName(srcCode, destCode);
                if(isMH() && !names.contains(code) && !isSelectInter() || isSelectInter() && srcCode == destCode) {
                    continue;
                }
                if(names.size() == 1) {
                    code = names.get(0);
                }

                Path path = finder.getPath(node);
                if (path == null) {
                    continue;
                }
                
                double v = Math.pow(Habitat.getPatchCapacity(node) * srcCapa, alphaParam.getBeta())
                        * Math.exp(-alphaParam.getAlpha() * finder.getCost(node));
                
                List<Node> nodes = path.getNodes();
                for (int i = 1; i < nodes.size()-1; i++) {
                    Feature f = (Feature)nodes.get(i).getObject();
                    mapVal.get(f.getId()).get(code).add(v);
                }
                for (Edge e : path.getEdges()) {
                    Feature f = (Feature)e.getObject();
                    mapVal.get(f.getId()).get(code).add(v);
                }
            }
        }
        return null;
    }
    
    @Override
    public String[] getResultNames(AbstractGraph graph) {
        if(!isMH()) {
            return super.getResultNames(graph);
        }
        if(isSelectAll()) {
            List<Integer> codes = new ArrayList<>(graph.getHabitat().getIdHabitats());
            List<String> names = new ArrayList<>();
            for(int i = 0; i < codes.size(); i++) {
                for(int j = i; j < codes.size(); j++) {
                    names.add(getName(codes.get(i), codes.get(j)));
                }
            }
            return names.toArray(new String[names.size()]);
        } else {
            return new String[] {getSelection()};
        }
    }

    private String getName(int code1, int code2) {
        return Math.min(code1, code2) + "/" + Math.max(code1, code2);
    }
    
    @Override
    public String getShortName() {
        return "BC";
    }
    
    @Override
    public void setParams(Map<String, Object> params) {
        alphaParam.setParams(params);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        return alphaParam.getParams();
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return alphaParam.getParamPanel(linkset);
    }
    
    @Override
    public Type getType() {
        return Type.WEIGHT;
    }

    @Override
    public TypeParam getTypeParam() {
        return TypeParam.NODE;
    }    
    
}
