/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.metric.local;

import java.util.LinkedHashMap;
import java.util.Map;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.thema.common.collection.TreeMapList;
import org.thema.common.param.ReflectObject.Comment;
import org.thema.common.param.ReflectObject.Name;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.DefaultParamPanel;
import org.thema.graphab.metric.ParamPanel;

/**
 * Reachable ressources
 * 
 * @author Gilles Vuidel
 */
public final class RrLocalMetric extends LocalSingleMetric  {

    public static final String MAX_DIST = "dist";
    public static final String MAX_CAPA = "capa";

    @Name("dist")
    @Comment("Maximum distance")
    private double maxDist;
    @Name("capa")
    @Comment("Maximum cumulative capacity")
    private double maxCapa;

    @Override
    public double calcSingleMetric(Graphable g, AbstractGraph gen) {
        GraphPathFinder pathFinder = gen.getPathFinder((Node)g, maxDist);
        TreeMapList<Double, Double> distCapa = new TreeMapList<>();
        for(Node node : pathFinder.getComputedNodes()) {
            distCapa.putValue(pathFinder.getCost(node), Habitat.getPatchCapacity(node));
        }            
        
        double dist = 0, capa = 0;
        double sum = 0;
        for(double d : distCapa.keySet()) {
            sum += (d - dist) * capa;
            dist = d;
            for(double ca : distCapa.get(d)) {            
                capa += ca;
            }
            if(capa > maxCapa) {
                capa = maxCapa;
                break;
            }
        }
        if(dist < maxDist) {
            sum += (maxDist-dist) * capa;
        }
        
        return sum / (maxDist*maxCapa);
    }

    @Override
    public String getShortName() {
        return "Rr";
    }

    @Override
    public boolean calcNodes() {
        return true;
    }
    
    @Override
    public void setParams(Map<String, Object> params) {
       maxDist = (Double)params.get(MAX_DIST);
       maxCapa = (Double)params.get(MAX_CAPA);
    }

    @Override
    public LinkedHashMap<String, Object> getParams() {
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put(MAX_DIST, maxDist);
        params.put(MAX_CAPA, maxCapa);
        return params;
    }

    @Override
    public ParamPanel getParamPanel(Linkset linkset) {
        return new DefaultParamPanel(this);
    }
    
    @Override
    public Type getType() {
        return Type.WEIGHT;
    }
    
}
