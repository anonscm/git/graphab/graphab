/*
 * Copyright (C) 2023 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.metric.local;

import org.thema.graphab.metric.MHMetric;

/**
 *
 * @author gvuidel
 */
public abstract class AbstractMHLocalMetric extends LocalMetric implements MHMetric {

    private String selection;
    
    @Override
    public void setSelection(String selection) {
        this.selection = selection;
    }

    @Override
    public String getSelection() {
        return selection;
    }
    
    public boolean isMH() {
        return selection != null;
    }
    
    public boolean isSelectAll() {
        return ALL.equals(selection);
    }
    
    public boolean isSelectInter() {
        return INTER.equals(selection);
    }
    
    public boolean isSelectOne() {
        return isMH() && !isSelectAll() && !isSelectInter();
    }
}
