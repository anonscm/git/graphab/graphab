/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab;

import org.thema.graphab.habitat.CapaPatchDialog;
import org.locationtech.jts.geom.Geometry;
import java.awt.geom.Rectangle2D;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.math.MathException;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.SchemaException;
import org.geotools.graph.structure.Edge;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.thema.common.Config;
import org.thema.common.ConsoleProgress;
import org.thema.common.JTS;
import org.thema.common.ProgressBar;
import org.thema.common.collection.HashMap2D;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.SimpleParallelTask;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOFeature;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.drawshape.image.RasterShape;
import org.thema.drawshape.layer.RasterLayer;
import org.thema.drawshape.style.RasterStyle;
import org.thema.graph.Modularity;
import org.thema.graph.pathfinder.EdgeWeighter;
import org.thema.graphab.addpatch.AddPatchCommand;
import org.thema.graphab.dataset.VectorDataset;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.AbstractGraph.Type;
import org.thema.graphab.graph.DeltaAddGraph;
import org.thema.graphab.graph.DefaultGraph;
import org.thema.graphab.graph.ModifiedGraph;
import org.thema.graphab.graph.ModularityGraph;
import org.thema.graphab.graph.MultiGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.habitat.MonoVectorHabitat;
import org.thema.graphab.habitat.MultiHabitat;
import org.thema.graphab.links.CircuitLinkset;
import org.thema.graphab.links.CircuitRaster;
import org.thema.graphab.links.CostLinkset;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Linkset.Distance;
import org.thema.graphab.links.Linkset.Topology;
import org.thema.graphab.links.Path;
import org.thema.graphab.links.RasterLinkset;
import org.thema.graphab.metric.AlphaParamMetric;
import org.thema.graphab.metric.DeltaMetricTask;
import org.thema.graphab.metric.MHMetric;
import org.thema.graphab.metric.Metric;
import org.thema.graphab.metric.MetricResult;
import org.thema.graphab.metric.global.CompMetricResult;
import org.thema.graphab.metric.global.DeltaMetricResult;
import org.thema.graphab.metric.global.GlobalMetric;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.graphab.metric.local.LocalMetric;
import org.thema.graphab.metric.local.LocalMetricResult;
import org.thema.graphab.model.DistribModel;
import org.thema.graphab.model.DistribModel.Agreg;
import org.thema.graphab.model.Logistic;
import org.thema.graphab.util.RSTGridReader;
import org.thema.graphab.util.Range;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.ParallelExecutor;

/**
 * Command Line Interface class.
 * 
 * @author Gilles Vuidel
 */
public class CLITools {

    private Project project;
    private List<Habitat> useHabitats = new ArrayList<>();
    private List<Linkset> useLinksets = new ArrayList<>();
    private List<AbstractGraph> useGraphs = new ArrayList<>();
    private List<MetricResult> useMetrics = new ArrayList<>();
    private boolean save = true;
    
    /**
     * Executes the commands from the command line
     * @param argArray the command line arguments
     * @return the last project created or loaded in the command line
     * @throws IOException
     * @throws SchemaException
     * @throws MathException 
     */
    public Project execute(String [] argArray) throws IOException, SchemaException, MathException {
        if(argArray[0].equals("--help")) { //NOI18N
            System.out.println("Usage :\njava -jar graphab.jar --metrics\n" + //NOI18N
                    "java -jar graphab.jar [-proc n] --create prjname landrasterfile [nodata=val] [dir=path]\n" + //NOI18N
                    "java -jar graphab.jar [-mpi | -proc n] [-nosave] [-distconv excost=val] --project prjfile.xml command1 [command2 ...]\n" + //NOI18N
                    "Commands list :\n" + //NOI18N
                    "--show\n" +  //NOI18N
                    "--show_habitats --show_linksets --show_graphs --show_metrics\n" +  //NOI18N
                    "--dem rasterfile\n" + //NOI18N
                    "--habitat name=habitatname codes=code1,...,coden [minarea=val] [maxsize=val] [con8] [novoronoi]\n" + //NOI18N
                    "--vhabitat name=habitatname file=vectorlayer|from=habitatname [mincapa=val] [capa=field] [novoronoi]\n" + //NOI18N
                    "--usehabitat ALL|habitat1,...,habitatn\n" + //NOI18N
                    "--removehabitat ALL|[habitat1,...,habitatn]\n" + //NOI18N
                    "--mergehabitat [habitat1,...,habitatn] [novoronoi]\n" +  //NOI18N
                    "--capa [area [exp=value] [code1,..,coden=weight ...]] | [file=capacity.csv id=fieldname capa=fieldname] | [maxcost=[{]valcost[}] codes=code1,code2,...,coden [weight]]\n" +                     //NOI18N
                    "--linkset distance=euclid|cost [name=linkname] [topo=planar|complete|planarcost] [inter] [filter=condition] [maxcost=valcost] [slope=coef] [remcrosspath|nopathsaved] [[code1,..,coden=cost1 ...] codei,..,codej=min:inc:max | extcost=rasterfile]\n" + //NOI18N
                    "--uselinkset ALL|linkset1,...,linksetn\n" + //NOI18N
                    "--removelinkset ALL|[linkset1,...,linksetn]\n" + //NOI18N
                    "--graph [name=graphname] [nointra] [threshold=[{]min:inc:max[}]]\n" + //NOI18N
                    "--usegraph ALL|graph1,...,graphn\n" + //NOI18N
                    "--removegraph ALL|[graph1,...,graphn]\n" + //NOI18N
                    "--mergegraph [name=graphname] [graph1,...,graphn]\n" +                     //NOI18N
                    "--cluster d=val p=val [beta=val] [nb=val]\n" +     //NOI18N
                    "--metapatch [mincapa=value] [novoronoi]\n" +             //NOI18N
                    "--corridor maxcost=[{]min:inc:max[}] [format=raster|vector] [beta=exp|var=name d=[{]val[}] p=val]\n" +                     //NOI18N
                    "--dataset name=dataname file=vectorlayer.gpkg\n" + //NOI18N
                    "--removedataset [dataset1,...,datasetn]\n" + //NOI18N
                    "--distance_matrix type=space|graph distance=euclidean|leastcost|circuit|flow|circuitflow [d=val p=val]\n" + //NOI18N
                    "--gmetric global_metric_name [resfile=file.txt] [mh=all|inter|val] [param1=[{]min:inc:max[}] [param2=[{]min:inc:max[}] ...]]\n" + //NOI18N
                    "--cmetric global_metric_name [mh=all|inter|val] [param1=[{]min:inc:max[}] [param2=[{]min:inc:max[}] ...]]\n" + //NOI18N
                    "--lmetric local_metric_name [name=resname] [mh=all|inter|val] [filter=condition] [param1=[{]min:inc:max[}] [param2=[{]min:inc:max[}] ...]]\n" + //NOI18N
                    "--usemetric metric1,...,metricn\n" + //NOI18N
                    "--removemetric [metric1,...,metricn]\n" + //NOI18N
                    "--interp [var=patch_var_name d=val p=val] [name=rastername] [resol=val] [multi[=dist_max] [ag=sum|max|avg]]\n" +                     //NOI18N
                    "--model variable distW=[{]min:inc:max[}] [vars=var1,...,varn] [raster=r1,...,rn]\n" + //NOI18N
                    "--delta [filter=condition | [obj=patch|link [sel=id1,id2,...,idn | fsel=file.txt]]\n" +                     //NOI18N
                    "--addpatch npatch=val [hab=name] gridres=min:inc:max [capa=capa_file] | patchfile=file.gpkg [capa=capa_field]\n" + //NOI18N
                    "--remelem nstep global_metric_name [param1=val ...] obj=patch|link [sel=id1,id2,...,idn|fsel=file.txt]\n" + //NOI18N
                    "--gtest nstep global_metric_name [param1=val ...] obj=patch|link sel=id1,id2,...,idn|fsel=file.txt\n" + //NOI18N
                    "--gremove global_metric_name [param1=val ...] [patch=id1,id2,...,idn|fpatch=file.txt] [link=id1,id2,...,idm|flink=file.txt]\n" + //NOI18N
                    "--landmod zone=filezones.gpkg id=fieldname code=fieldname [sel=id1,id2,...,idn]\n" + //NOI18N
                    "--landmodgm zone=filezones.gpkg id=fieldname code=fieldname [sel=id1,id2,...,idn]\n" + //NOI18N

                    "\nmin:inc:max -> val1,val2,val3..."); //NOI18N
            return null;
        }
        if(argArray[0].equals("--advanced")) { //NOI18N
            System.out.println("Advanced commands :\n" + //NOI18N
                    "[-distconv mincost=val]\n" +
                    "--linkset distance=euclid [name=linkname] [topo=planar|complete|planarcost] [maxcost=valcost] [nopathsaved] [height=rasterfile]\n" + //NOI18N
                    "--linkset distance=circuit [name=linkname] [topo=planar|complete|planarcost] [slope=coef] [[code1,..,coden=cost1 ...] codei,..,codej=min:inc:max | extcost=rasterfile]\n" + //NOI18N
                    "--buffer file=layer.gpkg maxcost=[{]min:inc:max[}]\n" +
                    "--extractpathcost\n" +
                    "--circuit [corridor=current_max] [optim] [con4] [link=id1,id2,...,idm|flink=file.txt]\n"); //NOI18N
            return null;
        }
        
        if(argArray[0].equals("--metrics")) { //NOI18N
            showMetrics();
            return null;
        }
        
        List<String> args = new ArrayList<>(Arrays.asList(argArray));
        String p = args.remove(0);
        
        // global options
        while(p != null && !p.startsWith("--")) { //NOI18N
            switch (p) {
                case "-proc": //NOI18N
                    String n = args.remove(0);
                    ParallelFExecutor.setNbProc(Integer.parseInt(n));
                    ParallelExecutor.setNbProc(Integer.parseInt(n));
                    break;
                case "-nosave": //NOI18N
                    save = false;
                    break;
                case "-distconv": //NOI18N
                    String arg = args.remove(0);
                    if(arg.startsWith("excost=")) {
                        double costMax = Double.parseDouble(arg.split("=")[1]); //NOI18N
                        if(costMax > 0) {
                            Range.costMax = costMax;
                        }
                    } else if(arg.startsWith("mincost=")) {
                        Range.minCost = Double.parseDouble(arg.split("=")[1]); //NOI18N
                    }
                    break;
                default:
                    throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN OPTION {0}"), p));
            }
            p = args.isEmpty() ? null : args.remove(0);
        }
        
        Config.setProgressBar(new ConsoleProgress());
        TaskMonitor.setHeadlessStream(new PrintStream(File.createTempFile("java", "monitor"))); //NOI18N
        if(p == null) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO COMMAND TO EXECUTE"));
        }
        
        switch (p) {
            case "--create": //NOI18N
                project = createProject(args);
                break;
            case "--project": //NOI18N
                p = args.remove(0);
                project = Project.loadProject(new File(p), false);
                break;
            default:
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN COMMAND {0}"), p));
        }
        
        p = args.isEmpty() ? "--show" : args.remove(0); //NOI18N

        // treat each command
        while(p != null) {
            if(p.equals("--model")) { //NOI18N
                batchModel(args);
            } else if(p.equals("--habitat")) { //NOI18N
                createHabitat(args);
            } else if(p.equals("--vhabitat")) { //NOI18N
                createVectorHabitat(args);
            } else if(p.equals("--mergehabitat")) { //NOI18N
                mergeHabitat(args);
            } else if(p.equals("--linkset")) { //NOI18N
                createLinkset(args);
            } else if(p.equals("--graph")) { //NOI18N
                createGraph(args);
            } else if(p.equals("--mergegraph")) { //NOI18N
                mergeGraph(args);
            } else if(p.equals("--dataset")) { //NOI18N
                createDataset(args);
            } else if(p.equals("--gmetric")) { //NOI18N
                calcGlobalMetric(args);
            } else if(p.equals("--cmetric")) { //NOI18N
                calcCompMetric(args);
            } else if(p.equals("--lmetric")) { //NOI18N
                calcLocalMetric(args);
            } else if(p.equals("--delta")) { //NOI18N
                calcDeltaMetric(args);
            } else if(p.equals("--gtest")) { //NOI18N
                addGlobal(args);
            } else if(p.equals("--addpatch")) { //NOI18N
                addPatch(args);
            } else if(p.equals("--remelem")) { //NOI18N
                remElem(args);
            } else if(p.equals("--gremove")) { //NOI18N
                remGlobal(args);
            } else if(p.equals("--circuit")) { //NOI18N
                circuit(args);
            } else if(p.equals("--corridor")) { //NOI18N
                corridor(args);
            } else if(p.equals("--metapatch")) { //NOI18N
                createMetapatch(args);
            } else if(p.equals("--cluster")) { //NOI18N
                clustering(args);
            } else if(p.equals("--capa")) { //NOI18N
                calcCapa(args);
            } else if(p.equals("--landmod")) { //NOI18N
                landmod(args);
            } else if(p.equals("--landmodgm")) { //NOI18N
                landmodgm(args);
            } else if(p.equals("--interp")) { //NOI18N
                interp(args);
            } else if(p.startsWith("--use")) { //NOI18N
                useObj(p, args);
            } else if(p.startsWith("--remove")) { //NOI18N
                remObj(p, args);
            } else if(p.startsWith("--show")) { //NOI18N
                showElem(p);
            } else if(p.equals("--dem")) { //NOI18N
                setDEM(args);
            } else if(p.equals("--distance_matrix")) { //NOI18N
                calcDataDistance(args);
            } else if(p.equals("--topo")) { //NOI18N
                createTopoLinks(args);
            } else if(p.equals("--buffer")) { //NOI18N
                createBuffer(args);
            } else if(p.equals("--extractpathcost")) { //NOI18N
                extractPathCost(args);
            } else {
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN COMMAND {0}"), p));
            }
            p = args.isEmpty() ? null : args.remove(0);
        }

        return project;
    }

    private void showElem(String show) {
        if("--show".equals(show)) {
            System.out.println("\n===== Habitats ====="); //NOI18N
            for(AbstractMonoHabitat hab : project.getHabitats()) {
                System.out.println(hab.getIdHab() + " - " + hab.getName()); //NOI18N
            }
            System.out.println("\n" + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== LINKSETS ====="));
            for(Linkset cost : project.getLinksets()) {
                System.out.println(cost.getName());
            }
            System.out.println("\n" + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== GRAPHS ====="));
            for(AbstractGraph graph : project.getGraphs()) {
                System.out.println(graph.getName());
            }
            System.out.println("\n" + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== METRICS ====="));
            for(AbstractGraph graph : project.getGraphs()) {
                for(MetricResult metric : graph.getMetrics()) {
                    System.out.println(metric.getFullName());
                }
            }
        } else if("--show_habitats".equals(show)) {
            for(AbstractMonoHabitat hab : project.getHabitats()) {
                System.out.println("\n===== Habitats ====="); //NOI18N
                System.out.println(hab.getIdHab() + " - " + hab.getName()); //NOI18N
                System.out.println(hab.getInfo());
            }
        } else if("--show_linksets".equals(show)) {
            for(Linkset linkset : project.getLinksets()) {
                System.out.println("\n" + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== LINKSETS ====="));
                System.out.println(linkset.getInfo());
            }
        } else if("--show_graphs".equals(show)) {
            for(AbstractGraph graph : project.getGraphs()) {
                System.out.println("\n" + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== GRAPHS ====="));
                System.out.println(graph.getInfo());
            }
        } else if("--show_metrics".equals(show)) {
             System.out.println("\n" + java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== METRICS ====="));
            for(AbstractGraph graph : project.getGraphs()) {
                for(MetricResult metric : graph.getMetrics()) {
                    System.out.println(metric.getFullName());
                    System.out.println(MetricResult.getInfo(metric));
                }
            }
        }
    }

    private void showMetrics() {
        System.out.println(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== GLOBAL METRICS ====="));
        for(Metric indice : Project.GLOBAL_METRICS) {
            System.out.println(indice.getShortName() + " - " + indice.getName()); //NOI18N
            if(indice.hasParams()) {
                System.out.println("\tparams : " + Arrays.deepToString(indice.getParams().keySet().toArray())); //NOI18N
            }
        }
        System.out.println(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("===== LOCAL METRICS ====="));
        for(Metric indice : Project.LOCAL_METRICS) {
            System.out.println(indice.getShortName() + " : " + indice.getName()); //NOI18N
            if(indice.hasParams()) {
                System.out.println("\tparams : " + Arrays.deepToString(indice.getParams().keySet().toArray())); //NOI18N
            }
        }
    }
    
    private void useObj(String p, List<String> args) {
        String param = args.remove(0);
        String [] toks = param.equals("ALL") ? new String[0] : param.split(","); //NOI18N
        switch (p) {
            case "--usedataset": //NOI18N
                useHabitats.clear();
                for(String tok : toks) {
                    if(project.getHabitat(tok) != null && project.getHabitat(tok) instanceof VectorDataset) {
                        useHabitats.add(project.getHabitat(tok));
                    } else {
                        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN DATASET {0}"), tok));
                    }
                }   break;
            case "--usehabitat": //NOI18N
                useHabitats.clear();
                for(String tok : toks) {
                    if(project.isHabitatExists(tok)) {
                        useHabitats.add(project.getHabitat(tok));
                    } else {
                        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN HABITAT {0}"), tok));
                    }
                }   break;
            case "--uselinkset": //NOI18N
                useLinksets.clear();
                for(String tok : toks) {
                    if(project.isLinksetExists(tok)) {
                        useLinksets.add(project.getLinkset(tok));
                    } else {
                        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN LINKSET {0}"), tok));
                    }
                }   break;
            case "--usegraph": //NOI18N
                useGraphs.clear();
                for(String tok : toks) {
                    if(project.isGraphExists(tok)) {
                        useGraphs.add(project.getGraph(tok));
                    } else {
                        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN GRAPH {0}"), tok));
                    }
                }   break;
            case "--usemetric":
                useMetrics.clear();
                for(String tok : toks) {
                    for(AbstractGraph g : getGraphs()) {
                        if(g.getMetricsName().contains(tok)) {
                            useMetrics.add(g.getMetric(tok));
                        }
                    }
                }
                if(useMetrics.isEmpty()) {
                    throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN METRICS {0}"), new Object[]{toks}));
                }
                break;
            default:
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN COMMAND {0}"), p));
        }
    }

    private void remObj(String p, List<String> args) throws IOException, SchemaException {
        Set<String> objNames = null;
        if(!args.isEmpty() && !args.get(0).startsWith("--")) { //NOI18N
            String param = args.remove(0);
            if(!param.equals("ALL")) { //NOI18N
                objNames = new HashSet<>(Arrays.asList(param.split(","))); //NOI18N
            }
        }
        switch (p) {
            case "--removedataset": //NOI18N
                for(String tok : objNames == null ? project.getHabitatNames(): objNames) {
                    if(project.isHabitatExists(tok) && project.getHabitat(tok) instanceof VectorDataset) {
                        project.removeHabitat(tok, true);
                    } 
                }   break;
            case "--removehabitat": //NOI18N
                for(String tok : objNames == null ? project.getHabitatNames(): objNames) {
                    if(project.isHabitatExists(tok)) {
                        project.removeHabitat(tok, true);
                    } 
                }   break;
            case "--removelinkset": //NOI18N
                for(String tok : objNames == null ? project.getLinksetNames() : objNames) {
                    if(project.isLinksetExists(tok)) {
                        project.removeLinkset(tok, true);
                    } 
                }   break;
            case "--removegraph": //NOI18N
                for(String tok : objNames == null ? project.getGraphNames() : objNames) {
                    if(project.isGraphExists(tok)) {
                        project.removeGraph(tok, true);
                    } 
                }   break;
            case "--removemetric": //NOI18N
                for(String tok : objNames == null ? project.getMetricNames() : objNames) {
                    if(project.getMetricNames().contains(tok.toLowerCase())) {
                        project.removeMetric(tok);
                    } 
                }   break;
        }
    }
    
    private Collection<? extends Habitat> getHabitats() {
        return useHabitats.isEmpty() ? project.getHabitats() : useHabitats;
    }
    
    private Collection<Linkset> getLinksets() {
        return useLinksets.isEmpty() ? project.getLinksets() :  useLinksets;
    }

    private Collection<AbstractGraph> getGraphs() {
        return useGraphs.isEmpty() ? project.getGraphs() :  useGraphs;
    }
    
    private Collection<MetricResult> getMetrics() {
        return useMetrics.isEmpty() ? getGraphs().stream().flatMap(g -> g.getMetrics().stream()).collect(Collectors.toList()) 
                :  useMetrics;
    }
    
    private Project createProject(List<String> args) throws IOException, SchemaException {
        String name = args.remove(0);
        File land = new File(args.remove(0));
        
        double nodata = Double.NaN;
        File dir = new File("."); //NOI18N

        // parameter
        while(!args.isEmpty() && !args.get(0).startsWith("--")) { //NOI18N
            String p = args.remove(0);
            String[] tok = p.split("="); //NOI18N
            switch (tok[0]) {
                case "nodata": //NOI18N
                    nodata = Double.parseDouble(tok[1]);
                    break;
                case "dir": //NOI18N
                    dir = new File(tok[1]);
                    break;
                default:
                    throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN PARAMETER {0}"), p));
            }
        }

        GridCoverage2D coverage;
        if(land.getName().toLowerCase().endsWith(".rst")) { //NOI18N
            coverage = new RSTGridReader(land).read(null);
        } else {
            coverage = IOImage.loadCoverage(land);
        }
        int dataType = coverage.getRenderedImage().getSampleModel().getDataType();
        if(dataType == DataBuffer.TYPE_DOUBLE || dataType == DataBuffer.TYPE_FLOAT) {
            throw new RuntimeException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("IMAGE DATA TYPE IS NOT INTEGER TYPE"));
        }
        TreeSet<Integer> codes = NewProjectDialog.getCodes(coverage);

        return new Project(name, new File(dir, name), coverage, codes, nodata);

    }
    
    private void createHabitat(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("name", "codes"), Arrays.asList("minarea", "maxsize", "con8", "novoronoi")); //NOI18N
        Range range = Range.parse(params.get("codes")); //NOI18N
        HashSet<Integer> patchCodes = new HashSet<>();
        for(Double code : range.getValues()) {
            patchCodes.add(code.intValue());
        }
        double minArea = 0;
        double maxSize = 0;
        boolean con8 = params.containsKey("con8"); //NOI18N
        if(params.containsKey("minarea")) { //NOI18N
            minArea = Double.parseDouble(params.get("minarea")) * 10000; //NOI18N
        }
        if(params.containsKey("maxsize")) { //NOI18N
            maxSize = Double.parseDouble(params.get("maxsize")); //NOI18N
        }
        
        boolean voronoi = !params.containsKey("novoronoi"); //NOI18N
        
        MonoHabitat habitat = new MonoHabitat(params.get("name"), project, patchCodes, con8, minArea, maxSize, voronoi); //NOI18N
        project.addHabitat(habitat, save);
        useHabitats.clear();
        useHabitats.add(habitat);
    }
    
    private void createVectorHabitat(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("name"), Arrays.asList("file", "from", "mincapa", "capa", "novoronoi")); //NOI18N
        boolean voronoi = !params.containsKey("novoronoi"); //NOI18N
        double minCapa = params.containsKey("mincapa") ? Double.parseDouble(params.get("mincapa")) : 0.0; //NOI18N
        AbstractMonoHabitat habitat;
        if(params.containsKey("file")) { //NOI18N
            List<? extends Feature> features = IOFeature.loadFeatures(new File(params.get("file"))); //NOI18N
            habitat = new MonoVectorHabitat(params.get("name"), project, features, params.get("capa"), minCapa, voronoi); //NOI18N
        } else {
            habitat = project.getHabitat(params.get("from")).createHabitat(params.get("name"), minCapa, voronoi); //NOI18N
        }
        project.addHabitat(habitat, save);
        useHabitats.clear();
        useHabitats.add(habitat);
    }
    
    private void mergeHabitat(List<String> args) throws IOException, SchemaException {
        MultiHabitat habitat;
        boolean voronoi = true;
        if(!args.isEmpty() && args.get(0).equals("novoronoi")) { //NOI18N
            voronoi = false;
            args.remove(0);
        }
        if(args.isEmpty() || args.get(0).startsWith("--")) { //NOI18N
            habitat = new MultiHabitat((Collection)getHabitats(), voronoi);
        } else {
            String[] names = args.remove(0).split(","); //NOI18N
            if(names[0].equals("ALL")) { //NOI18N
                names = project.getHabitatNames().toArray(String[]::new);
            }
            List<AbstractMonoHabitat> habitats = new ArrayList<>();
            for(String name : names) {
                habitats.add(project.getHabitat(name));
            }
            if(!args.isEmpty() && args.get(0).equals("novoronoi")) { //NOI18N
                voronoi = false;
                args.remove(0);
            }
            habitat = new MultiHabitat(habitats, voronoi);
        }
        useHabitats.clear();
        useHabitats.add(habitat);
    }
    
    private void setDEM(List<String> args) throws IOException {
        project.setDemFile(new File(args.remove(0)), save);
    }
    
    private void batchModel(List<String> args) throws IOException, MathException {
        
        String var = args.remove(0);
        String arg = args.remove(0);
        String [] tok = arg.split("="); //NOI18N
        Range rangeW = Range.parse(tok[1]);
        
        final List<String> otherVars;
        if(!args.isEmpty() && args.get(0).startsWith("vars=")) { //NOI18N
            otherVars = Arrays.asList(args.remove(0).split("=")[1].split(",")); //NOI18N
        } else {
            otherVars = Collections.EMPTY_LIST;
        }
        
        final LinkedHashMap<String, GridCoverage2D> rasterVars = new LinkedHashMap<>();
        if(!args.isEmpty() && args.get(0).startsWith("raster=")) { //NOI18N
            List<String> rasters = Arrays.asList(args.remove(0).split("=")[1].split(",")); //NOI18N
            for(String raster : rasters) {
                File file = new File(raster);
                rasterVars.put(file.getName(), IOImage.loadCoverage(file));
            }
        }
 
        try (FileWriter wd = new FileWriter(new File(project.getDirectory(), "model-" + var + "-dW" + tok[1] + ".txt"))) { //NOI18N
            wd.write("Graph\tDataset\tMetric\tDistWeight\tR2\tp-value\tCoef\n"); //NOI18N
            for(AbstractGraph graph : getGraphs()) {
                System.out.println(graph.getName());
                
                // add metrics from the graph
                TreeSet<String> vars = new TreeSet<>(graph.getPatchAttr()
                        .stream().map((name) -> name.substring(0, name.length()-graph.getName().length()-1)).collect(Collectors.toList()));
                vars.addAll(otherVars);
                vars.addAll(rasterVars.keySet());
                
                for(Habitat habitat : getHabitats()) {
                    if(!(habitat instanceof VectorDataset)) {
                        continue;
                    }
                    VectorDataset dataset = (VectorDataset) habitat;
                    new ParallelFExecutor(new SimpleParallelTask<String>(new ArrayList<>(vars)) {
                        @Override
                        protected void executeOne(String v) {
                            System.out.println(graph.getName() + " - " + dataset.getName() + " : " + v); //NOI18N
                            if(!otherVars.contains(v) && !rasterVars.containsKey(v)) {
                                v = v+"_"+graph.getName(); //NOI18N
                            }
                            LinkedHashMap<String, GridCoverage2D> mRaster = new LinkedHashMap<>();
                            boolean rast = rasterVars.containsKey(v);
                            if(rast) {
                                mRaster.put(v, rasterVars.get(v));
                            }
                               
                            for(Double d : rast ? Collections.singletonList(1.0) : rangeW.getValues(graph.getLinkset())) {
                                DistribModel model = 
                                        new DistribModel(dataset, ((DefaultGraph)graph).getLinkset(), var, -Math.log(0.05) / d, rast ? Collections.EMPTY_LIST : Arrays.asList(v), 
                                                mRaster, true, false, 0, null);
                                try {
                                    model.estimModel(new TaskMonitor.EmptyMonitor());
                                    Logistic estim = model.getLogisticModel();
                                    synchronized(CLITools.this) {  
                                        wd.write(String.format(Locale.US, "%s\t%s\t%s\t%g\t%g\t%g\t%g\n", //NOI18N
                                                graph.getName(), dataset.getName(), v, d, estim.getR2(), estim.getProbaTest(), estim.getCoefs()[1]));
                                        wd.flush();
                                    }
                                } catch (IOException | MathException ex) {
                                    throw new RuntimeException(ex);
                                }
                            }
                            
                        }
                    }).executeAndWait();
                    
                    
                }
            }
        } 
    }

    private void createLinkset(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("distance"), null); //NOI18N
        
        Topology topo = Topology.PLANAR;
        double threshold = 0;
        String name = null;
        Distance type_dist;

        switch(params.remove("distance")) { //NOI18N
            case "euclid": //NOI18N
                type_dist = Distance.EUCLID;
                break;
            case "cost": //NOI18N
                type_dist = Distance.COST;
                break;
            case "circuit": //NOI18N
                type_dist = Distance.CIRCUIT;
                break;
            default:
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN LINKSET DISTANCE TYPE"));
        }
        if(params.containsKey("name")) { //NOI18N
            name = params.remove("name"); //NOI18N
        }
        if(params.containsKey("topo")) { //NOI18N
            switch(params.remove("topo")) { //NOI18N
                case "complete": //NOI18N
                    topo = Topology.COMPLETE;
                    break;
                case "planar": //NOI18N
                    topo = Topology.PLANAR;
                    break;
                case "planarcost": //NOI18N
                    topo = Topology.PLANAR_COST;
                    break;
                default:
                    throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN LINKSET TOPOLOGY"));
            }
        }
        boolean inter = false;
        if(params.containsKey("inter")) { //NOI18N
            inter = true;
            params.remove("inter"); //NOI18N
        }
        String filter = null;
        if(params.containsKey("filter")) { //NOI18N
            filter = params.remove("filter"); //NOI18N
        }
        if(params.containsKey("maxcost")) { //NOI18N
            threshold = Double.parseDouble(params.remove("maxcost")); //NOI18N
        }
        boolean realPaths = true;
        if(params.containsKey("nopathsaved")) { //NOI18N
            params.remove("nopathsaved"); //NOI18N
            realPaths = false;
        }
        useLinksets.clear();
        for(Habitat habitat : getHabitats()) {
            String linkName = name;
            if(type_dist == Distance.EUCLID) {
                File heightRaster = null;
                if(params.containsKey("height")) { //NOI18N
                    heightRaster = new File(params.remove("height")); //NOI18N
                }
                if(linkName == null) {
                    linkName = habitat.getName() + "_euclid_" + (topo == Topology.COMPLETE ? "comp" : "plan")+threshold; //NOI18N
                }
                Linkset cost = new EuclideLinkset(habitat, linkName, topo, inter, filter, realPaths, threshold, heightRaster);
                project.addLinkset(cost, save);
                useLinksets.add(cost);
            } else {
                boolean circuit = type_dist == Distance.CIRCUIT;
                double coefSlope = 0;
                if(params.containsKey("slope")) { //NOI18N
                    coefSlope = Double.parseDouble(params.remove("slope")); //NOI18N
                }
                boolean removeCrossPath = false;
                if(params.containsKey("remcrosspath")) { //NOI18N
                    params.remove("remcrosspath"); //NOI18N
                    removeCrossPath = true;
                }
                if(params.containsKey("extcost")) { //NOI18N
                    File extCost = new File(params.remove("extcost")); //NOI18N
                    if(linkName == null) {
                        linkName = habitat.getName() + (circuit ? "_circ_" : "_cost_") + extCost.getName(); //NOI18N //NOI18N
                    } 
                    Linkset cost = circuit ? new CircuitLinkset(habitat, linkName, topo, inter, filter, null, extCost, true, coefSlope) : 
                            new CostLinkset(habitat, linkName, topo, inter, filter, realPaths, removeCrossPath, threshold, extCost, coefSlope);
                    project.addLinkset(cost, save);
                    useLinksets.add(cost);
                } else {
                    int max = Collections.max(project.getCodes());
                    double [] costs = new double[max+1];
                    List<Double> dynCodes = null;
                    Range rangeCost = null;
                    String finalName = null;
                    Set<Integer> codeSet = new HashSet<>();
                    for(String param : params.keySet()) {
                        Range codes = Range.parse(param);
                        Range cost = Range.parse(params.get(param));
                        if(cost.isSingle()) {
                            for(Double code : codes.getValues()) {
                                if(code < costs.length) { 
                                    costs[code.intValue()] = cost.getMin();
                                }
                                codeSet.add(code.intValue());
                            }
                        }
                        if(rangeCost == null || !cost.isSingle()) {
                            if(rangeCost != null && !rangeCost.isSingle()) {
                                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("ONLY ONE RANGE CAN BE DEFINED FOR LINKSET"));
                            }
                            rangeCost = cost;
                            dynCodes = codes.getValues();
                            for(Double code : codes.getValues()) {
                                codeSet.add(code.intValue());
                            }
                            finalName = param.replace(',', '_');
                        }
                    }
                    if(!codeSet.containsAll(project.getCodes())){
                        HashSet<Integer> codes = new HashSet<>(project.getCodes());
                        codes.removeAll(codeSet);
                        throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("--LINKSET : SOME CODES ARE MISSING {0}"), Arrays.deepToString(codes.toArray())));
                    }
                    if(!project.getCodes().containsAll(codeSet)){
                        HashSet<Integer> codes = new HashSet<>(codeSet);
                        codes.removeAll(project.getCodes());
                        Logger.getLogger(CLITools.class.getName()).log(Level.WARNING, java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("--LINKSET : SOME CODES DO NOT EXIST IN LAND MAP {0}"), Arrays.deepToString(codes.toArray())));
                    }
                    boolean multi = !rangeCost.isSingle();
                    if(linkName == null) {
                        finalName = habitat.getName() + (circuit ? "_circ_" : "_cost_") + finalName; //NOI18N //NOI18N
                    } else {
                        finalName = linkName;
                    }
                    for(Double c : rangeCost.getValues()) {
                        System.out.println("Calc cost " + c); //NOI18N
                        for(Double code : dynCodes) {
                            if(code < costs.length) { 
                                costs[code.intValue()] = c;
                            }
                        }
                        String s = finalName + (multi ? "-" + c : ""); //NOI18N
                        Linkset cost = circuit ? new CircuitLinkset(habitat, s, topo, inter, filter, costs, null, true, coefSlope) : 
                                new CostLinkset(habitat, s, topo, inter, filter, costs, realPaths, removeCrossPath, threshold, coefSlope);
                        project.addLinkset(cost, save);
                        useLinksets.add(cost);
                    }
                }
            }
        }
    }

    private void createDataset(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("name", "file"), Collections.EMPTY_LIST); //NOI18N
        File file = new File(params.get("file")); //NOI18N
        List<DefaultFeature> features = IOFeature.loadFeatures(file);
        
        VectorDataset data = new VectorDataset(params.get("name"), project, features); //NOI18N
        project.addDataset(data, save);
        useHabitats.clear();
        useHabitats.add(data);
    }

    private void createGraph(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Collections.EMPTY_LIST, Arrays.asList("name", "nointra", "threshold")); //NOI18N
        
        Type type = Type.COMPLETE;
        boolean intra = true;
        Range range = null;
        String singleName = null;
        
        if(params.containsKey("name")) { //NOI18N
            singleName = params.get("name"); //NOI18N
        }
        
        if(params.containsKey("nointra")) { //NOI18N
            intra = false;
        }
        if(params.containsKey("threshold")) { //NOI18N
            type = Type.PRUNED;
            range = Range.parse(params.get("threshold")); //NOI18N
        }
        
        if(singleName != null && (getLinksets().size() > 1 || range != null && !range.isSingle())) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NAME PARAMETER CAN BE USED ONLY WHEN CREATING ONLY ONE GRAPH"));
        }
        
        useGraphs.clear();
        for(Linkset cost : getLinksets()) {
            if(type == Type.COMPLETE) {
                String name = singleName == null ? "comp_" + cost.getName() : singleName; //NOI18N
                DefaultGraph g = new DefaultGraph(name, cost, intra && cost.isRealPaths());
                System.out.println("Create graph " + g.getName()); //NOI18N
                project.addGraph(g, save);
                useGraphs.add(g);
            } else {
                for(Double d : range.getValues(cost)) {
                    String name = singleName == null ? "thresh_" + d + "_" + cost.getName() : singleName; //NOI18N
                    DefaultGraph g = new DefaultGraph(name, cost, d, intra && cost.isRealPaths());
                    System.out.println("Create graph " + g.getName()); //NOI18N
                    project.addGraph(g, save);
                    useGraphs.add(g);
                }
            }
        }
    }
    
    private void mergeGraph(List<String> args) throws IOException, SchemaException {
        MultiGraph graph;
        String name = null;
        Collection<DefaultGraph> graphs;
        if(!args.isEmpty() && args.get(0).startsWith("name=")) { //NOI18N
            name = args.remove(0).split("=")[1]; //NOI18N
        }
        if(args.isEmpty() || args.get(0).startsWith("--")) { //NOI18N
            graphs = (Collection)getGraphs();
        } else {
            String[] names = args.remove(0).split(","); //NOI18N
            if(names[0].equals("ALL")) { //NOI18N
                names = project.getGraphNames().toArray(String[]::new);
            }
            graphs = new ArrayList<>();
            for(String s : names) {
                if(project.getGraph(s) instanceof MultiGraph) {
                    throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("CANNOT MERGE MULTIGRAPH"));
                }
                graphs.add((DefaultGraph)project.getGraph(s));
            }
        }
        if(name == null) {
            name = ""; //NOI18N
            for(AbstractGraph g : graphs) {
                name += "-" + g.getName(); //NOI18N
            }
            name = name.substring(1);
        }
        graph = new MultiGraph(name, graphs);
        project.addGraph(graph, save);
        
        useGraphs.clear();
        useGraphs.add(graph);
    }
    
    private void clustering(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("d", "p"), Arrays.asList("beta", "nb")); //NOI18N
        final double alpha = AlphaParamMetric.getAlpha(Double.parseDouble(params.get("d")), Double.parseDouble(params.get("p"))); //NOI18N
        final double beta = params.containsKey("beta") ? Double.parseDouble(params.get("beta")) : 1; //NOI18N
        final int nb = params.containsKey("nb") ? Integer.parseInt(params.get("nb")) : -1; //NOI18N

        List<AbstractGraph> newGraphs = new ArrayList<>();
        for(final AbstractGraph graph : new ArrayList<>(getGraphs())) {
            Modularity mod = new Modularity(graph.getGraph(), new EdgeWeighter() {
                    @Override
                    public double getWeight(Edge e) {
                        return Math.pow(Habitat.getPatchCapacity(e.getNodeA()) * Habitat.getPatchCapacity(e.getNodeB()), beta) 
                            * Math.exp(-alpha*graph.getCost(e));
                    }
                    @Override
                    public double getToGraphWeight(double dist) {
                        return 0;
                    }
                });

            if(nb == -1) {
                mod.setKeepList(Collections.EMPTY_SET);
            } else {
                mod.setKeepList(Collections.singleton(nb));
            }
            mod.partitions();
            
            Set<Modularity.Cluster> part = nb == -1 ? mod.getBestPartition() : mod.getPartition(nb);
            ModularityGraph g = new ModularityGraph(null, graph, part);
            project.addGraph(g, save);
            newGraphs.add(g);
        }
        
        useGraphs = newGraphs;
    }

    private void calcGlobalMetric(List<String> args) throws IOException, SchemaException {
        if(args.isEmpty()) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NEEDS A GLOBAL METRIC SHORTNAME"));
        }
        String indName = args.remove(0);
        GlobalMetric metric = Project.getGlobalMetric(indName);
        File resFile = new File(project.getDirectory(), metric.getShortName() + ".txt"); //NOI18N
        if(!args.isEmpty() && args.get(0).startsWith("resfile=")) { //NOI18N
            resFile = new File(project.getDirectory(), args.remove(0).split("=")[1]); //NOI18N
        }

        if(!args.isEmpty() && args.get(0).startsWith("mh=")) { //NOI18N
            if(metric instanceof MHMetric) {
                ((MHMetric)metric).setSelection(args.remove(0).split("=")[1]); //NOI18N
            } else {
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO MULTI HABITAT METRIC"));
            }
        }
        
        Map<String, Range> ranges = readMetricParams(args);
        
        System.out.println("Global metric " + metric.getName()); //NOI18N
        List<String> paramNames = new ArrayList<>(ranges.keySet());
        useMetrics.clear();
        try (FileWriter fw = new FileWriter(resFile)) {
            fw.write("Graph"); //NOI18N
            for(String param : paramNames) {
                fw.write("\t" + param); //NOI18N
            }
            for(String resName : metric.getResultNames(getGraphs().iterator().next())) {
                fw.write("\t" + resName); //NOI18N
            }
            fw.write("\n"); //NOI18N
            
            for(AbstractGraph graph : getGraphs()) {    
                HashMap<String, Object> params = new HashMap<>();                
                
                int [] indParam = new int[ranges.size()];
                boolean end = false;
                while(!end) {
                    for(int i = 0; i < indParam.length; i++) {
                        params.put(paramNames.get(i), ranges.get(paramNames.get(i)).getValues(graph.getLinkset()).get(indParam[i]));
                    }
                    metric.setParams(params);
                    
                    GlobalMetricResult res = new GlobalMetricResult(metric, graph);
                    project.addMetric(res, save);
                    useMetrics.add(res);
                    System.out.println(graph.getName() + " - " + metric.getDetailName() + " : " + res.getResult()[0]); //NOI18N
                    
                    fw.write(graph.getName());
                    for(String param : paramNames) {
                        fw.write("\t" + params.get(param)); //NOI18N
                    }
                    for(Double val : res.getResult()) {
                        fw.write("\t" + val); //NOI18N
                    }
                    fw.write("\n"); //NOI18N
                    fw.flush();
                    
                    if(indParam.length > 0) {
                        indParam[0]++;
                    }
                    for(int i = 0; i < indParam.length-1; i++) {
                        if(indParam[i] >= ranges.get(paramNames.get(i)).getSize()) {
                            indParam[i] = 0;
                            indParam[i+1]++;
                        } else {
                            break;
                        }
                    }
                    if(indParam.length > 0) {
                        end = indParam[indParam.length-1] >= ranges.get(paramNames.get(indParam.length-1)).getSize();
                    } else {
                        end = true;
                    }
                }
            }
        }
    }
    
    private void calcCompMetric(List<String> args) throws IOException, SchemaException {
        if(args.isEmpty()) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NEEDS A GLOBAL METRIC SHORTNAME"));
        }
        
        String indName = args.remove(0);
        GlobalMetric m = Project.getGlobalMetric(indName);
        if(!args.isEmpty() && args.get(0).startsWith("mh=")) { //NOI18N
            if(m instanceof MHMetric) {
                ((MHMetric)m).setSelection(args.remove(0).split("=")[1]); //NOI18N
            } else {
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO MULTI HABITAT METRIC"));
            }
        }

        Map<String, Range> ranges = readMetricParams(args);
        useMetrics.clear();
        
        System.out.println("Component metric " + m.getName()); //NOI18N
        for(AbstractGraph graph : getGraphs()) {
            System.out.println(graph.getName());
            HashMap<String, Object> params = new HashMap<>();     
            List<String> paramNames = new ArrayList<>(ranges.keySet());
            int [] indParam = new int[ranges.size()];
            boolean end = false;
            while(!end) {
                for(int i = 0; i < indParam.length; i++) {
                    params.put(paramNames.get(i), ranges.get(paramNames.get(i)).getValues(graph.getLinkset()).get(indParam[i]));
                }
                m.setParams(params);
                CompMetricResult metric = new CompMetricResult(m.getDetailName(), m, graph);
                project.addMetric(metric, save);
                useMetrics.add(metric);
                
                if(indParam.length > 0) {
                    indParam[0]++;
                }
                for(int i = 0; i < indParam.length-1; i++) {
                    if(indParam[i] >= ranges.get(paramNames.get(i)).getSize()) {
                        indParam[i] = 0;
                        indParam[i+1]++;
                    } else {
                        break;
                    }
                }
                if(indParam.length > 0) {
                    end = indParam[indParam.length-1] >= ranges.get(paramNames.get(indParam.length-1)).getSize();
                } else {
                    end = true;
                }
            }
        }
    }
    
    private void calcLocalMetric(List<String> args) throws IOException, SchemaException {
        if(args.isEmpty()) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NEEDS A LOCAL METRIC SHORTNAME"));
        }
        String indName = args.remove(0);
        LocalMetric metric = Project.getLocalMetric(indName);
        String name = null;
        if(!args.isEmpty() && args.get(0).startsWith("name=")) { //NOI18N
            name = args.remove(0).split("=")[1];
        }
        if(!args.isEmpty() && args.get(0).startsWith("mh=")) { //NOI18N
            if(metric instanceof MHMetric) {
                ((MHMetric)metric).setSelection(args.remove(0).split("=")[1]); //NOI18N
            } else {
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO MULTI HABITAT METRIC"));
            }
        }
        String expr = null;
        if(!args.isEmpty() && args.get(0).startsWith("filter=")) { //NOI18N
            String arg = args.remove(0);
            expr = arg.substring(arg.indexOf("=")+1); //NOI18N
        }
        Map<String, Range> ranges = readMetricParams(args);
        useMetrics.clear();
        
        System.out.println("Local metric " + metric.getName()); //NOI18N
        for(AbstractGraph graph : getGraphs()) {
            System.out.println(graph.getName());
            HashMap<String, Object> params = new HashMap<>();
            List<String> paramNames = new ArrayList<>(ranges.keySet());
            int [] indParam = new int[ranges.size()];
            boolean end = false;
            while(!end) {
                for(int i = 0; i < indParam.length; i++) {
                    params.put(paramNames.get(i), ranges.get(paramNames.get(i)).getValues(graph.getLinkset()).get(indParam[i]));
                }
                metric.setParams(params);
                LocalMetricResult m = new LocalMetricResult(name == null ? metric.getDetailName() : name, metric, graph);
                if(expr != null) {
                    m.setFilter(expr);
                }
                project.addMetric(m, save);
                useMetrics.add(m);
                
                if(indParam.length > 0) {
                    indParam[0]++;
                }
                for(int i = 0; i < indParam.length-1; i++) {
                    if(indParam[i] >= ranges.get(paramNames.get(i)).getSize()) {
                        indParam[i] = 0;
                        indParam[i+1]++;
                    } else {
                        break;
                    }
                }
                if(indParam.length > 0) {
                    end = indParam[indParam.length-1] >= ranges.get(paramNames.get(indParam.length-1)).getSize();
                } else {
                    end = true;
                }
            }
        }
    }

    private void calcDeltaMetric(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList(), Arrays.asList("obj", "filter", "sel", "fsel")); //NOI18N

        List ids = new ArrayList();
        String expr = null;
        if(params.containsKey("sel")) { //NOI18N
            boolean patch = params.get("obj").equals("patch"); //NOI18N
            String [] toks = params.get("sel").split(","); //NOI18N
            for(String tok : toks) {
                ids.add(patch ? Integer.parseInt(tok) : tok);
            }
        } else if(params.containsKey("fsel")) { //NOI18N
            boolean patch = params.get("obj").equals("patch"); //NOI18N
            File f = new File(params.get("fsel")); //NOI18N
            List<String> lst = readFile(f);
            for(String id : lst) {
                ids.add(patch ? Integer.parseInt(id) : id);
            }
        } else {
            if(params.containsKey("filter")) { //NOI18N
                expr = params.get("filter"); //NOI18N
            }
        }
        
        for(MetricResult m : getMetrics()) {
            if(!(m instanceof GlobalMetricResult)) {
                continue;
            }
            GlobalMetricResult metric = (GlobalMetricResult)m;
            System.out.println("Global metric " + metric.getFullName()); //NOI18N

            DeltaMetricResult deltaMetric = new DeltaMetricResult(metric, ids);
            if(expr == null && params.containsKey("obj") && ids.isEmpty()) { //NOI18N
                deltaMetric.setFilter(params.get("obj").equals("patch") ? "node" : "edge"); //NOI18N
            } else {
                deltaMetric.setFilter(expr);
            }
            project.addMetric(deltaMetric, save);
                  
        }
    }
    
    private void addGlobal(List<String> args) throws IOException {
        int nbStep = Integer.parseInt(args.remove(0));
        String indName = args.remove(0);
        
        HashMap<String, Object> params = new HashMap<>();
        while(!args.get(0).startsWith("obj=")) { //NOI18N
            String [] tok = args.remove(0).split("="); //NOI18N
            Range r = Range.parse(tok[1]);
            if(r.isSingle()) {
                params.put(tok[0], r.getMin());
            } else {
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO RANGE FOR METRIC PARAMS IN --GTEST"));
            }
        }
        // obj=patch|link
        boolean patch = args.remove(0).split("=")[1].equals("patch"); //NOI18N

        List lstIds = new ArrayList();
        if(args.get(0).startsWith("sel=")) { //NOI18N
            String [] toks = args.remove(0).split("=")[1].split(","); //NOI18N
            for(String tok : toks) {
                lstIds.add(patch ? Integer.parseInt(tok) : tok);
            }
        } else if(args.get(0).startsWith("fsel=")) { //NOI18N
            File f = new File(args.remove(0).split("=")[1]); //NOI18N
            List<String> lst = readFile(f);
            for(String id : lst) {
                lstIds.add(patch ? Integer.parseInt(id) : id);
            }
        } else {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("SEL OR FSEL PARAMETER IS MISSING"));
        }

        GlobalMetric metric = Project.getGlobalMetric(indName);
        
        if(metric.hasParams()) {
            if(params.isEmpty()) {
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("PARAMS FOR {0} NOT FOUND IN --GTEST"), metric.getName()));
            }
            metric.setParams(params);
        }
        
        for(AbstractGraph graph : getGraphs()) {
            HashSet ids = new HashSet(lstIds);
            try (FileWriter w = new FileWriter(new File(project.getDirectory(), "gtest-" + graph.getName() + "-" + metric.getDetailName() + ".txt"));  //NOI18N
                 FileWriter wd = new FileWriter(new File(project.getDirectory(), "gtest-" + graph.getName() + "-" + metric.getDetailName() + "-detail.txt"))) { //NOI18N
                wd.write("Step\tId\t"+metric.getShortName()+"\n"); //NOI18N
                w.write("Step\tId\t"+metric.getShortName()+"\n"); //NOI18N
                
                System.out.println("Global metric " + metric.getName()); //NOI18N
                
                for(int i = 1; i <= nbStep; i++) {
                    System.out.println("Step : " + i); //NOI18N
                    
                    DeltaAddGraph deltaGraph = new DeltaAddGraph(graph,
                            patch ? ids : Collections.EMPTY_LIST, !patch ? ids : Collections.EMPTY_LIST);
                    Double [] init = new GlobalMetricResult(metric, deltaGraph).calculate(true, null).getResult();
                    wd.write(i + "\tinit\t" + init[0] + "\n"); //NOI18N
                    if(i == 1) {
                        w.write("0\tinit\t" + init[0] + "\n"); //NOI18N
                    }
                    Object bestId = null;
                    double maxVal = -Double.MAX_VALUE;
                    for(Object id : ids) {
                        deltaGraph.addElem(id);
                        Double [] res = new GlobalMetricResult(metric, deltaGraph).calculate(true, null).getResult();
                        if(res[0] > maxVal) {
                            bestId = id;
                            maxVal = res[0];
                        } 
                        
                        wd.write(i + "\t" + id + "\t" + res[0] + "\n"); //NOI18N
                        wd.flush();
                        deltaGraph.reset();
                    }
                    
                    w.write(i + "\t" + bestId + "\t" + maxVal + "\n"); //NOI18N
                    w.flush();
                    wd.flush();

                    ids.remove(bestId);
                }
            }
        }
        
    }
    
    private void remGlobal(List<String> args) throws IOException {
        String indName = args.remove(0);

        GlobalMetric metric = Project.getGlobalMetric(indName);
        if(metric.hasParams()) {
            int nParam = metric.getParams().size();
        
            HashMap<String, Object> params = new HashMap<>();
            for(int i = 0; i < nParam && !args.isEmpty(); i++) {
                String [] tok = args.remove(0).split("="); //NOI18N
                Range r = Range.parse(tok[1]);
                if(r.isSingle()) {
                    params.put(tok[0], r.getMin());
                } else {
                    throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO RANGE FOR INDICE PARAMS IN --GREMOVE"));
                }
            }

            metric.setParams(params);
        }

        List patchIds = new ArrayList();
        List linkIds = new ArrayList();
        if(args.get(0).startsWith("patch=")) { //NOI18N
            String [] toks = args.remove(0).split("=")[1].split(","); //NOI18N
            for(String tok : toks) {
                patchIds.add(Integer.parseInt(tok));
            }
        } else if(args.get(0).startsWith("fpatch=")) { //NOI18N
            File f = new File(args.remove(0).split("=")[1]); //NOI18N
            List<String> ids = readFile(f);
            for(String id : ids) {
                patchIds.add(Integer.parseInt(id));
            }
        }
        if(!args.isEmpty() && args.get(0).startsWith("link=")) { //NOI18N
            String [] toks = args.remove(0).split("=")[1].split(","); //NOI18N
            linkIds.addAll(Arrays.asList(toks));
        } else if(!args.isEmpty() && args.get(0).startsWith("flink=")) { //NOI18N
            File f = new File(args.remove(0).split("=")[1]); //NOI18N
            List<String> ids = readFile(f);
            linkIds.addAll(ids);
        }
        

        System.out.println("Global metric " + metric.getDetailName()); //NOI18N
        for(AbstractGraph graph : getGraphs()) {
            System.out.println("Graph " + graph.getName()); //NOI18N
            ModifiedGraph deltaGraph = new ModifiedGraph(graph, patchIds, linkIds);
            System.out.println("Remove " + (graph.getNodes().size()-deltaGraph.getNodes().size()) + " patches and " +
                    (graph.getEdges().size()-deltaGraph.getEdges().size()) + " links"); //NOI18N
            Double[] res = new GlobalMetricResult(metric, deltaGraph).calculate(true, null).getResult();     

            System.out.println(indName + " : " + res[0] + "\n"); //NOI18N
        }
    }
    
    private void remElem(List<String> args) throws IOException {
        int nElem = Integer.parseInt(args.remove(0));
        String indName = args.remove(0);

        GlobalMetric metric = Project.getGlobalMetric(indName);
        if(metric.hasParams()) {
            int nParam = metric.getParams().size();
        
            HashMap<String, Object> params = new HashMap<>();
            for(int i = 0; i < nParam && !args.isEmpty(); i++) {
                String [] tok = args.remove(0).split("="); //NOI18N
                Range r = Range.parse(tok[1]);
                if(r.isSingle()) {
                    params.put(tok[0], r.getMin());
                } else {
                    throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO RANGE FOR METRIC PARAMS IN --REMELEM"));
                }
            }

            metric.setParams(params);
        }
        
        // obj=patch|link
        boolean patch = args.remove(0).split("=")[1].equals("patch"); //NOI18N
        
        List ids = new ArrayList();
        if(!args.isEmpty() && args.get(0).startsWith("sel=")) { //NOI18N
            String [] toks = args.remove(0).split("=")[1].split(","); //NOI18N
            for(String tok : toks) {
                ids.add(patch ? Integer.parseInt(tok) : tok);
            }
        } else if(!args.isEmpty() && args.get(0).startsWith("fsel=")) { //NOI18N
            File f = new File(args.remove(0).split("=")[1]); //NOI18N
            List<String> lst = readFile(f);
            for(String id : lst) {
                ids.add(patch ? Integer.parseInt(id) : id);
            }
        }
        
        System.out.println("Global metric " + metric.getDetailName()); //NOI18N
        for(AbstractGraph graph : getGraphs()) {
            System.out.println("Graph " + graph.getName()); //NOI18N
            List remIds = new ArrayList();
            AbstractGraph gr = graph;
            try (FileWriter wd = new FileWriter(new File(project.getDirectory(), "rem" + (patch ? "patch" : "link") + "-" + metric.getDetailName() + "_" + graph.getName() + ".txt"))) { //NOI18N
                wd.write("Step\tId\t" + metric.getShortName() + "\n"); //NOI18N
                for(int step = 1; step <= nElem; step++) {
                    GlobalMetricResult initMetric = new GlobalMetricResult(metric, gr).calculate(true, null);
                    DeltaMetricTask deltaTask = ids.isEmpty() ? 
                            new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), initMetric, 
                                    patch ? gr.getHabitat().getPatches() : Collections.emptyList(),
                                    !patch ? gr.getLinkset().getPaths() : Collections.emptyList()) :
                            new DeltaMetricTask(new TaskMonitor.EmptyMonitor(), initMetric, ids);
                    ExecutorService.execute(deltaTask);
                    Map<Object, Double[]> result = deltaTask.getResult();
                    double max = Double.NEGATIVE_INFINITY;
                    Object bestId = null;
                    for(Object id : result.keySet()) {
                        Double[] res = result.get(id);
                        if(res[0] > max) {
                            max = res[0];
                            bestId = id;
                        }
                    }
                    double init = initMetric.getResult()[0];
                    double val = init - (max * init);
                    remIds.add(bestId);
                    if(step == 1) {
                        wd.write("0\tinit\t" + init + "\n"); //NOI18N
                    }
                    wd.write(step + "\t" + bestId + "\t" + val + "\n"); //NOI18N
                    wd.flush();
                    System.out.println("Remove elem " + bestId + " with metric value " + val); //NOI18N
                    gr = new ModifiedGraph(graph, patch ? remIds : Collections.EMPTY_LIST, !patch ? remIds : Collections.EMPTY_LIST);
                }
            }
        } 
    }
    
    private void addPatch(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("npatch"), Arrays.asList("hab", "gridres", "patchfile", "capa")); //NOI18N
        int nbPatch = Integer.parseInt(params.get("npatch")); //NOI18N
        String habName = params.get("hab"); //NOI18N
        
        if(params.containsKey("gridres")) { //NOI18N
            Range rangeRes = Range.parse(params.get("gridres")); //NOI18N
            File capaFile = null;
            if(params.containsKey("capa")) { //NOI18N
                capaFile = new File(params.get("capa")); //NOI18N
                if(!capaFile.exists())  {
                    throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("FILE {0} DOES NOT EXIST."), new Object[] {capaFile}));
                }
            } 
            
            for(MetricResult m : getMetrics()) {
                if(!(m instanceof GlobalMetricResult)) {
                    continue;
                }
                GlobalMetricResult metric = (GlobalMetricResult) m;
                AbstractGraph graph = m.getGraph();
                AbstractMonoHabitat habitat = habName == null ? (AbstractMonoHabitat)graph.getHabitat() : project.getHabitat(habName);
                if(graph.getLinkset().getDistMax() <= 0) {
                    System.out.println(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("LINKSET MUST HAVE MAXIMUM DISTANCE PARAMETER : {0}"), graph.getLinkset().getName()));
                    continue;
                }
                if(graph.isIntraPatchDist()) {
                    System.out.println(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("INTRAPATCH DISTANCE IS NOT SUPPORTED : {0}"), graph.getName()));
                    continue;
                }
                for(Double res : rangeRes.getValues()) {
                    System.out.println("Add patches with graph " + graph.getName() + 
                            " and metric " + metric.getName() + " at resolution " + res); //NOI18N //NOI18N
                    AddPatchCommand addPatchCmd = new AddPatchCommand(nbPatch, metric, habitat, capaFile, res);
                    addPatchCmd.run(Config.getProgressBar());
                }
            }
        } else {
            File patchFile = new File(params.get("patchfile")); //NOI18N
            String capaField = params.get("capa"); //NOI18N
            
            for(MetricResult m : getMetrics()) {
                if(!(m instanceof GlobalMetricResult)) {
                    continue;
                }
                GlobalMetricResult metric = (GlobalMetricResult) m;
                AbstractGraph graph = m.getGraph();
                AbstractMonoHabitat habitat = habName == null ? (AbstractMonoHabitat)graph.getHabitat() : project.getHabitat(habName);
                if(graph.getLinkset().getDistMax() <= 0) {
                    System.out.println(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("LINKSET MUST HAVE MAXIMUM DISTANCE PARAMETER : {0}"), graph.getLinkset().getName()));
                    continue;
                }
                if(graph.isIntraPatchDist()) {
                    System.out.println(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("INTRAPATCH DISTANCE IS NOT SUPPORTED : {0}"), graph.getName()));
                    continue;
                }
                System.out.println("Add patches with graph " + graph.getName() + " and metric " + metric.getName()); //NOI18N
                AddPatchCommand addPatchCmd = new AddPatchCommand(nbPatch, metric, habitat, patchFile, capaField);
                addPatchCmd.run(Config.getProgressBar());
            }
        }
        
    }
    
    private void calcCapa(List<String> args) throws IOException, SchemaException {
        
        CapaPatchDialog.CapaPatchParam params = new CapaPatchDialog.CapaPatchParam();

        String arg = args.remove(0);
        if(arg.startsWith("maxcost=")) { //NOI18N
            if(getLinksets().size() > 1) {
                throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("--CAPA COMMAND WORKS ONLY FOR ONE LINKSET. SELECT A LINKSET WITH --USELINKSET."));
            }
            Linkset linkset = getLinksets().iterator().next();
            params.calcArea = false;
            params.weightCost = false;
            Range maxcost = Range.parse(arg.split("=")[1]); //NOI18N
            params.maxCost = maxcost.getValue(linkset);
            params.costName = linkset.getName();
            String[] tokens = args.remove(0).split("=")[1].split(","); //NOI18N
            params.codes = new HashSet<>();
            for(String tok : tokens) {
                params.codes.add(Integer.parseInt(tok));
            }
            if(!args.isEmpty() && args.get(0).equals("weight")) { //NOI18N
                args.remove(0);
                params.weightCost = true;
            }
        } else if(arg.equals("area")) { //NOI18N
            params.calcArea = true;
            params.exp = 1.0;
            params.codeWeight = new HashMap<>();
            while(!args.isEmpty() && !args.get(0).startsWith("--")) { //NOI18N
                arg = args.remove(0);
                String[] tokens = arg.split("="); //NOI18N
                if(tokens[0].equals("exp")) { //NOI18N
                    params.exp = Double.parseDouble(tokens[1]);
                } else {
                    String[] codes = tokens[0].split(","); //NOI18N
                    double w = Double.parseDouble(tokens[1]);
                    for(String code : codes) {
                        params.codeWeight.put(Integer.parseInt(code), w);
                    }
                }
            }
        } else if(arg.startsWith("file=")) { //NOI18N
            params.importFile = new File(arg.split("=")[1]); //NOI18N
            params.idField = args.remove(0).split("=")[1]; //NOI18N
            params.capaField = args.remove(0).split("=")[1]; //NOI18N
        } else {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN ARGUMENT {0} FOR --CAPA COMMAND"), arg));
        }
        
        for(Habitat hab : getHabitats()) {
            if(hab instanceof MonoHabitat) {
                ((MonoHabitat)hab).setCapacities(params, save);
            }
        }
    }
    
    private void createMetapatch(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Collections.emptyList(), Arrays.asList("mincapa", "novoronoi")); //NOI18N
        double minCapa = 0;
        if(params.containsKey("mincapa")) { //NOI18N
            minCapa = Double.parseDouble(params.get("mincapa")); //NOI18N
        }
        boolean voronoi = !params.containsKey("novoronoi"); //NOI18N
        
        useHabitats.clear();
        for(AbstractGraph g : getGraphs()) {
            AbstractMonoHabitat metaPatch = project.createMetaPatchHabitat(g.getHabitat().getName() + "-meta-" + g.getName(), g, 0, minCapa, voronoi); //NOI18N
            project.addHabitat(metaPatch, true);
            useHabitats.add(metaPatch);
        }
    }
    
    private void landmod(final List<String> args) throws IOException, SchemaException, MathException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("zone", "id", "code"), Arrays.asList("sel")); //NOI18N
        File fileZone = new File(params.get("zone")); //NOI18N
        String idField = params.get("id"); //NOI18N
        String codeField = params.get("code"); //NOI18N
        Set<String> ids = null;
        if(params.containsKey("sel")) { //NOI18N
            ids = new HashSet<>(Arrays.asList(params.get("sel").split(","))); //NOI18N
            
        }
        // in threaded mode, does not manage reentrant call with old executor, solution : set nb proc to one
        ParallelFExecutor.setNbProc(1);
        LandModTask task = new LandModTask(project, fileZone, idField, codeField, ids, args);
        ExecutorService.execute(task);

        args.clear();
    }
    
    private void landmodgm(final List<String> args) throws IOException, SchemaException, MathException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("zone", "id", "code"), Arrays.asList("sel")); //NOI18N
        File fileZone = new File(params.get("zone")); //NOI18N
        String idField = params.get("id"); //NOI18N
        String codeField = params.get("code"); //NOI18N
        Set<String> ids = null;
        if(params.containsKey("sel")) { //NOI18N
            ids = new HashSet<>(Arrays.asList(params.get("sel").split(","))); //NOI18N
            
        }
        for(MetricResult m : getMetrics()) {
            if(!(m instanceof GlobalMetricResult)) {
                Logger.getLogger(CLITools.class.getName()).log(Level.WARNING, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("SKIP METRIC {0}"), m.getFullName());
                continue;
            }
            
            LandModTask task = new LandModTask(project, fileZone, idField, codeField, ids, (GlobalMetricResult) m, Config.getProgressBar("Landmod")); //NOI18N
            ExecutorService.executeSequential(task);
            
            Map<String, Double[]> result = task.getResult();
            try (FileWriter w = new FileWriter(new File(project.getDirectory(), "landmod-"+m.getFullName()+".csv"))) { //NOI18N
                w.write("id," + String.join(",", m.getMetric().getResultNames(m.getGraph())) + "\n"); //NOI18N
                w.write("init," + Arrays.asList(((GlobalMetricResult)m).getResult()).stream().map(String::valueOf).collect(Collectors.joining(",")) + "\n"); //NOI18N
                for(Entry<String, Double[]> id : result.entrySet()) {
                    w.write(id.getKey() + "," + Arrays.asList(id.getValue()).stream().map(String::valueOf).collect(Collectors.joining(",")) + "\n"); //NOI18N
                }
            }
        }
        
    }
    
    private void interp(List<String> args) throws IOException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList(), Arrays.asList("var", "d", "p", "name", "resol", "multi", "ag")); //NOI18N
        
        double d = params.containsKey("d") ? Double.parseDouble(params.get("d")) : Double.NaN; //NOI18N
        double p = params.containsKey("p") ? Double.parseDouble(params.get("p")) : Double.NaN; //NOI18N
        double alpha = AlphaParamMetric.getAlpha(d, p);
        boolean multi = params.containsKey("multi"); //NOI18N
        Agreg agreg = !params.containsKey("ag") || params.get("ag").equals("sum") ? Agreg.SUM : params.get("ag").equals("max") ? Agreg.MAX : Agreg.AVG; //NOI18N
        double dmax = multi && params.get("multi") != null ? Double.parseDouble(params.get("multi")) : Double.NaN; //NOI18N
        String name = params.get("name"); //NOI18N
        double res = params.containsKey("resol") ? Double.parseDouble(params.get("resol")) : project.getResolution(); //NOI18N
        
        if(params.containsKey("var")) { //NOI18N
            String var = params.get("var"); //NOI18N
        
            for(AbstractGraph graph : getGraphs()) {
                List<String> vars;
                List<String> patchAttrs = graph.getPatchAttr();
                if(patchAttrs.contains(var)) {
                    vars = Collections.singletonList(var);
                } else {
                    vars = new ArrayList<>();
                    for(String attr : patchAttrs) {
                        if(attr.toLowerCase().startsWith(var.toLowerCase())) {
                            vars.add(attr);
                        }
                    }
                }
                if(vars.isEmpty()) {
                    throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO PATCH ATTRIBUTE FOUND FOR VARIABLE {0}"),var));
                }
                for(String v : vars) {
                    RasterLayer raster = DistribModel.interpolate(null, v, res, alpha, 
                            graph, multi, dmax, agreg, Config.getProgressBar("Interpolation " + v)); //NOI18N
                    raster.saveRaster(new File(project.getDirectory(), (name != null ? name + "-" : "") + v.replace('/', '-') + ".tif")); //NOI18N //NOI18N
                }
            }
        } else {
            for(MetricResult m : getMetrics()) {
                if(!(m instanceof LocalMetricResult)) {
                    continue;
                }
                LocalMetricResult metric = (LocalMetricResult) m;
                double a = alpha;
                if(Double.isNaN(a)) {
                    Map<String, Object> pa = metric.getMetric().getParams();
                    a = AlphaParamMetric.getAlpha((Double)pa.get("d"), (Double)pa.get("p")); //NOI18N
                }
                if(multi && Double.isNaN(dmax)) {
                    dmax = -Math.log(0.05) / a;
                }
                RasterLayer raster = DistribModel.interpolate(metric, null, res, a, 
                        multi, dmax, agreg, Config.getProgressBar("Interpolation " + metric.getFullName())); //NOI18N
                raster.saveRaster(new File(project.getDirectory(), (name != null ? name + "-" : "") + metric.getFullName().replace('/', '-') + ".tif")); //NOI18N //NOI18N
                
            }
        }
    }

    private void circuit(List<String> args) throws IOException, SchemaException {
        final double threshold;
        if(!args.isEmpty() && args.get(0).startsWith("corridor=")) { //NOI18N
            threshold = Double.parseDouble(args.remove(0).split("=")[1]); //NOI18N
        } else {
            threshold = 0;
        }
        boolean optim = false;
        if(!args.isEmpty() && args.get(0).equals("optim")) { //NOI18N
            optim = true;
        }
        boolean con4 = false;
        if(!args.isEmpty() && args.get(0).equals("con4")) { //NOI18N
            con4 = true;
        }
        final Set<String> linkIds = new HashSet<>();
        if(!args.isEmpty() && args.get(0).startsWith("link=")) { //NOI18N
            String [] toks = args.remove(0).split("=")[1].split(","); //NOI18N
            linkIds.addAll(Arrays.asList(toks));
        } else if(!args.isEmpty() && args.get(0).startsWith("flink=")) { //NOI18N
            File f = new File(args.remove(0).split("=")[1]); //NOI18N
            linkIds.addAll(readFile(f));
        }
        
        for(Linkset link : getLinksets()) {
            if(link.getTypeDist() == Distance.EUCLID) {
                continue;
            }
            System.out.println("Linkset : " + link.getName()); //NOI18N
            
            final CircuitRaster circuit = link.getCircuitVersion(link.getHabitat()).getCircuit();
                    
            final File dir = new File(project.getDirectory(), link.getName() + "-circuit"); //NOI18N
            dir.mkdir();
            final List<DefaultFeature> corridors;
            try (FileWriter fw = new FileWriter(new File(dir, "resistances.csv"))) { //NOI18N
                fw.write("Id1,Id2,R\n"); //NOI18N
                corridors = new ArrayList<>();
                SimpleParallelTask task = new SimpleParallelTask<Path>(link.getPaths()) {
                    @Override
                    protected void executeOne(Path p) {
                        try {
                            if(!linkIds.isEmpty() && !linkIds.contains((String)p.getId())) {
                                return;
                            }
                            long t1 = System.currentTimeMillis();
                            CircuitRaster.PatchODCircuit odCircuit = circuit.getODCircuit(p.getPatch1(), p.getPatch2());
                            odCircuit.solve();
                            long t2 = System.currentTimeMillis();
                            synchronized (CLITools.this) {
                                System.out.println(p.getPatch1() + " - " + p.getPatch2() + " : " + odCircuit.getZone()); //NOI18N
                                System.out.print("R : " + odCircuit.getR()); //NOI18N
                                System.out.print(" - cost : " + p.getCost()); //NOI18N
                                System.out.println(" - time : " + (t2 - t1) / 1000.0 + "s"); //NOI18N
                                System.out.println("Nb iteration : " + odCircuit.getNbIter()); //NOI18N
                                System.out.println("Err max : " + odCircuit.getErrMax()); //NOI18N
                                System.out.println("Err max Wo 2 : " + odCircuit.getErrMaxWithoutFirst()); //NOI18N
                                System.out.println("Err sum : " + odCircuit.getErrSum()); //NOI18N
                                fw.write(p.getPatch1() + "," + p.getPatch2() + "," + odCircuit.getR() + "\n"); //NOI18N
                                fw.flush();
                                Raster raster = odCircuit.getCurrentMap();
                                new RasterLayer("", new RasterShape(raster, JTS.envToRect(odCircuit.getEnvelope()), new RasterStyle(), true), project.getCRS()) //NOI18N
                                        .saveRaster(new File(dir, p.getPatch1() + "-" + p.getPatch2() + "-cur.tif")); //NOI18N
                                if (threshold > 0) {
                                    raster = odCircuit.getCorridorMap(threshold);
                                    new RasterLayer("", new RasterShape(raster, JTS.envToRect(odCircuit.getEnvelope()), new RasterStyle(), true), project.getCRS()) //NOI18N
                                            .saveRaster(new File(dir, p.getPatch1() + "-" + p.getPatch2() + "-cor.tif")); //NOI18N
                                    Geometry poly = odCircuit.getCorridor(threshold);
                                    if (!poly.isEmpty()) {
                                        corridors.add(new DefaultFeature(p.getPatch1() + "-" + p.getPatch2(), poly)); //NOI18N
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(CLITools.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                };
                new ParallelFExecutor(task).executeAndWait();
            }
            if(threshold > 0) { 
                IOFeature.saveFeatures(corridors, new File(dir, "corridor-" + threshold + ".gpkg"), project.getCRS()); //NOI18N
            }
        }
    }
    
    private void corridor(List<String> args) throws IOException, SchemaException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("maxcost"), Arrays.asList("format", "beta", "var", "d", "p")); //NOI18N
                
        Range rMax = Range.parse(params.get("maxcost")); //NOI18N
        boolean raster = "raster".equals(params.get("format")); //NOI18N
        double beta = 0;
        if(params.containsKey("beta")) { //NOI18N
            beta = Double.parseDouble(params.get("beta")); //NOI18N
        }
        String var = null;
        if(params.containsKey("var")) { //NOI18N
            var = params.get("var"); //NOI18N
        }
        double p = 0;
        Range dr = null;
        if(params.containsKey("d")) { //NOI18N
            dr = Range.parse(params.get("d")); //NOI18N
        }
        if(params.containsKey("p")) { //NOI18N
            p = Double.parseDouble(params.get("p")); //NOI18N
        }
        
        if((beta != 0 || var != null) && (dr == null || p == 0)) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("BETA AND VAR PARAMETERS NEED D AND P PARAMETERS"));
        }
        
        for(AbstractGraph graph : getGraphs()) {
            Linkset link = graph.getLinkset();

            System.out.println("Graph : " + graph.getName()); //NOI18N
            for(double maxCost : rMax.getValues(link)) {
                ProgressBar progress = Config.getProgressBar("maxCost : " + maxCost); //NOI18N
                if(raster) {
                    double d = 0, alpha = 0;
                    if(dr != null && p != 0) {
                        d = dr.getValue(link);
                        alpha = AlphaParamMetric.getAlpha(d, p);
                    }
                    Raster corridors = link.computeRasterCorridor(progress, graph, maxCost, alpha, beta, var);
                    Rectangle2D zone = project.getZone();
                    new RasterLayer("corridor", new RasterShape(corridors, zone, new RasterStyle(), true), project.getCRS()) //NOI18N
                            .saveRaster(new File(project.getDirectory(), graph.getName() +
                                "-corridor-" + maxCost + (var != null ? "-" + var : beta != 0 ? ("-beta"+beta) : "")  //NOI18N
                                    + (d+p > 0 ? "-d" + d + "-p" + p : "") + ".tif")); //NOI18N
                } else {
                    List<Feature> corridors = link.computeCorridor(progress, graph, maxCost);
                    IOFeature.saveFeatures(corridors, new File(project.getDirectory(), graph.getName() +
                            "-corridor-" + maxCost + ".gpkg"), project.getCRS()); //NOI18N
                }
            }
        }
    }
    
    private void calcDataDistance(List<String> args) throws IOException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("type", "distance"), Arrays.asList("d", "p")); //NOI18N
        String distType = params.get("distance"); //NOI18N
        Habitat.Distance type;
        switch (distType) {
            case "euclidean": //NOI18N
                type = Habitat.Distance.EUCLIDEAN;
                break;
            case "leastcost": //NOI18N
                type = Habitat.Distance.LEASTCOST;
                break;
            case "circuit": //NOI18N
                type = Habitat.Distance.CIRCUIT;
                break;
            case "flow": //NOI18N
                type = Habitat.Distance.FLOW;
                break;
            case "circuitflow": //NOI18N
                type = Habitat.Distance.CIRCUIT_FLOW;
                break;
            default:
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN DISTANCE TYPE : {0}"), distType));
        }
        for(Habitat dataset : getHabitats()) {
            if(params.get("type").equals("space")) { //NOI18N
                ProgressBar mon = Config.getProgressBar(dataset.getName() + " - distances...", getLinksets().size()*100); //NOI18N
                for(Linkset linkset : getLinksets()) {
                    double [][][] distances = dataset.calcSpaceDistanceMatrix(linkset, type, mon.getSubProgress(100));
                    dataset.saveMatrix(distances, new File(project.getDirectory(), "distance_" + dataset.getName() + "_space_" + linkset.getName() + "-" + distType + ".txt")); //NOI18N
                }
            } else {
                double alpha = Double.NaN;
                if(type == Habitat.Distance.FLOW || type == Habitat.Distance.CIRCUIT_FLOW) {
                    alpha = AlphaParamMetric.getAlpha(Double.parseDouble(params.get("d")), Double.parseDouble(params.get("p"))); //NOI18N
                }
                ProgressBar mon = Config.getProgressBar(dataset.getName() + " - distances...", getGraphs().size()*100); //NOI18N
                for(AbstractGraph graph : getGraphs()) {
                    if(graph.getHabitat().getIdHabitats().containsAll(dataset.getIdHabitats())) {
                        double [][][] distances = dataset.calcGraphDistanceMatrix(graph, type, alpha, mon.getSubProgress(100));
                        dataset.saveMatrix(distances, new File(project.getDirectory(), "distance_" + dataset.getName() + "_graph_" + graph.getName() + "-" + distType +
                                (Double.isNaN(alpha) ? "" : "-d"+params.get("d")+"-p"+params.get("p")) + ".txt")); //NOI18N
                    }
                }
            }
        }
    }
    
    private void createTopoLinks(List<String> args) throws IOException {
        for(AbstractGraph graph : getGraphs()) {
            graph.createTopoLinks();
        }
    }
    
    private void createBuffer(List<String> args) throws IOException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("file", "maxcost"), Collections.emptyList()); //NOI18N
        Range dists = Range.parse(params.get("maxcost"));
        File file = new File(params.get("file"));
        CoordinateReferenceSystem crs = IOFeature.getCRS(file);
        
        for(Linkset linkset : getLinksets()) {
            if(!(linkset instanceof RasterLinkset)) {
                System.out.println("Linkset " + linkset.getName() + " skipped.");
                continue;
            }
            List<DefaultFeature> buffers = new ArrayList<>();
            List<DefaultFeature> features = IOFeature.loadFeatures(file);
            for(double dist : dists.getValues(linkset)) {
                for(DefaultFeature f : features) {
                    DefaultFeature buf = new DefaultFeature(f, true, true);
                    buf.setGeometry(((RasterLinkset)linkset).getPathFinder().getBuffer(f.getGeometry(), dist));
                    buf.addAttribute("radius", dist);
                    buffers.add(buf);
                }
            }
            IOFeature.saveFeatures(buffers, new File(file.getParentFile(), 
                    file.getName().substring(0, file.getName().lastIndexOf('.')) + "-buffer-" + linkset.getName() + ".gpkg"), crs);
        }
    }
    
    
    private void extractPathCost(List<String> args) throws IOException {
        extractAndCheckParams(args, Collections.emptyList(), Collections.emptyList()); //NOI18N
        for(Linkset linkset : getLinksets()) {
            if(linkset instanceof CostLinkset) {
                HashMap2D map = ((CostLinkset)linkset).extractCostFromPath();
                map.saveToCSV(new File(linkset.getHabitat().getDir(), linkset.getName() + "-links-extract-cost.csv"));
            }
        }
    }
    
    private List<String> readFile(File f) throws IOException {
        List<String> list = new ArrayList<>();
        try (BufferedReader r = new BufferedReader(new FileReader(f))) {
            String l;
            while((l = r.readLine()) != null) {
                if(!l.trim().isEmpty()) {
                    list.add(l.trim());
                }
            }
        }
        return list;
    }

    private Map<String, Range> readMetricParams(List<String> args) {
        HashMap<String, Range> ranges = new LinkedHashMap<>();
        while(!args.isEmpty() && args.get(0).contains("=")) { //NOI18N
            String [] tok = args.remove(0).split("="); //NOI18N
            Range r = Range.parse(tok[1]);
            ranges.put(tok[0], r);
        }
        return ranges;
    }
    
    private static Map<String, String> extractAndCheckParams(List<String> args, List<String> mandatoryParams, List<String> optionalParams) {
        Map<String, String> params = new LinkedHashMap<>();
                
        while(!args.isEmpty() && !args.get(0).startsWith("--")) { //NOI18N
            String arg = args.remove(0);
            if(arg.contains("=")) { //NOI18N
                params.put(arg.substring(0, arg.indexOf("=")), arg.substring(arg.indexOf("=")+1)); //NOI18N
            } else {
                params.put(arg, null);
            }
        }
        
        // check mandatory parameters
        if(!params.keySet().containsAll(mandatoryParams)) {
            HashSet<String> set = new HashSet<>(mandatoryParams);
            set.removeAll(params.keySet());
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("MANDATORY PARAMETERS ARE MISSING : {0}"), new Object[] {Arrays.deepToString(set.toArray())}));
        }
        
        // check unknown parameters if optionalParams is set
        if(optionalParams != null) {
            HashSet<String> set = new HashSet<>(params.keySet());
            set.removeAll(mandatoryParams);
            set.removeAll(optionalParams);
            if(!set.isEmpty()) {
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN PARAMETERS : {0}"), new Object[] {Arrays.deepToString(set.toArray())}));
            }
        }
        
        return params;
    }

    
}
