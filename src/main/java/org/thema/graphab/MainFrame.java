/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.awt.SplashScreen;
import java.awt.Toolkit;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.geotools.feature.SchemaException;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.thema.common.Config;
import org.thema.common.JavaLoader;
import org.thema.common.ProgressBar;
import org.thema.common.Util;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.swing.LoggingDialog;
import org.thema.common.swing.PreferencesDialog;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.drawshape.image.RasterShape;
import org.thema.drawshape.layer.FeatureLayer;
import org.thema.drawshape.layer.RasterLayer;
import org.thema.drawshape.style.CircleStyle;
import org.thema.drawshape.style.FeatureStyle;
import org.thema.drawshape.style.RasterStyle;
import org.thema.drawshape.style.table.ColorRamp;
import org.thema.drawshape.style.table.FeatureAttributeCollection;
import org.thema.drawshape.style.table.FeatureAttributeIterator;
import org.thema.drawshape.style.table.StrokeRamp;
import org.thema.drawshape.style.table.UniqueColorTable;
import org.thema.graph.shape.GraphGroupLayer;
import org.thema.graphab.Project.Method;
import org.thema.graphab.addpatch.AddPatchDialog;
import org.thema.graphab.dataset.NewDatasetDialog;
import org.thema.graphab.dataset.VectorDataset;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.DefaultGraph;
import org.thema.graphab.graph.MultiGraph;
import org.thema.graphab.graph.NewGraphDialog;
import org.thema.graphab.graph.NewMultiGraphDialog;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.CapaPatchDialog;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.HabitatLayer;
import org.thema.graphab.habitat.HabitatPanel;
import org.thema.graphab.habitat.MetaPatchDialog;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.habitat.MonoVectorHabitat;
import org.thema.graphab.habitat.VectorHabitatDialog;
import org.thema.graphab.links.CorridorDialog;
import org.thema.graphab.links.NewLinksetDialog;
import org.thema.graphab.metric.AbstractAttributeMetricResult;
import org.thema.graphab.metric.AbstractLocalMetricResult;
import org.thema.graphab.metric.BatchGraphMetricDialog;
import org.thema.graphab.metric.BatchGraphMetricTask;
import org.thema.graphab.metric.BatchParamMetricDialog;
import org.thema.graphab.metric.CalcMetricDialog;
import org.thema.graphab.metric.MetricDialog;
import org.thema.graphab.metric.MetricResult;
import org.thema.graphab.metric.global.CompMetricResult;
import org.thema.graphab.metric.global.DeltaMetricResult;
import org.thema.graphab.metric.global.GlobalMetric;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.graphab.metric.local.LocalMetric;
import org.thema.graphab.metric.local.LocalMetricResult;
import org.thema.graphab.model.MetricInterpolDlg;
import org.thema.graphab.model.RandomPointDlg;
import org.thema.graphab.mpi.MpiLauncher;
import org.thema.graphab.util.SerieFrame;
import org.thema.parallel.ExecutorService;

/**
 * The main frame of the Graphab software.
 * 
 * @author Gilles Vuidel
 */
public class MainFrame extends javax.swing.JFrame {

    private Project project;
    private LoggingDialog logFrame;
    private MetricDialog metricDlg;

    /** 
     * Creates new form MainFrame
     */
    public MainFrame() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/thema/graphab/ressources/ico64_graphab.png"))); //NOI18N
        initComponents();
        setLocationRelativeTo(null);
        setTitle("Graphab - " + getVersion()); //NOI18N
        mapViewer.putAddLayerButton();
        mapViewer.putExportButton();
        
        Config.setProgressBar(mapViewer.getProgressBar());
        logFrame = new LoggingDialog(this);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mapViewer = new org.thema.drawshape.ui.MapViewer();
        statusPanel = new javax.swing.JPanel();
        jMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newProjectMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        prefMenuItem = new javax.swing.JMenuItem();
        logMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        quitMenuItem = new javax.swing.JMenuItem();
        habMenu = new javax.swing.JMenu();
        habitatRasterMenuItem = new javax.swing.JMenuItem();
        habitatVectorMenuItem = new javax.swing.JMenuItem();
        capaMenuItem = new javax.swing.JMenuItem();
        metaPatchMenuItem = new javax.swing.JMenuItem();
        graphMenu = new javax.swing.JMenu();
        costDistMenuItem = new javax.swing.JMenuItem();
        createMenuItem = new javax.swing.JMenuItem();
        mergeGraphMenuItem = new javax.swing.JMenuItem();
        corridorMenuItem = new javax.swing.JMenuItem();
        dataMenu = new javax.swing.JMenu();
        addDataMenuItem = new javax.swing.JMenuItem();
        randomPointMenuItem = new javax.swing.JMenuItem();
        setDEMMenuItem = new javax.swing.JMenuItem();
        metricMenu = new javax.swing.JMenu();
        showMetricMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        globalMetricMenuItem = new javax.swing.JMenuItem();
        compMetricMenuItem = new javax.swing.JMenuItem();
        localMetricMenuItem = new javax.swing.JMenuItem();
        delatMetricMenuItem = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        batchGraphMetricMenuItem = new javax.swing.JMenuItem();
        batchParamMetricMenu = new javax.swing.JMenu();
        batchParamLocalMenuItem = new javax.swing.JMenuItem();
        batchParamGlobalMenuItem = new javax.swing.JMenuItem();
        analysisMenu = new javax.swing.JMenu();
        interpMetricMenuItem = new javax.swing.JMenuItem();
        addPatchMenuItem = new javax.swing.JMenuItem();
        landmodMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle"); // NOI18N
        setTitle(bundle.getString("MainFrame.title")); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 734, Short.MAX_VALUE)
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        fileMenu.setText(bundle.getString("MainFrame.fileMenu.text")); // NOI18N

        newProjectMenuItem.setText(bundle.getString("MainFrame.newProjectMenuItem.text")); // NOI18N
        newProjectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newProjectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newProjectMenuItem);

        openMenuItem.setText(bundle.getString("MainFrame.openMenuItem.text")); // NOI18N
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        prefMenuItem.setText(bundle.getString("MainFrame.prefMenuItem.text")); // NOI18N
        prefMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prefMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(prefMenuItem);

        logMenuItem.setText(bundle.getString("MainFrame.logMenuItem.text")); // NOI18N
        logMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(logMenuItem);
        fileMenu.add(jSeparator1);

        quitMenuItem.setText(bundle.getString("MainFrame.quitMenuItem.text")); // NOI18N
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        jMenuBar.add(fileMenu);

        habMenu.setText(bundle.getString("MainFrame.habMenu.text")); // NOI18N

        habitatRasterMenuItem.setText(bundle.getString("MainFrame.habitatRasterMenuItem.text")); // NOI18N
        habitatRasterMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                habitatRasterMenuItemActionPerformed(evt);
            }
        });
        habMenu.add(habitatRasterMenuItem);

        habitatVectorMenuItem.setText(bundle.getString("MainFrame.habitatVectorMenuItem.text")); // NOI18N
        habitatVectorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                habitatVectorMenuItemActionPerformed(evt);
            }
        });
        habMenu.add(habitatVectorMenuItem);

        capaMenuItem.setText(bundle.getString("MainFrame.capaMenuItem.text")); // NOI18N
        capaMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                capaMenuItemActionPerformed(evt);
            }
        });
        habMenu.add(capaMenuItem);

        metaPatchMenuItem.setText(bundle.getString("MainFrame.metaPatchMenuItem.text")); // NOI18N
        metaPatchMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                metaPatchMenuItemActionPerformed(evt);
            }
        });
        habMenu.add(metaPatchMenuItem);

        jMenuBar.add(habMenu);

        graphMenu.setText(bundle.getString("MainFrame.graphMenu.text")); // NOI18N

        costDistMenuItem.setText(bundle.getString("MainFrame.costDistMenuItem.text")); // NOI18N
        costDistMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                costDistMenuItemActionPerformed(evt);
            }
        });
        graphMenu.add(costDistMenuItem);

        createMenuItem.setText(bundle.getString("MainFrame.createMenuItem.text")); // NOI18N
        createMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createGraphMenuItemActionPerformed(evt);
            }
        });
        graphMenu.add(createMenuItem);

        mergeGraphMenuItem.setText(bundle.getString("MainFrame.mergeGraphMenuItem.text")); // NOI18N
        mergeGraphMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mergeGraphMenuItemActionPerformed(evt);
            }
        });
        graphMenu.add(mergeGraphMenuItem);

        corridorMenuItem.setText(bundle.getString("MainFrame.corridorMenuItem.text")); // NOI18N
        corridorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corridorMenuItemActionPerformed(evt);
            }
        });
        graphMenu.add(corridorMenuItem);

        jMenuBar.add(graphMenu);

        dataMenu.setText(bundle.getString("MainFrame.dataMenu.text")); // NOI18N

        addDataMenuItem.setText(bundle.getString("MainFrame.addDataMenuItem.text")); // NOI18N
        addDataMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDataMenuItemActionPerformed(evt);
            }
        });
        dataMenu.add(addDataMenuItem);

        randomPointMenuItem.setText(bundle.getString("MainFrame.randomPointMenuItem.text")); // NOI18N
        randomPointMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomPointMenuItemActionPerformed(evt);
            }
        });
        dataMenu.add(randomPointMenuItem);

        setDEMMenuItem.setText(bundle.getString("MainFrame.setDEMMenuItem.text")); // NOI18N
        setDEMMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setDEMMenuItemActionPerformed(evt);
            }
        });
        dataMenu.add(setDEMMenuItem);

        jMenuBar.add(dataMenu);

        metricMenu.setText(bundle.getString("MainFrame.metricMenu.text")); // NOI18N

        showMetricMenuItem.setText(bundle.getString("MainFrame.showMetricMenuItem.text")); // NOI18N
        showMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showMetricMenuItemActionPerformed(evt);
            }
        });
        metricMenu.add(showMetricMenuItem);
        metricMenu.add(jSeparator2);

        globalMetricMenuItem.setText(bundle.getString("MainFrame.globalMetricMenuItem.text")); // NOI18N
        globalMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                globalMetricMenuItemActionPerformed(evt);
            }
        });
        metricMenu.add(globalMetricMenuItem);

        compMetricMenuItem.setText(bundle.getString("MainFrame.compMetricMenuItem.text")); // NOI18N
        compMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                compMetricMenuItemActionPerformed(evt);
            }
        });
        metricMenu.add(compMetricMenuItem);

        localMetricMenuItem.setText(bundle.getString("MainFrame.localMetricMenuItem.text")); // NOI18N
        localMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                localMetricMenuItemActionPerformed(evt);
            }
        });
        metricMenu.add(localMetricMenuItem);

        delatMetricMenuItem.setText(bundle.getString("MainFrame.delatMetricMenuItem.text")); // NOI18N
        delatMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deltaIndiceMenuItemActionPerformed(evt);
            }
        });
        metricMenu.add(delatMetricMenuItem);
        metricMenu.add(jSeparator3);

        batchGraphMetricMenuItem.setText(bundle.getString("MainFrame.batchGraphMetricMenuItem.text")); // NOI18N
        batchGraphMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchGraphMetricMenuItemActionPerformed(evt);
            }
        });
        metricMenu.add(batchGraphMetricMenuItem);

        batchParamMetricMenu.setText(bundle.getString("MainFrame.batchParamMetricMenu.text")); // NOI18N

        batchParamLocalMenuItem.setText(bundle.getString("MainFrame.batchParamLocalMenuItem.text")); // NOI18N
        batchParamLocalMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchParamLocalMenuItemActionPerformed(evt);
            }
        });
        batchParamMetricMenu.add(batchParamLocalMenuItem);

        batchParamGlobalMenuItem.setText(bundle.getString("MainFrame.batchParamGlobalMenuItem.text")); // NOI18N
        batchParamGlobalMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchParamGlobalMenuItemActionPerformed(evt);
            }
        });
        batchParamMetricMenu.add(batchParamGlobalMenuItem);

        metricMenu.add(batchParamMetricMenu);

        jMenuBar.add(metricMenu);

        analysisMenu.setText(bundle.getString("MainFrame.analysisMenu.text")); // NOI18N

        interpMetricMenuItem.setText(bundle.getString("MainFrame.interpMetricMenuItem.text")); // NOI18N
        interpMetricMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                interpMetricMenuItemActionPerformed(evt);
            }
        });
        analysisMenu.add(interpMetricMenuItem);

        addPatchMenuItem.setText(bundle.getString("MainFrame.addPatchMenuItem.text")); // NOI18N
        addPatchMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPatchMenuItemActionPerformed(evt);
            }
        });
        analysisMenu.add(addPatchMenuItem);

        landmodMenuItem.setText(bundle.getString("MainFrame.landmodMenuItem.text")); // NOI18N
        landmodMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                landmodMenuItemActionPerformed(evt);
            }
        });
        analysisMenu.add(landmodMenuItem);

        jMenuBar.add(analysisMenu);

        helpMenu.setText(bundle.getString("MainFrame.helpMenu.text")); // NOI18N

        aboutMenuItem.setText(bundle.getString("MainFrame.aboutMenuItem.text")); // NOI18N
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        jMenuBar.add(helpMenu);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mapViewer, javax.swing.GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(statusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mapViewer, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(164, 164, 164)
                    .addComponent(statusPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(343, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void newProjectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newProjectMenuItemActionPerformed
        final NewProjectDialog dlg = new NewProjectDialog(this);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            try {
                project = dlg.createProject();
                mapViewer.setRootLayer(project.getRootLayer());
                mapViewer.setTreeLayerVisible(true);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Error") + ex.getLocalizedMessage());
            }
        }).start();
        

    }//GEN-LAST:event_newProjectMenuItemActionPerformed

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        final File f = Util.getFile(".xml", java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Project_file"));
        if (f == null) {
            return;
        }
        new Thread(() -> {
            try {
                loadProject(f);
            } catch (IOException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Error_while_loading_project") + ex.getLocalizedMessage());
            }
        }).start();
        

    }//GEN-LAST:event_openMenuItemActionPerformed

    private void costDistMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_costDistMenuItemActionPerformed
        NewLinksetDialog dlg = new NewLinksetDialog(this, project);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }

        new Thread(() -> {
            try {
                project.addLinkset(dlg.getLinkset(), true);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
        
    }//GEN-LAST:event_costDistMenuItemActionPerformed

    private void createGraphMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createGraphMenuItemActionPerformed
       final NewGraphDialog dlg = new NewGraphDialog(this, project);
       dlg.setVisible(true);
       if(!dlg.isOk) {
           return;
       }

       new Thread(() -> {
           try {
               project.addGraph(new DefaultGraph(dlg.name, dlg.linkset, dlg.threshold, dlg.intraPatchDist), true);
           } catch (IOException | SchemaException ex) {
               Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
               JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
           }
       }).start();
       


    }//GEN-LAST:event_createGraphMenuItemActionPerformed

    private void addDataMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDataMenuItemActionPerformed
        NewDatasetDialog dlg = new NewDatasetDialog(this, project);
        dlg.setVisible(true);
        
        if(!dlg.isOk) {
            return;
        }
        
        
        new Thread(() -> {
            try {
                List<DefaultFeature> features = dlg.shpFile ? IOFeature.loadFeatures(dlg.file) : IOFeature.loadFeatures(dlg.file, dlg.xAttr, dlg.yAttr, null);
                project.addDataset(new VectorDataset(dlg.name, project, features), true);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_addDataMenuItemActionPerformed

    private void globalMetricMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_globalMetricMenuItemActionPerformed
        final CalcMetricDialog<GlobalMetric> dlg = new CalcMetricDialog(this, project, project.getGraphs(), Method.GLOBAL);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            try {
                long start = System.currentTimeMillis();
                GlobalMetricResult metric = new GlobalMetricResult(dlg.name == null ? dlg.metric.getDetailName() : dlg.name.trim(), dlg.metric, dlg.graph);
                project.addMetric(metric, true);
                String res = metric.getFullName() + " : " + Arrays.deepToString(metric.getResult()) + "\n"; //NOI18N
                
                System.out.println(res);
                System.out.println("Elapsed time : " + (System.currentTimeMillis()-start)); //NOI18N
                JOptionPane.showMessageDialog(MainFrame.this, new JScrollPane(new JTextArea(res, 10, 40)));
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            } 
        }).start();
    }//GEN-LAST:event_globalMetricMenuItemActionPerformed

    private void batchGraphMetricMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batchGraphMetricMenuItemActionPerformed
        final BatchGraphMetricDialog dlg = new BatchGraphMetricDialog(this, project);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        if(dlg.min + dlg.inc > dlg.max) {
            JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Les_valeurs_de_seuils_sont_erronées."));
           return;
        }

        new Thread(() -> {
            ProgressBar monitor = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Calc_metrics..."));
            GlobalMetric indice = dlg.metric;
            BatchGraphMetricTask task = new BatchGraphMetricTask(monitor, dlg.linkset, dlg.distAbs, 
                    dlg.min, dlg.inc, dlg.max, dlg.metric, dlg.intraPatchDist);
            new ParallelFExecutor(task).executeAndWait();
            if(task.isCanceled()) {
                return;
            }
            SortedMap<Double, Double[]> results = task.getResult();
            XYSeriesCollection series = new XYSeriesCollection();
            String[] resultNames = indice.getResultNames(new DefaultGraph("g", dlg.linkset, //NOI18N
                    dlg.min, dlg.intraPatchDist));
            for (int j = 0; j < resultNames.length; j++) {
                XYSeries serie = new XYSeries(indice.getName());
                for (Double x1 : results.keySet()) {
                    serie.add(x1, results.get(x1)[j]);
                }
                series.addSeries(serie);
            }
            SerieFrame frm = new SerieFrame(indice.getDetailName(),
                    series, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Threshold"), "");
            frm.pack();
            frm.setLocationRelativeTo(MainFrame.this);
            frm.setVisible(true);
            monitor.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Saving..."));
            monitor.close();
        }).start();
}//GEN-LAST:event_batchGraphMetricMenuItemActionPerformed

    private void deltaIndiceMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deltaIndiceMenuItemActionPerformed
        final CalcMetricDialog<GlobalMetric> dlg = new CalcMetricDialog<>(this, project, project.getGraphs(), Method.DELTA);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        JPanel panel = new JPanel();
        JCheckBox checkNode = new JCheckBox(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Nodes"), true); panel.add(checkNode);
        JCheckBox checkEdge = new JCheckBox(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Edges")); panel.add(checkEdge);
        int res = JOptionPane.showConfirmDialog(this, panel, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Delta_metrics"), JOptionPane.OK_CANCEL_OPTION);
        if(res != JOptionPane.OK_OPTION) {
            return;
        }

        final boolean node = checkNode.isSelected();
        final boolean edge = checkEdge.isSelected();
        
        if(!node && !edge) {
            // nothing to do
            return;
        }

        new Thread(() -> {
            try {
                DeltaMetricResult metric = new DeltaMetricResult(dlg.name == null ? "d_" + dlg.metric.getDetailName() : dlg.name.trim(), dlg.metric, dlg.graph); //NOI18N
                if(!node) {
                    metric.setFilter("edge"); //NOI18N
                }
                if(!edge) {
                    metric.setFilter("node"); //NOI18N
                }
                project.addMetric(metric, true);

                // show the result
                viewMetricResult(metric, 0);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_deltaIndiceMenuItemActionPerformed

    private void localMetricMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_localMetricMenuItemActionPerformed
        final CalcMetricDialog<LocalMetric> dlg = new CalcMetricDialog<>(this, project, project.getGraphs(), Method.LOCAL);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            try {
                long start = System.currentTimeMillis();
                LocalMetricResult metric = new LocalMetricResult(dlg.name == null ? dlg.metric.getDetailName() : dlg.name.trim(), dlg.metric, dlg.graph);
                metric.setFilter(dlg.filter);
                project.addMetric(metric, true);
                Logger.getLogger(MainFrame.class.getName()).info(metric.getFullName() + " - Elapsed time : " + (System.currentTimeMillis()-start) + "ms"); //NOI18N
                
                // show the result
                viewMetricResult(metric, 0);
                
            } catch(IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            } 
        }).start();
    }//GEN-LAST:event_localMetricMenuItemActionPerformed

    private void compMetricMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_compMetricMenuItemActionPerformed
       
        final CalcMetricDialog<GlobalMetric> dlg = new CalcMetricDialog<>(this, project, project.getGraphs(), Method.COMP);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            try {
                CompMetricResult metric = new CompMetricResult(dlg.name == null ? dlg.metric.getDetailName() : dlg.name.trim(), dlg.metric, dlg.graph);
                project.addMetric(metric, true);
                
                viewMetricResult(metric, 0);
                
            } catch(IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_compMetricMenuItemActionPerformed

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuItemActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_quitMenuItemActionPerformed

    private void prefMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prefMenuItemActionPerformed
        PreferencesDialog dlg = new PreferencesDialog(this, true);
        dlg.setProcPanelVisible(true);
        dlg.setVisible(true);
    }//GEN-LAST:event_prefMenuItemActionPerformed

    private void batchParamLocalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batchParamLocalMenuItemActionPerformed
        List<LocalMetric> metrics = new ArrayList<>();
        for(LocalMetric ind : Project.getLocalMetrics()) {
            if(ind.hasParams()) {
                metrics.add(ind);
            }
        }
        final BatchParamMetricDialog<LocalMetric> dlg = new BatchParamMetricDialog<>
                (this, project, project.getGraphs(), metrics);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
         new Thread(() -> {
             try {
                 int n = (int)((dlg.max - dlg.min) / dlg.inc) + 1;
                 ProgressBar monitor = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Calc_local_metric_") + dlg.metric.getName(), n*100);
                 
                 Map<String, Object> params = dlg.metric.getParams();
                 for(double p = dlg.min; p <= dlg.max; p += dlg.inc) {
                     params.put(dlg.param, p);
                     dlg.metric.setParams(params);
                     monitor.setNote(dlg.param + " : " + String.format("%g", p)); //NOI18N
                     new LocalMetricResult("Batch", dlg.metric, dlg.graph).calculate(true, monitor.getSubProgress(100));                      //NOI18N
                 }
                 
                 monitor.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Saving..."));
                 
                 if(dlg.metric.calcEdges()) {
                    dlg.graph.getLinkset().saveLinks(false);
                 }
                 if(dlg.metric.calcNodes()) {
                    dlg.graph.getHabitat().savePatch();
                 }
                 
                 monitor.close();
             } catch(IOException | SchemaException ex) {
                 Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(MainFrame.this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
             }
        }).start();
    }//GEN-LAST:event_batchParamLocalMenuItemActionPerformed

    private void batchParamGlobalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batchParamGlobalMenuItemActionPerformed
        List<GlobalMetric> metrics = new ArrayList<>();
        for(GlobalMetric ind : project.getGlobalMetricsFor(Method.GLOBAL)) {
            if(ind.hasParams()) {
                metrics.add(ind);
            }
        }
        final BatchParamMetricDialog<GlobalMetric> dlg = new BatchParamMetricDialog<>
                (this, project, project.getGraphs(), metrics);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            ProgressBar monitor = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Calc_global_metric_") + dlg.metric.getName(),
                    (int)((dlg.max-dlg.min)/dlg.inc+1)*100);
            TreeMap<Double, Double[]> results = new TreeMap<>();
            Map<String, Object> params = dlg.metric.getParams();
            for(double p = dlg.min; p <= dlg.max; p += dlg.inc) {
                params.put(dlg.param, p);
                dlg.metric.setParams(params);
                monitor.setNote(dlg.param + " : " + String.format("%g", p));
                Double [] res = new GlobalMetricResult("Batch", dlg.metric, dlg.graph).calculate(true, monitor.getSubProgress(100)).getResult();
                results.put(p, res);
            }
            monitor.close();
            XYSeriesCollection series = new XYSeriesCollection();
            for (int j = 0; j < ((GlobalMetric)dlg.metric).getResultNames(dlg.graph).length; j++) {
                XYSeries serie = new XYSeries(((GlobalMetric)dlg.metric).getResultNames(dlg.graph)[j]);
                for (Double x1 : results.keySet()) {
                    serie.add(x1, results.get(x1)[j]);
                }
                series.addSeries(serie);
            }
            SerieFrame frm = new SerieFrame(dlg.metric.getName() ,
                    series, dlg.param, "");
            frm.pack();
            frm.setLocationRelativeTo(MainFrame.this);
            frm.setVisible(true);
        }).start();
    }//GEN-LAST:event_batchParamGlobalMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        String text = "<html>\n" +
                "<h1>Graphab 3</h1>\n" +
                "<p>\n" +
                "Laboratoire ThéMA - UMR 6049<br/>\n" +
                "CNRS - Université de Franche Comté<br/>\n" +
                "</p>\n" +
                "<br/>\n" +
                "<p>\n" +
                "J. C. Foltête, G. Vuidel, C. Clauzel, X. Girardet, M. Bourgeois, P. Savary<br>\n" +
                "</p>\n" +
                "<br/>\n" +
                "<a href=\"https://sourcesup.renater.fr/www/graphab\">https://sourcesup.renater.fr/www/graphab</a>\n" +
                "</html>";
        JEditorPane pane = new JEditorPane("text/html", text);
        pane.setBackground(new Color(0, 0, 0, 0));
        pane.setEditable(false);
        JOptionPane.showMessageDialog(this, pane, 
            "Graphab - " + getVersion(), JOptionPane.PLAIN_MESSAGE, new ImageIcon(getIconImage()));
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void logMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logMenuItemActionPerformed
        logFrame.setVisible(true);
    }//GEN-LAST:event_logMenuItemActionPerformed

    private void interpMetricMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_interpMetricMenuItemActionPerformed
        new MetricInterpolDlg(this, project, true).setVisible(true);
    }//GEN-LAST:event_interpMetricMenuItemActionPerformed

    private void metaPatchMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_metaPatchMenuItemActionPerformed
        final MetaPatchDialog dlg = new MetaPatchDialog(null, project.getGraphs());
                dlg.setVisible(true);
                if(!dlg.isOk) {
                    return;
                }
                if(project.isHabitatExists(dlg.habitatName)) {
                    JOptionPane.showMessageDialog(this, "The habitat name already exists", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                new Thread(() -> {
                    try {
                        AbstractMonoHabitat metaPatch = project.createMetaPatchHabitat(dlg.habitatName, dlg.graph, dlg.alpha, dlg.minCapa, true);
                        project.addHabitat(metaPatch, true);
                    } catch (IOException | SchemaException ex) {
                        Logger.getLogger(HabitatLayer.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(this, "Error " + ex.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
                    }
                }).start();
    }//GEN-LAST:event_metaPatchMenuItemActionPerformed

    private void setDEMMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setDEMMenuItemActionPerformed
        File f = Util.getFile(".tif|.asc", "DEM raster");
        if(f == null) {
            return;
        }
        try {
            project.setDemFile(f, true);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Error " + ex.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_setDEMMenuItemActionPerformed

    private void addPatchMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPatchMenuItemActionPerformed
        new AddPatchDialog(this, project).setVisible(true);
    }//GEN-LAST:event_addPatchMenuItemActionPerformed

    private void habitatRasterMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_habitatRasterMenuItemActionPerformed
        final HabitatPanel panel = new HabitatPanel();
        panel.setProject(project);

        int res = JOptionPane.showConfirmDialog(this, panel, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("New_habitat"), 
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if(res != JOptionPane.OK_OPTION) {
            return;
        }
        if(!panel.validatePanel()) {
            return;
        }
        
        new Thread(() -> {
            try {
                project.addHabitat(panel.getHabitat(), true);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_habitatRasterMenuItemActionPerformed

    private void capaMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_capaMenuItemActionPerformed
        
        MonoHabitat hab = (MonoHabitat) JOptionPane.showInputDialog(this, 
                java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Select_habitat"), 
                java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Set_capacity"), JOptionPane.OK_CANCEL_OPTION, null, 
                project.getHabitats().stream().filter((h) -> h instanceof MonoHabitat).toArray(), null);
        if(hab == null) {
            return;
        }
        
        final CapaPatchDialog dlg = new CapaPatchDialog(this, project, hab);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }

        new Thread(() -> {
            try {
                hab.setCapacities(dlg.params, true);
                JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Capacity_saved."));
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(HabitatLayer.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_capaMenuItemActionPerformed

    private void mergeGraphMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mergeGraphMenuItemActionPerformed
        final NewMultiGraphDialog dlg = new NewMultiGraphDialog(this, project);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            try {
                MultiGraph graph = new MultiGraph(dlg.name, dlg.graphs);
                project.addGraph(graph, true);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_mergeGraphMenuItemActionPerformed

    private void habitatVectorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_habitatVectorMenuItemActionPerformed
        final VectorHabitatDialog dlg = new VectorHabitatDialog(this, project);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        
        new Thread(() -> {
            try {
                List<DefaultFeature> features = IOFeature.loadFeatures(dlg.vectorLayer);
                project.addHabitat(new MonoVectorHabitat(dlg.name, project, features, dlg.capaAttr, true), true);
            } catch (IOException | SchemaException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("An_error_occured") + ex.getLocalizedMessage());
            }
        }).start();
    }//GEN-LAST:event_habitatVectorMenuItemActionPerformed

    private void landmodMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_landmodMenuItemActionPerformed
        LandModDialog dlg = new LandModDialog(this, project);
        dlg.setVisible(true);
        
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            ProgressBar monitor = Config.getProgressBar("Land changes...");
            Config.setProgressBar(new TaskMonitor.EmptyMonitor());
            LandModTask landModTask = new LandModTask(project, dlg.layer, dlg.idField, dlg.codeField, null, dlg.metric, monitor);
            try {
                ExecutorService.execute(landModTask);
            } finally {
                Config.setProgressBar(monitor);
            }
            
            try {
                landModTask.saveResult(new File(project.getDirectory(), "landmod-"+dlg.metric.getFullName()+".csv"));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            
            double init = dlg.metric.getResult()[0];
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            for(Entry<String, Double[]> entry : landModTask.getResult().entrySet()) {
                dataset.addValue((entry.getValue()[0]-init)/init*100, "serie", entry.getKey());
            }
            JFreeChart chart = ChartFactory.createBarChart("", "Change id", "% var "+dlg.metric.getName(),
                    dataset,
                    PlotOrientation.VERTICAL, false, false, false);
            ChartPanel chartPanel = new ChartPanel(chart, 500, 500, 100, 100,
                    2000, 2000, true, true, true, true, true, true);
            JFrame frm = new JFrame("Land changes " + dlg.metric.getFullName());
            frm.getContentPane().add(chartPanel, BorderLayout.CENTER);
            frm.setVisible(true);
            frm.pack();
            frm.setLocationRelativeTo(this);
            
            List<DefaultFeature> polyResult = landModTask.getPolyResult();
            ColorRamp ramp = new ColorRamp(ColorRamp.reverse(ColorRamp.RAMP_SYM_GREEN_RED), 
                    new FeatureAttributeIterator<Double>(polyResult, "variation"));
            double limit = Math.max(Math.abs(ramp.getMinValue()), Math.abs(ramp.getMaxValue()));
            ramp.setBounds(-limit, limit);
            FeatureStyle style = new FeatureStyle("variation", ramp, "variation", ramp);
            style.setAttrLabel(dlg.idField);
            FeatureLayer l = new FeatureLayer("Land changes", polyResult, style);
            l.setRemovable(true);
            project.getAnalysisLayer().addLayerFirst(l);
   
        }).start();
    }//GEN-LAST:event_landmodMenuItemActionPerformed

    private void corridorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_corridorMenuItemActionPerformed
        final CorridorDialog dlg = new CorridorDialog(this, true, project);
        dlg.setVisible(true);
        if(!dlg.isOk) {
            return;
        }
        new Thread(() -> {
            if(dlg.raster) {
                Raster corridors = dlg.graph.getLinkset().computeRasterCorridor(Config.getProgressBar("Corridor..."), 
                        dlg.graph, dlg.maxCost, dlg.distProbaPanel.getAlpha(), 0, dlg.var);
                RasterLayer l = new RasterLayer(dlg.graph.getName() +
                        "-corridor-" + dlg.maxCost, new RasterShape(corridors, project.getZone(), new RasterStyle(), true), project.getCRS());
                l.setRemovable(true);
                project.getAnalysisLayer().addLayerFirst(l);
                try {
                    l.saveRaster(new File(project.getDirectory(), dlg.graph.getName() +
                            "-corridor-" + dlg.maxCost + "-" + dlg.var 
                                    + "-d" + dlg.distProbaPanel.getDist() + "-p" + dlg.distProbaPanel.getP() + ".tif"));
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            } else {
                List<Feature> corridors = dlg.graph.getLinkset().computeCorridor(Config.getProgressBar("Corridor..."), dlg.graph, dlg.maxCost);
                FeatureLayer l = new FeatureLayer(dlg.graph.getName() +
                        "-corridor-" + dlg.maxCost, corridors, new FeatureStyle(new Color(32, 192, 0, 50), null));
                l.setRemovable(true);
                project.getAnalysisLayer().addLayerFirst(l);

                try {
                    IOFeature.saveFeatures(corridors, new File(project.getDirectory(), dlg.graph.getName() +
                            "-corridor-" + dlg.maxCost + ".gpkg"), project.getCRS());
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }).start();
                
    }//GEN-LAST:event_corridorMenuItemActionPerformed

    private void showMetricMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showMetricMenuItemActionPerformed
        if(metricDlg == null) {
            metricDlg = new MetricDialog(this, project);
        }
        metricDlg.updateMetrics(project);
        metricDlg.setVisible(true);
    }//GEN-LAST:event_showMetricMenuItemActionPerformed

    private void randomPointMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomPointMenuItemActionPerformed
        new RandomPointDlg(this, project).setVisible(true);
    }//GEN-LAST:event_randomPointMenuItemActionPerformed

    /**
     * Changes the current view, to show the result of a local, delta or component metric.
     * 
     * @param metric the metric result to show
     * @param indexAttr index of the attribute, if the metric has several result attributes
     */
    public static void viewMetricResult(MetricResult metric, int indexAttr) {
        // show the result
        AbstractGraph graph = metric.getGraph();
        String attr = ((AbstractAttributeMetricResult)metric).getAttrNames().get(indexAttr).toString();
        graph.getProject().getRootLayer().setLayersVisible(false);
        graph.getProject().getGraphLayers().setExpanded(true);
        GraphGroupLayer gl = graph.getLayers();
        gl.setExpanded(true);
        gl.setSpatialView(false);
        
        if(metric.getMethod() == Method.LOCAL || metric.getMethod() == Method.DELTA) {
            FeatureLayer nodeLayer = gl.getNodeLayer();
            CircleStyle nodeStyle = (CircleStyle) nodeLayer.getStyle();
            Number max = Collections.max(new FeatureAttributeCollection<Double>(nodeLayer.getFeatures(), Habitat.CAPA_ATTR));
            Number min = Collections.min(new FeatureAttributeCollection<Double>(nodeLayer.getFeatures(), Habitat.CAPA_ATTR));
            nodeStyle.setCircleAttr(Habitat.CAPA_ATTR, min.doubleValue(), max.doubleValue());
            if(((AbstractLocalMetricResult)metric).calcNodes()) {
                nodeStyle.setAttrFill(attr);
                nodeStyle.setRampFill(new ColorRamp(ColorRamp.RAMP_RED, 
                        new FeatureAttributeIterator<>(nodeLayer.getFeatures(), attr)));
                nodeStyle.setAttrContour("idhab"); //NOI18N
                nodeStyle.setRampContour(new UniqueColorTable(graph.getHabitat().getIdHabitats(), 
                            graph.getHabitat().getIdHabitats().stream().map(HabitatLayer::getColorHab).collect(Collectors.toList())));
                nodeStyle.setRampStroke(new StrokeRamp(1.5f, 1.5f));

            } else {
                nodeStyle.setAttrFill(null);
                nodeStyle.setRampFill(new ColorRamp(new Color[]{new Color(0xcbcba7)}));
            }
            nodeLayer.setVisible(true);
            FeatureLayer edgeLayer = gl.getEdgeLayer();
            FeatureStyle style = (FeatureStyle) edgeLayer.getStyle();
            style.setAttrContour(null);
            if(((AbstractLocalMetricResult)metric).calcEdges()) {
                style.setAttrStroke(attr);
                style.setRampStroke(new StrokeRamp(0.5f, 4, new FeatureAttributeIterator<>(edgeLayer.getFeatures(), attr)));
            } else {
                style.setAttrStroke(null);
            }
            edgeLayer.setVisible(true);
        } else { // component metric
            FeatureLayer compLayer = (FeatureLayer) gl.getLayers().get(0);
            compLayer.setStyle(new FeatureStyle(attr, new ColorRamp(ColorRamp.RAMP_BROWN,
                    new FeatureAttributeIterator<>(graph.getComponents().keySet(), attr))));
            compLayer.setVisible(true);
        }
    }
    
    /**
     * Loads a project and shows it on this frame.
     * 
     * @param prjFile the xml project file
     * @throws IOException 
     */
    public void loadProject(File prjFile) throws IOException {
        project = Project.loadProject(prjFile, true);
        ProgressBar progressBar = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("CREATE LAYERS..."));
        progressBar.setIndeterminate(true);
        mapViewer.setRootLayer(project.getRootLayer());
        mapViewer.setTreeLayerVisible(true);
        progressBar.close();
    }
    
    /**
     * @return the version stored in the manifest
     */
    public static String getVersion() {
        String version = MainFrame.class.getPackage().getImplementationVersion();
        return version == null ? "unpackage version" : version; //NOI18N
    }
    
    /**
     * Main entry point for MPI, CLI or GUI.
     * 
     * @param args the command line arguments
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        
        try {
            Project.loadPluginMetric();
        } catch (Exception ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, null, ex);
        }
        
        // MPI Execution
        if(args.length > 0 && args[0].equals("-mpi")) { //NOI18N
            new MpiLauncher(Arrays.copyOfRange(args, 1, args.length)).run();
            return;
        }
        
        // CLI execution
        if(args.length > 0 && !args[0].equals(JavaLoader.NOFORK)) {
            if(!GraphicsEnvironment.isHeadless() && SplashScreen.getSplashScreen() != null) {
                SplashScreen.getSplashScreen().close();
            }
            new CLITools().execute(args);
            
            return;
        }
        
        Config.setNodeClass(MainFrame.class);
        
        // Relaunch java with preferences memory
        try {
            if(args.length == 0) {
                if(JavaLoader.forkJava(MainFrame.class, 2048)) {
                    return;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Default execution (UI)
        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            if(e instanceof CancellationException) {
                JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("EXECUTION HAS BEEN CANCELLED"));
            } else {
                Logger.getGlobal().log(Level.SEVERE, null, e);
                JOptionPane.showMessageDialog(null, java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("AN ERROR HAS OCCURRED : {0}"), e));
            }
        });
        PreferencesDialog.initLanguage();

        try {  // Set System L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, null, e);
        }

        java.awt.EventQueue.invokeLater(() -> 
            new MainFrame().setVisible(true)
        );
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem addDataMenuItem;
    private javax.swing.JMenuItem addPatchMenuItem;
    private javax.swing.JMenu analysisMenu;
    private javax.swing.JMenuItem batchGraphMetricMenuItem;
    private javax.swing.JMenuItem batchParamGlobalMenuItem;
    private javax.swing.JMenuItem batchParamLocalMenuItem;
    private javax.swing.JMenu batchParamMetricMenu;
    private javax.swing.JMenuItem capaMenuItem;
    private javax.swing.JMenuItem compMetricMenuItem;
    private javax.swing.JMenuItem corridorMenuItem;
    private javax.swing.JMenuItem costDistMenuItem;
    private javax.swing.JMenuItem createMenuItem;
    private javax.swing.JMenu dataMenu;
    private javax.swing.JMenuItem delatMetricMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem globalMetricMenuItem;
    private javax.swing.JMenu graphMenu;
    private javax.swing.JMenu habMenu;
    private javax.swing.JMenuItem habitatRasterMenuItem;
    private javax.swing.JMenuItem habitatVectorMenuItem;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuItem interpMetricMenuItem;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JMenuItem landmodMenuItem;
    private javax.swing.JMenuItem localMetricMenuItem;
    private javax.swing.JMenuItem logMenuItem;
    private org.thema.drawshape.ui.MapViewer mapViewer;
    private javax.swing.JMenuItem mergeGraphMenuItem;
    private javax.swing.JMenuItem metaPatchMenuItem;
    private javax.swing.JMenu metricMenu;
    private javax.swing.JMenuItem newProjectMenuItem;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JMenuItem prefMenuItem;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JMenuItem randomPointMenuItem;
    private javax.swing.JMenuItem setDEMMenuItem;
    private javax.swing.JMenuItem showMetricMenuItem;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables

}
