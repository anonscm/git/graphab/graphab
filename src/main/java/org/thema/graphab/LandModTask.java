/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.math.MathException;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.feature.SchemaException;
import org.geotools.geometry.Envelope2D;
import org.thema.common.Config;
import org.thema.common.ProgressBar;
import org.thema.common.collection.HashMapList;
import org.thema.data.IOFeature;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.DefaultGraph;
import org.thema.graphab.graph.MultiGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.habitat.MonoHabitat;
import org.thema.graphab.habitat.MultiHabitat;
import org.thema.graphab.links.CircuitLinkset;
import org.thema.graphab.links.CostLinkset;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.graphab.mpi.MpiLauncher;
import org.thema.parallel.AbstractParallelTask;

/**
 * Parallel task for creating multiple projects from land modifications and executing CLI commands for each.
 * This task works in theaded and MPI mode.
 * 
 * @author Gilles Vuidel
 */
public class LandModTask extends AbstractParallelTask<Map<String, Double[]>, Map<String, Double[]>> implements Serializable {

    private final File fileZone;
    private final String idField;
    private final String codeField;
    private final Set<String> ids;
    
    private final List<String> args;
    
    private final GlobalMetricResult metric;
    
    private transient Project project;
    private transient HashMapList<String, DefaultFeature> zones;
    private transient List<String> zoneIds;
    private transient Envelope2D landEnv;
    
    private transient Map<String, Double[]> result;

    /**
     * Creates a new LandmodTask
     * @param project the initial project 
     * @param fileZone the shapefile containing polygons of land modifications
     * @param idField the shapefile field containing identifier
     * @param codeField the shapefile field containing the new land code
     * @param ids can be null
     * @param args the CLI commands to execute after creating each project
     */
    public LandModTask(Project project, File fileZone, String idField, String codeField, Set<String> ids, List<String> args) {
        this.project = project;
        this.fileZone = fileZone;
        this.idField = idField;
        this.codeField = codeField;
        this.ids = ids;
        this.args = args;
        this.metric = null;
    }
    
    /**
     * Creates a new LandmodTask
     * @param project the initial project 
     * @param fileZone the shapefile containing polygons of land modifications
     * @param idField the shapefile field containing identifier
     * @param codeField the shapefile field containing the new land code
     * @param ids can be null
     * @param metric the global metric
     * @param monitor
     */
    public LandModTask(Project project, File fileZone, String idField, String codeField, Set<String> ids, GlobalMetricResult metric, ProgressBar monitor) {
        super(monitor);
        this.project = project;
        this.fileZone = fileZone;
        this.idField = idField;
        this.codeField = codeField;
        this.ids = ids;
        this.metric = metric;
        this.args = null;
    }

    @Override
    public void init() {
        // useful for MPI only, because project is not serializable
        if(project == null) {
            project = MpiLauncher.getProject();
        }
        try {
            List<DefaultFeature> features = IOFeature.loadFeatures(fileZone);
            if(!features.get(0).getAttributeNames().contains(idField)) {
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOW FIELD : {0}"), idField));
            }
            if(!features.get(0).getAttributeNames().contains(codeField)) {
                throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOW FIELD : {0}"), codeField));
            }
            zones = new HashMapList<>();
            for(DefaultFeature f : features) {
                String id = f.getAttribute(idField).toString();
                if(ids == null || ids.contains(id)) {
                    zones.putValue(id, f);
                }
            }
            zoneIds = new ArrayList(zones.keySet());
            // sort to ensure the same order for several JVM in MPI mode
            Collections.sort(zoneIds);
            
            super.init();
            landEnv = IOImage.loadTiffWithoutCRS(new File(project.getDirectory(), Project.LAND_RASTER)).getEnvelope2D();
            result = new HashMap<>();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public Map<String, Double[]> execute(int start, int end) {
        Map<String, Double[]> res = new HashMap<>();
        for(String id : zoneIds.subList(start, end)) {
            try {
                Project prj = createProject(id, zones.get(id));
                
                if(args != null) {
                    // execute next CLI commands
                    ArrayList<String> newArgs = new ArrayList<>(args);
                    newArgs.add(0, "--project"); //NOI18N
                    newArgs.add(1, prj.getProjectFile().getAbsolutePath());
                    new CLITools().execute(newArgs.toArray(new String[0]));
                } else {
                    // calculate habitat, linkset, graph and global metric
                    List<Integer> idHabs = metric.getGraph().getLinkset().getHabitat().getIdHabitats();
                    Map<Integer, AbstractMonoHabitat> habitats = new HashMap<>();
                    for(AbstractMonoHabitat hab : project.getHabitats()) {
                        if(idHabs.contains(hab.getIdHab())) {
                            if(hab instanceof MonoHabitat) {
                                MonoHabitat monoHab = (MonoHabitat) hab;
                                MonoHabitat newHab = new MonoHabitat(hab.getName(), prj, monoHab.getPatchCodes(),
                                        monoHab.isCon8(), monoHab.getMinArea(), monoHab.getMaxSize(), monoHab.hasVoronoi());
                                prj.addHabitat(newHab, true);
                                habitats.put(hab.getIdHab(), newHab);
                            } else {
                                throw new UnsupportedOperationException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("ONLY RASTER HABITAT IS SUPPORTED"));
                            }
                        }
                    }
                    
                    for(Linkset linkset : metric.getGraph().getLinksets()) {
                        List<Integer> idH = linkset.getHabitat().getIdHabitats();
                        Habitat hab;
                        if(idH.size() == 1) {
                            hab = habitats.get(idH.iterator().next());
                        } else {
                            hab = new MultiHabitat(idH.stream().map(habitats::get).collect(Collectors.toList()), linkset.getHabitat().hasVoronoi());
                        }
                        Linkset newLinkset;
                        if(linkset instanceof EuclideLinkset) {
                            newLinkset = new EuclideLinkset(hab, linkset.getName(), linkset.getTopology(), 
                                    linkset.isInter(), linkset.getNodeFilter(), linkset.isRealPaths(), linkset.getDistMax());
                        } else if(linkset instanceof CostLinkset) {
                            newLinkset = linkset.getCostVersion(hab);
                        } else if(linkset instanceof CircuitLinkset) {
                            newLinkset = linkset.getCircuitVersion(hab);
                        } else {
                            throw new UnsupportedOperationException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNSUPPORTED LINKSET : {0}"), linkset.getName()));
                        }
                        prj.addLinkset(newLinkset, true);
                    }
                    AbstractGraph newGraph;
                    AbstractGraph graph = metric.getGraph();
                    if(graph instanceof MultiGraph) {
                        List<DefaultGraph> graphs = new ArrayList<>();
                        for(DefaultGraph g : ((MultiGraph) graph).getGraphs()) {
                            DefaultGraph newG = new DefaultGraph(g.getName(), prj.getLinkset(g.getLinkset().getName()), g.getThreshold(), g.isIntraPatchDist());
                            graphs.add(newG);
                            prj.addGraph(newG, true);
                        }
                        newGraph = new MultiGraph(graph.getName(), graphs);
                    } else {
                        newGraph = new DefaultGraph(graph.getName(), prj.getLinkset(graph.getLinkset().getName()), ((DefaultGraph)graph).getThreshold(), graph.isIntraPatchDist());
                    }
                    prj.addGraph(newGraph, true);
                    GlobalMetricResult metricRes = new GlobalMetricResult(metric.getMetric(), newGraph);
                    prj.addMetric(metricRes, true);
                    res.put(id, metricRes.getResult());
                    
                }
            } catch (IOException | SchemaException | MathException  ex) {
                throw new RuntimeException(ex);
            }
            incProgress(1);
        }
        return res;
    }

    @Override
    public int getSplitRange() {
        return zones.size();
    }

    @Override
    public void gather(Map<String, Double[]> results) {
        result.putAll(results);
    }

    @Override
    public Map<String, Double[]> getResult() {
        return result;
    }
    
    public List<DefaultFeature> getPolyResult() {
        String metricAttr = metric.getMetric().getResultNames(metric.getGraph())[0];
        double init = metric.getResult()[0];
        List<DefaultFeature> flatList = zones.values().stream().flatMap(List::stream).collect(Collectors.toList());
        DefaultFeature.addAttribute(metricAttr, flatList, null);
        DefaultFeature.addAttribute("variation", flatList, null);
        
        for(String id : result.keySet()) {
            for(DefaultFeature f : zones.get(id)) {
                f.setAttribute(metricAttr, result.get(id)[0]);
                f.setAttribute("variation", (result.get(id)[0]-init)/init*100);
            }
        }
        
        return flatList;
    }

    public void saveResult(File file) throws IOException {
        if(metric == null) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("NO RESULT TO SAVE"));
        }
        try (FileWriter w = new FileWriter(file)) {
            w.write("id," + String.join(",", metric.getMetric().getResultNames(metric.getGraph())) + "\n"); //NOI18N
            w.write("init," + Arrays.asList(metric.getResult()).stream().map(String::valueOf).collect(Collectors.joining(",")) + "\n"); //NOI18N
            for(Map.Entry<String, Double[]> id : result.entrySet()) {
                w.write(id.getKey() + "," + Arrays.asList(id.getValue()).stream().map(String::valueOf).collect(Collectors.joining(",")) + "\n"); //NOI18N
            }
        }
    }
    
    /**
     * Creates a new Project based on the initial project while changing the landmap on areas covering the features zones
     * @param id the identifier for the new project
     * @param zones the zones to change in the land map
     * @return the new created project
     * @throws IOException
     * @throws SchemaException 
     */
    private Project createProject(String id, List<DefaultFeature> zones) throws IOException, SchemaException {
        WritableRaster src = project.getRasterLand();
        WritableRaster raster0 = project.getRasterLand().createCompatibleWritableRaster();
        WritableRaster raster = raster0.createWritableTranslatedChild(src.getMinX(), src.getMinY());
        raster.setRect(src);
        TreeSet<Integer> codes = new TreeSet<>(project.getCodes());
        
        // update land map
        for(DefaultFeature zone : zones) {
            int code = ((Number)zone.getAttribute(codeField)).intValue();
            Geometry trGeom = project.getSpace2grid().transform(zone.getGeometry());
            for(int i = 0; i < trGeom.getNumGeometries(); i++) {
                Geometry transGeom = trGeom.getGeometryN(i);
                Envelope env = transGeom.getEnvelopeInternal();
                int miny = Math.max((int)env.getMinY(), raster.getMinY());
                int minx = Math.max((int)env.getMinX(), raster.getMinX());
                int maxy = Math.min((int)Math.ceil(env.getMaxY()), raster.getMinY() + raster.getHeight());
                int maxx = Math.min((int)Math.ceil(env.getMaxX()), raster.getMinX() + raster.getWidth());
                Coordinate c = new Coordinate();
                GeometryFactory geomFact = new GeometryFactory();
                for(c.y = miny+0.5; c.y < maxy; c.y++) {
                    for(c.x = minx+0.5; c.x < maxx; c.x++) {
                        if(transGeom.intersects(geomFact.createPoint(c))) {
                            raster.setSample((int)c.x, (int)c.y, 0, code);
                        }
                    }
                }
            }
            codes.add(code);
        }
        
        GridCoverage2D newLand = new GridCoverageFactory().create("", raster0, landEnv); //NOI18N

        // create project
        File dir = new File(project.getDirectory(), id);       
        return new Project(project.getName() + "-" + id, dir, newLand, codes, project.getNoData()); //NOI18N
    }
}
