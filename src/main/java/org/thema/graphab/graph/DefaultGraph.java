/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
package org.thema.graphab.graph;

import java.util.ResourceBundle;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Path;

/**
 *
 * @author Gilles Vuidel
 */
public class DefaultGraph extends AbstractGraph {

    private final Linkset linkset;
    private final double threshold;
    
    /**
     * Creates a new complete graph.
     * @param name name of the graph
     * @param linkset the linkset
     * @param intraPatchDist use intra patch distances for path calculation ?
     */
    public DefaultGraph(String name, Linkset linkset, boolean intraPatchDist) {
        this(name, linkset, 0, intraPatchDist);
    }
    
    /**
     * Creates a new graph.
     * @param name name of the graph
     * @param linkset the linkset
     * @param threshold the threshold if any
     * @param intraPatchDist use intra patch distances for path calculation ?
     */
    public DefaultGraph(String name, Linkset linkset, double threshold, boolean intraPatchDist) {
        super(name, intraPatchDist);
        this.linkset = linkset;
        this.threshold = threshold;
        // intra patch distance can be used only if linkset contains real paths
        if(intraPatchDist && !linkset.isRealPaths()) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle").getString("INTRA PATCH DISTANCES CAN BE USED ONLY WITH A LINKSET CONTAINING REAL PATHS"));
        }
    }

    @Override
    public DefaultGraph newInstance(String name) {
        return new DefaultGraph(name, getLinkset(), getThreshold(), isIntraPatchDist());
    }

    @Override
    public boolean isLinkIncluded(Path p) {
        return getType() != Type.PRUNED || p.getCost() <= threshold;
    }

    /**
     * @return the linkset used by this graph
     */
    public Linkset getLinkset() {
        return linkset;
    }

    /**
     * @return the edge threshold
     */
    public double getThreshold() {
        return threshold;
    }

    @Override
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle");
        
        String info = bundle.getString("NewGraphDialog.linksetLabel.text") + " : " + linkset.getName() + "\n"; //NOI18N
        
        if(getType() == Type.COMPLETE) {
            info += bundle.getString("NO PRUNING") + "\n";
        } else {
            info += bundle.getString("PRUNED DISTANCE: ") + getThreshold() + "\n";
        }
        
        return super.getInfo().replace("?DETAIL?", info); //NOI18N
    }

    @Override
    public Type getType() {
        return threshold > 0 ? Type.PRUNED : Type.COMPLETE;
    }
 
}
