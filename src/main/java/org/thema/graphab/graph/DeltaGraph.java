/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.graph;

import java.util.Collection;
import java.util.Map;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.habitat.Habitat;

/**
 * A graph where one element can be removed and put back, one at a time.
 * Used for delta metric calculation.
 * 
 * @author Gilles Vuidel
 */
public class DeltaGraph extends ModifiedGraph {

    private Graphable removedElem;

    private DefaultFeature remCompFeature;
    private Graph remComp;
    private Map<Integer, Graph> addComps;

    /**
     * Creates a new DeltaGraphGenerator based on the graph gen.
     * Dupplicates the parent graph.
     * @param gen the parent graph
     */
    public DeltaGraph(AbstractGraph gen) {
        super(gen);
        this.removedElem = null;
    }

    /**
     * @return the current removed element, a node or an edge or null if no element is currently removed.
     */
    public Graphable getRemovedElem() {
        return removedElem;
    }


    /**
     * Removes the element elem from this graph. Only one element can be removed at a time.
     * 
     * @param elem the element to remove, a node or an edge
     * @throws IllegalStateException if an element is already removed
     */
    public void removeElem(Graphable elem) {
        if(removedElem != null) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle").getString("AN ELEMENT IS ALREADY REMOVED"));
        }
        DefaultFeature compFeature = null;
        Graph gr = null;
        for(DefaultFeature comp : getComponents().keySet()) {
            Graph g = getComponents().get(comp);
            if(elem instanceof Edge && g.getEdges().contains(elem) ||
                    elem instanceof Node && g.getNodes().contains(elem)) {
                gr = g;
                compFeature = comp;
                break;
            }
        }

        if(elem instanceof Edge) {
            Edge e = (Edge) elem;
            graph.getEdges().remove(e);
            gr.getEdges().remove(e);
            e.getNodeA().remove(e);
            e.getNodeB().remove(e);
        } else {
            Node n = (Node) elem;
            gr.getNodes().remove(n);
            graph.getNodes().remove(n);
            patchNodes.remove(Habitat.getPatch(n));
            for(Edge e : n.getEdges()) {
                gr.getEdges().remove(e);
                graph.getEdges().remove(e);
                e.getOtherNode(n).remove(e);
            }
        }

        remComp = gr;
        remCompFeature = compFeature;
        addComps = partition(gr);
        
        components.remove(compFeature);
        for(int id : addComps.keySet()) {
            components.put(new DefaultFeature(id, null), addComps.get(id));
        }

        removedElem = elem;
        
        pathGraph = null;
        node2PathNodes = null;        
    }

    /**
     * If an element has been removed, put back the element in the graph.
     */
    public void reset() {
        if(removedElem == null) {
            return;
        }
        if(removedElem instanceof Edge) {
            Edge e = (Edge) removedElem;
            graph.getEdges().add(e);
            remComp.getEdges().add(e);
            e.getNodeA().add(e);
            e.getNodeB().add(e);
        } else {
            Node n = (Node) removedElem;
            graph.getNodes().add(n);
            patchNodes.put(Habitat.getPatch(n), n);
            remComp.getNodes().add(n);
            for(Edge e : (Collection<Edge>)n.getEdges()) {
                graph.getEdges().add(e);
                remComp.getEdges().add(e);
                e.getOtherNode(n).add(e);
            }
        }

        for(int id : addComps.keySet()) {
            components.remove(new DefaultFeature(id, null));
        }
        components.put(remCompFeature, remComp);

        removedElem = null;
        remComp = null;
        remCompFeature = null;
        addComps = null;

        pathGraph = null;
        node2PathNodes = null;
    }

    
}
