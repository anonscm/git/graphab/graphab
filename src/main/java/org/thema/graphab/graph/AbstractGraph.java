/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
package org.thema.graphab.graph;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.operation.union.CascadedPolygonUnion;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.geotools.graph.build.basic.BasicGraphBuilder;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Node;
import org.geotools.graph.structure.basic.BasicGraph;
import org.locationtech.jts.geom.Coordinate;
import org.thema.common.JTS;
import org.thema.common.collection.HashMap2D;
import org.thema.common.collection.HashMapList;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graph.shape.GraphGroupLayer;
import org.thema.graphab.Project;
import org.thema.graphab.Project.Method;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.links.IntraLinks;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.MultiLinkset;
import org.thema.graphab.links.Path;
import org.thema.graphab.metric.MetricResult;
import org.thema.graphab.metric.global.GlobalMetricResult;

/**
 *
 * @author Gilles Vuidel
 */
public abstract class AbstractGraph {

    /** Graph type */
    public enum Type {COMPLETE, PRUNED, MULTI }
    
    private final String name;
    private final boolean intraPatchDist;
    private TreeMap<String, MetricResult> metrics;
    
    protected transient Map<DefaultFeature, Graph> components;
    protected transient Graph graph, pathGraph;
    private transient GraphGroupLayer layers;
    protected transient HashMap<Feature, Node> patchNodes;
    protected transient HashMapList<Node, Node> node2PathNodes;

    /**
     * Creates a new graph.
     * @param name name of the graph
     * @param intraPatchDist use intra patch distances for path calculation ?
     */
    public AbstractGraph(String name, boolean intraPatchDist) {
        this.name = name;
        this.intraPatchDist = intraPatchDist;
        this.metrics = new TreeMap<>();
    }

    /**
     * @return true if intrapatch distances are used in pathfinder, false otherwise
     */
    public boolean isIntraPatchDist() {
        return intraPatchDist;
    }

    /**
     * Creates, if needed, and returns the graph.
     * @return the graph
     */
    public synchronized Graph getGraph() {
        if(graph == null) {
            createGraph();
        }

        return graph;
    }
    
    /**
     * Creates, if needed, and returns the path graph.
     * @return the path graph if intraPatchDist = true, the graph otherwise
     */
    protected synchronized Graph getPathGraph() {
        if(pathGraph == null) {
            if(!isIntraPatchDist()) {
                pathGraph = getGraph();
                node2PathNodes = null;
            } else {
                createPathGraph();
            }
        }

        return pathGraph;
    }

    /**
     * Returns a list of pathgraph nodes corresponding to the graph node if intraPatchDist = true,
     * returns the same node otherwise.
     * @param node a graph node
     * @return the list of pathgraph nodes corresponding to the graph node
     */
    protected List<Node> getPathNodes(Node node) {
        if(!isIntraPatchDist()) {
            return Collections.singletonList(node);
        }
        getPathGraph();
        return node2PathNodes.get(node);
    }


    /**
     * @return all nodes of the graph
     */
    public Collection<Node> getNodes() {
        return getGraph().getNodes();
    }
    
    /**
     * @return all edges of the graph
     */
    public Collection<Edge> getEdges() {
        return getGraph().getEdges();
    }
    
    /**
     * @param patch the patch
     * @return the node representing the patch in this graph or null
     */
    public Node getNode(Feature patch) {
        getGraph();
        return patchNodes.get(patch);
    }

    
    /**
     * Attention version très lente
     * Parcours tous les noeuds pour trouver le bon
     * @param linkId the link identifier
     * @return the edge corresponding to the link id in this graph or null
     */
    public Edge getEdge(String linkId) {
        for(Edge e : getEdges()) {
            if(((Feature)e.getObject()).getId().equals(linkId)) {
                return e;
            }
        }
        return null;
    }

    /**
     * @return all links in this graph
     */
    public List<Path> getLinks() {
        ArrayList<Path> links = new ArrayList<>();
        for(Edge e : getEdges()) {
            links.add((Path)e.getObject());
        }
        
        return links;
    }
    
    /**
     * @param edge
     * @return the cost of this edge
     */
    public final double getCost(Edge edge) {
        return ((Path)edge.getObject()).getCost();
    }
    
    /**
     * @param edge
     * @param alpha
     * @return the flow of this edge
     */
    public final double getFlow(Edge edge, double alpha) {
        return Habitat.getPatchCapacity(edge.getNodeA()) * Habitat.getPatchCapacity(edge.getNodeB()) 
                / Math.pow(getHabitat().getTotalPatchCapacity(), 2) 
                * Math.exp(-alpha * getCost(edge));
    }



    /**
     * Creates, if needed, and returns all graph components.
     * @return all components of this graph
     */
    public synchronized Map<DefaultFeature, Graph> getComponents() {
        if(components == null) {
            loadComponents();
        }
        return components;
    }

    /**
     * Creates and return a pathfinder from nodeOrigin
     * @param nodeOrigin the starting node
     * @return the calculated pathfinder
     */
    public GraphPathFinder getPathFinder(Node nodeOrigin) {
         return new GraphPathFinder(nodeOrigin, this);
    }
    
    /**
     * Creates and return a pathfinder from nodeOrigin and stop calculation when the cost distance exceeds maxCost
     * @param nodeOrigin the starting node
     * @param maxCost maximal cost distance
     * @return the calculated pathfinder
     */
    public GraphPathFinder getPathFinder(Node nodeOrigin, double maxCost) {
         return new GraphPathFinder(nodeOrigin, maxCost, this);
    }
    
    /**
     * Creates and return a pathfinder from nodeOrigin where weight are not cost distance but flows : -ln(ai*aj/A^2) + alpha*cost.
     * @param nodeOrigin the starting node
     * @param alpha exponential coefficient
     * @return the calculated pathfinder
     */
    public GraphPathFinder getFlowPathFinder(Node nodeOrigin, double alpha) {
         return new GraphPathFinder(nodeOrigin, Double.NaN, alpha, this);
    }
    
    /**
     * @return the sum of the capacity of the patch nodes
     */
    public double getPatchCapacity() {
        double sum = 0;
        for(Object n : getGraph().getNodes()) {
            sum += Habitat.getPatchCapacity((Node)n);
        }
        return sum;
    }

    public void save() throws IOException {
        if(components == null) {
            createComponents();
        }
        IOFeature.saveFeatures(components.keySet(), getGraphFile(), getProject().getCRS());
    }

    public final File getGraphFile() {
        File dir = getHabitat().getDir();
        dir.mkdir();

        return new File(dir, getName() + "-voronoi.gpkg"); //NOI18N
    }
    
    protected final File getGraphTopoFile() {
        File dir = getHabitat().getDir();
        dir.mkdir();

        return new File(dir, getName() + "-topo_links.gpkg"); //NOI18N
    }
    
    public void removeFiles() {
        getGraphFile().delete();
        getGraphTopoFile().delete();
    }
    
    public void createTopoLinks() throws IOException {
        GraphLayers layer = new GraphLayers("", this, getProject().getCRS()); //NOI18N
        layer.setSpatialView(false);
        layer.getEdgeLayer().export(getGraphTopoFile());
    }
     /**
     * Loads the polygon features representing components of the graph, based on voronoi polygon.
     * Loads the geopackage, containing component metric results.
     * @return the polygon features
     * @throws IOException 
     */
    protected List<DefaultFeature> loadVoronoiGraph() throws IOException {
        List<DefaultFeature> features = IOFeature.loadFeatures(getGraphFile(), "Id"); //NOI18N

        HashMap<Object, DefaultFeature> map = new HashMap<>();
        for(DefaultFeature f : features) {
            map.put(f.getId(), f);
        }

        return features;
    }
    
    /**
     * Creates, if needed, the layers and returns them.
     * @return the layers of the graph
     */
    public synchronized GraphGroupLayer getLayers() {
        if(layers == null) {
            layers = new GraphLayers(name, this, getProject().getCRS());   
        }

        return layers;
    }

    /**
     * Creates the graph and stores it in graph.
     */
    protected synchronized void createGraph() {
        BasicGraphBuilder gen = new BasicGraphBuilder();
        patchNodes = new HashMap<>();
        for(DefaultFeature p : getHabitat().getPatches()) {
            Node n = gen.buildNode();
            n.setObject(p);
            gen.addNode(n);
            patchNodes.put(p, n);
        }

        for(Path p : getLinkset().getPaths()) {
            if(isLinkIncluded(p)) {
                Edge e = gen.buildEdge(patchNodes.get(p.getPatch1()), patchNodes.get(p.getPatch2()));
                e.setObject(p);
                gen.addEdge(e);
            }
        }

        graph = gen.getGraph();
    }
    
    /**
     * Is this link (path) is included in this graph
     * @param p the link
     * @return 
     */
    public boolean isLinkIncluded(Path p) {
        return true;
    }
    
    /**
     * @return the linkset used by this graph
     */
    public abstract Linkset getLinkset();
    
    public Collection<Linkset> getLinksets() {
        if(getLinkset() instanceof MultiLinkset) {
            return ((MultiLinkset)getLinkset()).getLinksets();
        } else {
            return Collections.singleton(getLinkset());
        }
    }
    
    protected IntraLinks getIntraLinks() {
        return getLinkset().getIntraLinks();
    }
    
    public boolean hasVoronoi() {
        return getHabitat().hasVoronoi();
    }
    
    /**
     * The voronoi polygon associated with the patch id
     * @param id the patch identifier
     * @return a polygon feature representing the voronoi polygon of the patch id
     */
    public final Feature getVoronoi(int id) {
        return getHabitat().getVoronoi(id);
    }
    
    
    /**
     * Dupplicates the graph and changes the name of the graph 
     * @param name the graph name
     * @return new Graph based on this one
     */
    public abstract AbstractGraph newInstance(String name);
    
    /**
     * Creates a sub graph containing the indComp component only.
     * @param comp the component feature
     * @return a new Graph representing the ith component of this graph
     */
    public AbstractGraph getComponentGraphGen(DefaultFeature comp) {
        return new ModifiedGraph(this, comp);
    }
    
    /**
     * Calculates the pathgraph used for intrapatch distances and stores it in pathGraph.
     * Stores also the mapping between nodes of the upper graph and the pathgraph nodes.
     */
    protected synchronized void createPathGraph() {

        HashMap2D<Node, Coordinate, Node> coord2PathNode = new HashMap2D<>(Collections.EMPTY_SET, Collections.EMPTY_SET, null);
        
        node2PathNodes = new HashMapList<>();

        BasicGraphBuilder gen = new BasicGraphBuilder();

        for(Edge edge : getEdges()) {
            Path p = (Path) edge.getObject();
            Coordinate c = p.getCoordinate(p.getPatch1());
            Node n1 = coord2PathNode.getValue(edge.getNodeA(), c);
            if(n1 == null) {
                n1 = gen.buildNode();
                n1.setObject(edge.getNodeA());
                gen.addNode(n1);
                node2PathNodes.putValue(edge.getNodeA(), n1);
                coord2PathNode.setValue(edge.getNodeA(), c, n1);
            }
            c = p.getCoordinate(p.getPatch2());
            Node n2 = coord2PathNode.getValue(edge.getNodeB(), c);
            if(n2 == null) {
                n2 = gen.buildNode();
                n2.setObject(edge.getNodeB());
                gen.addNode(n2);
                node2PathNodes.putValue(edge.getNodeB(), n2);
                coord2PathNode.setValue(edge.getNodeB(), c, n2);
            }
            Edge e = gen.buildEdge(n1, n2);
            e.setObject(edge);
            gen.addEdge(e);
        }
        IntraLinks intraLinks = getIntraLinks();
        for(Node node : getNodes()) {
            // add isolated patch
            if(!node2PathNodes.containsKey(node)) {
                Node n = gen.buildNode();
                n.setObject(node);
                gen.addNode(n);
                node2PathNodes.putValue(node, n);
            } else { // link all nodes of same patch
                Map<Coordinate, Node> nodes = coord2PathNode.getLine(node);
                List<Coordinate> coords = new ArrayList<>();
                for(Coordinate c : nodes.keySet()) {
                    if(nodes.get(c) != null) {
                        coords.add(c);
                    }
                }
                for(int i = 0; i < coords.size(); i++) {
                    for(int j = i+1; j < coords.size(); j++) {
                        Coordinate c1 = coords.get(i);
                        Coordinate c2 = coords.get(j);
                        Edge e = gen.buildEdge(nodes.get(c1), nodes.get(c2));
                        double[] costs = intraLinks.getIntraLinkCost(c1, c2);
                        if(costs == null) {
                            throw new RuntimeException("No intra patch dist for " + node.getObject()); //NOI18N
                        }
                        e.setObject(costs);
                        gen.addEdge(e);
                    }
                }
            }
        }

        pathGraph = gen.getGraph();
    }    

    /**
     * Calculates the components of the graph g.
     * @param g the graph
     * @return a list of graph, one graph for each component
     */
    public static Map<Integer, Graph> partition(Graph g) {
        HashSet<Node> nodes = new HashSet<>(g.getNodes());
        Map<Integer, Graph> comps = new HashMap<>();
        while(!nodes.isEmpty()) {
            Node n = nodes.iterator().next();
            nodes.remove(n);
            List<Node> part = new ArrayList<>();
            part.add(n);

            LinkedList<Node> queue = new LinkedList<>();
            queue.add(n);
            while(!queue.isEmpty()) {
                n = queue.poll();
                Iterator it = n.getRelated();
                while(it.hasNext()) {
                    Node node = (Node) it.next();
                    if(nodes.contains(node)) {
                       nodes.remove(node);
                       part.add(node);
                       queue.add(node);
                    }

                }
            }

            HashSet<Edge> edges = new HashSet<>();
            int minId = Integer.MAX_VALUE;
            for (Node node : part) {
                edges.addAll(node.getEdges());
                int id = (int) Habitat.getPatch(node).getId();
                if(id < minId) {
                    minId = id;
                }
            }
            comps.put(minId, new BasicGraph(part, edges));
        }

        return comps;

    }

    private synchronized void loadComponents() {
        if(getGraphFile().exists()) {
            try {
                List<DefaultFeature> features = loadVoronoiGraph();
                Map<Integer, DefaultFeature> mapFeatures = features.stream().collect(Collectors.toMap((Feature f) -> (Integer)f.getId(), (f) -> f));
                components = new HashMap<>();
                
                if(features.size() == 1) {
                    components.put(features.get(0), new BasicGraph(new ArrayList(getGraph().getNodes()), new ArrayList(getGraph().getEdges())));
                } else {
                    Map<Integer, Graph> partition = partition(getGraph());                   
                    for(int id : partition.keySet()) {
                        components.put(mapFeatures.get(id), partition.get(id));
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            createComponents();
        }
    }

    private void createComponents() {
        components = new HashMap<>();
        List<String> attrNames = new ArrayList<>(0);
        Map<Integer, Graph> partition = partition(getGraph());
        if(partition.size() == 1) {
            components.put(new DefaultFeature(partition.keySet().iterator().next(), JTS.geomFromRect(getProject().getZone()), attrNames, new ArrayList(0)), partition.values().iterator().next());
        } else {
            for(int id : partition.keySet()) {
                Graph gr = partition.get(id);
                List<Geometry> geoms = new ArrayList<>();
                for(Node n : gr.getNodes()) {
                    DefaultFeature f = Habitat.getPatch(n);
                    geoms.add(hasVoronoi() ? getVoronoi((int) f.getId()).getGeometry() : f.getGeometry());                
                }
                Geometry g = CascadedPolygonUnion.union(geoms);
                if(!hasVoronoi()) {
                    g = g.convexHull();
                }
                components.put(new DefaultFeature(id, g, attrNames, new ArrayList(0)), gr);
            }
        }
    }
    
    /**
     * @return the name of the graph
     */
    @Override
    public String toString() {
        return name;
    }


    /**
     * May be COMPLETE, PRUNED or MULTI
     * @return the type of the graph
     */
    public abstract Type getType();

    /**
     * @return the name of the graph
     */
    public String getName() {
        return name;
    }
    
    public Habitat getHabitat() {
        return getLinkset().getHabitat();
    }

    public Project getProject() {
        return getLinkset().getProject();
    }

    /**
     * @return the patch attribute names containing the graph name
     */
    public List<String> getPatchAttr() {
        List<String> attrs = new ArrayList<>();
        for(String attr : getHabitat().getAttributeNames()) {
            if(attr.toLowerCase().endsWith("_" + getName().toLowerCase())) {
                attrs.add(attr); //NOI18N
            }
        }
        return attrs;
    }
    
    /**
     * @return the link attribute names containing the graph name
     */
    public List<String> getLinkAttr() {
        List<String> attrs = new ArrayList<>();
        for(String attr : getLinkset().getPaths().get(0).getAttributeNames()) {
            if(attr.toLowerCase().endsWith("_" + getName().toLowerCase())) {
                attrs.add(attr); //NOI18N
            }
        }
        return attrs;
    }
    
    /**
     * @return graph informations
     */
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle");
        
        String info = bundle.getString("NewGraphDialog.nameLabel.text") + " : " + name + "\n\n"; //NOI18N
        
        if(intraPatchDist) {
            info += java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle").getString("INCLUDE INTRA-PATCH DISTANCE");
        } else {
            info += java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle").getString("NO INTRA-PATCH DISTANCE");
        }
        
        info += "\n?DETAIL?"; //NOI18N
        
        info += "\n# " + bundle.getString("nodes") + " : " + getGraph().getNodes().size(); //NOI18N
        info += "\n# " + bundle.getString("edges") + " : " + getGraph().getEdges().size(); //NOI18N
        info += "\n# " + bundle.getString("components") + " : " + getComponents().size(); //NOI18N
        
        return info;
    }
    
    /**
     * Clones the graph and removes some nodes and edges
     * @param idNodes a collection of patch id to remove
     * @param idEdges a collection of path id to remove
     * @return a dupplicated graph without idNodes and idEdges
     */
    public final Graph dupGraphWithout(Collection idNodes, Collection idEdges) {
        Graph g = getGraph();

        BasicGraphBuilder builder = new BasicGraphBuilder();
        HashMap<Node, Node> mapNodes = new HashMap<>();
        for(Node node : (Collection<Node>)g.getNodes()) {
            if(idNodes.contains(((Feature)node.getObject()).getId())) {
                continue;
            }
            Node n = builder.buildNode();
            n.setID(node.getID());
            n.setObject(node.getObject());
            builder.addNode(n);
            mapNodes.put(node, n);
        }

        for(Edge edge : (Collection<Edge>)g.getEdges()) {
            if(idEdges.contains(((Feature)edge.getObject()).getId())) {
                continue;
            }
            if(!mapNodes.containsKey(edge.getNodeA()) || !mapNodes.containsKey(edge.getNodeB())) {
                continue;
            }
            Edge e = builder.buildEdge(mapNodes.get(edge.getNodeA()), mapNodes.get(edge.getNodeB()));
            e.setID(edge.getID());
            e.setObject(edge.getObject());
            builder.addEdge(e);
        }
        return builder.getGraph();
    }

    /**
     * Inverse function of Arrays.deepToString. Convert a string to a list.
     * The element are separated by comma.
     * The string can have squared brackets, the string "[1,2]" is the same as "1,2"
     * @param sArray the string representing an array
     * @param patch if true convert elements to integer
     * @return the list of elements contained in the string
     */
    protected List stringToList(String sArray, boolean patch) {
        String[] split = sArray.replace("[", "").replace("]", "").split(","); //NOI18N
        List lst = new ArrayList();
        for(String s : split) {
            if(s.trim().isEmpty()) {
                continue;
            }
            if(patch) {
                // for patch id
                int id = Integer.parseInt(s.trim());
                lst.add(id);
            } else {
                // for link id
                lst.add(s.trim());
            }
        }
        return lst;
    }
    
    public Stream<GlobalMetricResult> getGlobalMetrics() {
        return (Stream)getMetrics(Method.GLOBAL);
    }
    
    public Stream<MetricResult> getMetrics(Method method) {
        return metrics().values().stream().filter(m -> m.getMethod().equals(method));
    }
    
    public Collection<MetricResult> getMetrics() {
        return metrics().values();
    }
    
    public Collection<String> getMetricsName() {
        return metrics().keySet();
    }
    
    public MetricResult getMetric(String name) {
        return metrics().get(name.toLowerCase());
    }
    
    public void addMetric(MetricResult metric) {
        metrics().put(metric.getName().toLowerCase(), metric);
    }
    
    public boolean removeMetric(MetricResult metric) {
        return metrics().remove(metric.getName().toLowerCase()) == metric;
    }
    
    /**
     * Just for compatibility before 2.9.8
     * To remove for Graphab 3.0
     * @return 
     */
    protected synchronized TreeMap<String, MetricResult> metrics() {
        if(metrics == null) {
            metrics = new TreeMap<>();
        }
        return metrics;
    }
}
