/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Path;

/**
 *
 * @author gvuidel
 * @param <T>
 */
public class ModifiedGraph<T extends AbstractGraph> extends AbstractGraph {
    
    protected T parentGraph;

    /**
     * Warning this constructor does not initialize graph and patchNodes attributes.
     * This attributes must be defined in the constructor or in createGraph method
     * @param name
     * @param parentGraph 
     */
    protected ModifiedGraph(String name, T parentGraph) {
        super(name, parentGraph.isIntraPatchDist());
        this.parentGraph = parentGraph;
    }
    
    /**
     * Creates a new graph, dupplicate nodes and edges
     * @param gen the parent graph
     */
    public ModifiedGraph(T gen) {
        this(gen, Collections.emptyList(), Collections.emptyList());
    }
    
    /**
     * Creates a new graph, removing nodes and edges contained in the 2 collections
     * @param gen the parent graph
     * @param remIdNodes collection of patches id to remove
     * @param remIdEdges ollection of links id to remove
     */
    public ModifiedGraph(T gen, Collection remIdNodes, Collection remIdEdges) {
        this(gen, Arrays.deepToString(remIdNodes.toArray()), Arrays.deepToString(remIdEdges.toArray()));
    }
    
    /**
     * Creates a new graph, removing nodes and edges contained in the 2 strings
     * @param gen the parent graph
     * @param remIdNodes the list of patch id to remove, separated by comma
     * @param remIdEdges the list of link id to remove, separated by comma
     * @see #stringToList(java.lang.String, boolean) 
     */
    public ModifiedGraph(T gen, String remIdNodes, String remIdEdges) {
        super(gen.getName() + "!" + remIdNodes + "!" + remIdEdges, gen.isIntraPatchDist());
        this.graph = gen.dupGraphWithout(stringToList(remIdNodes, true), stringToList(remIdEdges, false));
        this.parentGraph = gen;
        patchNodes = new HashMap<>();
        for(Node n : this.graph.getNodes()) {
            patchNodes.put((Feature)n.getObject(), n);
        }
    }
    
    /**
     * Creates a sub graph containing the component only.
     * @param g the graph
     * @param comp the component feature
     */
    public ModifiedGraph(T g, DefaultFeature comp) {
        super(g.getName() + "_comp" + comp.getId(), g.isIntraPatchDist());
        this.graph = g.getComponents().get(comp);
        this.components = Collections.singletonMap(comp, this.graph);
        this.parentGraph = g;
        patchNodes = new HashMap<>();
        for(Node n : this.graph.getNodes()) {
            patchNodes.put((Feature)n.getObject(), n);
        }
    }

    public T getParentGraph() {
        return parentGraph;
    }
    
    public Linkset getLinkset() {
        return parentGraph.getLinkset();
    }
    
    @Override
    protected void createGraph() {
        // does nothing
    }

    @Override
    public boolean isLinkIncluded(Path p) {
        return parentGraph.isLinkIncluded(p); 
    }
    
    @Override
    public AbstractGraph newInstance(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Type getType() {
        return parentGraph.getType();
    }

}
