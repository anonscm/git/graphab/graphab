/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.thema.graphab.habitat.Habitat;
import org.thema.graphab.links.MultiLinkset;
import org.thema.graphab.links.Path;

/**
 *
 * @author gvuidel
 */
public class MultiGraph extends AbstractGraph { 
    
    private final List<DefaultGraph> graphs;
    
    private transient MultiLinkset linkset;

    public MultiGraph(String name, Collection<DefaultGraph> graphs) {
        super(name, graphs.stream().allMatch(g -> g.isIntraPatchDist()));
        this.graphs = new ArrayList<>(graphs);
    }

    @Override
    public MultiGraph newInstance(String name) {
        return new MultiGraph(name, graphs.stream().map(g->g.newInstance(g.getName())).collect(Collectors.toList()));
    }
    
    public List<DefaultGraph> getGraphs() {
        return graphs;
    }
    
    public MultiLinkset getLinkset() {
        if(linkset == null) {
            linkset = new MultiLinkset(graphs.stream().map(g -> g.getLinkset()).collect(Collectors.toList()));
        }
        return linkset;
    }

    @Override
    public boolean isLinkIncluded(Path p) {
        boolean included = false;
        List<Integer> idHabs = List.of(Habitat.getPatchIdHab(p.getPatch1()), Habitat.getPatchIdHab(p.getPatch2()));
        for(AbstractGraph g : graphs) {
            if(g.getHabitat().getIdHabitats().containsAll(idHabs)) {
                included = included || g.isLinkIncluded(p);
            }
        }
        return included;
    }

    @Override
    public Type getType() {
        return Type.MULTI;
    }

    @Override
    public String getInfo() {
        String info = java.util.ResourceBundle.getBundle("org/thema/graphab/graph/Bundle").getString("MERGED GRAPH:") + "\n";
        info += graphs.stream().map(DefaultGraph::getName).collect(Collectors.joining("\n")) + "\n"; //NOI18N
        
        return super.getInfo().replace("?DETAIL?", info); //NOI18N
    }
    
}
