/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.dataset;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.geotools.feature.SchemaException;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.habitat.MonoVectorHabitat;
import org.thema.graphab.links.Path;
import org.thema.graphab.links.SpacePathFinder;
import org.thema.graphab.metric.local.LocalMetricResult;
import org.thema.graphab.model.DistribModel.Agreg;

/**
 *
 * @author gvuidel
 */
public class VectorDataset extends MonoVectorHabitat {
    
    public VectorDataset(String name, Project project, Collection<? extends Feature> features) throws IOException, SchemaException {
        super(name, project, features, null, false);
        
        // set capacities to zero
        for(DefaultFeature f : getPatches()) {
            f.setAttribute(CAPA_ATTR, 0.0);
        }
        savePatch();
    }
    
    public void importMetric(LocalMetricResult metric, double alpha, boolean multiAttach, double dMax, Agreg agreg) throws IOException, SchemaException {
        List<String> attrs = metric.getAttrNames();
        for(String attr : attrs) {
            DefaultFeature.addAttribute(attr, getPatches(), 0);
        }
        
        SpacePathFinder pathFinder = metric.getGraph().getLinkset().getPathFinder();
        
        for(DefaultFeature f : getPatches()) {
            Map<DefaultFeature, Path> patchDists;
            if(multiAttach) {
                patchDists = pathFinder.calcPaths(f.getGeometry(), dMax, false);
            } else {
                patchDists = pathFinder.calcPathNearestPatch(f.getGeometry());
            }

            double [] cum = new double[attrs.size()];
            double [] weight = new double[attrs.size()];
            for(DefaultFeature patch : patchDists.keySet()) {
                for(int i = 0; i < attrs.size(); i++) {
                    Number val = (Number)patch.getAttribute(attrs.get(i));
                    if(val != null) {
                        double w = Math.exp(-alpha * patchDists.get(patch).getCost());
                        switch(agreg) {
                            case SUM:
                                cum[i] += val.doubleValue() * w;
                                break;
                            case MAX:
                                cum[i] = Math.max(cum[i], val.doubleValue() * w);
                                break;
                            case AVG:
                                cum[i] += val.doubleValue() * w * w;
                                weight[i] += w;
                                break;
                        }

                    }
                }
            }
            for(int i = 0; i < attrs.size(); i++) {
                if(weight[i] > 0) {
                    cum[i] = cum[i] / weight[i];
                }
                f.setAttribute(attrs.get(i), cum[i]);
            }
        }
        
        savePatch();
        
    }
}
