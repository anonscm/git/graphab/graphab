/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.dataset;

import org.thema.graphab.habitat.MatrixDistanceDialog;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import org.thema.drawshape.layer.FeatureLayer;
import org.thema.graphab.Project;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author Gilles Vuidel
 */
public class DatasetLayer extends FeatureLayer {

    private final VectorDataset dataset;
    
    public DatasetLayer(VectorDataset dataset) {
        super(dataset.getName(), dataset.getPatches(), null, dataset.getProject().getCRS());
        this.dataset = dataset;
    }
    
    @Override
    public JPopupMenu getContextMenu() {
        JPopupMenu menu = super.getContextMenu(); 
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/dataset/Bundle").getString("IMPORT LOCAL METRIC")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MetricDatasetDlg(null, dataset).setVisible(true);
            }
        });
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DISTANCE MATRIX")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MatrixDistanceDialog(null, dataset, null).setVisible(true);
            }
        });
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Remove...")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                HashSet<String> linksetNames = new HashSet<>();
                for (Linkset l : dataset.getProject().getLinksets()) {
                    if(l.getHabitat().getIdHabitats().contains(dataset.getIdHab())) {
                        linksetNames.add(l.getName());
                    }
                }
                int res = JOptionPane.showConfirmDialog(null, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Do_you_want_to_remove_the_links_") + getName() + " ?" 
                        + (!linksetNames.isEmpty() ? java.util.ResourceBundle.getBundle("org/thema/graphab/dataset/Bundle").getString("LINKSETS")+ Arrays.deepToString(linksetNames.toArray()) +
                                java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("_will_be_removed.") : ""), java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Remove"), JOptionPane.YES_NO_OPTION);
                if (res != JOptionPane.YES_OPTION) {
                    return;
                }
                try {
                    dataset.getProject().removeHabitat(dataset.getName(), true);
                    dataset.getProject().getDatasetLayers().removeLayer(DatasetLayer.this);
                } catch (IOException ex) {
                    Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        return menu;
    }
}
