/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.feature.SchemaException;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.thema.common.Config;
import org.thema.common.ProgressBar;
import org.thema.common.io.tab.CSVTabReader;
import org.thema.common.parallel.AbstractParallelFTask;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.SimpleParallelTask;
import org.thema.data.IOFeature;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.MainFrame;
import org.thema.graphab.Project;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.RasterPathFinder;
import org.thema.graphab.util.SpatialOp;

/**
 *
 * @author gvuidel
 */
public class MonoHabitat extends AbstractMonoHabitat {

    private final Set<Integer> patchCodes;
    private final boolean con8;
    /**
     * taille minimale pour un patch en unité du système de coordonnées
     * donc normalement m2
     */
    private final double minArea;
    
    private final double maxSize;
    
    private CapaPatchDialog.CapaPatchParam capacityParams;
    

    public MonoHabitat(String name, Project project, Set<Integer> patchCodes, boolean con8, 
            double minArea, double maxSize, boolean calcVoronoi) throws IOException, SchemaException {
        super(project.getNextIdHab(), name, project);
        this.patchCodes = patchCodes;
        this.con8 = con8;
        this.minArea = minArea;
        this.maxSize = maxSize;
        
        this.capacityParams = new CapaPatchDialog.CapaPatchParam();
        
        GridCoverage2D landCov = project.getLandCover();
        
        TreeMap<Integer, Envelope> envMap = new TreeMap<>();

        WritableRaster rasterPatchs = SpatialOp.extractPatch(landCov.getRenderedImage(), patchCodes, project.getNoData(), con8, (int)(maxSize/project.getResolution()), envMap);      

        Logger.getLogger(MonoHabitat.class.getName()).log(Level.INFO, "Nb patch : " + envMap.size()); //NOI18N

        ProgressBar monitor = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Vectorizing..."), envMap.size());

        // parallelize the recoding may be dangerous if envMap.firstKey() <= envMap.size(), it may have overlay in id while recoding in parallel
        if(!envMap.isEmpty() && envMap.firstKey() <= envMap.size()) {
            throw new IllegalStateException("Error in patch extraction, id overlays"); //NOI18N
        }
        final SortedMap<Integer, DefaultFeature> sortedPatch = Collections.synchronizedSortedMap(new TreeMap<>());
        GeometryFactory geomFac = new GeometryFactory();
        SimpleParallelTask<Integer> task = new SimpleParallelTask<Integer>(
                new ArrayList<>(envMap.keySet()), monitor) {
            int nbRem = 0;
            @Override
            protected void executeOne(Integer id) {
                Geometry g = geomFac.toGeometry(envMap.get(id));
                g.apply(project.getGrid2space());
                if(minArea == 0 || (g.getArea() / minArea) > (1-1E-9)) {
                    Geometry geom = SpatialOp.vectorize(rasterPatchs, envMap.get(id), id.doubleValue());
                    geom.apply(project.getGrid2space());
                    if(minArea == 0 || (geom.getArea() / minArea) > (1-1E-9)) {                       
                        sortedPatch.put(SpatialOp.getFirstPixel(rasterPatchs, geom, id, project.getSpace2grid()), new DefaultFeature(id, geom));
                    }
                    else {
                        synchronized(MonoHabitat.this) {
                            SpatialOp.recode(rasterPatchs, geom, id, 0, project.getSpace2grid());
                            nbRem++;
                        }
                    }
                } else {
                    synchronized(MonoHabitat.this) {
                        SpatialOp.recode(rasterPatchs, g, id, 0, project.getSpace2grid());
                        nbRem++;
                    }
                }

            }
            
        };
        new ParallelFExecutor(task).executeAndWait();

        if(sortedPatch.isEmpty()) {
            Logger.getLogger(MonoHabitat.class.getName()).log(Level.WARNING, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("THERE IS NO PATCH IN THE MAP. CHECK PATCH CODE AND MIN AREA."));
        }
        
        WritableRaster land = landCov.getRenderedImage().copyData(null).createWritableTranslatedChild(1, 1);
        List<String> attrNames = new ArrayList<>(PATCH_ATTRS);

        int n = 1;
        patches = new ArrayList<>();
        for(DefaultFeature patch : sortedPatch.values()) {
            List lst = new ArrayList(Arrays.asList(patch.getGeometry().getArea(), patch.getGeometry().getLength(), 
                    patch.getGeometry().getArea(), getIdHab()));
            patches.add(new DefaultFeature(n+getStartId(), patch.getGeometry(), attrNames, lst));
            SpatialOp.recode(rasterPatchs, patch.getGeometry(), (int)patch.getId(), n+getStartId(), project.getSpace2grid());
            n++;
        }
        sortedPatch.clear();
        
        monitor.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Saving..."));
        monitor.setIndeterminate(true);
        
        GridCoverage2D clustCov = new GridCoverageFactory().create(getName(),
                new BufferedImage(new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_GRAY),
                    false, false, ColorModel.OPAQUE, DataBuffer.TYPE_INT),
                    rasterPatchs, false, null), project.getRasterPatchEnv());
       
        IOImage.saveTiffCoverage(new File(getDir(), PATCH_RASTER), clustCov, "LZW"); //NOI18N
        clustCov = null;
        
        if(!patches.isEmpty()) {
            IOFeature.saveFeatures(patches, new File(getDir(), PATCH_SHAPE), project.getCRS());
            monitor.reset();
            if(calcVoronoi) {
               calcVoronoi(rasterPatchs, monitor);
            }
        }
        
        monitor.close();
        
    }
    
    /**
     * @return the codes defining patches in the landscape map
     */
    public Set<Integer> getPatchCodes() {
        return patchCodes;
    }
    
     /**
     * @return is patch extraction is 8 connex or 4 connex ?
     */
    public boolean isCon8() {
        return con8;
    }

    /**
     * @return the minimal area for patches
     */
    public double getMinArea() {
        return minArea;
    }

    public double getMaxSize() {
        return maxSize;
    }

    /**
     * @return the parameters used for calculating the capacity of the patches
     */
    public CapaPatchDialog.CapaPatchParam getCapacityParams() {
        return capacityParams;
    }
    
    /**
     * Sets the capacities of the patches given the parameters
     * @param param the parameters for calculating the new capacities
     * @param save save the project ?
     * @throws IOException 
     */
    public void setCapacities(CapaPatchDialog.CapaPatchParam param, boolean save) throws IOException, SchemaException {
        if(param.importFile != null) {
            CSVTabReader r = new CSVTabReader(param.importFile);
            r.read(param.idField);

            if(r.getKeySet().size() < getPatches().size()) {
                throw new IOException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Some_patch_ids_are_missing."));
            }

            for(Number id : (Set<Number>)r.getKeySet()) {
                DefaultFeature p = getPatch(id.intValue());
                setCapacity(p, ((Number)r.getValue(id, param.capaField)).doubleValue());
            }
            r.dispose();
        } else if(param.isCalcArea()) {
            if(!param.isWeightedArea()) {
                for(DefaultFeature patch : getPatches()) {
                    setCapacity(patch, Math.pow(patch.getGeometry().getArea(), param.exp));
                }
            } else {
                // weighted area
                Raster rPatch = getRasterPatch();
                Raster land = getProject().getRasterLand();
                for(DefaultFeature patch : getPatches()) {
                    int id = (int) patch.getId();
                    double sum = 0;
                    Envelope env = getProject().getSpace2grid().transform(patch.getGeometry()).getEnvelopeInternal();
                    for(double y = (int)env.getMinY() + 0.5; y <= Math.ceil(env.getMaxY()); y++) {
                        for(double x = (int)env.getMinX() + 0.5; x <= Math.ceil(env.getMaxX()); x++) {
                            if(rPatch.getSample((int)x, (int)y, 0) == id) {
                                double w = param.codeWeight.get(land.getSample((int)x, (int)y, 0));
                                sum += w * getProject().getResolution()*getProject().getResolution();
                            }
                        }
                    }
                    setCapacity(patch, Math.pow(sum, param.exp));
                }
            }
        } else {
            calcNeighborAreaCapacity(getProject().getLinkset(param.costName), param.maxCost, param.codes, param.weightCost);
        }
        capacityParams = param;
        
        if(save) {
            savePatch();
            getProject().save();
        }
    }
    
    private void calcNeighborAreaCapacity(final Linkset linkset, final double maxCost, final HashSet<Integer> codes, final boolean weight) {
        ProgressBar progressBar = Config.getProgressBar(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("CALC PATCH CAPACITY..."));
        AbstractParallelFTask task = new AbstractParallelFTask(progressBar) {
            @Override
            protected Object execute(int start, int end) {
                if(isCanceled()) {
                    return null;
                }
               
                RasterPathFinder pathfinder = linkset.getCostVersion(MonoHabitat.this).getPathFinder();

                for(int i = start; i < end; i++) {
                    DefaultFeature patch = getPatches().get(i);
                    double capa = pathfinder.getNeighborhood(patch, maxCost,
                            getProject().getRasterLand(), codes, weight);
                    setCapacity(patch, capa);
                    incProgress(1);
                }

               return null;
            }
            @Override
            public int getSplitRange() {
                return getPatches().size();
            }
            @Override
            public void finish(Collection results) {}
            @Override
            public Object getResult() {
                return null;
            }

        };

        new ParallelFExecutor(task).executeAndWait();

        progressBar.close();
        
    }
    
    /**
     * Returns detailed informations of the linkset.
     * 
     * The language is local dependent
     * @return 
     */
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle");
        
        String info = super.getInfo() + "\n";
        info += "\n" + bundle.getString("NewProjectDialog.connexPanel.border.title_1") + " : ";
        if(isCon8()) {
            info += bundle.getString("NewProjectDialog.con8RadioButton.text_1");
        } else {
            info += bundle.getString("NewProjectDialog.con4RadioButton.text_1");
        }
        info += "\n" + bundle.getString("NewProjectDialog.jLabel1.text_1") + " : " + getPatchCodes().toString();
        info += "\n" + bundle.getString("NewProjectDialog.jLabel2.text_1") + " : " + getMinArea() + " m2";
        info += "\n" + bundle.getString("HabitatPanel.maxSizeLabel.text") + " : " + getMaxSize() + " m";

        return info;
    }
}
