/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import org.geotools.feature.SchemaException;
import org.thema.drawshape.layer.FeatureLayer;
import org.thema.drawshape.style.FeatureStyle;
import org.thema.graphab.Project;
import org.thema.graphab.links.Linkset;

/**
 *
 * @author Gilles Vuidel
 */
public class HabitatLayer extends FeatureLayer {

    private final AbstractMonoHabitat habitat;
    public static final Color [] COLORS = new Color[] {new Color(0x65a252), new Color(0x556450), new Color(0xaac8a1), new Color(0x809679), new Color(0xd5fac9),
        new Color(0x6ec852), new Color(0x376429), new Color(0x8afa67), new Color(0x53963e), 
        new Color(0x50c82b), new Color(0x286416), new Color(0x64fa36), new Color(0x3c9620)};
    
    public HabitatLayer(AbstractMonoHabitat habitat) {
        super(habitat.getIdHab() + " - " + habitat.getName(), habitat.getPatches(),  //NOI18N
                new FeatureStyle(getColorHab(habitat.getIdHab()), getColorHab(habitat.getIdHab())), habitat.getProject().getCRS());
        this.habitat = habitat;
    }
    
    public static Color getColorHab(int idHab) {
        return COLORS[idHab % COLORS.length];
    }
    
    @Override
    public JPopupMenu getContextMenu() {
        JPopupMenu menu = super.getContextMenu(); 
        
        if(habitat instanceof MonoHabitat) {
            menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("SET CAPACITY")) {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    final CapaPatchDialog dlg = new CapaPatchDialog(null, habitat.getProject(), (MonoHabitat)habitat);
                    dlg.setVisible(true);
                    if(!dlg.isOk) {
                        return;
                    }

                    new Thread(() -> {
                        try {
                            ((MonoHabitat)habitat).setCapacities(dlg.params, true);
                            JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("CAPACITY SAVED."));
                        } catch (IOException | SchemaException ex) {
                            Logger.getLogger(HabitatLayer.class.getName()).log(Level.SEVERE, null, ex);
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }).start();
                }
            });
        }
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("REMOVE PATCHES")) {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String res = JOptionPane.showInputDialog(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("CREATE A NEW HABITAT WHILE REMOVING PATCHES WITH A CAPACITY LESS THAN :"), 0);
                if(res == null) {
                    return;
                }
                final double minCapa = Double.parseDouble(res.trim());
                final String name = habitat.getName() + "-" + res.trim(); //NOI18N

                new Thread(() -> {
                    try {
                        AbstractMonoHabitat newHab = habitat.createHabitat(name, minCapa);
                        habitat.getProject().addHabitat(newHab, true);
                    } catch (IOException | SchemaException ex) {
                        Logger.getLogger(HabitatLayer.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(null, java.text.MessageFormat.format(
                                java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("ERROR {0}"), ex.getLocalizedMessage()), 
                                java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("ERROR"), JOptionPane.ERROR_MESSAGE); 
                    }
                }).start();
            }
        });
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DISTANCE MATRIX")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MatrixDistanceDialog(null, habitat, null).setVisible(true);
            }
        });
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Remove...")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                HashSet<String> linksetNames = new HashSet<>();
                for (Linkset l : habitat.getProject().getLinksets()) {
                    if(l.getHabitat().getIdHabitats().contains(habitat.getIdHab())) {
                        linksetNames.add(l.getName());
                    }
                }
                int res = JOptionPane.showConfirmDialog(null, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Do_you_want_to_remove_the_habitat_") + getName() + " ?\n" 
                        + (!linksetNames.isEmpty() ? java.util.ResourceBundle.getBundle("org/thema/graphab/dataset/Bundle").getString("LINKSETS") + Arrays.deepToString(linksetNames.toArray()) + 
                                java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("_will_be_removed.") : ""), java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Remove"), JOptionPane.YES_NO_OPTION);
                if (res != JOptionPane.YES_OPTION) {
                    return;
                }
                try {
                    habitat.getProject().removeHabitat(habitat.getName(), true);
                    habitat.getProject().getHabitatLayers().removeLayer(HabitatLayer.this);
                } catch (IOException ex) {
                    Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        menu.add(new AbstractAction(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Properties...")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, habitat.getInfo(), 
                        java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Properties"), JOptionPane.INFORMATION_MESSAGE);
            }
        });
        
        return menu;
    }
}
