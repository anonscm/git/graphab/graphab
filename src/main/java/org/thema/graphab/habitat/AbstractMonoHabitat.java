/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.awt.Rectangle;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.feature.SchemaException;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.thema.data.IOFeature;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import static org.thema.graphab.habitat.Habitat.AREA_ATTR;
import static org.thema.graphab.habitat.Habitat.CAPA_ATTR;
import static org.thema.graphab.habitat.Habitat.IDHAB_ATTR;
import static org.thema.graphab.habitat.Habitat.ID_RANGE;
import static org.thema.graphab.habitat.Habitat.PATCH_ATTRS;
import static org.thema.graphab.habitat.Habitat.PERIM_ATTR;
import static org.thema.graphab.habitat.Habitat.getPatchCapacity;

/**
 *
 * @author gvuidel
 */
public abstract class AbstractMonoHabitat extends Habitat {
    
    public static final String PATCH_RASTER = "patches.tif"; //NOI18N
    public static final String PATCH_SHAPE = "patches.gpkg"; //NOI18N
    
    private final int idHab;
    
    private final String name;
    
    private transient Project project;

    protected transient Project.Ref<WritableRaster> patchRaster;  
    
    private transient double totalPatchCapacity;

    protected AbstractMonoHabitat(int idHab, String name, Project project) {
        this.idHab = idHab;
        this.name = name;
        this.project = project;
        
        getDir().mkdirs();
    }
    
    
     /**
     * If it is not already loaded, load the raster.
     * @return the raster patch
     */
    @Override
    public WritableRaster getRasterPatch() {
        WritableRaster raster = patchRaster != null ? patchRaster.get() : null;
        if(raster == null) {
            raster = loadRasterPatch();
        }
        return raster;
    }
    
    public synchronized WritableRaster loadRasterPatch() {
        if(patchRaster == null || patchRaster.get() == null) {
            try {
                RenderedImage img = IOImage.loadTiffWithoutCRS(new File(getDir(), PATCH_RASTER)).getRenderedImage();
                WritableRaster raster;
                if(img.getNumXTiles() == 1 && img.getNumYTiles() == 1) {
                    raster = (WritableRaster) img.getTile(0, 0);
                } else {
                    raster = (WritableRaster) img.getData();
                }
                patchRaster = new Project.SoftRef<>(raster);
                return raster;
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            return patchRaster.get();
        }
    }
    
    @Override
    public WritableRaster getRasterPatch(int patchId) {
        return getRasterPatch();
    }
    
    @Override
    public Rectangle getRasterPatchBounds() {
        return getRasterPatch().getBounds();
    }

    public int getStartId() {
        return idHab*ID_RANGE;
    }
    
    public int getIdHab() {
        return idHab;
    }
    
    @Override
    public List<Integer> getIdHabitats() {
        return Collections.singletonList(idHab);
    }
    
    @Override
    public int getPatchId(int x, int y) {
        return getRasterPatch().getSample(x, y, 0);
    }
    
    @Override
    public final DefaultFeature getPatch(int id) {
        id = id % ID_RANGE;
        if(id > 0 && id <= getPatches().size()) {
            return getPatches().get(id-1);
        } else {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("UNKNOWN PATCH ID : {0}"), id));
        }
    }
    
    @Override
    public void savePatch() throws IOException, SchemaException {
        if(!getPatches().isEmpty()) {
            IOFeature.saveFeatures(getPatches(), new File(getDir(), PATCH_SHAPE), getProject().getCRS());
        }
    }
    
    public void removeFiles() {
        super.removeFiles();
        new File(getDir(), PATCH_SHAPE).delete();
        new File(getDir(), PATCH_RASTER).delete();
        new File(getDir(), PATCH_RASTER.replace(".tif", ".tfw")).delete(); //NOI18N
    }
    
    /**
     * Sets the capacity of a patch.
     * Reset the total capacity.
     * @param patch a patch
     * @param capa the new capacity
     */
    protected void setCapacity(DefaultFeature patch, double capa) {
        patch.setAttribute(CAPA_ATTR, capa);
        totalPatchCapacity = 0;
    }
    
    /**
     * @return the sum of the capacity of all patches
     */
    @Override
    public synchronized double getTotalPatchCapacity() {
        if(totalPatchCapacity == 0) {
            double sum = 0;
            for(Feature f : getPatches()) {
                sum += getPatchCapacity(f);
            }
            totalPatchCapacity = sum;
        }

        return totalPatchCapacity;
    }

    /**
     * Create a patch without adding it in the project
     * @param geom the geometry of the patch (Point or Polygon)
     * @param capa the capacity of the patch
     * @return the new patch
     */
    public DefaultFeature createPatch(Geometry geom, double capa) {
        List<String> attrNames = new ArrayList<>(PATCH_ATTRS);
        List attrs = new ArrayList(Arrays.asList(new Double[attrNames.size()]));
        attrs.set(attrNames.indexOf(CAPA_ATTR), capa);
        attrs.set(attrNames.indexOf(AREA_ATTR), geom.getDimension() == 0 ? project.getResolution()*project.getResolution() : geom.getArea());
        attrs.set(attrNames.indexOf(PERIM_ATTR), geom.getDimension() == 0 ? project.getResolution()*4 : geom.getLength());
        attrs.set(attrNames.indexOf(IDHAB_ATTR), idHab);
        return new DefaultFeature(idHab*ID_RANGE+getPatches().size()+1, geom, attrNames, attrs);
    }
    
    public boolean canCreatePatch(Geometry geom) {
        if(geom instanceof Point) {
            return canCreatePatch((Point) geom);
        }
        // tester si pas dans un patch
        GeometryFactory geomFactory = geom.getFactory();
        Geometry geomGrid = project.getSpace2grid().transform(geom);
        Envelope env = geomGrid.getEnvelopeInternal();
        for(double y = (int)env.getMinY() + 0.5; y <= Math.ceil(env.getMaxY()); y++) {
            for(double x = (int)env.getMinX() + 0.5; x <= Math.ceil(env.getMaxX()); x++) {
                Point p = geomFactory.createPoint(new Coordinate(x, y));
                if(geomGrid.contains(p) &&
                        !canCreatePatch((Point)project.getGrid2space().transform(p))) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean canCreatePatch(Point p) {
        try {
            if(!project.isInZone(p.getX(), p.getY())) {
                return false;
            }
        } catch (IOException ex) {
            Logger.getLogger(AbstractMonoHabitat.class.getName()).log(Level.SEVERE, null, ex);
        }
        Coordinate cg = project.getSpace2grid().transform(p.getCoordinate(), new Coordinate());
        if(getRasterPatch().getSample((int)cg.x, (int)cg.y, 0) > 0) {
            return false;
        }
        
        return true;
    }
    
     /**
     * Creates and adds a new patch into the project.
     * The rasters patch and landscape are modified
     * @param point the geometry of the patch 
     * @param capa the capacity of the patch
     * @return the new patch
     * @throws IOException 
     * @throws IllegalArgumentException if a patch already exists at the same position
     */
    public synchronized DefaultFeature addPatch(Point point, double capa) throws IOException {
        // tester si pas dans un patch ou touche un patch
        if(!canCreatePatch(point)) {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("PATCH ALREADY EXISTS AT THE SAME POSITION : {0}"), point.toString()));
        }
        DefaultFeature patch = createPatch(point, capa);
        int id = (Integer)patch.getId();
        Coordinate cg = getProject().getSpace2grid().transform(point.getCoordinate(), new Coordinate());
        // on passe les raster en strong reference pour qu'ils ne puissent pas être supprimé
        patchRaster = new Project.StrongRef<>(getRasterPatch());
        getRasterPatch().setSample((int)cg.x, (int)cg.y, 0, id);
        
        patches.add(patch);
        patchIndex = null;
        return patch;
    }
    
    /**
     * Creates and adds a new patch into the project.
     * The rasters patch and landscape are modified
     * @param geom the geometry of the patch 
     * @param capa the capacity of the patch
     * @return the new patch
     * @throws IOException 
     * @throws IllegalArgumentException if a patch already exists at the same position
     */
    public synchronized DefaultFeature addPatch(Geometry geom, double capa) throws IOException {
        if(geom instanceof Point) { // pas sûr que ce soit utile...
            return addPatch((Point) geom, capa);
        }
        // tester si pas dans un patch ou touche un patch
        if(!canCreatePatch(geom)) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("PATCH ALREADY EXIST AT THE SAME POSITION"));
        }
                    
        DefaultFeature patch = createPatch(geom, capa);
        int id = (Integer)patch.getId();
        // on passe les raster en strong reference pour qu'ils ne puissent pas être supprimé
        patchRaster = new Project.StrongRef<>(getRasterPatch());
        GeometryFactory geomFactory = geom.getFactory();
        Geometry geomGrid = getProject().getSpace2grid().transform(geom);
        Envelope env = geomGrid.getEnvelopeInternal();
        for(double y = (int)env.getMinY() + 0.5; y <= Math.ceil(env.getMaxY()); y++) {
            for(double x = (int)env.getMinX() + 0.5; x <= Math.ceil(env.getMaxX()); x++) {
                Point p = geomFactory.createPoint(new Coordinate(x, y));
                if(geomGrid.contains(p)) {
                    getRasterPatch().setSample((int)x, (int)y, 0, id);
                }
            }
        }
        patches.add(patch);
        patchIndex = null;
        return patch;
    }

    /**
     * Remove a point patch previously added by {@link #addPatch }.
     * Only the latter added patch can be removed.
     * @param patch the patch to remove
     * @throws IOException 
     * @throws IllegalArgumentException if the patch geometry is not a Point or if the patch is not the last
     */
    public synchronized void removePointPatch(Feature patch) throws IOException {
        if(!(patch.getGeometry() instanceof Point)) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("CANNOT REMOVE PATCH WITH GEOMETRY DIFFERENT OF POINT"));
        }
        Coordinate cg = getProject().getSpace2grid().transform(patch.getGeometry().getCoordinate(), new Coordinate());
        int id = (Integer)patch.getId();
        if(id != patches.size()) {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("THE PATCH TO REMOVE IS NOT THE LAST ONE - ID : {0}"), patch.getId()));
        }
        getRasterPatch().setSample((int)cg.x, (int)cg.y, 0, 0);
        patches.remove(patches.size()-1);
    }
    
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public Project getProject() {
        return project;
    }

    public void load(Project prj) throws IOException {
        this.project = prj;
        if(!new File(getDir(), PATCH_SHAPE).exists() && new File(getDir(), PATCH_RASTER).exists()) {
            // empty habitat
            this.patches = new ArrayList<>();
        } else {
            List<DefaultFeature> features = IOFeature.loadFeatures(new File(getDir(), PATCH_SHAPE), "Id"); //NOI18N
            this.patches = new ArrayList<>(features);
            for(DefaultFeature f : features) {
                this.patches.set((((Integer)f.getId())%ID_RANGE)-1, f);
            }
        }
    }
    
    /**
     * Creates a new habitat based on this one by removing patches with capacity lower than minCapa.
     * @param habName the new habitat name
     * @param minCapa the minimum capacity for the patch
     * @return the new habitat
     * @throws IOException
     * @throws SchemaException 
     */
    public AbstractMonoHabitat createHabitat(String habName, double minCapa) throws IOException, SchemaException {
        return new MonoVectorHabitat(habName, getProject(), getPatches(), CAPA_ATTR, minCapa, hasVoronoi());
    }
    
    /**
     * Creates a new habitat based on this one by removing patches with capacity lower than minCapa.
     * @param habName the new habitat name
     * @param minCapa the minimum capacity for the patch
     * @param vovonoi create voronoi ?
     * @return the new habitat
     * @throws IOException
     * @throws SchemaException 
     */
    public AbstractMonoHabitat createHabitat(String habName, double minCapa, boolean vovonoi) throws IOException, SchemaException {
        return new MonoVectorHabitat(habName, getProject(), getPatches(), CAPA_ATTR, minCapa, vovonoi);
    }
    
}
