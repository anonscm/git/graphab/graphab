/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BandedSampleModel;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.feature.SchemaException;
import org.thema.common.Config;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.process.Rasterizer;


/**
 *
 * @author gvuidel
 */
public class MonoVectorHabitat extends AbstractMonoHabitat {

    public MonoVectorHabitat(String name, Project project, Collection<? extends Feature> features, String capaAttr, boolean calcVoronoi) throws IOException, SchemaException {
        this(name, project, features, capaAttr, 0, calcVoronoi);
    }
    
    public MonoVectorHabitat(String name, Project project, Collection<? extends Feature> features, String capaAttr, double minCapa, boolean calcVoronoi) throws IOException, SchemaException {
        super(project.getNextIdHab(), name, project);
        
        List<String> attrNames = new ArrayList<>(PATCH_ATTRS);
        attrNames.addAll(features.iterator().next().getAttributeNames());

        int n = 1;
        patches = new ArrayList<>();
        for(Feature patch : features) {
            double capa = capaAttr == null ? patch.getGeometry().getArea() : ((Number)patch.getAttribute(capaAttr)).doubleValue();
            if(capa >= minCapa) {
                List lst = new ArrayList(Arrays.asList(patch.getGeometry().getArea(), patch.getGeometry().getLength(), capa, getIdHab()));
                lst.addAll(patch.getAttributes());
                patches.add(new DefaultFeature(n+getStartId(), patch.getGeometry(), attrNames, lst));
                n++;
            }
        }
        
        savePatch();
        WritableRaster land = project.getRasterLand();
        
        WritableRaster rasterPatch = Raster.createWritableRaster(new BandedSampleModel(DataBuffer.TYPE_INT, 
                land.getWidth()+2, land.getHeight()+2, 1), null);
        
        new Rasterizer(new DefaultFeatureCoverage<>(patches), project.getResolution(), "(Id)").rasterize(rasterPatch, project.getSpace2grid(), null); //NOI18N
        
        // set borders to -1
        for(int i = 0; i < rasterPatch.getWidth(); i++) {
            rasterPatch.setSample(i, 0, 0, -1);
            rasterPatch.setSample(i, rasterPatch.getHeight()-1, 0, -1);
        }
        for(int i = 0; i < rasterPatch.getHeight(); i++) {
            rasterPatch.setSample(0, i, 0, -1);
            rasterPatch.setSample(rasterPatch.getWidth()-1, i, 0, -1);
        }
        
        // set nodata to -1
        for(int i = 1; i < rasterPatch.getHeight()-1; i++) {
            for(int j = 1; j < rasterPatch.getWidth()-1; j++) {
                if(land.getSample(j, i, 0) == project.getNoData()) {
                    rasterPatch.setSample(j, i, 0, -1);
                }
            }
        }
        
        GridCoverage2D clustCov = new GridCoverageFactory().create(getName(),
                new BufferedImage(new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_GRAY),
                    false, false, Transparency.OPAQUE, DataBuffer.TYPE_INT),
                    rasterPatch, false, null), project.getRasterPatchEnv());

        IOImage.saveTiffCoverage(new File(getDir(), PATCH_RASTER), clustCov, "LZW"); //NOI18N
        
        if(calcVoronoi) {
           calcVoronoi(rasterPatch, Config.getProgressBar());
        }
    }

     /**
     * Returns detailed informations of the linkset.
     * 
     * The language is local dependent
     * @return 
     */
    public String getInfo() {
        
        String info = super.getInfo() + "\n\n"; //NOI18N
        info += java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("HABITAT FROM VECTOR LAYER");

        return info;
    }
    
}
