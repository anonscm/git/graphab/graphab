/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.geotools.feature.SchemaException;
import org.thema.graphab.Project;

/**
 *
 * @author gvuidel
 */
public class HabitatPanel extends javax.swing.JPanel {

    private TreeSet<Integer> codes;
    private Project project;
    
    /**
     * Creates new form HabitatPanel
     */
    public HabitatPanel() {
        initComponents();
    }

    public void setCodes(TreeSet<Integer> codes) {
        this.codes = codes;
        codesList.setModel(new DefaultComboBoxModel(codes.toArray()));
    }

    public void setProject(Project project) {
        this.project = project;
        if(!project.getCodes().equals(codes)) {
            setCodes(project.getCodes());
        }
    }

    public boolean validatePanel() {
        if(project != null && project.isHabitatExists(getHabitatName())) {
            JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("The_name_already_exists") + getHabitatName());
            return false;
        }
        
        if(getHabitatCodes().isEmpty()) {
            JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("SELECT HABITAT CODE(S)."), 
                    java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
    
    public String getHabitatName() {
        return habNameTextField.getText();
    }
    
    public MonoHabitat getHabitat() throws IOException, SchemaException {
        // on le convertit d'ha en m2 Attention on suppose que le système de coordonnées est en mètre
        double minArea = (Double)minAreaSpinner.getValue() * 10000;
        boolean con8 = con8RadioButton.isSelected();
        double maxSize = (Double)maxSizeSpinner.getValue();
        return new MonoHabitat(getHabitatName(), project, getHabitatCodes(), con8, minArea, maxSize, true);
    }

    public Set<Integer> getHabitatCodes() {
        List list = codesList.getSelectedValuesList();
        if(list.size() == 1) {
            return Collections.singleton((Integer)list.get(0));
        }
        return new HashSet<>(list);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        codesLabel = new javax.swing.JLabel();
        minAreaLabel = new javax.swing.JLabel();
        minAreaSpinner = new javax.swing.JSpinner();
        connexPanel = new javax.swing.JPanel();
        con4RadioButton = new javax.swing.JRadioButton();
        con8RadioButton = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        codesList = new javax.swing.JList();
        nameLabel = new javax.swing.JLabel();
        habNameTextField = new javax.swing.JTextField();
        maxSizeLabel = new javax.swing.JLabel();
        maxSizeSpinner = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle"); // NOI18N
        codesLabel.setText(bundle.getString("NewProjectDialog.jLabel1.text_1")); // NOI18N

        minAreaLabel.setText(bundle.getString("NewProjectDialog.jLabel2.text_1")); // NOI18N

        minAreaSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 1.0d));

        connexPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("NewProjectDialog.connexPanel.border.title_1"))); // NOI18N

        con4RadioButton.setSelected(true);
        con4RadioButton.setText(bundle.getString("NewProjectDialog.con4RadioButton.text_1")); // NOI18N

        con8RadioButton.setText(bundle.getString("NewProjectDialog.con8RadioButton.text_1")); // NOI18N

        javax.swing.GroupLayout connexPanelLayout = new javax.swing.GroupLayout(connexPanel);
        connexPanel.setLayout(connexPanelLayout);
        connexPanelLayout.setHorizontalGroup(
            connexPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(connexPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(connexPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(con4RadioButton)
                    .addComponent(con8RadioButton))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        connexPanelLayout.setVerticalGroup(
            connexPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(connexPanelLayout.createSequentialGroup()
                .addComponent(con4RadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(con8RadioButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel4.setText(bundle.getString("NewProjectDialog.jLabel4.text")); // NOI18N

        jScrollPane1.setViewportView(codesList);

        nameLabel.setText(bundle.getString("NewProjectDialog.jLabel5.text")); // NOI18N

        habNameTextField.setText(bundle.getString("NewProjectDialog.habNameTextField.text")); // NOI18N

        maxSizeLabel.setText(bundle.getString("HabitatPanel.maxSizeLabel.text")); // NOI18N

        maxSizeSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 1.0d));

        jLabel6.setText(bundle.getString("HabitatPanel.jLabel6.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(codesLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(21, 21, 21)
                        .addComponent(connexPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(minAreaLabel)
                            .addComponent(maxSizeLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(maxSizeSpinner)
                            .addComponent(minAreaSpinner))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addGap(36, 36, 36))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(habNameTextField)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLabel)
                    .addComponent(habNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(codesLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(connexPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(minAreaLabel)
                    .addComponent(minAreaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(maxSizeLabel)
                    .addComponent(maxSizeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel codesLabel;
    private javax.swing.JList codesList;
    private javax.swing.JRadioButton con4RadioButton;
    private javax.swing.JRadioButton con8RadioButton;
    private javax.swing.JPanel connexPanel;
    private javax.swing.JTextField habNameTextField;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel maxSizeLabel;
    private javax.swing.JSpinner maxSizeSpinner;
    private javax.swing.JLabel minAreaLabel;
    private javax.swing.JSpinner minAreaSpinner;
    private javax.swing.JLabel nameLabel;
    // End of variables declaration//GEN-END:variables


}
