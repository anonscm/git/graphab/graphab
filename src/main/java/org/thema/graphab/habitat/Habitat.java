/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import com.ezylang.evalex.EvaluationException;
import com.ezylang.evalex.Expression;
import com.ezylang.evalex.parser.ParseException;
import java.awt.Rectangle;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.feature.SchemaException;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.util.AffineTransformation;
import org.locationtech.jts.index.strtree.ItemBoundable;
import org.locationtech.jts.index.strtree.ItemDistance;
import org.locationtech.jts.index.strtree.STRtree;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.parallel.SimpleParallelTask;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.GraphPathFinder;
import org.thema.graphab.links.CircuitRaster;
import org.thema.graphab.links.EuclideLinkset;
import org.thema.graphab.links.Linkset;
import org.thema.graphab.links.Path;
import org.thema.graphab.links.PlanarLinks;
import org.thema.graphab.links.SpacePathFinder;
import org.thema.graphab.metric.Circuit;
import org.thema.graphab.util.DistanceOp;
import org.thema.graphab.util.SpatialOp;
import org.thema.parallel.AbstractParallelTask;
import org.thema.parallel.ExecutorService;

/**
 *
 * @author Gilles Vuidel
 */
public abstract class Habitat implements Comparable<Habitat>{
    
    public enum Distance {EUCLIDEAN, LEASTCOST, CIRCUIT, FLOW, CIRCUIT_FLOW}
    
    public static final String VORONOI_SHAPE = "voronoi.gpkg"; //NOI18N
    public static final String LINKS_SHAPE = "links.gpkg"; //NOI18N
    
    // Patch attributes
    public static final String IDHAB_ATTR = "idhab"; //NOI18N
    public static final String CAPA_ATTR = "capacity"; //NOI18N
    public static final String AREA_ATTR = "area"; //NOI18N
    public static final String PERIM_ATTR = "perim"; //NOI18N
    protected static final List<String> PATCH_ATTRS = Arrays.asList(AREA_ATTR, PERIM_ATTR, CAPA_ATTR, IDHAB_ATTR);
    
    public static final int ID_RANGE = 10000000;
    
    protected transient List<DefaultFeature> patches;
    protected transient Map<Integer, Feature> voronoi;
    protected transient PlanarLinks planarLinks;
    protected transient STRtree patchIndex;
    
    
    /**
     * The patches are ordered by their id.
     * list(0) : patch id 1 + x * ID_RANGE
     * list(n-1) : patch id n + x * ID_RANGE
     * @return all the patches
     */
    public List<DefaultFeature> getPatches() {
        return patches;
    }
    
    /**
     * The patches filtered by filter
     * @param filter filtering condition
     * @return the patches where filter condition is true
     */
    public List<DefaultFeature> getPatches(String filter) {
        if(filter == null || filter.isEmpty()) {
            return getPatches();
        }
        List<DefaultFeature> result = new ArrayList<>();
        Expression filterExpr = new Expression(filter);
        for(DefaultFeature f : getPatches()) {
            try {
                Set<String> vars = filterExpr.getUsedVariables();
                Map<String, Object> values = new HashMap<>();
                for(String var : vars) {
                    if(var.equalsIgnoreCase("id")) {
                        values.put(var, f.getId());
                    } else {
                        Object val = f.getAttribute(var);
                        if(val == null) {
                            values.put(var, "");
                        } else {
                            values.put(var, val);
                        }
                    }
                }
                if(filterExpr.withValues(values).evaluate().getBooleanValue()) {
                    result.add(f);
                }
            } catch(EvaluationException | ParseException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }
     
    /**
     * @param id the identifier of the patch
     * @return the patch with identifier id
     * @throws IllegalArgumentException if the patch does not exist
     */
    public abstract DefaultFeature getPatch(int id);
    
    public abstract List<Integer> getIdHabitats();
    
    /**
     * Creates the index if it does not exist already
     * @return a spatial index on patch features
     */
    public synchronized STRtree getPatchIndex() {
        if(patchIndex == null) {
            patchIndex = new STRtree();
            for(Feature f : getPatches()) {
                patchIndex.insert(f.getGeometry().getEnvelopeInternal(), f);
            }
        }

        return patchIndex;
    }
    
    protected void calcVoronoi(WritableRaster rasterPatch, ProgressBar progressBar) throws IOException, SchemaException {
        if(getPatches().size() == 1) {
            return;
        }
        progressBar.setNote("Voronoi..."); //NOI18N
        progressBar.setMaximum(200);
        neighborhoodEuclid(rasterPatch, getProject().getGrid2space(), getProject().getResolution(), progressBar.getSubProgress(100));
        planarLinks = createLinks(rasterPatch, progressBar.getSubProgress(100));
        IOFeature.saveFeatures(planarLinks.getFeatures(), new File(getDir(), LINKS_SHAPE), getProject().getCRS());

        List<? extends Feature> voronois = SpatialOp.vectorizeVoronoi(rasterPatch, getProject().getGrid2space());
        IOFeature.saveFeatures(voronois, new File(getDir(), VORONOI_SHAPE), getProject().getCRS());
    }
    
    /**
     * 
     * @param x coordinate in raster patch coordinate
     * @param y coordinate in raster patch coordinate
     * @return the patch id or 0 or -1
     */
    public abstract int getPatchId(int x, int y);
    
    public Set<Integer> getPatchIds(int x, int y) {
        int id = getPatchId(x, y);
        if(id > 0) {
            return Collections.singleton(id);
        } else {
            return Collections.emptySet();
        }
    }
    
    public abstract WritableRaster getRasterPatch();
    
    public abstract WritableRaster getRasterPatch(int patchId);
    
    public abstract Rectangle getRasterPatchBounds();
    
    /**
     * Checks if the geometry is contained in the landscape map and if none of the pixels belong to a patch 
     * @param geom the geometry to test
     * @return true if the patch can be created
     */
    public abstract boolean canCreatePatch(Geometry geom);
    
    /**
     * Checks if the point is contained in the landscape map and if the pixel does not belong to a patch 
     * @param p the point to test
     * @return true if the patch can be created
     */
    public abstract boolean canCreatePatch(Point p);
    
    /**
     * The voronoi polygon associated with the patch id
     * @param id the patch identifier
     * @return a polygon feature representing the voronoi polygon of the patch id
     */
    public final Feature getVoronoi(int id) {
        return getVoronoi().get(id);
    }
    
    /**
     * @return the voronoi features 
     * @throws IllegalStateException if the habitat has no voronoi
     */
    private synchronized Map<Integer, Feature> getVoronoi() {
        if(voronoi == null) {
            File file = new File(getDir(), VORONOI_SHAPE);
            if(!file.exists()) {
                throw new IllegalStateException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("THE HABITAT {0} DOES NOT CONTAIN VORONOI."), getName()));
            }
            try {
                List<DefaultFeature> features = IOFeature.loadFeatures(file, "Id"); //NOI18N
                voronoi = new HashMap<>();
                for(Feature f : features) {
                    voronoi.put((Integer)f.getId(), f);
                }
            } catch (IOException ex) {
                Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return voronoi;
    }
    
    /**
     * @return true if the habitat has voronoi ie. planar topology
     */
    public boolean hasVoronoi() {
        return new File(getDir(), VORONOI_SHAPE).exists();
    }
    
    /**
     * @return the planar topology
     */
    public synchronized PlanarLinks getPlanarLinks() {
        
        if(planarLinks == null) {
            File linkFile = new File(getDir(), LINKS_SHAPE);
            if(linkFile.exists()) {
                try {
                    List<DefaultFeature> features = IOFeature.loadFeatures(linkFile, "Id"); //NOI18N
                    List<Path> paths = new ArrayList<>(features.size());
                    for(Feature f : features) {
                        paths.add(Path.loadPath(f, this));
                    }
                    
                    planarLinks = new PlanarLinks(getPatches(), paths);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            } else if(getPatches().size() == 1) {
                planarLinks = new PlanarLinks(getPatches());
            } else {
                try {
                    WritableRaster voronoiR = getRasterPatch().createCompatibleWritableRaster();
                    voronoiR.setRect(getRasterPatch());
                    calcVoronoi(voronoiR, new TaskMonitor.EmptyMonitor());
                    //throw new IllegalStateException("The project does not contain voronoi.");
                } catch (IOException | SchemaException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        
        return planarLinks;
    }
    
    public abstract String getName();
    
    public abstract Project getProject();
    
    public File getDir() {
        return new File(getProject().getDirectory(), getName());
    }
    
    /**
     * Saves the csv file containing patch metric results.
     * @throws IOException
     * @throws SchemaException 
     */
    public abstract void savePatch() throws IOException, SchemaException;
    
    public void removeFiles() {
        new File(getDir(), LINKS_SHAPE).delete();
        new File(getDir(), VORONOI_SHAPE).delete();        
    }
    
    public abstract double getTotalPatchCapacity();
    
    /**
     * @param node a node of a graph
     * @return the patch associated with this graph node
     */
    public static DefaultFeature getPatch(org.geotools.graph.structure.Node node) {
        return (DefaultFeature)node.getObject();
    }
    
    /**
     * @param node a node of a graph
     * @return the capacity of the patch associated with this graph node
     */
    public static double getPatchCapacity(org.geotools.graph.structure.Node node) {
        return getPatchCapacity((Feature)node.getObject());
    }
    
    /**
     * @param patch a patch
     * @return the capacity of the patch 
     */
    public static double getPatchCapacity(Feature patch) {
        return ((Number)patch.getAttribute(CAPA_ATTR)).doubleValue();
    }
    
    /**
     * @param node a node of a graph
     * @return the area of the patch associated with this graph node
     */
    public static double getPatchArea(org.geotools.graph.structure.Node node) {
        return getPatchArea((Feature)node.getObject());
    }

    /**
     * @param patch a patch
     * @return the area of the patch 
     */
    public static double getPatchArea(Feature patch) {
        return ((Number)patch.getAttribute(AREA_ATTR)).doubleValue();
    }
    
    /**
     * @param node a node of a graph
     * @return the habitat id of the patch 
     */
    public static int getPatchIdHab(org.geotools.graph.structure.Node node) {
        return Habitat.getPatchIdHab((Feature)node.getObject());
    }
    
    /**
     * @param patch a patch
     * @return the habitat id of the patch 
     */
    public static int getPatchIdHab(Feature patch) {
        return ((Number)patch.getAttribute(IDHAB_ATTR)).intValue();
    }
    
    /**
     * @param idPatch a patch id
     * @return the habitat id of the patch 
     */
    public static int getPatchIdHab(int idPatch) {
        return idPatch / ID_RANGE;
    }

    public Collection<String> getAttributeNames() {
        return getPatches().get(0).getAttributeNames();
    }
        
    /**
     * Creates planar topology.Fill the raster voronoi with the nearest patch id.
     * @param voronoi an empty raster
     * @param grid2space
     * @param resolution
     * @param monitor
     * @throws IOException
     * @throws SchemaException 
     */
    protected void neighborhoodEuclid(final WritableRaster voronoi, 
            final AffineTransformation grid2space, double resolution, ProgressBar monitor) throws IOException, SchemaException {
        final STRtree index = new STRtree();
        final int NPOINTS = 50;
        final GeometryFactory factory = new GeometryFactory();
        monitor.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("Neighbor"));
        monitor.setMaximum(voronoi.getHeight());
        for(Feature f : getPatches()) {
            Geometry geom = f.getGeometry();
            if(geom.getNumPoints() > NPOINTS) {
                for(int i = 0; i < geom.getNumGeometries(); i++) {
                    Polygon p = (Polygon)geom.getGeometryN(i);
                    Coordinate [] coords = p.getExteriorRing().getCoordinates();
                    int ind = 0;
                    while(ind < coords.length-1) {
                        LineString line = factory.createLineString(Arrays.copyOfRange(coords, ind,
                                ind+NPOINTS+1 >= coords.length-1 ? coords.length : ind+NPOINTS+1));
                        DefaultFeature df = new DefaultFeature(f.getId(), line, null, null);
                        index.insert(line.getEnvelopeInternal(), df);
                        ind += NPOINTS;
                    }
                    for(int j = 0; j < p.getNumInteriorRing(); j++) {
                        index.insert(p.getInteriorRingN(j).getEnvelopeInternal(),
                                new DefaultFeature(f.getId(), p.getInteriorRingN(j), null, null));
                    }
                }
            } else {
                index.insert(geom.getEnvelopeInternal(), f);
            }
        }
        
        index.build();
        
        final ItemDistance distComp = new ItemDistance() {
            @Override
            public double distance(ItemBoundable i1, ItemBoundable i2) {
                Geometry g1 = ((Feature)i1.getItem()).getGeometry();
                Geometry g2 = ((Feature)i2.getItem()).getGeometry();
                Coordinate c = g1 instanceof Point ? ((Point)g1).getCoordinate() : ((Point)g2).getCoordinate();
                Geometry geom = g1 instanceof Point ? g2 : g1;
                if(geom instanceof LineString) {
                    return DistanceOp.distancePointLine(c, geom.getCoordinates());
                } else  if(geom instanceof Point) {
                    return c.distance(geom.getCoordinate());
                } else {
                    double d = Double.MAX_VALUE;
                    for(int g = 0; g < geom.getNumGeometries(); g++) {
                        Polygon poly = ((Polygon)geom.getGeometryN(g));
                        double dd = DistanceOp.distancePointLine(c, poly.getExteriorRing().getCoordinates());
                        if(dd < d) {
                            d = dd;
                        }
                        int n = poly.getNumInteriorRing();
                        for(int i = 0; i < n; i++) {
                            dd = DistanceOp.distancePointLine(c, poly.getInteriorRingN(i).getCoordinates());
                            if(dd < d) {
                                d = dd;
                            }
                        }
                    }
                    return d;
                }
            }
        };
        final int nPatch = getPatches().size();
        AbstractParallelTask task = new AbstractParallelTask<Void, Void>(monitor) {
            @Override
            public Void execute(int start, int end) {
                Coordinate c = new Coordinate();
                Envelope env = new Envelope();
                for(int y = start; y < end; y++) {
                    for(int x = 0; x < voronoi.getWidth(); x++) {
                        if(voronoi.getSample(x, y, 0) == 0) {
                            c.x = x+0.5; c.y = y+0.5;
                            grid2space.transform(c, c);
                            env.init(c);
                            DefaultFeature cell = new DefaultFeature("c", factory.createPoint(c)); //NOI18N
                            Feature nearest = null;
                            int k =  nPatch > 5 ? 5 : (nPatch-1);
                            int i = k;
                            // loop while all k nearest features have equal distance (eps : res/100)
                            while(i == k && k < nPatch) {
                                k *= 2;
                                if(k > nPatch) {
                                    k = nPatch;
                                }
                                Object[] nearests = index.nearestNeighbour(env, cell, distComp, k);
                                nearest = (Feature)nearests[0];
                                double dMin = cell.getGeometry().distance(nearest.getGeometry());
                                i = 1;
                                while(i < k && Math.abs(dMin - cell.getGeometry().distance(((Feature)nearests[i]).getGeometry())) < resolution/100) {
                                    if((int)((Feature)nearests[i]).getId() < (int)nearest.getId()) {
                                        nearest = (Feature)nearests[i];
                                    }
                                    i++;
                                }
                            }
                            voronoi.setSample(x, y, 0, (Integer)nearest.getId());
                        }
                    }
                    incProgress(1);
                }
                return null;
            }

            @Override
            public int getSplitRange() {
                return voronoi.getHeight();
            }

            @Override
            public void gather(Void results) {
            }
            @Override
            public Void getResult() {
                throw new UnsupportedOperationException(); 
            }
        };
        
        long time = System.currentTimeMillis();
        
        ExecutorService.execute(task);

        System.out.println("Temps calcul : " + (System.currentTimeMillis() - time) / 1000); //NOI18N
    }
    
    /**
     * Create the planar topology from the voronoi calculated by {@link #neighborhoodEuclid }
     * @param voronoiRaster
     * @param monitor
     * @return 
     */
    public PlanarLinks createLinks(Raster voronoiRaster, ProgressBar monitor) {
        monitor.setNote(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("PLANAR TOPOLOGY..."));
        monitor.setMaximum(voronoiRaster.getHeight());
        
        Path.newSetOfPaths();
        
        PlanarLinks links = new PlanarLinks(getPatches());

        for(int y = 1; y < voronoiRaster.getHeight()-1; y++) {
            monitor.setProgress(y);
            for(int x = 1; x < voronoiRaster.getWidth()-1; x++) {
                int id = voronoiRaster.getSample(x, y, 0);
                if(id <= 0) {
                    continue;
                }

                DefaultFeature f = getPatch(id);
                int id1 = voronoiRaster.getSample(x-1, y, 0);
                if(id1 > 0 && id != id1) {
                    DefaultFeature f1 = getPatch(id1);
                    if(!links.isLinkExist(f, f1)) {
                        links.addLink(new Path(f, f1));
                    }
                }
                id1  = voronoiRaster.getSample(x, y-1, 0);
                if(id1 > 0 && id != id1) {
                    DefaultFeature f1 = getPatch(id1);
                    if(!links.isLinkExist(f, f1)) {
                        links.addLink(new Path(f, f1));
                    }
                }
            }
        }

        monitor.close();
        
        return links;
    }

    @Override
    public int compareTo(Habitat t) {
        return getName().compareTo(t.getName());
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return 83 * 7 + Objects.hashCode(this.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Habitat other = (Habitat) obj;
        return Objects.equals(this.getName(), other.getName());
    }
    
    public double [][][] calcSpaceDistanceMatrix(Linkset costDist, Distance type, ProgressBar mon) {
        final List<DefaultFeature> features = getPatches();
        final double [][][] distances = new double[features.size()][features.size()][2];  

        SimpleParallelTask.IterParallelTask task;
        if(type == Distance.LEASTCOST || type == Distance.EUCLIDEAN) {
            final Linkset linkset = (type == Distance.EUCLIDEAN) ? new EuclideLinkset(this, "temp", Linkset.Topology.COMPLETE, false, null, false, 0)  //NOI18N
                    : costDist.getCostVersion(this);
            task = new SimpleParallelTask.IterParallelTask(features.size(), mon) {
                @Override
                protected void executeOne(Integer ind) {
                    SpacePathFinder pathFinder = linkset.getPathFinder();
                    HashMap<DefaultFeature, Path> dist = pathFinder.calcPaths(features.get(ind).getGeometry(), 0, false);
                    for(int j = 0; j < features.size(); j++) {
                        distances[ind][j][0] = dist.get(features.get(j)).getCost();
                        distances[ind][j][1] = dist.get(features.get(j)).getDist();
                    }
                }
            };
        } else if(type == Distance.CIRCUIT) {
            final CircuitRaster circuit;
            try {
                circuit = costDist.getCircuitVersion(this).getCircuit();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            task = new SimpleParallelTask.IterParallelTask(features.size(), mon) {
                @Override
                protected void executeOne(Integer ind) {
                    for(int j = ind+1; j < features.size(); j++) {
                        distances[ind][j][0] = circuit.getODCircuit(features.get(ind), features.get(j)).getR();
                        distances[ind][j][1] = Double.NaN;
                        distances[j][ind][0] = distances[ind][j][0];
                        distances[j][ind][1] = Double.NaN;
                    }
                }
            };
        } else {
            throw new IllegalArgumentException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("DISTANCE TYPE UNKNOWN IN SPACE MODE : {0}"), type));
        }
        new ParallelFExecutor(task).executeAndWait();
        if(task.isCanceled()) {
            throw new CancellationException();
        }
        
        return distances;
    }
    
    public double [][][] calcGraphDistanceMatrix(AbstractGraph graph, Distance type, double alpha, ProgressBar mon) {
        
        if(!graph.getHabitat().getIdHabitats().containsAll(getIdHabitats())) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("GRAPH MUST CONTAIN SELECTED HABITAT"));
        }

        final List<DefaultFeature> features = getPatches();
        final double [][][] distances = new double[features.size()][features.size()][2];  

        SimpleParallelTask.IterParallelTask task = null;
        
        switch(type) {
        case EUCLIDEAN:
        case LEASTCOST:
            task = new SimpleParallelTask.IterParallelTask(features.size(), mon) {
                @Override
                protected void executeOne(Integer ind) {
                    Feature patch1 = features.get(ind);
                    GraphPathFinder finder = graph.getPathFinder(graph.getNode(patch1));
                    for(int j = 0; j < features.size(); j++) {
                        if(ind == j) {
                            continue;
                        }
                        Feature patch2 = features.get(j);
                        Double dist = finder.getCost(graph.getNode(patch2));
                        if(dist == null) {
                            dist = Double.NaN;
                        } 
                        distances[ind][j][0] = dist;
                    }

                }
            };
            break;
        case FLOW:    
            task = new SimpleParallelTask.IterParallelTask(features.size(), mon) {
                @Override
                protected void executeOne(Integer ind) {
                    Feature patch1 = features.get(ind);
                    GraphPathFinder finder = graph.getFlowPathFinder(graph.getNode(patch1), alpha);
                    for(int j = 0; j < features.size(); j++) {
                        if(ind == j) {
                            continue;
                        }
                        Feature patch2 = features.get(j);
                        Double dist = finder.getCost(graph.getNode(patch2));
                        if(dist == null) {
                            dist = Double.NaN;
                        } else {
                            dist += - Math.log(Habitat.getPatchCapacity(patch1)*Habitat.getPatchCapacity(patch2)
                                    / Math.pow(graph.getHabitat().getTotalPatchCapacity(), 2));
                        }
                        distances[ind][j][0] = dist;
                    }
                }
            };
            break;
        case CIRCUIT:
        case CIRCUIT_FLOW:
            Circuit circuit = type == Distance.CIRCUIT_FLOW ? new Circuit(graph, alpha) : new Circuit(graph);

            task = new SimpleParallelTask.IterParallelTask(features.size(), mon) {
                @Override
                protected void executeOne(Integer ind) {
                    Feature patch1 = features.get(ind);

                    for(int j = 0; j < features.size(); j++) {
                        if(ind == j) {
                            continue;
                        }
                        Feature patch2 = features.get(j);
                        if(patch1.equals(patch2)) {
                            continue;
                        }
                        distances[ind][j][0] = circuit.computeR(graph.getNode(patch1), graph.getNode(patch2));
                    }
                    mon.incProgress(1);
                }
            };
        }
        
        new ParallelFExecutor(task).executeAndWait();
        if(task.isCanceled()) {
            throw new CancellationException();
        }
        
        return distances;
    }
    
    public void saveMatrix(double [][][] matrix, File file) throws IOException {
        final List<DefaultFeature> features = getPatches();
        try (FileWriter fw = new FileWriter(file)) {
            fw.write("Id1\tId2\tDistance\tLength\n"); //NOI18N
            for(int i = 0; i < features.size(); i++) {
                for(int j = 0; j < features.size(); j++) {
                    fw.write(features.get(i).getId() + "\t" + features.get(j).getId() + "\t" + matrix[i][j][0] + "\t" + matrix[i][j][1] + "\n"); //NOI18N
                }
            }
        } 
    }
    
    /**
     * Returns detailed informations of the habitat.
     * 
     * The language is local dependent
     * @return 
     */
    public String getInfo() {
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle");
        
        String info = bundle.getString("NewProjectDialog.jLabel5.text") + " : " + getName() + "\n";
        
        info += java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/Bundle").getString("# PATCHES : {0}"), getPatches().size());

        return info;
    }
}
