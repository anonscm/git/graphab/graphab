/*
 * Copyright (C) 2022 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.graphab.habitat;

import java.awt.Rectangle;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.geotools.feature.SchemaException;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.thema.common.Config;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.Project;

/**
 *
 * @author gvuidel
 */
public class MultiHabitat extends Habitat {

    private final List<AbstractMonoHabitat> habitats;
    
    private transient AbstractMonoHabitat[] tabHabitats;


    public MultiHabitat(Set<Integer> idHabitats, Project project, boolean calcVoronoi) {
        this(project.getHabitats().stream().filter(hab->idHabitats.contains(hab.getIdHab())).collect(Collectors.toList()), calcVoronoi);
    }
    
    public MultiHabitat(Collection<AbstractMonoHabitat> habitats, boolean calcVoronoi) {
        this.habitats = new ArrayList<>(habitats);
        Collections.sort((List)this.habitats);
        
        if(calcVoronoi && !hasVoronoi()) {
            getDir().mkdirs();
            WritableRaster voronoiR = getRasterPatch();            
            try {
                calcVoronoi(voronoiR, Config.getProgressBar());
            } catch (IOException | SchemaException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
    
    @Override
    public synchronized List<DefaultFeature> getPatches() {
        if(patches == null || habitats.stream().mapToInt(h->h.getPatches().size()).sum() != patches.size()) {
            patches = new ArrayList<>();
            for(Habitat hab : habitats) {
                patches.addAll(hab.getPatches());
            }
        }
        
        return patches;
    }

    protected synchronized AbstractMonoHabitat getHabitat(int patchId) {
        if(tabHabitats == null) {
            int max = habitats.stream().mapToInt(h->h.getIdHab()).max().getAsInt();
            
            tabHabitats = new AbstractMonoHabitat[max+1];
            for(AbstractMonoHabitat hab : habitats) {
                tabHabitats[hab.getIdHab()] = hab;
            }
        }
        int ind = patchId / ID_RANGE;
        return tabHabitats[ind];
    }
    
    @Override
    public DefaultFeature getPatch(int id) {
        return getHabitat(id).getPatch(id);
    }

    @Override
    public int getPatchId(int x, int y) {
        int id = -1;
        for(Habitat hab : habitats) {
            id = hab.getPatchId(x, y);
            if(id > 0) {
                return id;
            }
        }
        return id;
    }
    
    public Set<Integer> getPatchIds(int x, int y) {
        Set<Integer> ids = new HashSet<>();
        for(Habitat hab : habitats) {
            int id = hab.getPatchId(x, y);
            if(id > 0) {
                ids.add(id);
            }
        }
        return ids;
    }

    @Override
    public WritableRaster getRasterPatch(int patchId) {
        return getHabitat(patchId).getRasterPatch();
    }
    
    /**
     * Calculates a merged rasterpatch
     * @return 
     */
    @Override
    public final WritableRaster getRasterPatch() {
        WritableRaster raster = habitats.get(0).getRasterPatch().createCompatibleWritableRaster();
        raster.setRect(habitats.get(0).getRasterPatch());
        for(AbstractMonoHabitat hab : habitats.subList(1, habitats.size())) {
            Raster rast = hab.getRasterPatch();
            for(int y = 0; y < raster.getHeight(); y++) {
                for(int x = 0; x < raster.getWidth(); x++) {
                    if(rast.getSample(x, y, 0) > 0) {
                        raster.setSample(x, y, 0, rast.getSample(x, y, 0));
                    }
                }
            }
        }
        return raster;
    }

    @Override
    public Rectangle getRasterPatchBounds() {
        return habitats.get(0).getRasterPatchBounds();
    }

    @Override
    public String getName() {
        String name = "";
        for(Habitat hab : habitats) {
            name += "-" + hab.getName();
        }
        return name.replaceFirst("-", "");
    }

    @Override
    public Project getProject() {
        return habitats.get(0).getProject();
    }

    @Override
    public List<String> getAttributeNames() {
        Set<String> attrs = new HashSet<>(habitats.get(0).getAttributeNames());
        for(Habitat hab : habitats) {
            attrs.retainAll(hab.getAttributeNames());
        }
        return new ArrayList<>(attrs);
    }

    @Override
    public void savePatch() throws IOException, SchemaException {
        for(Habitat hab : habitats) {
            hab.savePatch();
        }
    }

    @Override
    public double getTotalPatchCapacity() {
        double capa = 0;
        for(Habitat hab : habitats) {
            capa += hab.getTotalPatchCapacity();
        }
        return capa;
    }
    
    public List<AbstractMonoHabitat> getHabitats() {
        return habitats;
    }
    
    @Override
    public List<Integer> getIdHabitats() {
        List<Integer> ids = new ArrayList<>();
        for(AbstractMonoHabitat hab : habitats) {
            ids.add(hab.getIdHab());
        }
        return ids;
    }
    
    /**
     * Checks if the geometry is contained in the landscape map and if none of the pixels belong to a patch 
     * Test for all habitats
     * @param geom the geometry to test
     * @return true if the patch can be created
     */
    public boolean canCreatePatch(Geometry geom) {
        return habitats.stream().allMatch(h -> h.canCreatePatch(geom));
    }
    
    /**
     * Checks if the point is contained in the landscape map and if the pixel does not belong to a patch 
     * Test for all habitats
     * @param p the point to test
     * @return true if the patch can be created
     */
    public boolean canCreatePatch(Point p) {
        return habitats.stream().allMatch(h -> h.canCreatePatch(p));
    }
}
