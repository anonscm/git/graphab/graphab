/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.addpatch;

import org.locationtech.jts.geom.Geometry;
import java.io.IOException;
import java.util.Map;
import org.geotools.graph.build.basic.BasicGraphBuilder;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Node;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.graph.ModifiedGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.links.Path;

/**
 * GraphGenerator which can add new patch in a graph without creates it in the project (soft adding).
 * 
 * @author Gilles Vuidel
 */
public class AddPatchGraphGenerator extends ModifiedGraph<AbstractGraph> {

    private Node addedElem;

    /**
     * Creates a new AddPatchGraphGenerator based on graph gen.
     * The graph gen is dupplicated.
     * @param gen the underlying graph
     */
    public AddPatchGraphGenerator(AbstractGraph gen) {
        super(gen);
        this.addedElem = null;
                
        if(gen.isIntraPatchDist()) {
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("org/thema/graphab/addpatch/Bundle").getString("INTRA PATCH DISTANCE IS NOT SUPPORTED"));
        }
    }
    
    /**
     * Add a patch in this graph. The graph can only have one added patch.
     * Add also the links (edges) linking the patch with others.
     * @param geom the patch geometry (can be point or polygon)
     * @param capa the capacity of the patch
     * @throws IOException 
     * @throws IllegalStateException if the graph have already an added patch
     */
    public void addPatch(Geometry geom, double capa) throws IOException {
        addPatch(geom, capa, (AbstractMonoHabitat)getHabitat());
    }
    /**
     * The graph can only have one added patch.AAdd also the links (edges) linking the patch with others.ing the patch with others.
     * @param geom the patch geometry (can be point or polygon)
     * @param capa the capacity of the patch
     * @param habitat the habitat of the patch
     * @throws IOException 
     * @throws IllegalStateException if the graph have already an added patch
     */
    public void addPatch(Geometry geom, double capa, AbstractMonoHabitat habitat) throws IOException {
        if(addedElem != null) {
            throw new IllegalStateException(java.util.ResourceBundle.getBundle("org/thema/graphab/addpatch/Bundle").getString("GRAPH ALREADY CONTAINS AN ADDED ELEMENT"));
        }
        
        BasicGraphBuilder builder = new BasicGraphBuilder();
        // crée le noeud
        DefaultFeature patch = habitat.createPatch(geom, capa);
        Node node = builder.buildNode();
        node.setObject(patch);
        graph.getNodes().add(node);
        
        // puis crée les liens
        Map<DefaultFeature, Path> newLinks = getParentGraph().getLinkset().calcNewLinks(patch);
        for(DefaultFeature d : newLinks.keySet()) {
            Path path = new Path(patch, d, newLinks.get(d).getCost(), newLinks.get(d).getDist());  
            if(isLinkIncluded(path)) {
                Node nodeB = null;
                for(Node n : getGraph().getNodes()) {
                    if(((Feature)n.getObject()).getId().equals(d.getId())) {
                        nodeB = n;
                        break;
                    }
                }
                if(nodeB == null) {
                    throw new IllegalStateException(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/thema/graphab/addpatch/Bundle").getString("GRAPH DOES NOT CONTAIN THE PATCH NODE : {0}"), d.getId()));
                }
                Edge edge = builder.buildEdge(node, nodeB);
                edge.setObject(path);
                node.add(edge);
                nodeB.add(edge);
                graph.getEdges().add(edge);
            }
        }
        
        addedElem = node;
        
        components = null;
    }
    
    /**
     * Remove the added patch and his links.
     * {@link #addPatch } must be called before
     */
    public void reset() {
        graph.getNodes().remove(addedElem);

        for(Object o : addedElem.getEdges()) {
            Edge e = (Edge) o;
            graph.getEdges().remove(e);
            e.getOtherNode(addedElem).remove(e);
        }
        
        components = null;
        addedElem = null;
    }
   
}
