/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.graphab.addpatch;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.SchemaException;
import org.thema.common.ProgressBar;
import org.thema.common.collection.TreeMapList;
import org.thema.data.IOFeature;
import org.thema.data.IOImage;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graphab.Project;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.parallel.ExecutorService;

/**
 * Main class for add patch feature.
 * 
 * @author Gilles Vuidel
 */
public class AddPatchCommand {
    
    // Common parameters
    private final int nbPatch;
    private final GlobalMetricResult metric;
    private final AbstractMonoHabitat habitat;
    private final boolean saveDetail = true;
     
    // Grid parameters
    private File capaFile;
    private double res;
    
    // Shape file parameters
    private File shapeFile;
    private String capaField;
    
    
    // results
    private List<DefaultFeature> addedPatches;
    private TreeMap<Integer, Double> metricValues;

    /**
     * Creates an add patch command where patches will be tested on a grid.The patch are created at the center of the grid cell with one pixel size.
     * 
     * @param nbPatch number of patches to add
     * @param metric the metric to maximize
     * @param habitat habitat of the added patch
     * @param capaFile raster file containing capacity of the new patches
     * @param res the resolution of the grid (cell size)
     */
    public AddPatchCommand(int nbPatch, GlobalMetricResult metric, AbstractMonoHabitat habitat, File capaFile, double res) {
        this.nbPatch = nbPatch;
        this.metric = metric;
        this.habitat = habitat;
        this.capaFile = capaFile;
        this.res = res;
    }

    /**
     * Creates an add patch command where patches will be tested on a shapefile.
     * @param nbPatch number of patches to add
     * @param metric the metric to maximize
     * @param habitat habitat of the added patch
     * @param shapeFile th shapefile of patches to test, can be point or polygon geometry
     * @param capaField the field name containing capacity, can be null
     */
    public AddPatchCommand(int nbPatch, GlobalMetricResult metric, AbstractMonoHabitat habitat, File shapeFile, String capaField) {
        this.nbPatch = nbPatch;
        this.metric = metric;
        this.habitat = habitat;
        this.shapeFile = shapeFile;
        this.capaField = capaField;
    }
    
    /**
     * @return true if the patches are tested on a grid, false for a shapefile
     */
    public boolean isGridSampling() {
        return shapeFile == null;
    }
    
    /**
     * Launch the add patch calculation.
     * @param bar the progress bar
     * @throws IOException
     * @throws SchemaException 
     */
    public void run(ProgressBar bar) throws IOException, SchemaException {
        metricValues = new TreeMap<>();
        
        if(isGridSampling()) {
            addedPatches = addPatchGrid(bar, metricValues, saveDetail);
        } else {
            addedPatches = addPatchShp(bar, metricValues, saveDetail);
        }
        
        saveResults();
    }

    /**
     * {@link #run(org.thema.common.ProgressBar) } must be called before
     * @return the list of added patches
     */
    public List<DefaultFeature> getAddedPatches() {
        return addedPatches;
    }

    /**
     * {@link #run(org.thema.common.ProgressBar) } must be called before
     * @return the metric values after adding each patch
     */
    public TreeMap<Integer, Double> getMetricValues() {
        return metricValues;
    }
    
    /**
     * Launch the add patch calculation when adding one patch at each step
     * @param testGeoms the geometries of the patches to test with their capacity
     * @param mon the progress bar
     * @param metricValues the resulting metric values
     * @param saveDetail save detail or not ?
     * @return the list of added patches
     * @throws IOException
     * @throws SchemaException 
     */
    private List<DefaultFeature> addPatchSimple(HashMap<Geometry, Double> testGeoms,  
            ProgressBar mon, TreeMap<Integer, Double> metricValues, boolean saveDetail) throws IOException, SchemaException {

        mon.setMaximum(nbPatch*100);

        List<DefaultFeature> addedPatches = new ArrayList<>();
        
        AbstractGraph gen = metric.getGraph();
        double currentInd = metric.getResult()[0];
        metricValues.put(0, currentInd);

        Logger.getLogger(AddPatchCommand.class.getName()).log(Level.INFO, "Initial " + metric.getName() + " : " + currentInd); 
        
        Geometry bestGeom = null;
        double capa = 0;
        
        for(int i = 0; i < nbPatch; i++) {
            AddPatchTask task = new AddPatchTask(bestGeom, capa, habitat, gen, metric.getMetric(), testGeoms, mon.getSubProgress(100));
            ExecutorService.execute(task);
            TreeMapList<Double, Geometry> patchIndices = task.getResult();
            if(patchIndices.isEmpty()) {
                Logger.getLogger(AddPatchCommand.class.getName()).log(Level.INFO, "No more patches can be added !");
                return addedPatches;
            }
            List<Geometry> bestGeoms = patchIndices.lastEntry().getValue();
            bestGeom = bestGeoms.get((int)(Math.random()*bestGeoms.size()));

            int step = i+1;
            capa = testGeoms.get(bestGeom);
            DefaultFeature patch = habitat.addPatch(bestGeom, capa);
            gen.getLinkset().addLinks(patch);
            
            patch = new DefaultFeature(patch, true, true);
            patch.addAttribute("step", step);
            patch.addAttribute("delta-" + metric.getFullName(), patchIndices.lastKey()-currentInd);
            patch.addAttribute(metric.getFullName(), patchIndices.lastKey());
            addedPatches.add(patch);
            
            metricValues.put(addedPatches.size(), patchIndices.lastKey());
            currentInd = patchIndices.lastKey();
            AbstractGraph gNew = gen.newInstance(gen.getName());
            // check if we obtain the same result after adding the patch "truly"
            double test = new GlobalMetricResult("addPatch", metric.getMetric(), gNew).calculate(true, null).getResult()[0];
            double err = Math.abs(test - currentInd) / test;
            if(err > 1e-6) {
                System.out.println("Metric precision under 1e-6 : " + err + " - m1 " + currentInd + " - m2 " + test);
                //throw new RuntimeException("Metric precision under 1e-6 : " + err + " - m1 " + currentInd + " - m2 " + test);
            }
            
            if(saveDetail) {
                List<DefaultFeature> debug = new ArrayList<>();
                for(Double val : patchIndices.keySet()) {
                    for(Geometry g : patchIndices.get(val)) {
                        debug.add(new DefaultFeature(g.getCentroid().getX()+","+g.getCentroid().getY()+" - " + (step), g, 
                                Arrays.asList("step", metric.getFullName()), 
                                Arrays.asList(step, val)));
                    }
                }
                File dir = getResultDir();
                dir = new File(dir, "detail");
                dir.mkdirs();
                IOFeature.saveFeatures(debug, new File(dir, "detail_" + step + ".gpkg"));
            }

            Logger.getLogger(AddPatchCommand.class.getName()).log(Level.INFO, 
                    "Step " + step + " : 1 added patches " + bestGeom.getCoordinate() + " from " + bestGeoms.size() + " best points  for " + metric.getName() + " = " + patchIndices.lastKey()); 
        }

        return addedPatches;
    }
    
    /**
     * Launch the add patch calculation for a shapefile.
     * Prepare the data and call {@link #addPatchSimple(java.util.HashMap, org.thema.common.ProgressBar, java.util.TreeMap, boolean) }
     * @param bar the progress bar
     * @param metricValues the resulting metric values
     * @param saveDetail save detail or not ?
     * @return the list of added patches
     * @throws IOException
     * @throws SchemaException 
     */
    private List<DefaultFeature> addPatchShp(ProgressBar bar, TreeMap<Integer, Double> metricValues, boolean saveDetail) throws IOException, SchemaException  {
        List<DefaultFeature> points = IOFeature.loadFeatures(shapeFile);
        HashMap<Geometry, Double> testGeoms = new HashMap<>();
        for(Feature f : points) {
            testGeoms.put(f.getGeometry(), 
                    capaField == null ? 1 : ((Number)f.getAttribute(capaField)).doubleValue());
        }
        return addPatchSimple(testGeoms, bar, metricValues, saveDetail);
    }
    
    /**
     * Launch the add patch calculation for a grid.
     * Prepare the data and call {@link #addPatchSimple }
     * if nbMultiPatch == 1.
     * Calls {@link #addPatchWindow } if nbMultiPatch > 1 and no MPI environment.
     * Uses {@link AddPatchMultiMPITask } if nbMultiPatch > 1 with MPI environment.
     * @param bar the progress bar
     * @param metricValues the resulting metric values
     * @param saveDetail save detail or not ?
     * @return the list of added patches
     * @throws IOException
     * @throws SchemaException 
     */
    private List<DefaultFeature> addPatchGrid(ProgressBar mon, TreeMap<Integer, Double> indiceValues, boolean saveDetail) throws IOException, SchemaException  {
        Project project = metric.getGraph().getProject();
        Rectangle2D rect = project.getZone();
        double dx = rect.getWidth() - Math.floor((rect.getWidth()) / res) * res;
        double dy = rect.getHeight() - Math.floor((rect.getHeight()) / res) * res;
        rect = new Rectangle2D.Double(rect.getX()+dx/2, rect.getY()+dy/2,
            rect.getWidth()-dx, rect.getHeight()-dy);
        
        GridCoverage2D capaCov = null;
        try {
            if(capaFile != null) {
                capaCov = IOImage.loadCoverage(capaFile);
            }

            GeometryFactory geomFactory = new GeometryFactory();
            HashMap<Geometry, Double> testPoints = new HashMap<>();
            for(double y = rect.getMinY()+res/2; y < rect.getMaxY(); y += res) {
                for (double x = rect.getMinX()+res/2; x < rect.getMaxX(); x += res) {
                    testPoints.put(geomFactory.createPoint(new Coordinate(x, y)), capaCov == null ? 1 : capaCov.evaluate(new Point2D.Double(x, y), new double[1])[0]);
                }
            }
            return addPatchSimple(testPoints, mon, indiceValues, saveDetail);

        } finally {
            if(capaCov != null) {
                capaCov.dispose(true);
            }
        }
    }
    
    private void saveResults() throws IOException, SchemaException {
        String name = metric.getFullName();
        File dir = getResultDir();
        dir.mkdir();
        try (FileWriter w = new FileWriter(new File(dir, "addpatch-" + name + ".txt"))) {
            w.write("#patch\tmetric value\n");
            for(Integer nbP : metricValues.keySet()) {
                w.write(nbP + "\t" + metricValues.get(nbP) + "\n");
            }
        }

        IOFeature.saveFeatures(addedPatches, new File(dir, "addpatch-" + name + ".gpkg"));
        AbstractGraph newGraph = metric.getGraph().newInstance(metric.getGraph().getName());
        IOFeature.saveFeatures(newGraph.getLinks(), new File(dir, "links-" + name + ".gpkg"));
        newGraph.getLayers().getEdgeLayer().export(new File(dir, "topo-links-" + name + ".gpkg"));
        
    }
    
    private File getResultDir() {
        String name = metric.getFullName();
        if(isGridSampling()) {
            return new File(metric.getGraph().getProject().getDirectory(), "addpatch_n" + nbPatch + "_" + name + 
                    "_res" + res);
        } else {
            return new File(metric.getGraph().getProject().getDirectory(), "addpatch_n" + nbPatch + "_" + name + 
                    "_vect" + shapeFile.getName());
        }
    }
    
}
