/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.graphab.addpatch;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.geotools.coverage.grid.GridCoverage2D;
import org.thema.common.ProgressBar;
import org.thema.common.collection.TreeMapList;
import org.thema.data.feature.DefaultFeature;
import org.thema.graphab.graph.AbstractGraph;
import org.thema.graphab.habitat.AbstractMonoHabitat;
import org.thema.graphab.metric.global.GlobalMetric;
import org.thema.graphab.metric.global.GlobalMetricResult;
import org.thema.graphab.mpi.MpiLauncher;
import org.thema.parallel.AbstractParallelTask;

/**
 * Parallel task testing the adding of one patch at a time (from a set of patches) on a graph and calculates a metric for each.
 * Works in threaded and MPI environment.
 * @author Gilles Vuidel
 */
public class AddPatchTask extends AbstractParallelTask<TreeMapList<Double, Geometry>, TreeMapList<Double, Geometry>> 
                    implements Serializable {

    // Patch to be added at init, useful only for MPI env
    private final Geometry addedGeom;
    private final double capaGeom;
    
    private final HashMap<Geometry, Double> testGeoms;
    private final List<Geometry> geoms;
    private final GlobalMetric metric;
    private final String graphName;
    private final String habName;
    
    private transient AbstractGraph graph;
    private transient AbstractMonoHabitat habitat;
    private transient TreeMapList<Double, Geometry> result;

    /**
     * Creates a new AddPatchTask.
     * 
     * @param addedGeom geometry of the previous added patch, can be null if no patch already added
     * @param capaGeom capacity of the previous added patch, can be Double.NaN if no patch already added
     * @param habitat habitat or null
     * @param graph the graph
     * @param metric the metric to maximize
     * @param testGeoms the set of patch geometries to test, with their capacity
     * @param monitor th progress bar
     */
    public AddPatchTask(Geometry addedGeom, double capaGeom, AbstractMonoHabitat habitat, AbstractGraph graph, GlobalMetric metric, 
            HashMap<Geometry, Double> testGeoms, ProgressBar monitor) {
        super(monitor);
        this.addedGeom = addedGeom;
        this.capaGeom = capaGeom;
        this.metric = metric;
        this.graph = graph;
        this.habitat = habitat;
        this.habName = habitat != null ? habitat.getName() : null;
        this.graphName = graph.getName();
        this.testGeoms = testGeoms;
        geoms = new ArrayList<>(testGeoms.keySet());
    }

    @Override
    public void init() {
        super.init();
        // useful for mpi only cause gen is transient
        if(graph == null) {
            graph = MpiLauncher.getProject().getGraph(graphName);
        }
        if(habitat == null) {
            habitat = habName == null ?(AbstractMonoHabitat)graph.getLinkset().getHabitat() : graph.getProject().getHabitat(habName);
        }
        try {
            // si il y a un patch à ajouter et qu'il n'a pas encore été ajouté
            // utile seulement pour MPI
            if(addedGeom != null && habitat.canCreatePatch(addedGeom)) {
                // add the new patch to the project and the graph
                DefaultFeature patch = habitat.addPatch(addedGeom, capaGeom);
                graph.getLinkset().addLinks(patch);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        
        graph = graph.newInstance(graph.getName());
    }

    @Override
    public TreeMapList<Double, Geometry> execute(int start, int end) {
        TreeMapList<Double, Geometry> results = new TreeMapList<>();
        for(Geometry geom : geoms.subList(start, end)) {
            try {
                double indVal = addPatchSoft(geom, metric, graph, testGeoms.get(geom), habitat);
                if(!Double.isNaN(indVal)) {
                    results.putValue(indVal, geom);
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return results;
    }

    @Override
    public void gather(TreeMapList<Double, Geometry> results) {
        if(result == null) {
            result = new TreeMapList<>();
        }
        for(Double val : results.keySet()) {
            for (Geometry g : results.get(val)) {
                result.putValue(val, g);
            }
        }
    }
    
    @Override
    public int getSplitRange() {
        return testGeoms.size();
    }
    
    /**
     * @return the metric value associated with each patch geometry
     */
    @Override
    public TreeMapList<Double, Geometry> getResult() {
        return result;
    }
    
    /**
     * Add a patch to the graph and calculate the metric.
     * The graph gen is dupplicated before adding the patch.
     * The project is not modified : soft method.
     * @param point the point representing the patch
     * @param metric the metric
     * @param gen the graph
     * @param capaCov the grid containing the capacity
     * @return the value of the metric after adding the patch to the graph or NaN if the patch cannot be added or if capacity &lt;= 0
     * @throws IOException 
     */
    public static double addPatchSoft(Point point, GlobalMetric metric, AbstractGraph gen, GridCoverage2D capaCov, AbstractMonoHabitat habitat) throws IOException {
        double capa = 1;
        if(capaCov != null) {
            Point2D.Double p2 = new Point2D.Double(point.getX(), point.getY());
            if(capaCov.getEnvelope2D().contains(p2)) {
                capa = capaCov.evaluate(p2, new double[1])[0];
            } else {
                return Double.NaN;
            }
        }
        return addPatchSoft(point, metric, gen, capa, habitat);
    }
    
    /**
     * Add a patch to the graph and calculate the metric.The graph gen is dupplicated before adding the patch.
     * The project is not modified : soft method.
     * @param geom the geometry of the patch
     * @param metric the metric
     * @param gen the graph
     * @param capa the capacity of the patch
     * @param habitat
     * @return the value of the metric after adding the patch to the graph or NaN if the patch cannot be added or if capacity &lt;= 0
     * @throws IOException 
     */
    public static double addPatchSoft(Geometry geom, GlobalMetric metric, AbstractGraph gen, double capa, AbstractMonoHabitat habitat) throws IOException {
        if(!gen.getHabitat().canCreatePatch(geom)) {
            return Double.NaN;
        }

        if(capa <= 0) {
            return Double.NaN;
        }
        AddPatchGraphGenerator graph = new AddPatchGraphGenerator(gen);
        graph.addPatch(geom, capa, habitat);

        return new GlobalMetricResult("addPatch", metric, graph).calculate(false, null).getResult()[0];
    }
    
}
